<?php

declare(strict_types=1);

namespace Tests\Controller;

use App\Entity\ComponenteNucleoFamiliare;
use App\Entity\Ente;
use App\Entity\Pratica;
use App\Entity\Servizio;
use App\Services\CPSUserProvider;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Response;
use Tests\Base\AbstractAppTestCase;

class EsploraControllerTest extends AbstractAppTestCase
{
  /**
   * @var CPSUserProvider
   */
  protected $userProvider;

  public function setUp(): void
  {
    parent::setUp();
    $this->userProvider = $this->container->get('ocsdc.cps.userprovider');
    $this->cleanDb(ComponenteNucleoFamiliare::class);
    $this->cleanDb(Pratica::class);
    $this->cleanDb(Servizio::class);
    $this->cleanDb(Ente::class);
  }

  public function testICanSeeServiziAsAnonumousUser(): void
  {
    $repo = $this->em->getRepository('App:Servizio');
    $this->createService('Primo servizio');

    $serviceCountAfterInsert = count($repo->findAll());

    $crawler = $this->client->request('GET', $this->router->generate('servizi_list'));
    $renderedServicesCount = $crawler->filter('.servizio')->count();
    $this->assertEquals($serviceCountAfterInsert, $renderedServicesCount);
  }

  public function testICanSeeAServiceDetailAsAnonymousUser(): void
  {
    $servizio = $this->createService('Secondo servizio');

    $servizioDetailUrl = $this->router->generate('servizi_show', ['slug' => $servizio->getSlug()], Router::ABSOLUTE_URL);

    $crawler = $this->client->request('GET', '/servizi/');
    $detailLink = $crawler->selectLink($servizio->getName())->link()->getUri();

    $this->assertEquals($servizioDetailUrl, $detailLink);

    $this->client->request('GET', $detailLink);
    $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
  }
}
