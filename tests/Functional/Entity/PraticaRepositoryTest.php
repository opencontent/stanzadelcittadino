<?php

declare(strict_types=1);

namespace Tests\Functional\Entity;

use App\Applications\Exceptions\ApplicationLiteException;
use App\Entity\ApplicationLite;
use App\Entity\Categoria;
use App\Entity\CPSUser;
use App\Entity\Ente;
use App\Entity\Pratica;
use App\Entity\PraticaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tests\Builders\ServizioBuilder;
use Tests\Helpers\EntitiesHelper;

/**
 * @group functional
 * @group ApplicationLite
 */
class PraticaRepositoryTest extends KernelTestCase
{
  private ?EntityManagerInterface $entityManager;
  private Ente $ente;

  protected function setUp(): void
  {
    $this->entityManager = static::getContainer()->get(EntityManagerInterface::class);

    $this->entityManager->beginTransaction();

    $ente = new Ente();
    $ente->setName('comune-di-bugliano');
    $ente->setSlug('comune-di-bugliano');
    $this->entityManager->persist($ente);
    $this->ente = $ente;
  }

  protected function tearDown(): void
  {
    if ($this->entityManager->getConnection()->isTransactionActive()) {
      // Rollback della transazione alla fine del test
      $this->entityManager->rollback();
    }

    $this->entityManager->close();
    $this->entityManager = null;

    parent::tearDown();
  }

  public function testShouldCorrectlySaveApplicationLite(): void
  {
    $applicant = new CPSUser();
    $applicant->setUsername('applicant');

    $topics = new Categoria();
    $topics->setName('generic_category');

    $servizio = ServizioBuilder::createAsApplicationLiteService()
      ->withTopic($topics)
      ->build();

    $applicationLite = ApplicationLite::create(
      $servizio,
      $applicant,
      'should correctly save pratica lite',
      Pratica::STATUS_SUBMITTED,
      $this->ente,
    );
    $applicationLiteId = $applicationLite->getId();

    $this->entityManager->persist($applicant);
    $this->entityManager->persist($topics);
    $this->entityManager->persist($servizio);
    $this->entityManager->persist($applicationLite);
    $this->entityManager->flush();

    /** @var ApplicationLite $fetchedApplication */
    $fetchedApplication = $this->entityManager
      ->getRepository(Pratica::class)
      ->findOneBy(['id' => $applicationLiteId->toString()]);

    $this->assertInstanceOf(ApplicationLite::class, $fetchedApplication);
    $this->assertSame($applicationLiteId->toString(), $fetchedApplication->getId()->toString());
    $this->assertSame(ApplicationLite::TYPE_APPLICATION_LITE, $fetchedApplication->getType());
  }

  /** @throws ApplicationLiteException */
  public function testShouldCorrectlyRetrieveApplicationLiteByExternalId(): void
  {
    $applicant = new CPSUser();
    $applicant->setUsername(uniqid('applicant'));

    $topics = EntitiesHelper::createCategoria();

    $servizio = ServizioBuilder::createAsApplicationLiteService()
      ->withTopic($topics)
      ->build();

    $applicationLite1 = ApplicationLite::createAsExternal(
      'b0d3a6ea-603a-4ff5-ad6e-11afd564a2c1',
      $servizio,
      $applicant,
      'should correctly retrieve ApplicationLite by externalId',
      Pratica::STATUS_SUBMITTED,
      $this->ente,
    );

    $externalId = '0b33e70e-1d92-41ff-9178-d76852e1bef1';
    $applicationLite2 = ApplicationLite::createAsExternal(
      $externalId,
      $servizio,
      $applicant,
      'should correctly retrieve ApplicationLite by externalId',
      Pratica::STATUS_SUBMITTED,
      $this->ente,
    );
    $applicationLite2Id = $applicationLite2->getId();

    $this->entityManager->persist($applicant);
    $this->entityManager->persist($topics);
    $this->entityManager->persist($servizio);
    $this->entityManager->persist($applicationLite1);
    $this->entityManager->persist($applicationLite2);
    $this->entityManager->flush();


    /** @var PraticaRepository $repo */
    $repo = $this->entityManager->getRepository(Pratica::class);
    $fetchedApplication = $repo->findApplicationLiteByExternalId($externalId);

    $this->assertInstanceOf(ApplicationLite::class, $fetchedApplication);
    $this->assertSame($applicationLite2Id->toString(), $fetchedApplication->getId()->toString());
    $this->assertSame(ApplicationLite::TYPE_APPLICATION_LITE, $fetchedApplication->getType());
  }
}
