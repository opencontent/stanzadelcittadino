<?php

declare(strict_types=1);

namespace Functional\ApplicationLiteImport\Infrastructure;

use App\ApplicationLiteImport\Application\ImportCommand\ImportCommand;
use App\ApplicationLiteImport\Application\ImportCommand\ImportData;
use App\ApplicationLiteImport\Infrastructure\HttpRequestToImportCommandMapper;
use App\Applications\Application\DTOs\ApplicationLiteData;
use App\Applications\Application\DTOs\ProcessHistory;
use App\Entity\CPSUser;
use App\Entity\Ente;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class HttpRequestToImportCommandMapperFunctionalTest extends KernelTestCase
{
  private ?HttpRequestToImportCommandMapper $mapper;
  private ?EntityManagerInterface $entityManager;
  private Ente $ente;

  protected function setUp(): void
  {
    $this->entityManager = static::getContainer()->get(EntityManagerInterface::class);
    $this->mapper = static::getContainer()->get(HttpRequestToImportCommandMapper::class);

    $this->entityManager->beginTransaction();

    $ente = new Ente();
    $ente->setName('comune-di-bugliano');
    $ente->setSlug('comune-di-bugliano');
    $this->entityManager->persist($ente);
    $this->ente = $ente;
  }

  protected function tearDown(): void
  {
    if ($this->entityManager->getConnection()->isTransactionActive()) {
      // Rollback della transazione alla fine del test
      $this->entityManager->rollback();
    }

    $this->entityManager->close();
    $this->entityManager = null;

    parent::tearDown();
  }

  public function testMapValidJson(): void
  {
    $json = <<<'JSON'
      {
          "application": {
              "external_id": "12345",
              "subject": "Oggetto della pratica",
              "applicant": {
                  "given_name": "Mario",
                  "family_name": "Rossi",
                  "identifiers": {
                      "tax_code": "RSSMRA80A01H501Z"
                  }
              },
              "status": 2000,
              "service": {
                  "id": "7037f0f0-28eb-4237-9cc7-28c150f13a0d"
              },
              "actions": [],
              "process_history": [
                {
                  "status": 2000,
                  "status_changed_at": "2025-01-20T14:30:00+02:00"
                }
              ]
          }
      }
      JSON;

    $requester = new CPSUser();

    $importCommand = $this->mapper->map($json, $this->ente, $requester);

    $this->assertInstanceOf(ImportCommand::class, $importCommand);
    $this->assertInstanceOf(ImportData::class, $importCommand->importData);
    $this->assertInstanceOf(ApplicationLiteData::class, $importCommand->importData->application);

    $application = $importCommand->importData->application;
    $this->assertInstanceOf(ProcessHistory::class, $application->processHistory[0]);
    $this->assertEquals(2000, $application->processHistory[0]->status);
    $this->assertEquals(new DateTime('2025-01-20T14:30:00+02:00'), $application->processHistory[0]->statusChangedAt);
  }
}
