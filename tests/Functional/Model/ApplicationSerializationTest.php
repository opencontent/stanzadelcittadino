<?php

declare(strict_types=1);

namespace Tests\Functional\Model;

use App\Model\Application;
use DateTime;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @group functional
 */
class ApplicationSerializationTest extends KernelTestCase
{
  private ?SerializerInterface $serializer;

  protected function setUp(): void
  {
    $this->serializer = static::getContainer()->get(SerializerInterface::class);
  }

  public function testSerialization(): void
  {
    // Crea un'istanza di Application e imposta alcuni valori
    $application = new Application();
    $application->setId('123e4567-e89b-12d3-a456-426614174000');
    $application->setUser('user-uuid-123');
    $application->setUserName('John Doe');
    $application->setService('service-slug');
    $application->setServiceId('service-id-uuid');
    $application->setCreationTime(1714521200);
    $application->setCreatedAt(new DateTime('2024-04-30T12:00:00Z'));
    $application->setStatus('active');
    $application->setPriority('high');
    $application->setProcessHistory([
      [
        'status' => 1000,
        'status_changed_at' => new DateTime('2024-04-30T15:00:00Z'),
      ],
    ]);

    // Serializza l'oggetto Application in formato JSON
    $jsonOutput = $this->serializer->serialize($application, 'json');

    // Output JSON atteso
    $expectedJson = <<<JSON
      {
        "id": "123e4567-e89b-12d3-a456-426614174000",
        "user": "user-uuid-123",
        "user_name": "John Doe",
        "service": "service-slug",
        "service_id": "service-id-uuid",
        "creation_time": 1714521200,
        "created_at": "2024-04-30T12:00:00+00:00",
        "status": "active",
        "priority": "high",
        "geographic_areas": [],
        "locale": "it",
        "stamps": [],
        "process_history": [
          {
            "status": 1000,
            "status_changed_at": "2024-04-30T15:00:00+00:00"
           }
        ]
      }
      JSON;

    $this->assertJsonStringEqualsJsonString($expectedJson, $jsonOutput);
  }
}
