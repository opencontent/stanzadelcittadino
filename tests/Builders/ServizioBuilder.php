<?php

declare(strict_types=1);

namespace Tests\Builders;

use App\Entity\ApplicationLite;
use App\Entity\Categoria;
use App\Entity\Pratica;
use App\Entity\Servizio;

class ServizioBuilder
{
  private string $name = 'Default service name';
  private ?Categoria $topics = null;
  private string $praticaFCQN = Pratica::class;

  public static function createAsApplicationLiteService(): self
  {
    $instance = new self();
    $instance->asApplicationLiteService();

    return $instance;
  }

  public function asApplicationLiteService(): self
  {
    $this->praticaFCQN = ApplicationLite::class;

    return $this;
  }

  public function withName(string $name): self
  {
    $this->name = $name;

    return $this;
  }

  public function withTopic(Categoria $categoria): self
  {
    $this->topics = $categoria;

    return $this;
  }

  public function build(): Servizio
  {
    $service = new Servizio();
    $service->setName($this->name);
    $service->setTopics($this->topics);
    $service->setPraticaFCQN($this->praticaFCQN);
    $service->setPraticaFlowServiceName('PraticaFlowServiceName');

    return $service;
  }
}
