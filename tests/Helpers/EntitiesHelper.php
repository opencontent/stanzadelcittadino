<?php

declare(strict_types=1);

namespace Tests\Helpers;

use App\Entity\Categoria;
use App\Entity\CPSUser;
use App\Entity\Ente;
use App\Entity\ServiceGroup;
use App\Entity\Servizio;
use App\Entity\FormIO;
use App\Entity\User;
use DateTime;

class EntitiesHelper
{
  public static function createEnte(): Ente
  {
    // Ente
    $ente = new Ente();
    $ente->setName('Comune di Bugliano');

    return $ente;

  }

  public static function createCategoria(): Categoria
  {
    $category = new Categoria();
    $uniquePostfix = uniqid('', true);
    $category->setName('Category ' . $uniquePostfix);
    $category->setSlug('category-' . $uniquePostfix);

    return $category;
  }

  public static function createFormIOService(Ente $ente, Categoria $categoria): Servizio
  {

    $servizio = new Servizio();

    $servizio->setName('Servizio di test');
    $servizio->setSlug('servizio-di-test');
    $servizio->setPraticaFCQN(FormIO::class);
    $servizio->setPraticaFlowServiceName('ocsdc.form.flow.formio');
    $servizio->setEnte($ente);
    $servizio->setTopics($categoria);
    $servizio->setProtocolRequired(false);

    $servizio->setDescription('Service lorem ipsum');
    $servizio->setShortDescription('Service lorem ipsum');
    $servizio->setHowto('Service lorem ipsum');
    $servizio->setWho('Service lorem ipsum');
    $servizio->setSpecialCases('Service lorem ipsum');
    $servizio->setMoreInfo('Service lorem ipsum');
    $servizio->setCompilationInfo('Service lorem ipsum');
    $servizio->setFinalIndications('Service lorem ipsum');
    $servizio->setCoverage(['a', 'b', 'c']);

    $now = new DateTime();
    $servizio->setCreatedAt($now);
    $servizio->setUpdatedAt($now);


    return $servizio;
  }

  public static function createServiceGroup(): ServiceGroup
  {

    $serviceGroup = new ServiceGroup();
    $serviceGroup->setName('Service Group');
    $serviceGroup->setTopics(self::createCategoria());

    $serviceGroup->setDescription('Service Group lorem ipsum');
    $serviceGroup->setHowto('Service Group lorem ipsum');
    $serviceGroup->setWho('Service Group lorem ipsum');
    $serviceGroup->setSpecialCases('Service Group lorem ipsum');
    $serviceGroup->setMoreInfo('Service Group lorem ipsum');
    $serviceGroup->setCoverage(['c', 'b', 'f']);

    return $serviceGroup;
  }

  public static function createCategory(): Categoria
  {
    $category = new Categoria();
    $category->setName('Categoria');
    $category->setSlug('categoria');
    $category->setDescription('Description categoria');

    return $category;
  }

  public static function createCpsAnonymousUser(): CPSUser
  {
    $user = new CPSUser();
    $email = $user->getId()->toString() . '@' . User::ANONYMOUS_FAKE_EMAIL_DOMAIN;
    $sessionString = uniqid($user->getId()->toString(), true);
    $user
      ->setUsername($user->getId()->toString())
      ->setNome('Anonymous')
      ->setCognome('User')
      ->setCodiceFiscale($user->getId()->toString())
      ->setDataNascita(new DateTime())
      ->setLuogoNascita('-')
      ->setEmail($email)
      ->setEmailContatto($email)
      ->setConfirmationToken(hash('sha256', $sessionString));

    $user->setEnabled(true);

    return $user;
  }
}
