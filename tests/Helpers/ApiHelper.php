<?php

declare(strict_types=1);

namespace Tests\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use JsonException;

class ApiHelper
{
  public const API_BASE_URL = 'http://localhost/comune-di-bugliano';

  /**
   * @throws GuzzleException
   * @throws JsonException
   */
  public static function getAdminJwtToken(): string
  {

    $client = new Client();
    $headers = ['Content-Type' => 'application/json'];

    $data = [
      'username' => 'admin',
      'password' => 'admin',
    ];

    $request = new Request(
      'POST',
      self::API_BASE_URL . '/api/auth',
      $headers,
      json_encode($data, JSON_THROW_ON_ERROR)
    );

    $response = $client->send($request);
    $responseData = json_decode($response->getBody()->getContents(), true);

    return $responseData['token'];
  }
}
