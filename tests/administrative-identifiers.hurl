# Ger senza autenticazione

GET {{base_url}}/api/administrative-identifiers

HTTP 401
[Asserts]
jsonpath "$.code" == 401
jsonpath "$.message" == "JWT Token not found"


# Get con autenticazione
GET {{base_url}}/api/administrative-identifiers
Authorization: Bearer {{user_token}}

HTTP 200
[Asserts]
jsonpath "$.meta" != null
jsonpath "$.meta.count" != null
jsonpath "$.links" != null
jsonpath "$.links.self" != null
jsonpath "$.data" isCollection

# Post di un identifier
POST {{base_url}}/api/administrative-identifiers
Authorization: Bearer {{user_token}}

[Options]
variable: type=spid-hurl
variable: identifier={{newUuid}}
{
  "type": "{{type}}",
  "identifier": "{{identifier}}"
}

HTTP 201
[Asserts]
jsonpath "$.id" != null

[Captures]
ai_id: jsonpath "$.id"

# Post dello stesso identifier
POST {{base_url}}/api/administrative-identifiers
Authorization: Bearer {{user_token}}
{
  "type": "{{type}}",
  "identifier": "{{identifier}}"
}

HTTP 400
[Asserts]
jsonpath "$.detail" == "Administrative identifier already exists"

# Get dell'identifier appena generato
GET {{base_url}}/api/administrative-identifiers/{{ai_id}}
Authorization: Bearer {{user_token}}

HTTP 200
[Asserts]
jsonpath "$.id" == {{ai_id}}
jsonpath "$.type" == {{type}}
jsonpath "$.identifier" == {{identifier}}

# Put dell'identifier appena generato
PUT {{base_url}}/api/administrative-identifiers/{{ai_id}}
Authorization: Bearer {{user_token}}

[Options]
variable: new_identifier={{newUuid}}
{
  "type": "{{type}}",
  "identifier": "{{new_identifier}}"
}

HTTP 200
[Asserts]
jsonpath "$.id" == {{ai_id}}
jsonpath "$.type" == {{type}}
jsonpath "$.identifier" == {{new_identifier}}

# Patch con oggetto non completo
PATCH {{base_url}}/api/administrative-identifiers/{{ai_id}}
Authorization: Bearer {{user_token}}

[Options]
variable: new_type=new-spid-hurl
{
  "type": "{{new_type}}"
}

HTTP 400
[Asserts]
jsonpath "$.errors.identifier" == "Identifier is required"

# Delete dell'identifier creato
DELETE {{base_url}}/api/administrative-identifiers/{{ai_id}}
Authorization: Bearer {{user_token}}

HTTP 204
