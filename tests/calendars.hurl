########################################################################################################################
#                                       AUTENTICAZIONE                                                                 #
########################################################################################################################

POST {{base_url}}/api/auth
{
  "username": "{{user}}",
  "password": "{{pass}}"
}

HTTP 200
[Asserts]
jsonpath "$.token" != null
[Captures]
token: jsonpath "$.token"


########################################################################################################################
#                                       CREAZIONE CALENDARIO                                                           #
########################################################################################################################

GET {{base_url}}/api/users?roles=operator&username={{user}}
Authorization: Bearer {{token}}

HTTP 200
[Asserts]
jsonpath "$" isCollection
jsonpath "$[0].id" != null
[Captures]
owner_id: jsonpath "$[0].id"

POST {{base_url}}/api/calendars
Authorization: Bearer {{token}}

{
  "owner": "{{owner_id}}",
  "moderators": [],
  "opening_hours": [],
  "title": "API 1",
  "type": "time_fixed_slots",
  "contact_email": "",
  "rolling_days": 180,
  "drafts_duration": 1,
  "drafts_duration_increment": 0,
  "minimum_scheduling_notice": 0,
  "allow_cancel_days": 0,
  "is_moderated": true,
  "location": "Created during test, to be removed at the end",
  "external_calendars": [],
  "closing_periods": []
}

HTTP 201
[Asserts]
jsonpath "$.id" != null
[Captures]
calendar1_id: jsonpath "$.id"

POST {{base_url}}/api/calendars/{{calendar1_id}}/opening-hours
Authorization: Bearer {{token}}
   {
      "name": {
        "it": "Orario 1",
        "en": "Opening hour 1",
        "de": "Öffnungszeiten 1"
      },
      "start_date": "2025-01-01T00:00:00",
      "end_date": "2025-12-31T00:00:00",
      "days_of_week": [1, 2, 3, 4, 5, 6, 7],
      "begin_hour": "08:00",
      "end_hour": "10:00",
      "is_moderated": false,
      "meeting_minutes": 60,
      "interval_minutes": 0,
      "meeting_queue": 1
    }

HTTP 201
[Asserts]
jsonpath "$.id" != null
[Captures]
opening_hour1_id: jsonpath "$.id"

POST {{base_url}}/api/calendars
Authorization: Bearer {{token}}

{
  "owner": "{{owner_id}}",
  "moderators": [],
  "opening_hours": [],
  "title": "API 2",
  "type": "time_fixed_slots",
  "contact_email": "",
  "rolling_days": 180,
  "drafts_duration": 1,
  "drafts_duration_increment": 0,
  "minimum_scheduling_notice": 0,
  "allow_cancel_days": 0,
  "is_moderated": true,
  "location": "Created during test, to be removed at the end",
  "external_calendars": [],
  "closing_periods": []
}

HTTP 201
[Asserts]
jsonpath "$.id" != null
[Captures]
calendar2_id: jsonpath "$.id"

POST {{base_url}}/api/calendars/{{calendar2_id}}/opening-hours
Authorization: Bearer {{token}}
   {
      "name": {
        "it": "Orario 2",
        "en": "Opening hour 2",
        "de": "Öffnungszeiten 2"
      },
      "start_date": "2025-01-01T00:00:00",
      "end_date": "2025-12-31T00:00:00",
      "days_of_week": [1, 2, 3, 4, 5, 6, 7],
      "begin_hour": "08:00",
      "end_hour": "10:00",
      "is_moderated": false,
      "meeting_minutes": 60,
      "interval_minutes": 0,
      "meeting_queue": 1
    }

HTTP 201
[Asserts]
jsonpath "$.id" != null
[Captures]
opening_hour2_id: jsonpath "$.id"

########################################################################################################################
#                                       RICERCA DISPONIBILITÀ                                                          #
########################################################################################################################

GET {{base_url}}/api/calendars/{{opening_hour1_id}}/availabilities

HTTP 404

GET {{base_url}}/api/calendars/{{calendar1_id}}/availabilities?available=true

HTTP 200
[Asserts]
jsonpath "$" isCollection
jsonpath "$[0].date" != null
jsonpath "$[0].available" == true
[Captures]
available_date: jsonpath "$[0].date"

GET {{base_url}}/api/calendars/{{opening_hour1_id}}/availabilities

HTTP 404

GET {{base_url}}/api/availabilities?calendar_ids={{calendar1_id}},{{calendar2_id}}&from_date={{available_date}}&available=true

HTTP 200
[Asserts]
jsonpath "$.meta" != null
jsonpath "$.meta.count" != null
jsonpath "$.meta.parameter.calendar_ids" == "{{calendar1_id}},{{calendar2_id}}"
jsonpath "$.meta.parameter.from_date" == "{{available_date}}"
jsonpath "$.meta.parameter.to_date" != null
jsonpath "$.meta.parameter.available" != null
jsonpath "$.links" != null
jsonpath "$.links.self" != null
jsonpath "$.data" isCollection
jsonpath "$.data[0].date" == "{{available_date}}"
jsonpath "$.data[0].calendar_ids" isCollection

GET {{base_url}}/api/availabilities/{{available_date}}?calendar_ids={{opening_hour1_id}}

HTTP 404

GET {{base_url}}/api/availabilities/{{available_date}}?calendar_ids={{calendar1_id}},{{calendar2_id}}&available=true

HTTP 200
[Asserts]
jsonpath "$.meta" != null
jsonpath "$.meta.count" != null
jsonpath "$.meta.parameter.calendar_ids" == "{{calendar1_id}},{{calendar2_id}}"
jsonpath "$.meta.parameter.date" == "{{available_date}}"
jsonpath "$.meta.parameter.available" != null
jsonpath "$.data" isCollection
jsonpath "$.data[0].date" == "{{available_date}}"
jsonpath "$.data[0].start_time" != null
jsonpath "$.data[0].end_time" != null
jsonpath "$.data[0].slots_available" != null
jsonpath "$.data[0].availability" == true
jsonpath "$.data[0].opening_hour_id" != null
jsonpath "$.data[0].calendar_id" != null

########################################################################################################################
#                                       ELIMINAZIONE CALENDARIO                                                        #
########################################################################################################################

DELETE {{base_url}}/api/calendars/{{calendar1_id}}/opening-hours/{{opening_hour1_id}}
Authorization: Bearer {{token}}

HTTP 204

DELETE {{base_url}}/api/calendars/{{calendar1_id}}
Authorization: Bearer {{token}}

HTTP 204

DELETE {{base_url}}/api/calendars/{{calendar2_id}}/opening-hours/{{opening_hour2_id}}
Authorization: Bearer {{token}}

HTTP 204

DELETE {{base_url}}/api/calendars/{{calendar2_id}}
Authorization: Bearer {{token}}

HTTP 204
