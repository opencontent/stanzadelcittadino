<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\AllegatoOperatore;
use App\Entity\Pratica;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class AllegatoOperatoreTest extends TestCase
{
  public function testConstructorInitializesFields(): void
  {
    $allegatoOperatore = new AllegatoOperatore();

    $this->assertEquals(AllegatoOperatore::TYPE_DEFAULT, $allegatoOperatore->getType());
    $this->assertInstanceOf(ArrayCollection::class, $allegatoOperatore->getPratiche());
  }

  public function testAddAndRemovePratica(): void
  {
    $allegatoOperatore = new AllegatoOperatore();
    $pratica = $this->createMock(Pratica::class);

    // Test adding pratica
    $allegatoOperatore->addPratica($pratica);
    $this->assertTrue($allegatoOperatore->getPratiche()->contains($pratica));

    // Test removing pratica
    $allegatoOperatore->removePratica($pratica);
    $this->assertFalse($allegatoOperatore->getPratiche()->contains($pratica));
  }

  public function testGetType(): void
  {
    $allegatoOperatore = new AllegatoOperatore();
    $this->assertEquals(AllegatoOperatore::TYPE_DEFAULT, $allegatoOperatore->getType());
  }
}
