<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\GeographicArea;
use App\Entity\Pratica;
use App\Entity\ServiceGroup;
use App\Entity\Servizio;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

class GeographicAreaTest extends TestCase
{
  private GeographicArea $geographicArea;

  protected function setUp(): void
  {
    $this->geographicArea = new GeographicArea();
  }

  public function testConstructor(): void
  {
    $this->assertInstanceOf(UuidInterface::class, $this->geographicArea->getId());
    $this->assertInstanceOf(ArrayCollection::class, $this->geographicArea->getServices());
    $this->assertInstanceOf(ArrayCollection::class, $this->geographicArea->getServicesGroup());
    $this->assertInstanceOf(ArrayCollection::class, $this->geographicArea->getApplications());
  }

  public function testSetAndGetName(): void
  {
    $name = 'Test Area';
    $this->geographicArea->setName($name);
    $this->assertEquals($name, $this->geographicArea->getName());
  }

  public function testSetAndGetSlug(): void
  {
    $slug = 'test-area';
    $this->geographicArea->setSlug($slug);
    $this->assertEquals($slug, $this->geographicArea->getSlug());
  }

  public function testSetAndGetDescription(): void
  {
    $description = 'This is a test area description';
    $this->geographicArea->setDescription($description);
    $this->assertEquals($description, $this->geographicArea->getDescription());
  }

  public function testSetAndGetGeofence(): void
  {
    $geofence = '{"type":"Polygon","coordinates":[[[0,0],[0,1],[1,1],[1,0],[0,0]]]}';
    $this->geographicArea->setGeofence($geofence);
    $this->assertEquals($geofence, $this->geographicArea->getGeofence());
  }

  public function testSetAndGetServices(): void
  {
    $services = new ArrayCollection([new Servizio(), new Servizio()]);
    $this->geographicArea->setServices($services);
    $this->assertSame($services, $this->geographicArea->getServices());
  }

  public function testSetAndGetServicesGroup(): void
  {
    $servicesGroup = new ArrayCollection([new ServiceGroup(), new ServiceGroup()]);
    $this->geographicArea->setServicesGroup($servicesGroup);
    $this->assertSame($servicesGroup, $this->geographicArea->getServicesGroup());
  }

  public function testSetTranslatableLocale(): void
  {
    $locale = 'en';
    $this->geographicArea->setTranslatableLocale($locale);
    // Note: We can't test the result directly as $locale is private and has no getter
    // This test ensures the method doesn't throw an exception
    $this->addToAssertionCount(1);
  }

  public function testAddAndRemoveApplication(): void
  {
    $application = new Pratica();

    $this->geographicArea->addApplication($application);
    $this->assertTrue($this->geographicArea->getApplications()->contains($application));

    $this->geographicArea->removeApplication($application);
    $this->assertFalse($this->geographicArea->getApplications()->contains($application));
  }

  public function testTimestampableTraitMethods(): void
  {
    $now = new DateTime();

    $this->geographicArea->setCreatedAt($now);
    $this->assertEquals($now, $this->geographicArea->getCreatedAt());

    $this->geographicArea->setUpdatedAt($now);
    $this->assertEquals($now, $this->geographicArea->getUpdatedAt());
  }
}
