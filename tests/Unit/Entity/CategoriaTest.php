<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\Categoria;
use App\Entity\Servizio;
use App\Entity\ServiceGroup;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class CategoriaTest extends TestCase
{
  public function testConstructorSetsId(): void
  {
    $categoria = new Categoria();
    $this->assertNotNull($categoria->getId());
    $this->assertTrue(Uuid::isValid((string) $categoria->getId()));
  }

  public function testGetAndSetName(): void
  {
    $categoria = new Categoria();
    $name = 'Test Name';
    $categoria->setName($name);

    $this->assertEquals($name, $categoria->getName());
  }

  public function testGetAndSetSlug(): void
  {
    $categoria = new Categoria();
    $slug = 'test-slug';
    $categoria->setSlug($slug);

    $this->assertEquals($slug, $categoria->getSlug());
  }

  public function testGetAndSetDescription(): void
  {
    $categoria = new Categoria();
    $description = 'Test Description';
    $categoria->setDescription($description);

    $this->assertEquals($description, $categoria->getDescription());
  }

  public function testGetAndSetChildren(): void
  {
    $categoria = new Categoria();
    $children = new ArrayCollection();
    $categoria->setChildren($children);

    $this->assertEquals($children, $categoria->getChildren());
  }

  public function testGetAndSetParent(): void
  {
    $categoria = new Categoria();
    $parent = new Categoria();
    $categoria->setParent($parent);

    $this->assertEquals($parent, $categoria->getParent());
  }

  public function testGetParentId(): void
  {
    $categoria = new Categoria();
    $parent = new Categoria();
    $categoria->setParent($parent);

    $this->assertEquals($parent->getId(), $categoria->getParentId());
  }

  public function testGetParentIdReturnsNullWhenNoParent(): void
  {
    $categoria = new Categoria();

    $this->assertNull($categoria->getParentId());
  }

  public function testGetAndSetServices(): void
  {
    $categoria = new Categoria();
    $services = new ArrayCollection();
    $categoria->setServices($services);

    $this->assertEquals($services, $categoria->getServices());
  }

  public function testGetAndSetServicesGroup(): void
  {
    $categoria = new Categoria();
    $servicesGroup = new ArrayCollection();
    $categoria->setServicesGroup($servicesGroup);

    $this->assertEquals($servicesGroup, $categoria->getServicesGroup());
  }

  public function testGetAndSetUserGroups(): void
  {
    $categoria = new Categoria();
    $userGroups = new ArrayCollection();
    $categoria->setUserGroups($userGroups);

    $this->assertEquals($userGroups, $categoria->getUserGroups());
  }

  public function testGetVisibleService(): void
  {
    $categoria = new Categoria();
    $publicService = $this->createMock(Servizio::class);
    $publicService->method('getStatus')->willReturn(Servizio::PUBLIC_STATUSES[0]);

    $privateService = $this->createMock(Servizio::class);
    $privateService->method('getStatus')->willReturn('private');

    $categoria->getServices()->add($publicService);
    $categoria->getServices()->add($privateService);

    $visibleServices = $categoria->getVisibleService();
    $this->assertCount(1, $visibleServices);
    $this->assertSame($publicService, $visibleServices[0]);
  }

  public function testGetVisibleServicesGroup(): void
  {
    $categoria = new Categoria();
    $serviceGroup = $this->createMock(ServiceGroup::class);

    $publicService = $this->createMock(Servizio::class);
    $publicService->method('getStatus')->willReturn(Servizio::PUBLIC_STATUSES[0]);

    $privateService = $this->createMock(Servizio::class);
    $privateService->method('getStatus')->willReturn('private');

    $serviceGroup->method('getServices')->willReturn(new ArrayCollection([$publicService, $privateService]));

    $categoria->getServicesGroup()->add($serviceGroup);

    $visibleServicesGroup = $categoria->getVisibleServicesGroup();
    $this->assertCount(1, $visibleServicesGroup);
    $this->assertSame($serviceGroup, $visibleServicesGroup[0]);
  }

  public function testHasRelations(): void
  {
    $categoria = new Categoria();
    $this->assertFalse($categoria->hasRelations());

    $child = new Categoria();
    $categoria->getChildren()->add($child);
    $this->assertTrue($categoria->hasRelations());
  }

  public function testHasVisibleRelations(): void
  {
    $categoria = new Categoria();
    $this->assertFalse($categoria->hasVisibleRelations());

    $child = new Categoria();
    $categoria->getChildren()->add($child);
    $this->assertTrue($categoria->hasVisibleRelations());
  }
}
