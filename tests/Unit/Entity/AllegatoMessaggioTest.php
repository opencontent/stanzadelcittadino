<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\AllegatoMessaggio;
use App\Entity\Message;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use stdClass;

class AllegatoMessaggioTest extends TestCase
{
  public function testConstructorInitializesFields(): void
  {
    $allegatoMessaggio = new AllegatoMessaggio();

    $this->assertEquals(AllegatoMessaggio::TYPE_DEFAULT, $allegatoMessaggio->getType());
    $this->assertInstanceOf(ArrayCollection::class, $allegatoMessaggio->getMessages());
    $this->assertInstanceOf(ArrayCollection::class, $allegatoMessaggio->getNumeriProtocollo());
  }

  public function testAddAndRemoveMessage(): void
  {
    $allegatoMessaggio = new AllegatoMessaggio();
    $message = $this->createMock(Message::class);

    $allegatoMessaggio->addMessage($message);
    $this->assertTrue($allegatoMessaggio->getMessages()->contains($message));

    $allegatoMessaggio->removeMessage($message);
    $this->assertFalse($allegatoMessaggio->getMessages()->contains($message));
  }

  public function testAddNumeroDiProtocollo(): void
  {
    $allegatoMessaggio = new AllegatoMessaggio();
    $numeroDiProtocollo = '123456';

    $allegatoMessaggio->addNumeroDiProtocollo($numeroDiProtocollo);
    $this->assertTrue($allegatoMessaggio->getNumeriProtocollo()->contains($numeroDiProtocollo));
  }

  public function testGetPratiche(): void
  {
    $allegatoMessaggio = new AllegatoMessaggio();
    $message = $this->createMock(Message::class);
    $application = new stdClass();
    $message->method('getApplication')->willReturn($application);

    $allegatoMessaggio->addMessage($message);
    $pratiche = $allegatoMessaggio->getPratiche();

    $this->assertInstanceOf(ArrayCollection::class, $pratiche);
    $this->assertTrue($pratiche->contains($application));
  }

  public function testSetAndGetIdRichiestaIntegrazione(): void
  {
    $allegatoMessaggio = new AllegatoMessaggio();
    $idRichiestaIntegrazione = 'integration_id';

    $allegatoMessaggio->setIdRichiestaIntegrazione($idRichiestaIntegrazione);
    $this->assertEquals($idRichiestaIntegrazione, $allegatoMessaggio->getIdRichiestaIntegrazione());
  }
}
