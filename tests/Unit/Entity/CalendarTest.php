<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\AdminUser;
use App\Entity\Calendar;
use App\Entity\OperatoreUser;
use App\Entity\OpeningHour;
use App\Model\Calendar\ReservationLimits;
use App\Model\DateTimeInterval;
use App\Model\ExternalCalendar;
use DateTime;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use ReflectionClass;

class CalendarTest extends TestCase
{
  private Calendar $calendar;

  protected function setUp(): void
  {
    $this->calendar = new Calendar();
  }

  public function testConstructorInitializesDefaults(): void
  {
    $this->assertInstanceOf(UuidInterface::class, $this->calendar->getId());
    $this->assertEquals(Calendar::TYPE_TIME_FIXED, $this->calendar->getType());
    $this->assertFalse($this->calendar->getIsModerated());
    $this->assertInstanceOf(Collection::class, $this->calendar->getModerators());
    $this->assertEquals(Calendar::DEFAULT_CANCEL_DAYS, $this->calendar->getAllowCancelDays());
    $this->assertEquals(Calendar::DEFAULT_ROLLING_DAYS, $this->calendar->getRollingDays());
    $this->assertEquals(Calendar::DEFAULT_DRAFT_DURATION, $this->calendar->getDraftsDuration());
    $this->assertEquals(Calendar::DEFAULT_DRAFT_INCREMENT, $this->calendar->getDraftsDurationIncrement());
    $this->assertFalse($this->calendar->isAllowOverlaps());
    $this->assertEquals(0, $this->calendar->getMinimumSchedulingNotice());
    $this->assertInstanceOf(Collection::class, $this->calendar->getOpeningHours());
    $this->assertEquals([], $this->calendar->getExternalCalendars());
    $this->assertEquals([], $this->calendar->getClosingPeriods());
  }

  public function testSetAndGetTitle(): void
  {
    $title = 'Test Calendar Title';
    $this->calendar->setTitle($title);
    $this->assertEquals($title, $this->calendar->getTitle());
  }

  public function testSetAndGetType(): void
  {
    $this->calendar->setType(Calendar::TYPE_TIME_VARIABLE);
    $this->assertEquals(Calendar::TYPE_TIME_VARIABLE, $this->calendar->getType());
  }

  public function testSetAndGetOwner(): void
  {
    $owner = new AdminUser();
    $this->calendar->setOwner($owner);
    $this->assertSame($owner, $this->calendar->getOwner());
  }

  public function testSetAndGetContactEmail(): void
  {
    $email = 'test@example.com';
    $this->calendar->setContactEmail($email);
    $this->assertEquals($email, $this->calendar->getContactEmail());
  }

  public function testSetAndGetRollingDays(): void
  {
    $days = 15;
    $this->calendar->setRollingDays($days);
    $this->assertEquals($days, $this->calendar->getRollingDays());
  }

  public function testSetAndGetDraftsDuration(): void
  {
    $duration = 60;
    $this->calendar->setDraftsDuration($duration);
    $this->assertEquals($duration, $this->calendar->getDraftsDuration());
  }

  public function testSetAndGetDraftsDurationIncrement(): void
  {
    $increment = 86400; // 1 day in seconds
    $this->calendar->setDraftsDurationIncrement($increment);
    $this->assertEquals($increment, $this->calendar->getDraftsDurationIncrement());
  }

  public function testSetAndGetMinimumSchedulingNotice(): void
  {
    $notice = 24;
    $this->calendar->setMinimumSchedulingNotice($notice);
    $this->assertEquals($notice, $this->calendar->getMinimumSchedulingNotice());
  }

  public function testSetAndGetAllowCancelDays(): void
  {
    $days = 5;
    $this->calendar->setAllowCancelDays($days);
    $this->assertEquals($days, $this->calendar->getAllowCancelDays());
  }

  public function testSetAndGetIsModerated(): void
  {
    $this->calendar->setIsModerated(true);
    $this->assertTrue($this->calendar->getIsModerated());
  }

  public function testSetAndGetAllowOverlaps(): void
  {
    $this->calendar->setAllowOverlaps(true);
    $this->assertTrue($this->calendar->isAllowOverlaps());
  }

  public function testAddAndRemoveModerator(): void
  {
    $moderator = new OperatoreUser();
    $this->calendar->addModerator($moderator);
    $this->assertTrue($this->calendar->getModerators()->contains($moderator));

    $this->calendar->removeModerator($moderator);
    $this->assertFalse($this->calendar->getModerators()->contains($moderator));
  }

  public function testAddAndRemoveOpeningHour(): void
  {
    $openingHour = new OpeningHour();
    $this->calendar->addOpeningHour($openingHour);
    $this->assertTrue($this->calendar->getOpeningHours()->contains($openingHour));

    $this->calendar->removeOpeningHour($openingHour);
    $this->assertFalse($this->calendar->getOpeningHours()->contains($openingHour));
  }

  public function testSetAndGetLocation(): void
  {
    $location = 'Test Location';
    $this->calendar->setLocation($location);
    $this->assertEquals($location, $this->calendar->getLocation());
  }

  public function testSetAndGetExternalCalendars(): void
  {
    $tmp = new ExternalCalendar();
    $tmp->setName('Google Calendar');
    $tmp->setUrl('https://calendar.google.com');
    $externalCalendars[] = $tmp;
    $this->calendar->setExternalCalendars([$tmp->jsonSerialize()]);
    $this->assertEquals($externalCalendars, $this->calendar->getExternalCalendars());
  }

  public function testSetAndGetClosingPeriods(): void
  {
    $tmp = new DateTimeInterval();
    $tmp->setFromTime(new DateTime('2024-01-01 00:00:00'));
    $tmp->setToTime(new DateTime('2024-01-01 23:59:59'));
    $closingPeriods[] = $tmp;
    $this->calendar->setClosingPeriods([$tmp->jsonSerialize()]);
    $this->assertEquals($closingPeriods, $this->calendar->getClosingPeriods());
  }

  public function testSetAndGetReservationLimits(): void
  {
    $reservationLimits = ReservationLimits::fromArray(
      [
        'max_reservations_per_day' => 5,
        'interval' => 'daily',
      ]
    );
    $this->calendar->setReservationLimits($reservationLimits);
    $this->assertEquals($reservationLimits, $this->calendar->getReservationLimits());
  }

  public function testSetAndGetCreatedAt(): void
  {
    $date = new DateTime();
    $this->calendar->setCreatedAt($date);
    $this->assertEquals($date, $this->calendar->getCreatedAt());
  }

  public function testSetAndGetUpdatedAt(): void
  {
    $date = new DateTime();
    $this->calendar->setUpdatedAt($date);
    $this->assertEquals($date, $this->calendar->getUpdatedAt());
  }

  public function testSetTitleHandlesNull(): void
  {
    $this->calendar->setTitle(null);
    $this->assertNull($this->calendar->getTitle());
  }

  public function testSetContactEmailHandlesEmptyString(): void
  {
    $this->calendar->setContactEmail('');
    $this->assertEquals('', $this->calendar->getContactEmail());
  }

  public function testModeratorsCollectionIsInitialized(): void
  {
    $this->assertInstanceOf(Collection::class, $this->calendar->getModerators());
    $this->assertCount(0, $this->calendar->getModerators());
  }

  public function testOpeningHoursCollectionIsInitialized(): void
  {
    $this->assertInstanceOf(Collection::class, $this->calendar->getOpeningHours());
    $this->assertCount(0, $this->calendar->getOpeningHours());
  }

  public function testExternalCalendarsHandlesEmptyArray(): void
  {
    $this->calendar->setExternalCalendars([]);
    $this->assertEquals([], $this->calendar->getExternalCalendars());
  }

  public function testClosingPeriodsHandlesEmptyArray(): void
  {
    $this->calendar->setClosingPeriods([]);
    $this->assertEquals([], $this->calendar->getClosingPeriods());
  }

  /*public function testReservationLimitsHandlesEmptyArray(): void
  {
    $this->calendar->setReservationLimits([]);
    $this->assertEquals([], $this->calendar->getReservationLimits());
  }*/

  public function testOwnerIsInitiallyNull(): void
  {
    $this->assertNull($this->calendar->getOwner());
  }

  public function testLocationIsInitiallyEmpty(): void
  {
    $this->assertEmpty($this->calendar->getLocation());
  }

  public function testIsModeratedIsInitiallyFalse(): void
  {
    $this->assertFalse($this->calendar->getIsModerated());
  }

  public function testAllowOverlapsIsInitiallyFalse(): void
  {
    $this->assertFalse($this->calendar->isAllowOverlaps());
  }

  public function testDefaultValuesAreSet(): void
  {
    $this->assertEquals(Calendar::DEFAULT_CANCEL_DAYS, $this->calendar->getAllowCancelDays());
    $this->assertEquals(Calendar::DEFAULT_ROLLING_DAYS, $this->calendar->getRollingDays());
    $this->assertEquals(Calendar::DEFAULT_DRAFT_DURATION, $this->calendar->getDraftsDuration());
    $this->assertEquals(Calendar::DEFAULT_DRAFT_INCREMENT, $this->calendar->getDraftsDurationIncrement());
    $this->assertEquals(0, $this->calendar->getMinimumSchedulingNotice());
  }

  public function testSetAndGetId(): void
  {
    $id = Uuid::uuid4();
    $reflection = new ReflectionClass($this->calendar);
    $property = $reflection->getProperty('id');
    $property->setAccessible(true);
    $property->setValue($this->calendar, $id);

    $this->assertEquals($id, $this->calendar->getId());
  }

  /*public function testSetAndGetIdHandlesInvalidUuid(): void
  {
    $this->expectException(\InvalidArgumentException::class);
    $this->calendar->setId('invalid-uuid');
  }*/

  public function testCreatedAtIsInitiallyNull(): void
  {
    $this->assertNull($this->calendar->getCreatedAt());
  }

  public function testUpdatedAtIsInitiallyNull(): void
  {
    $this->assertNull($this->calendar->getUpdatedAt());
  }

  /*public function testSetAndGetCreatedAtHandlesNull(): void
  {
    $this->calendar->setCreatedAt(null);
    $this->assertNull($this->calendar->getCreatedAt());
  }

  public function testSetAndGetUpdatedAtHandlesNull(): void
  {
    $this->calendar->setUpdatedAt(null);
    $this->assertNull($this->calendar->getUpdatedAt());
  }*/
}
