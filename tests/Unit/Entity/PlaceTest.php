<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\Place;
use App\Entity\Categoria;
use App\Entity\GeographicArea;
use App\Entity\ContactPoint;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\UuidInterface;

class PlaceTest extends TestCase
{
  public function testPlaceConstructorInitializesProperties(): void
  {
    $place = new Place();

    $this->assertNotNull($place->getId());
    $this->assertInstanceOf(UuidInterface::class, $place->getId());
    $this->assertInstanceOf(ArrayCollection::class, $place->getGeographicAreas());
    $this->assertCount(0, $place->getGeographicAreas());
  }

  public function testSetNameAndGetName(): void
  {
    $place = new Place();
    $place->setName('Test Place');

    $this->assertEquals('Test Place', $place->getName());
  }

  public function testSetOtherNameAndGetOtherName(): void
  {
    $place = new Place();
    $place->setOtherName('Other Name');

    $this->assertEquals('Other Name', $place->getOtherName());
  }

  public function testSetTopicAndGetTopic(): void
  {
    $place = new Place();
    $topic = $this->createMock(Categoria::class);
    $place->setTopic($topic);

    $this->assertSame($topic, $place->getTopic());
  }

  public function testGetTopicId(): void
  {
    $place = new Place();
    $topic = $this->createMock(Categoria::class);
    $topicId = Uuid::uuid4();
    $topic->method('getId')->willReturn($topicId);

    $place->setTopic($topic);

    $this->assertEquals($topicId, $place->getTopicId());
  }

  public function testSetShortDescriptionAndGetShortDescription(): void
  {
    $place = new Place();
    $place->setShortDescription('Short description');

    $this->assertEquals('Short description', $place->getShortDescription());
  }

  public function testSetDescriptionAndGetDescription(): void
  {
    $place = new Place();
    $place->setDescription('Description');

    $this->assertEquals('Description', $place->getDescription());
  }

  public function testSetAddressAndGetAddress(): void
  {
    $place = new Place();
    $address = ['street_address' => '123 Main St', 'post_office_box_number' => 'PO Box 456'];
    $place->setAddress($address);

    $this->assertSame($address, $place->getAddress());
  }

  public function testSetLatitudeAndGetLatitude(): void
  {
    $place = new Place();
    $place->setLatitude('45.0');

    $this->assertEquals('45.0', $place->getLatitude());
  }

  public function testSetLongitudeAndGetLongitude(): void
  {
    $place = new Place();
    $place->setLongitude('90.0');

    $this->assertEquals('90.0', $place->getLongitude());
  }

  public function testSetMoreInfoAndGetMoreInfo(): void
  {
    $place = new Place();
    $place->setMoreInfo('More information');

    $this->assertEquals('More information', $place->getMoreInfo());
  }

  public function testSetIdentifierAndGetIdentifier(): void
  {
    $place = new Place();
    $place->setIdentifier('Identifier');

    $this->assertEquals('Identifier', $place->getIdentifier());
  }

  public function testAddGeographicAreaAndGetGeographicAreas(): void
  {
    $place = new Place();
    $area = $this->createMock(GeographicArea::class);
    $place->addGeographicArea($area);

    $this->assertCount(1, $place->getGeographicAreas());
    $this->assertTrue($place->getGeographicAreas()->contains($area));
  }

  public function testRemoveGeographicArea(): void
  {
    $place = new Place();
    $area = $this->createMock(GeographicArea::class);
    $place->addGeographicArea($area);
    $place->removeGeographicArea($area);

    $this->assertCount(0, $place->getGeographicAreas());
    $this->assertFalse($place->getGeographicAreas()->contains($area));
  }

  public function testGetGeographicAreaIds(): void
  {
    $place = new Place();
    $area1 = $this->createMock(GeographicArea::class);
    $area2 = $this->createMock(GeographicArea::class);
    $area1Id = Uuid::uuid4();
    $area2Id = Uuid::uuid4();
    $area1->method('getId')->willReturn($area1Id);
    $area2->method('getId')->willReturn($area2Id);

    $place->addGeographicArea($area1);
    $place->addGeographicArea($area2);

    $geographicAreaIds = $place->getGeographicAreaIds();

    $this->assertCount(2, $geographicAreaIds);
    $this->assertContains($area1Id, $geographicAreaIds);
    $this->assertContains($area2Id, $geographicAreaIds);
  }

  public function testSetCoreContactPointAndGetCoreContactPoint(): void
  {
    $place = new Place();
    $contactPoint = $this->createMock(ContactPoint::class);
    $place->setCoreContactPoint($contactPoint);

    $this->assertSame($contactPoint, $place->getCoreContactPoint());
  }

  public function testGetHumanReadableAddress(): void
  {
    $place = new Place();
    $address = [
      'street_address' => 'Via Roma',
      'post_office_box_number' => '1',
      'postal_code' => '00100',
      'address_locality' => 'Roma',
    ];
    $place->setAddress($address);

    $expectedAddress = 'Via Roma, 1 00100 Roma';
    $this->assertEquals($expectedAddress, $place->getHumanReadableAddress());
    $place->setAddress(null);
    $this->assertNull($place->getHumanReadableAddress());
  }
}
