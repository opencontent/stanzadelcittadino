<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\CodeCounter;
use App\Entity\CodeGenerationStrategy;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Error;
use ReflectionClass;

/**
 * @covers \App\Entity\CodeCounter
 */
class CodeCounterTest extends TestCase
{
  private CodeCounter $codeCounter;

  protected function setUp(): void
  {
    $this->codeCounter = new CodeCounter();
  }

  public function testConstructorInitializesId(): void
  {
    $this->assertInstanceOf(UuidInterface::class, $this->codeCounter->getId());
    $this->assertNotNull($this->codeCounter->getId());
  }

  public function testGetAndSetCodeGenerationStrategy(): void
  {
    $strategy = $this->createMock(CodeGenerationStrategy::class);
    $this->codeCounter->setCodeGenerationStrategy($strategy);
    $this->assertSame($strategy, $this->codeCounter->getCodeGenerationStrategy());
  }

  public function testGetAndSetKey(): void
  {
    $key = 'test_key';
    $this->codeCounter->setKey($key);
    $this->assertEquals($key, $this->codeCounter->getKey());

    $this->codeCounter->setKey(null);
    $this->assertNull($this->codeCounter->getKey());
  }

  public function testGetAndSetCounter(): void
  {
    $this->codeCounter->setCounter(10);
    $this->assertEquals(10, $this->codeCounter->getCounter());
  }

  public function testIncrementCounterIncreasesCounter(): void
  {
    $strategy = $this->createMock(CodeGenerationStrategy::class);
    $strategy->method('getMaxCounterValue')->willReturn(10);
    $this->codeCounter->setCodeGenerationStrategy($strategy);
    $this->codeCounter->setCounter(0);

    $this->codeCounter->incrementCounter();
    $this->assertEquals(1, $this->codeCounter->getCounter());

    $this->codeCounter->incrementCounter();
    $this->assertEquals(2, $this->codeCounter->getCounter());
  }

  public function testIncrementCounterResetsWhenMaxValueIsReached(): void
  {
    $strategy = $this->createMock(CodeGenerationStrategy::class);
    $strategy->method('getMaxCounterValue')->willReturn(2);
    $this->codeCounter->setCodeGenerationStrategy($strategy);
    $this->codeCounter->setCounter(2);
    $this->codeCounter->incrementCounter();
    $this->assertEquals(1, $this->codeCounter->getCounter(), 'Counter should reset to 1 when max value is reached');
  }

  public function testIncrementCounterWithoutStrategy(): void
  {
    $this->expectException(Error::class);
    $this->codeCounter->incrementCounter();
  }

  public function testSetId(): void
  {
    $uuid = Uuid::uuid4();
    $reflection = new ReflectionClass($this->codeCounter);
    $property = $reflection->getProperty('id');
    $property->setAccessible(true);
    $property->setValue($this->codeCounter, $uuid);

    $this->assertSame($uuid, $this->codeCounter->getId());
  }
}
