<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\Ente;
use App\Entity\Servizio;
use App\Model\CustomTemplate;
use App\Model\DateTimeInterval;
use App\Model\DefaultProtocolSettings;
use App\Model\Mailer;
use App\Model\Tenant\ThemeOptions;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;
use DateTime;

class EnteTest extends TestCase
{
  private Ente $ente;

  protected function setUp(): void
  {
    $this->ente = new Ente();
  }

  public function testConstructor(): void
  {
    $this->assertInstanceOf(UuidInterface::class, $this->ente->getId());
    $this->assertInstanceOf(ArrayCollection::class, $this->ente->getProtocolloParameters());
    $this->assertFalse($this->ente->isIOEnabled());
    $this->assertTrue($this->ente->isLinkableApplicationMeetings());
    $this->assertEquals(Ente::NAVIGATION_TYPE_SERVICES, $this->ente->getNavigationType());
  }

  public function testSetAndGetBasicProperties(): void
  {
    $this->ente->setName('Test Ente');
    $this->ente->setSlug('test-ente');
    $this->ente->setCodiceMeccanografico('ABC123');
    $this->ente->setSiteUrl('https://test-ente.com');
    $this->ente->setCodiceAmministrativo('ADM123');
    $this->ente->setContatti('Test Contatti');
    $this->ente->setEmail('test@ente.com');
    $this->ente->setEmailCertificata('pec@ente.com');
    $this->ente->setIpaCode('IPA123');
    $this->ente->setCadastralCode('CAD123');
    $this->ente->setAooCode('AOO123');
    $this->ente->setSatisfyEntrypointId('SATISFY123');

    $this->assertEquals('Test Ente', $this->ente->getName());
    $this->assertEquals('Test Ente', $this->ente->getNameForEmail());
    $this->assertEquals('test-ente', $this->ente->getSlug());
    $this->assertEquals('ABC123', $this->ente->getCodiceMeccanografico());
    $this->assertEquals('https://test-ente.com', $this->ente->getSiteUrl());
    $this->assertEquals('ADM123', $this->ente->getCodiceAmministrativo());
    $this->assertEquals('Test Contatti', $this->ente->getContatti());
    $this->assertEquals('test@ente.com', $this->ente->getEmail());
    $this->assertEquals('pec@ente.com', $this->ente->getEmailCertificata());
    $this->assertEquals('IPA123', $this->ente->getIpaCode());
    $this->assertEquals('CAD123', $this->ente->getCadastralCode());
    $this->assertEquals('AOO123', $this->ente->getAooCode());
    $this->assertEquals('SATISFY123', $this->ente->getSatisfyEntrypointId());
    $this->assertTrue($this->ente->isSatisfyEnabled());
  }

  public function testSetAndGetMeta(): void
  {
    $meta = json_encode([
      'key' => 'value',
      'tenant_type' => 'comune',
      'enable_search_and_catalogue' => true,
      'main' => [
        ['text' => 'Services', 'url' => '/services'],
        ['text' => 'About', 'url' => '/about'],
      ],
      'social' => ['facebook' => 'fb_link', 'twitter' => 'twitter_link'],
      'builtin_services' => ['appointment_booking' => '/book'],
      'legals' => ['privacy_info' => '/privacy'],
    ]);
    $this->ente->setMeta($meta);

    $this->assertEquals($meta, $this->ente->getMeta());
    $this->assertEquals(['key' => 'value', 'tenant_type' => 'comune', 'enable_search_and_catalogue' => true, 'main' => [['text' => 'Services', 'url' => '/services'], ['text' => 'About', 'url' => '/about']], 'social' => ['facebook' => 'fb_link', 'twitter' => 'twitter_link'], 'builtin_services' => ['appointment_booking' => '/book'], 'legals' => ['privacy_info' => '/privacy']], $this->ente->getMetaAsArray());
    $this->assertEquals('value', $this->ente->getMetaAsArray('key'));
    $this->assertEquals('comune', $this->ente->getTenantType());
    $this->assertTrue($this->ente->isSearchAndCatalogueEnabled());
    $this->assertEquals('/services', $this->ente->getServicesUrl('Services'));
    $this->assertEquals('/book', $this->ente->getAppointmentBookingUrl());
    $this->assertEquals('/privacy', $this->ente->getPrivacyInfo());
    $this->assertTrue($this->ente->hasSocial());
  }

  public function testSetAndGetProtocolloParameters(): void
  {
    $servizio = $this->createMock(Servizio::class);
    $servizio->method('getSlug')->willReturn('test-service');

    $params = ['param1' => 'value1'];
    $this->ente->setProtocolloParametersPerServizio($params, $servizio);

    $this->assertEquals($params, $this->ente->getProtocolloParametersPerServizio($servizio));

    $defaultSettings = new DefaultProtocolSettings();
    $this->ente->setDefaultProtocolSettings($defaultSettings);
    $this->assertEquals($defaultSettings, $this->ente->getDefaultProtocolSettings());
  }

  public function testSetAndGetMailers(): void
  {
    $mailer1 = new Mailer();
    $mailer1->setTitle('Mailer 1');
    $mailer2 = new Mailer();
    $mailer2->setTitle('Mailer 2');

    $mailers = new ArrayCollection([$mailer1, $mailer2]);
    $this->ente->setMailers($mailers);

    $retrievedMailers = $this->ente->getMailers();
    $this->assertCount(2, $retrievedMailers);
    $this->assertInstanceOf(Mailer::class, $retrievedMailers[md5('Mailer 1')]);
    $this->assertInstanceOf(Mailer::class, $retrievedMailers[md5('Mailer 2')]);

    $this->assertInstanceOf(Mailer::class, $this->ente->getMailer(md5('Mailer 1')));
    $this->assertNull($this->ente->getMailer('non-existent'));
  }

  public function testSetAndGetGateways(): void
  {
    $gateways = ['gateway1', 'gateway2'];
    $this->ente->setGateways($gateways);
    $this->assertEquals($gateways, $this->ente->getGateways());
  }

  public function testSetAndGetBackofficeEnabledIntegrations(): void
  {
    $integrations = ['integration1', 'integration2'];
    $this->ente->setBackofficeEnabledIntegrations($integrations);
    $this->assertEquals($integrations, $this->ente->getBackofficeEnabledIntegrations());
  }

  public function testSetAndGetIOEnabled(): void
  {
    $this->ente->setIOEnabled(true);
    $this->assertTrue($this->ente->isIOEnabled());
  }

  public function testSetAndGetLinkableApplicationMeetings(): void
  {
    $this->ente->setLinkableApplicationMeetings(false);
    $this->assertFalse($this->ente->isLinkableApplicationMeetings());
  }

  public function testSetAndGetNavigationType(): void
  {
    $this->ente->setNavigationType(Ente::NAVIGATION_TYPE_CATEGORIES);
    $this->assertEquals(Ente::NAVIGATION_TYPE_CATEGORIES, $this->ente->getNavigationType());
  }

  public function testSetAndGetThemeOptions(): void
  {
    $themeOptions = new ThemeOptions();
    $this->ente->setThemeOptions($themeOptions);
    $this->assertInstanceOf(ThemeOptions::class, $this->ente->getThemeOptions());
  }

  public function testSetAndGetPdndSettings(): void
  {
    $this->ente->setDefaultPdndClientId('client123');
    $this->assertEquals('client123', $this->ente->getDefaultPdndClientId());

    $configIds = ['config1', 'config2'];
    $this->ente->setPdndConfigIds($configIds);
    $this->assertEquals($configIds, $this->ente->getPdndConfigIds());
  }

  public function testSetAndGetCustomTemplate(): void
  {
    $customTemplate = new CustomTemplate();
    $this->ente->setCustomTemplate($customTemplate);
    $this->assertInstanceOf(CustomTemplate::class, $this->ente->getCustomTemplate());
  }

  public function testSetAndGetClosingPeriods(): void
  {
    $closingPeriod1 = [
      'from_time' => '2023-01-01 00:00:00',
      'to_time' => '2023-01-02 00:00:00',
    ];
    $closingPeriod2 = [
      'from_time' => '2023-12-24 00:00:00',
      'to_time' => '2023-12-26 00:00:00',
    ];

    $this->ente->setClosingPeriods([$closingPeriod1, $closingPeriod2]);

    $retrievedClosingPeriods = $this->ente->getClosingPeriods();
    $this->assertCount(2, $retrievedClosingPeriods);
    $this->assertInstanceOf(DateTimeInterval::class, $retrievedClosingPeriods[0]);
    $this->assertInstanceOf(DateTimeInterval::class, $retrievedClosingPeriods[1]);

    $this->assertEquals(new DateTime('2023-01-01 00:00:00'), $retrievedClosingPeriods[0]->getFromTime());
    $this->assertEquals(new DateTime('2023-01-02 00:00:00'), $retrievedClosingPeriods[0]->getToTime());
    $this->assertEquals(new DateTime('2023-12-24 00:00:00'), $retrievedClosingPeriods[1]->getFromTime());
    $this->assertEquals(new DateTime('2023-12-26 00:00:00'), $retrievedClosingPeriods[1]->getToTime());
  }

  public function testFromEntity(): void
  {
    $originalEnte = new Ente();
    $originalEnte->setName('Original Ente');
    $originalEnte->setSlug('original-ente');
    $originalEnte->setMeta(json_encode(['key' => 'value']));
    $originalEnte->setCodiceMeccanografico('ABC123');
    $originalEnte->setSiteUrl('https://original-ente.com');
    $originalEnte->setCodiceAmministrativo('ADM123');
    $originalEnte->setContatti('Original Contatti');
    $originalEnte->setEmail('original@ente.com');
    $originalEnte->setEmailCertificata('original-pec@ente.com');

    $themeOptions = new ThemeOptions();
    $originalEnte->setThemeOptions($themeOptions);

    $newEnte = Ente::fromEntity($originalEnte);

    $this->assertEquals($originalEnte->getId(), $newEnte->getId());
    $this->assertEquals($originalEnte->getName(), $newEnte->getName());
    $this->assertEquals($originalEnte->getSlug(), $newEnte->getSlug());
    $this->assertEquals($originalEnte->getMetaAsArray(), $newEnte->getMetaAsArray());
    $this->assertEquals($originalEnte->getCodiceMeccanografico(), $newEnte->getCodiceMeccanografico());
    $this->assertEquals($originalEnte->getSiteUrl(), $newEnte->getSiteUrl());
    $this->assertEquals($originalEnte->getCodiceAmministrativo(), $newEnte->getCodiceAmministrativo());
    $this->assertEquals($originalEnte->getContatti(), $newEnte->getContatti());
    $this->assertEquals($originalEnte->getEmail(), $newEnte->getEmail());
    $this->assertEquals($originalEnte->getEmailCertificata(), $newEnte->getEmailCertificata());
    $this->assertEquals($originalEnte->getThemeOptions(), $newEnte->getThemeOptions());
  }
}
