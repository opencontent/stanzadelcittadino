<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\ContactPoint;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Email;

/**
 * @covers \App\Entity\ContactPoint
 */
class ContactPointTest extends TestCase
{
  private ContactPoint $contactPoint;

  protected function setUp(): void
  {
    $this->contactPoint = new ContactPoint();
  }

  public function testConstructorInitializesId(): void
  {
    $this->assertInstanceOf(UuidInterface::class, $this->contactPoint->getId());
    $this->assertNotNull($this->contactPoint->getId());
  }

  public function testGetNameAndSetName(): void
  {
    $name = 'Test Name';
    $this->contactPoint->setName($name);
    $this->assertEquals($name, $this->contactPoint->getName());

    $this->contactPoint->setName(null);
    $this->assertNull($this->contactPoint->getName());
  }

  public function testGetEmailAndSetEmail(): void
  {
    $email = 'test@example.com';
    $this->contactPoint->setEmail($email);
    $this->assertEquals($email, $this->contactPoint->getEmail());

    $this->contactPoint->setEmail(null);
    $this->assertNull($this->contactPoint->getEmail());
  }

  public function testGetUrlAndSetUrl(): void
  {
    $url = 'https://www.example.com';
    $this->contactPoint->setUrl($url);
    $this->assertEquals($url, $this->contactPoint->getUrl());

    $this->contactPoint->setUrl(null);
    $this->assertNull($this->contactPoint->getUrl());
  }

  public function testGetPhoneNumberAndSetPhoneNumber(): void
  {
    $phoneNumber = '+1234567890';
    $this->contactPoint->setPhoneNumber($phoneNumber);
    $this->assertEquals($phoneNumber, $this->contactPoint->getPhoneNumber());

    $this->contactPoint->setPhoneNumber(null);
    $this->assertNull($this->contactPoint->getPhoneNumber());
  }

  public function testGetPecAndSetPec(): void
  {
    $pec = 'pec@example.com';
    $this->contactPoint->setPec($pec);
    $this->assertEquals($pec, $this->contactPoint->getPec());

    $this->contactPoint->setPec(null);
    $this->assertNull($this->contactPoint->getPec());
  }

  public function testEmailValidation(): void
  {
    $validator = Validation::createValidator();

    $invalidEmail = 'not-an-email';
    $this->contactPoint->setEmail($invalidEmail);

    $violations = $validator->validate(
      $this->contactPoint->getEmail(),
      new Email()
    );

    $this->assertGreaterThan(0, $violations->count());

    $validEmail = 'test@example.com';
    $this->contactPoint->setEmail($validEmail);

    $violations = $validator->validate(
      $this->contactPoint->getEmail(),
      new Email()
    );

    $this->assertCount(0, $violations);
  }

  public function testPecValidation(): void
  {
    $validator = Validation::createValidator();

    $invalidPec = 'invalid-email';
    $this->contactPoint->setPec($invalidPec);

    $violations = $validator->validate(
      $this->contactPoint->getPec(),
      new Email()
    );

    $this->assertGreaterThan(0, $violations->count());

    $validPec = 'pec@example.com';
    $this->contactPoint->setPec($validPec);

    $violations = $validator->validate(
      $this->contactPoint->getPec(),
      new Email()
    );

    $this->assertCount(0, $violations);
  }
}
