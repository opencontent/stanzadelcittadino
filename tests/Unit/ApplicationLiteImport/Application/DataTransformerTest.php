<?php

declare(strict_types=1);

namespace Tests\Unit\ApplicationLiteImport\Application;

use App\ApplicationLiteImport\Application\DataTransformer;
use App\ApplicationLiteImport\Application\ImportCommand\ImportData;
use App\Applications\Application\DTOs\ApplicantData;
use App\Applications\Application\DTOs\ApplicationLiteData;
use App\Applications\Application\DTOs\BeneficiaryData;
use App\Applications\Application\DTOs\ServiceData;
use App\Entity\ApplicationLite;
use App\Entity\CPSUser;
use PHPUnit\Framework\TestCase;
use Tests\Builders\ApplicationLiteBuilder;

/**
 * @group unit
 * @group ApplicationLite
 */
class DataTransformerTest extends TestCase
{
  private function prepareDataForTest(): ApplicationLite
  {
    $applicant = new CPSUser();
    $applicant->setNome('John');
    $applicant->setCognome('Doe');
    $applicant->setCodiceFiscale('JHNDOE90A01F205X');
    $applicant->setEmail('john.doe@example.com');
    $applicant->setCpsCellulare('8888888888');
    $applicant->setCpsTelefono('9999999999');

    $beneficiary = new CPSUser();
    $beneficiary->setNome('Jane');
    $beneficiary->setCognome('Smith');
    $beneficiary->setCodiceFiscale('JNSMTH80B01F205X');
    $beneficiary->setEmail('jane.smith@example.com');
    $beneficiary->setCpsCellulare('7777777777');
    $beneficiary->setCpsTelefono('6666666666');

    return ApplicationLiteBuilder::createAsExternal()
      ->withExternalId('12345')
      ->withSubject('Test Subject')
      ->withStatus(ApplicationLite::STATUS_PENDING)
      ->withApplicant($applicant)
      ->withBeneficiary($beneficiary)
      ->build();
  }

  public function testToApplicationLiteApiModel(): void
  {
    // Arrange
    $applicationLite = $this->prepareDataForTest();
    $serviceId = $applicationLite->service()->getId();

    // Act
    $result = DataTransformer::toApplicationLiteApiModel($applicationLite);

    // Assert
    $this->assertInstanceOf(ApplicationLiteData::class, $result);
    $this->assertEquals('12345', $result->externalId);
    $this->assertEquals('Test Subject', $result->subject);
    $this->assertEquals(ApplicationLite::STATUS_PENDING, $result->status);

    $this->assertInstanceOf(ApplicantData::class, $result->applicant);
    $this->assertEquals('John', $result->applicant->givenName);
    $this->assertEquals('Doe', $result->applicant->familyName);
    $this->assertEquals('JHNDOE90A01F205X', $result->applicant->identifiers->taxCode);
    $this->assertEquals('john.doe@example.com', $result->applicant->contacts->email);
    $this->assertEquals('8888888888', $result->applicant->contacts->mobile);
    $this->assertEquals('9999999999', $result->applicant->contacts->phone);

    $this->assertInstanceOf(BeneficiaryData::class, $result->beneficiary);
    $this->assertEquals('Jane', $result->beneficiary->givenName);
    $this->assertEquals('Smith', $result->beneficiary->familyName);
    $this->assertEquals('JNSMTH80B01F205X', $result->beneficiary->identifiers->taxCode);
    $this->assertEquals('jane.smith@example.com', $result->beneficiary->contacts->email);
    $this->assertEquals('7777777777', $result->beneficiary->contacts->mobile);
    $this->assertEquals('6666666666', $result->beneficiary->contacts->phone);

    $this->assertInstanceOf(ServiceData::class, $result->service);
    $this->assertEquals($serviceId, $result->service->id);
    $this->assertEquals('Default service name', $result->service->name);
  }

  public function testToApplicationLiteImportApiModel(): void
  {
    // Arrange
    $applicationLite = $this->prepareDataForTest();

    // Act
    $result = DataTransformer::toApplicationLiteImportApiModel($applicationLite);

    // Assert
    $this->assertInstanceOf(ImportData::class, $result);
    $this->assertInstanceOf(ApplicationLiteData::class, $result->application);
    $this->assertEquals('12345', $result->application->externalId);
  }
}
