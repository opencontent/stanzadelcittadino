<?php

declare(strict_types=1);

namespace Tests\Unit\ApplicationLiteImport\Infrastructure;

use App\ApplicationLiteImport\Application\ImportCommand\ImportCommand;
use App\ApplicationLiteImport\Application\ImportCommand\ImportData;
use App\ApplicationLiteImport\Infrastructure\HttpRequestToImportCommandMapper;
use App\ApplicationLiteImport\Infrastructure\InvalidJsonException;
use App\Entity\CPSUser;
use App\Entity\Ente;
use App\Entity\User;
use JMS\Serializer\SerializerInterface;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 * @group ApplicationLite
 */
class HttpRequestToImportCommandMapperTest extends TestCase
{
  private SerializerInterface $serializer;
  private HttpRequestToImportCommandMapper $mapper;

  protected function setUp(): void
  {
    $this->serializer = $this->createMock(SerializerInterface::class);
    $this->mapper = new HttpRequestToImportCommandMapper($this->serializer);
  }

  public function testMapInvalidJsonThrowsException(): void
  {
    $invalidContent = '{"application": {"external_id": "123", "title": "Test Title"}}';
    $ente = $this->createMock(Ente::class);
    $requester = $this->createMock(User::class);

    $this->expectException(InvalidJsonException::class);
    $this->expectExceptionMessage('The provided JSON is not valid:');

    $this->mapper->map($invalidContent, $ente, $requester);
  }

  /** @throws InvalidJsonException */
  public function testMapValidJson(): void
  {
    // Arrange
    $content = <<<'JSON'
      {
        "application": {
          "external_id": "123",
          "subject": "Test subject",
          "applicant": {
            "given_name": "John",
            "family_name": "Doe",
            "identifiers": {
              "tax_code": "AAABBB80A12A123U"
            }
          },
          "status": 2000,
          "service": {
            "id": "85311642-195d-4ff0-8878-581d87170c0a"
          },
          "actions": [],
          "process_history": []
        }
      }
      JSON;

    $ente = new Ente();
    $requester = new CPSUser();
    $importData = new ImportData();

    $this->serializer
      ->method('deserialize')
      ->with($content, ImportData::class, 'json')
      ->willReturn($importData);

    // Act
    $importCommand = $this->mapper->map($content, $ente, $requester);

    // Assert
    $this->assertInstanceOf(ImportCommand::class, $importCommand);
    $this->assertSame($ente, $importCommand->ente);
    $this->assertSame($importData, $importCommand->importData);
    $this->assertSame($requester, $importCommand->requester);
  }

  public function testValidate(): void
  {

    $json = <<<'JSON'
      {
        "application": {
          "external_id": "12345",
          "subject": "Oggetto della pratica",
          "applicant": {
            "given_name": "Mario",
            "family_name": "Rossi",
            "identifiers": {
              "tax_code": "RSSMRA80A01H501Z"
            }
          },
          "status": 2000,
          "service": {
            "id": "7037f0f0-28eb-4237-9cc7-28c150f13a0d"
          },
          "actions": [],
          "process_history": []
        }
      }
      JSON;

    $isValid = $this->mapper->validate($json);
    $this->assertTrue($isValid, 'The JSON should be valid according to the defined schema.');
  }

  public function testValidateInvalidJson(): void
  {
    $this->expectException(InvalidJsonException::class);
    $this->expectExceptionMessage(
      'The provided JSON is not valid: The property applicant is required (application.applicant)',
    );

    $json = <<<'JSON'
      {
        "application": {
          "external_id": "12345",
          "subject": "Oggetto della pratica",
          "status": 2000,
          "service": {
            "id": "62fe0211-4de3-472d-b18e-abb0f7894925"
          }
        }
      }
      JSON;

    $this->mapper->validate($json);
  }
}
