<?php

declare(strict_types=1);

namespace Tests\Unit\Serializer;

use JMS\Serializer\SerializerInterface;
use JsonSchema\Constraints\Factory;
use JsonSchema\SchemaStorage;
use JsonSchema\Validator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use JsonException;

abstract class AbstractSerializerTest extends WebTestCase implements SerializerTestInterface
{
  protected SerializerInterface $serializer;

  public function setUp(): void
  {
    $kernel = self::bootKernel();
    $this->serializer = $kernel->getContainer()->get('jms_serializer');
  }

  protected function createJsonValidator(object $jsonSchemaObject): Validator
  {
    $schemaStorage = new SchemaStorage();
    $schemaId = 'file://' . uniqid('', true) . 'Schema';
    $schemaStorage->addSchema($schemaId, $jsonSchemaObject);

    // Provide $schemaStorage to the Validator so that references can be resolved during validation
    return new Validator(new Factory($schemaStorage));
  }

  /**
   * @throws JsonException
   */
  protected function getJsonSchemaObject(): object
  {
    return json_decode($this->getSchema(), false, 512, JSON_THROW_ON_ERROR);
  }

  protected function getValidationErrors($jsonValidator): string
  {
    $errors = '';
    foreach ($jsonValidator->getErrors() as $error) {
      $errors .= printf("[%s] %s\n", $error['property'], $error['message']);
    }

    return $errors;
  }
}
