<?php

declare(strict_types=1);

namespace Tests\Unit\Serializer;

use App\Dto\ServiceDto;
use App\Services\VersionService;
use Doctrine\ORM\EntityManagerInterface;
use HTMLPurifier;
use JMS\Serializer\SerializationContext;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Tests\Helpers\EntitiesHelper;
use JsonException;
use ReflectionException;

class ServiceSerializerTest extends AbstractSerializerTest
{
  private ServiceDto $serviceDto;

  /**
   * @group unit
   */
  public function setUp(): void
  {
    $kernel = self::bootKernel();
    $this->serializer = $kernel->getContainer()->get('jms_serializer');

    $router = $kernel->getContainer()->get('router');

    $mockVersionService = $this->createMock(VersionService::class);

    $mockEntityManager = $this->createMock(EntityManagerInterface::class);
    $mockHtmlPurifier = $this->createMock(HTMLPurifier::class);

    $mockedToken = $this->getMockBuilder(TokenInterface::class)
      ->disableOriginalConstructor()
      ->getMockForAbstractClass();
    $mockedToken
      ->method('getUser')
      ->willReturn(EntitiesHelper::createCpsAnonymousUser());
    $mockedTokenStorage = $this->getMockBuilder(TokenStorage::class)
      ->disableOriginalConstructor()
      ->getMock();
    $mockedTokenStorage
      ->method('getToken')
      ->willReturn($mockedToken);

    $this->serviceDto = new ServiceDto($router, $mockVersionService, $mockEntityManager, $mockHtmlPurifier, $this->serializer, $mockedTokenStorage);
  }

  /**
   * @throws JsonException
   * @throws ReflectionException
   */
  public function testSerializeService(): void
  {

    $ente = EntitiesHelper::createEnte();
    $category = EntitiesHelper::createCategoria();
    $service = EntitiesHelper::createFormIOService($ente, $category);

    $context = new SerializationContext();
    $context->setSerializeNull(true);
    $context->setGroups(['read']);
    $jsonContent = $this->serializer->serialize($this->serviceDto->fromEntity($service, 'http://formserver.localtest.me'), 'json', $context);

    $service->getPaymentParameters();

    $jsonToValidateObject = json_decode($jsonContent, false, 512, JSON_THROW_ON_ERROR);
    $jsonSchemaObject = $this->getJsonSchemaObject();

    // Provide $schemaStorage to the Validator so that references can be resolved during validation
    $jsonValidator = $this->createJsonValidator($jsonSchemaObject);

    // Do validation (use isValid() and getErrors() to check the result)
    $jsonValidator->validate($jsonToValidateObject, $jsonSchemaObject);

    // Per vedere l'output con gli errori il testa va lanciato con l'opzione --debug. Es. php bin/phpunit --debug  tests/Serializer/
    $this->assertTrue($jsonValidator->isValid(), $this->getValidationErrors($jsonValidator));
  }

  public function getSchema(): string
  {
    return <<<'JSON'
      {
        "type": "object",
        "properties": {
          "id": {
            "type": "string"
          },
          "type": {
            "type": "string"
          },
          "name": {
            "type": "string"
          },
          "slug": {
            "type": "string"
          },
          "tenant": {
            "type": "string"
          },
          "topics": {
            "type": "string"
          },
          "topics_id": {
            "type": "string"
          },
          "description": {
            "type": ["string", "null"]
          },
          "short_description": {
            "type": ["string", "null"]
          },
          "howto": {
            "type": ["string", "null"]
          },
          "how_to_do": {
            "type": ["string", "null"]
          },
          "what_you_need": {
            "type": ["string", "null"]
          },
          "what_you_get": {
            "type": ["string", "null"]
          },
          "costs": {
            "type": ["string", "null"]
          },
          "costs_attachments": {
            "type": "array",
            "items": {}
          },
          "who": {
            "type": ["string", "null"]
          },
          "special_cases": {
            "type": ["string", "null"]
          },
          "more_info": {
            "type": ["string", "null"]
          },
          "constraints": {
            "type": ["string", "null"]
          },
          "times_and_deadlines": {
            "type": ["string", "null"]
          },
          "booking_call_to_action": {
            "type": ["string", "null"]
          },
          "conditions": {
            "type": ["string", "null"]
          },
          "conditions_attachments": {
            "type": "array",
            "items": {}
          },
          "compilation_info": {
            "type": ["string", "null"]
          },
          "final_indications": {
            "type": ["string", "null"]
          },
          "coverage": {
            "type": "array",
            "items": {}
          },
          "response_type": {},
          "flow_steps": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "identifier": {
                  "type": "string"
                },
                "title": {},
                "type": {
                  "type": "string"
                },
                "description": {},
                "guide": {},
                "parameters": {
                  "type": "object",
                  "properties": {
                    "formio_id": {
                      "type": "string"
                    },
                    "url": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "formio_id",
                    "url"
                  ]
                }
              },
              "required": [
                "identifier",
                "title",
                "type",
                "description",
                "guide",
                "parameters"
              ]
            }
          },
          "protocol_required": {
            "type": ["boolean", "null"]
          },
          "protocol_handler": {
            "type": ["string", "null"]
          },
          "protocollo_parameters": {
            "type": "object",
            "properties": {},
            "required": []
          },
          "payment_required": {
            "type": ["number", "null"]
          },
          "is_payment_configured": {
            "type": "boolean"
          },
          "payment_parameters": {
            "type": "object",
            "properties": {
              "total_amounts": {
                "type": ["number", "string"]
              },
              "gateways": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "identifier": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "identifier"
                  ]
                }
              }
            },
            "required": [
              "total_amounts",
              "gateways"
            ]
          },
          "integrations": {
            "type": "array",
            "items": {}
          },
          "sticky": {
            "type": "boolean"
          },
          "status": {
            "type": "number"
          },
          "access_level": {
            "type": "number"
          },
          "access_url": {
            "type": "string"
          },
          "login_suggested": {},
          "scheduled_from": {},
          "scheduled_to": {},
          "service_group": {},
          "service_group_id": {},
          "shared_with_group": {},
          "user_group_ids": {
            "type": "array",
            "items": {}
          },
          "allow_reopening": {
            "type": "boolean"
          },
          "allow_withdraw": {
            "type": "boolean"
          },
          "allow_integration_request": {
            "type": "boolean"
          },
          "workflow": {
            "type": "number"
          },
          "recipients": {
            "type": "array",
            "items": {}
          },
          "recipients_id": {
            "type": "array",
            "items": {}
          },
          "geographic_areas": {
            "type": "array",
            "items": {}
          },
          "geographic_areas_id": {
            "type": "array",
            "items": {}
          },
          "max_response_time": {},
          "source": {
            "type": ["object", "null"],
            "properties": {
              "id": {
                "type": "string"
              },
              "url": {
                "type": "string"
              },
              "updated_at": {
                "type": "string"
              },
              "md5": {
                "type": "string"
              },
              "version": {
                "type": "string"
              },
              "identifier": {}
            },
            "required": [
              "id",
              "url",
              "updated_at",
              "md5",
              "version",
              "identifier"
            ]
          },
          "life_events": {
            "type": "array",
            "items": {}
          },
          "business_events": {
            "type": "array",
            "items": {}
          },
          "external_card_url": {},
          "feedback_messages": {
            "type": "object",
            "properties": {
              "status_draft": {
                "type": ["object", "null"],
                "properties": {
                  "message": {
                    "type": "string"
                  },
                  "subject": {
                    "type": "string"
                  },
                  "is_active": {
                    "type": "boolean"
                  }
                },
                "required": [
                  "message",
                  "subject",
                  "is_active"
                ]
              },
              "status_pre_submit": {
                "type": ["object", "null"],
                "properties": {
                  "message": {
                    "type": "string"
                  },
                  "subject": {
                    "type": "string"
                  },
                  "is_active": {
                    "type": "boolean"
                  }
                },
                "required": [
                  "message",
                  "subject",
                  "is_active"
                ]
              },
              "status_submitted": {
                "type": ["object", "null"],
                "properties": {
                  "message": {
                    "type": "string"
                  },
                  "subject": {
                    "type": "string"
                  },
                  "is_active": {
                    "type": "boolean"
                  }
                },
                "required": [
                  "message",
                  "subject",
                  "is_active"
                ]
              },
              "status_registered": {
                "type": ["object", "null"],
                "properties": {
                  "message": {
                    "type": "string"
                  },
                  "subject": {
                    "type": "string"
                  },
                  "is_active": {
                    "type": "boolean"
                  }
                },
                "required": [
                  "message",
                  "subject",
                  "is_active"
                ]
              },
              "status_pending": {
                "type": ["object", "null"],
                "properties": {
                  "message": {
                    "type": "string"
                  },
                  "subject": {
                    "type": "string"
                  },
                  "is_active": {
                    "type": "boolean"
                  }
                },
                "required": [
                  "message",
                  "subject",
                  "is_active"
                ]
              },
              "status_complete": {
                "type": ["object", "null"],
                "properties": {
                  "message": {
                    "type": "string"
                  },
                  "subject": {
                    "type": "string"
                  },
                  "is_active": {
                    "type": "boolean"
                  }
                },
                "required": [
                  "message",
                  "subject",
                  "is_active"
                ]
              },
              "status_cancelled": {
                "type": ["object", "null"],
                "properties": {
                  "message": {
                    "type": "string"
                  },
                  "subject": {
                    "type": "string"
                  },
                  "is_active": {
                    "type": "boolean"
                  }
                },
                "required": [
                  "message",
                  "subject",
                  "is_active"
                ]
              },
              "status_withdraw": {
                "type": ["object", "null"],
                "properties": {
                  "message": {
                    "type": "string"
                  },
                  "subject": {
                    "type": "string"
                  },
                  "is_active": {
                    "type": "boolean"
                  }
                },
                "required": [
                  "message",
                  "subject",
                  "is_active"
                ]
              }
            },
            "required": [
              "status_draft",
              "status_pre_submit",
              "status_submitted",
              "status_registered",
              "status_pending",
              "status_complete",
              "status_cancelled",
              "status_withdraw"
            ]
          },
          "identifier": {},
          "app_version": {
            "type": ["string", "null"]
          },
          "created_at": {
            "type": "string"
          },
          "updated_at": {
            "type": "string"
          },
          "satisfy_entrypoint_id": {
            "type": ["string", "null"]
          },
          "disable_message_notifications": {
            "type": "boolean"
          },
          "stamps": {
            "type": "array",
            "items": {}
          },
          "receipt": {
            "type": "string"
          },
          "pdnd_config_ids": {
            "type": "array",
            "items": {}
          },
          "payment_gateway_identifier": {},
          "payment_configs": {
            "type": "array",
            "items": {}
          }
        },
        "required": [
          "id",
          "type",
          "name",
          "slug",
          "tenant",
          "topics",
          "topics_id",
          "description",
          "short_description",
          "howto",
          "how_to_do",
          "what_you_need",
          "what_you_get",
          "costs",
          "costs_attachments",
          "who",
          "special_cases",
          "more_info",
          "constraints",
          "times_and_deadlines",
          "booking_call_to_action",
          "conditions",
          "conditions_attachments",
          "compilation_info",
          "final_indications",
          "coverage",
          "response_type",
          "flow_steps",
          "protocol_required",
          "protocol_handler",
          "protocollo_parameters",
          "payment_required",
          "is_payment_configured",
          "payment_parameters",
          "integrations",
          "sticky",
          "status",
          "access_level",
          "access_url",
          "login_suggested",
          "scheduled_from",
          "scheduled_to",
          "service_group",
          "service_group_id",
          "shared_with_group",
          "user_group_ids",
          "allow_reopening",
          "allow_withdraw",
          "allow_integration_request",
          "workflow",
          "recipients",
          "recipients_id",
          "geographic_areas",
          "geographic_areas_id",
          "max_response_time",
          "source",
          "life_events",
          "business_events",
          "external_card_url",
          "feedback_messages",
          "identifier",
          "app_version",
          "created_at",
          "updated_at",
          "satisfy_entrypoint_id",
          "disable_message_notifications",
          "stamps",
          "receipt",
          "pdnd_config_ids",
          "payment_gateway_identifier",
          "payment_configs"
        ]
      }
      JSON;
  }
}
