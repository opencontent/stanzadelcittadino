<?php

declare(strict_types=1);

namespace Tests\Services;

use App\Entity\Allegato;
use App\Entity\Ente;
use App\Entity\OperatoreUser;
use App\Entity\Pratica;
use App\Entity\RichiestaIntegrazioneDTO;
use App\Entity\Servizio;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Tests\Base\AbstractAppTestCase;

class PraticaIntegrationServiceTest extends AbstractAppTestCase
{
  public function setUp(): void
  {
    parent::setUp();

    system('rm -rf ' . __DIR__ . '/../../../var/uploads/pratiche/allegati/*');

    $this->userProvider = $this->container->get('ocsdc.cps.userprovider');
    $this->cleanDb(Allegato::class);
    $this->cleanDb(Pratica::class);
    $this->cleanDb(Servizio::class);
    $this->cleanDb(OperatoreUser::class);
    $this->cleanDb(Ente::class);
    $this->cleanDb(User::class);
  }

  public function testRequestIntegration(): void
  {
    $user = $this->createCPSUser();
    $pratica = $this->createPratica($user);
    $pratica->setStatus(Pratica::STATUS_PENDING);
    $this->em->persist($pratica);
    $this->em->flush();

    $operatore = $this->createOperatoreUser('test', 'test', $pratica->getEnte(), new ArrayCollection([$pratica->getServizio()]));

    $message = 'la mia richiesta di integrazione di test';
    $payload = ['test' => ['a', 'b', 'c']];

    $service = $this->container->get('ocsdc.pratica_integration_service');
    $request = new RichiestaIntegrazioneDTO($payload, $operatore, $message);

    $service->requestIntegration($pratica, $request);

    $this->assertTrue($pratica->haUnaRichiestaDiIntegrazioneAttiva());
    $this->assertNotNull($pratica->getRichiestaDiIntegrazioneAttiva());

    $this->assertEquals($message, $pratica->getRichiestaDiIntegrazioneAttiva()->getDescription());
    $this->assertEquals($payload, $pratica->getRichiestaDiIntegrazioneAttiva()->getPayload());
  }
}
