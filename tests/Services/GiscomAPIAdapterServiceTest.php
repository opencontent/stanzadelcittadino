<?php

declare(strict_types=1);

namespace Tests\Services;

use App\Entity\Pratica;
use App\Entity\SciaPraticaEdilizia;
use App\Mapper\Giscom\GiscomStatusMapper;
use App\Services\GiscomAPIAdapterService;
use App\Services\GiscomAPIMapperService;
use GuzzleHttp\Psr7\Response;
use Tests\Base\AbstractAppTestCase;
use Exception;

class GiscomAPIAdapterServiceTest extends AbstractAppTestCase
{
  public function testServiceExists(): void
  {
    $giscomApiAdapter = $this->container->get('ocsdc.giscom_api.adapter');
    $this->assertNotNull($giscomApiAdapter);

    $giscomApiAdapter = $this->container->get('ocsdc.giscom_api.adapter_direct');
    $this->assertNotNull($giscomApiAdapter);
  }

  public function testServiceSendsPraticaToGISCOM(): void
  {
    $pratica = $this->setupPraticaScia([], true);
    $pratica->setStatus(SciaPraticaEdilizia::STATUS_REGISTERED);
    $this->em->flush();

    $guzzleMock = $this->getMockGuzzleClient([
      new Response(201, [], json_encode([
        'Id' => 1234,
        'Stato' => [
          'Codice' => GiscomStatusMapper::GISCOM_STATUS_PREISTRUTTORIA,
          'Note' => '',
        ],
      ])),
      new Response(200, [], json_encode([
        $this->getCPSUserBaseData()['codiceFiscale'],
        $this->getCPSUserBaseData()['codiceFiscale'],
        $this->getCPSUserBaseData()['codiceFiscale'],
      ])),
    ]);


    $giscomApiAdapter = new GiscomAPIAdapterService(
      $guzzleMock,
      $this->em,
      $this->getMockLogger(),
      $this->getMockedGiscomMapper($this->em),
      $this->container->get('ocsdc.pratica_status_service'),
      $this->container->get('ocsdc.status_mapper.giscom')
    );
    $response = $giscomApiAdapter->sendPraticaToGiscom($pratica);
    $responseBody = json_decode($response->getBody(), true);
    $this->assertEquals(201, $response->getStatusCode());
    $this->assertNotNull($responseBody['Id']);
    $this->assertNotNull($responseBody['Stato']);

  }

  public function testServiceSendsPraticaToGISCOMWithPUTIfForIntegrazione(): void
  {
    $pratica = $this->setupPraticaScia([], true);
    $pratica->setStatus(Pratica::STATUS_PENDING_AFTER_INTEGRATION);
    $this->em->flush();

    $guzzleMock = $this->getMockGuzzleClient([
      new Response(201, [], json_encode([
        'Id' => 1234,
        'Stato' => [
          'Codice' => GiscomStatusMapper::GISCOM_STATUS_PREISTRUTTORIA,
          'Note' => '',
        ],
      ])),
      new Response(200, [], json_encode([
        $this->getCPSUserBaseData()['codiceFiscale'],
        $this->getCPSUserBaseData()['codiceFiscale'],
        $this->getCPSUserBaseData()['codiceFiscale'],
      ])),
    ]);

    $logger = $this->getMockLogger();
    $logger->expects($this->at(0))
      ->method('info')
      ->with($this->callback(fn ($subject) => false !== strpos($subject, 'Updating')));


    $giscomApiAdapter = new GiscomAPIAdapterService(
      $guzzleMock,
      $this->em,
      $logger,
      $this->getMockedGiscomMapper($this->em),
      $this->container->get('ocsdc.pratica_status_service'),
      $this->container->get('ocsdc.status_mapper.giscom')
    );
    $response = $giscomApiAdapter->sendPraticaToGiscom($pratica);
    $this->assertEquals(201, $response->getStatusCode());

    $responseBody = json_decode($response->getBody(), true);
    $this->assertNotNull($responseBody['Id']);
    $this->assertNotNull($responseBody['Stato']);
  }

  public function testServiceLogsRemoteError(): void
  {

    $pratica = $this->setupPraticaScia([], true);
    $guzzleMock = $this->getMockGuzzleClient([new Response(400)]);

    $loggerMock = $this->getMockLogger();
    $loggerMock->expects($this->once())->method('info');
    $loggerMock->expects($this->once())->method('error');


    $giscomApiAdapter = new GiscomAPIAdapterService(
      $guzzleMock,
      $this->em,
      $loggerMock,
      $this->getMockedGiscomMapper($this->em),
      $this->container->get('ocsdc.pratica_status_service'),
      $this->container->get('ocsdc.status_mapper.giscom')
    );

    $exception = null;
    try {
      $giscomApiAdapter->sendPraticaToGiscom($pratica);
    } catch (Exception $e) {
      $exception = $e;
    }

    $this->assertInstanceOf(Exception::class, $exception);
  }

  public function testServiceLogsRemoteResponse(): void
  {
    $pratica = $this->setupPraticaScia([], true);
    $pratica->setStatus(SciaPraticaEdilizia::STATUS_REGISTERED);
    $this->em->flush();

    $guzzleMock = $this->getMockGuzzleClient([
      new Response(201, [], json_encode([
        'Id' => 1234,
        'Stato' => [
          'Codice' => GiscomStatusMapper::GISCOM_STATUS_PREISTRUTTORIA,
          'Note' => '',
        ],
      ])),
      new Response(200, [], json_encode([
        $this->getCPSUserBaseData()['codiceFiscale'],
        $this->getCPSUserBaseData()['codiceFiscale'],
        $this->getCPSUserBaseData()['codiceFiscale'],
      ])),
    ]);

    $loggerMock = $this->getMockLogger();
    $loggerMock->expects($this->exactly(3))->method('info');

    $giscomApiAdapter = new GiscomAPIAdapterService(
      $guzzleMock,
      $this->em,
      $loggerMock,
      $this->getMockedGiscomMapper($this->em),
      $this->container->get('ocsdc.pratica_status_service'),
      $this->container->get('ocsdc.status_mapper.giscom')
    );
    $response = $giscomApiAdapter->sendPraticaToGiscom($pratica);
    $this->assertEquals(201, $response->getStatusCode());
    $responseBody = json_decode($response->getBody(), true);
    $this->assertNotNull($responseBody['Id']);
    $this->assertNotNull($responseBody['Stato']);
  }

  public function testServiceReadsRemoteCF(): void
  {
    $pratica = $this->setupPraticaScia();

    $guzzleMock = $this->getMockGuzzleClient([
      new Response(200, [], json_encode([
        $this->getCPSUserBaseData()['codiceFiscale'],
        $this->getCPSUserBaseData()['codiceFiscale'],
        $this->getCPSUserBaseData()['codiceFiscale'],
      ])),
    ]);

    $loggerMock = $this->getMockLogger();
    $loggerMock->expects($this->exactly(2))->method('info');

    $this->assertCount(0, $pratica->getRelatedCFs());

    $giscomApiAdapter = new GiscomAPIAdapterService(
      $guzzleMock,
      $this->em,
      $loggerMock,
      $this->getMockedGiscomMapper($this->em),
      $this->container->get('ocsdc.pratica_status_service'),
      $this->container->get('ocsdc.status_mapper.giscom')
    );

    $response = $giscomApiAdapter->askRelatedCFsforPraticaToGiscom($pratica);
    $this->assertEquals(200, $response->getStatusCode());
    $this->assertCount(3, json_decode($response->getBody()));

    $this->em->refresh($pratica);
    $this->assertCount(3, $pratica->getRelatedCFs());

  }

  private function getMockedGiscomMapper($em)
  {
    return new GiscomAPIMapperService($em);
  }
}
