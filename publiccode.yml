publiccodeYmlVersion: 0.4.0
applicationSuite: OpenCity Italia
name: OpenCity Italia - La Stanza del Cittadino
url: https://gitlab.com/opencity-labs/area-personale/core
landingURL: https://gitlab.com/opencity-labs/area-personale/core
releaseDate: 2025-03-10
softwareVersion: 3.14.0-rc.6
developmentStatus: stable
softwareType: standalone/web
platforms:
  - web
roadmap: https://link.opencontent.it/sdc-roadmap
categories:
  - communications
  - crm
  - digital-citizenship
dependsOn:
  open:
    - name: PHP
      versionMin: "7.4"
    - name: PostgreSQL
      versionMin: "10"
    - name: Apache
      versionMin: "2.4"
    - name: Shibboleth
      versionMin: "3"
    - name: Docker
      versionMin: "18.03"
      optional: true
logo: images/opencityitalia-sdc.png
maintenance:
  type: community
  contacts:
    - name: Gabriele Francescotto
      phone: +39 0461 917437
      affiliation: OpenCity Labs srl
      email: gabriele.francescotto@opencitylabs.it
legal:
  license: AGPL-3.0-or-later
  mainCopyrightOwner: OpenCity Labs srl
  repoOwner: OpenCity Labs srl
intendedAudience:
  scope:
    - local-authorities
  countries:
    - it
localisation:
  localisationReady: true
  availableLanguages:
    - it
    - de
it:
  piattaforme:
    spid: true
    pagopa: true
  conforme:
    lineeGuidaDesign: true
    gdpr: true
    misureMinimeSicurezza: true
    modelloInteroperabilita: true
description:
  it:
    apiDocumentation: https://link.opencontent.it/sdc-apidoc
    documentation: https://link.opencontent.it/sdc-manuale
    shortDescription: |
      Area personale, servizi digitali e prenotazione appuntamenti.
      Disponibile anche in SaaS (marketplace qualificato AgID)
    longDescription: >
      OpenCity Italia - La Stanza del Cittadino è il software open source che
      mette a disposizione di cittadini e imprese un’area personale e tutti i
      servizi comunali in forma digitale. Comprende l’area personale attraverso
      cui il cittadino può inviare pratiche (es. iscrizioni asilo nido,
      richiesta permesso ZTL, partecipazione a concorsi, ecc.), verificare lo
      stato di avanzamento, ricevere comunicazioni da parte dell’ente,
      effettuare pagamenti, ricevere bonus e agevolazioni, ottenere e conservare
      certificati e altri documenti.

      Tutti i servizi sono realizzati secondo il [modello](https://designers.italia.it/modello/comuni/) reso disponibile dal Designers Italia; hanno un’esperienza d’uso semplice, sono facili da attivare e possono essere personalizzati secondo le esigenze dell’ente.

      Il software, nato da un [progetto pilota in collaborazione con il Team Digitale di Presidenza del Consiglio e il Consorzio dei Comuni Trentini](https://www.forumpa.it/pa-digitale/servizi-digitali/design-dei-siti-web/), è attualmente utilizzato da centinaia di comuni italiani.

      E’ disponibile anche in [versione SaaS in cloud qualificata da AgID](https://catalogocloud.agid.gov.it/service/525).

      Può essere utilizzato in abbinamento a qualsiasi software per il sito web di un Comune, compreso il modulo [OpenCity Italia - Sito web](https://developers.italia.it/it/software/c_a116-comune-di-ala-comunweb) per i Comuni italiani realizzato secondo le linee guida di design, conforme al modello dati (ontologie e vocabolari controllati) definiti da AgID e anch’esso disponibile in versione SaaS in cloud qualificato da AgID.


      Il profilo del cittadino è progettato per gestire i dati personali nel rispetto del GDPR, al fine di ridurre la necessità di chiedere più volte agli utenti le stesse informazioni (principio once only) e di diminuire i tempi necessari per processare le pratiche.

      L’esperienza utente è realizzata seguendo le linee guida per il design, utilizzando il kit Bootstrap Italia, mentre il modello di architettura dei contenuti è conforme a quanto previsto dalle linee guida.

      **Nessun lock-in su architettura cloud e dati**

      OpenCiy Italia è integrata con la maggior parte dei sistemi di protocollo adottati dai Comuni italiani, con le piattaforme abilitanti PagoPA (attraverso intermediari tecnologici già presenti presso l'Ente), SPID/CIE/eIDAS e con l’App IO.

      Si integra facilmente con le altre applicazioni, attraverso OpenAPI e WebHook.


      OpenCity Italia ha un’architettura altamente scalabile, multi-tenant, composta da micro servizi cloud-native e disponibile in container (per evitare il cloud lock-in). L’interoperabilità è garantita da standard semantici nazioanli ed europei specifici del settore pubblico e da una piattaforma di API progettata secondo le linee guida per l’interoperabilità e utilizzando il catalogo nazionale della semantica dei dati (per evitare il data lock-in). La piattaforma segue un approccio a design pattern, in modo da offrire risposte tecnologicamente avanzate a ciascuna delle funzioni che abbiamo mappato, per poi utilizzarle in ciascuno dei servizi messi a disposizione degli enti pubblici locali.

      **Sicurezza**

      Rispetta i requisiti minimi di sicurezza previsti da AGID e tutti i requisiti previsti nel capitolato tecnico funzionale al rilascio di fondi PNRR per gli enti locali.
    features:
      - PNRR
      - PNRR/Misura/1.2
      - PNRR/Misura/1.4.1
      - PNRR/Beneficiari/Comuni
      - |-
        Tutti i servizi comunali in forma digitale, compresi quelli previsti
        dalle misure PNRR
      - |-
        Accesso ai servizi con SPID, CIE, eIDAS oppure senza autenticazione
        (es. prenotazione appuntamenti)
      - |-
        Accesso all’area personale con gestione e conservazione di messaggi,
        pratiche e documenti
      - |-
        Funzionalità di pagamento(calcolo importo, rateizzazione, notifiche
        pagamenti ricorrenti)
      - |-
        Gestione e pubblicazione di graduatorie per i servizi che lo prevedono
         e calcolo del punteggio
      - |-
        Funzionalità specifiche per il rilascio di benefici economici (bonus,
         agevolazioni, assegni)
      - |-
        Distinzione tra richiedente e beneficiario del servizio e processo di
         delega quando previsto
      - |-
        Gestione attestazione ISEE, dati relativi al veicolo e all’immobile
        del beneficiario del servizio
      - |-
        Gestione completa dell’iter di una pratica con visualizzazione dello
         stato
      - |-
        Riepilogo dei dati inseriti dall’utente con possibilità di modifica,
        conferma e invio della pratica
      - |-
        Ricevuta di invio della pratica automaticamente protocollata e inviata
         per e-mail
      - |-
        Controllo della validità di firme digitali e formati dei file, per la
        conservazione sostitutiva
      - |-
        Richiesta consenso al trattamento e gestione dei dati personali conforme
         al GDPR, per servizio
      - |-
        Ricezione dei messaggi relativi al servizio per email, nell’area
        personale e sull’app IO
      - Possibilità per l’utente di valutare l’esperienza d’uso del servizio
      - |-
        Segnalazione disservizi in città grazie all’integrazione con
        OpenSegnalazioni
      - Richiesta di assistenza grazie a integrazione con OpenSegnalazioni
      - |-
        Servizio di prenotazione di un appuntamento con funzionalità avanzate
         per cittadini e funzionari
      - |-
        Possibilità di scrivere al cittadino per chiedere la modifica o
        l’integrazione di una pratica
      - |-
        Funzioni di monitoraggio e analisi dati sull’andamento dei servizi
        e degli appuntamenti
      - |-
        Possibilità per l’ente di personalizzare i servizi esistenti e
        crearne di nuovi
      - |-
        Gestione servizi attraverso un catalogo dei servizi integrabile
        con il sito web comunale
      - |-
        Integrazione con i protocolli più diffusi (PiTre, Halley, Sicraweb,
        SIPAL, Civilia, Datagraph, Infor/Municipia, ...)
      - |-
        Semplicità d’uso anche da smartphone grazie alle interfacce
        Bootstrap Italia
      - |-
        Integrazione con PagoPA attraverso gateway di pagamento (es.
        MyPAY, IRIS, E-fil, ...)
      - |-
        Possibilità di raccogliere pratiche provenienti da diversi servizi
         in un unico fascicolo
    screenshots:
      - images/sdc012.png
      - images/sdc013.png
      - images/sdc014.png
      - images/sdc015.png
      - images/sdc016.png
      - images/sdc016.png
      - images/sdc017.png
      - images/sdc018.png
      - images/sdc019.png
      - images/sdc020.png
      - images/sdc021.png
      - images/sdc022.png
      - images/sdc023.png
      - images/sdc024.png
      - images/sdc025.png
      - images/sdc026.png
      - images/sdc027.png
      - images/sdc028.png
      - images/sdc029.png
usedBy:
  - Comune di Vicopisano
  - Comune di Vallelaghi
  - Comune di Avio
  - Comune di Riva del Garda
  - Comune di Merano
  - comune di Ala
  - Comune di Albiano
  - Comune di Aldeno
  - Comune di Altavalle
  - Comune di Altopiano della Vigolana
  - Comune di Amblar-Don
  - Comune di Andalo
  - Comune di Baselga di Piné
  - Comune di Bedollo
  - Comune di Besenello
  - Comune di Bieno
  - Comune di Bleggio Superiore
  - Comune di Bocenago
  - Comune di Bondone
  - Comune di Borgo Chiese
  - Comune di Borgo Danaunia
  - Comune di Borgo Lares
  - Comune di Borgo Valsugana
  - Comune di Brentonico
  - Comune di Bresimo
  - Comune di Caderzone Terme
  - Comune di Calceranica al Lago
  - Comune di Caldes
  - Comune di Caldonazzo
  - Comune di Calliano
  - Comune di Campitello di Fassa
  - Comune di Campodenno
  - Comune di Canal San Bovo
  - Comune di Canazei
  - Comune di Capriana
  - Comune di Carisolo
  - Comune di Carzano
  - Comune di Castel Condino
  - Comune di Castello Tesino
  - Comune di Castello Molina di Fiemme
  - Comune di Castelnuovo
  - Comune di Cavalese
  - Comune di Cavareno
  - Comune di Cavedago
  - Comune di Cavedine
  - Comune di Cavizzana
  - Comune di Cembra Lisignago
  - Comune di Cimone
  - Comune di Cinte Tesino
  - Comune di Cis
  - Comune di Cles
  - Comune di Comano Terme
  - Comune di Commezzadura
  - Comune di Contà
  - Comune di Croviana
  - Comune di Dambel
  - Comune di Denno
  - Comune di Dimaro Folgarida
  - Comune di Drena
  - Comune di Dro
  - Comune di Fai della Paganella
  - Comune di Fiave
  - Comune di Fierozzo
  - Comune di Folgaria
  - Comune di Fornace
  - Comune di Frassilongo
  - Comune di Garniga Terme
  - Comune di Giovo
  - Comune di Giustino
  - Comune di Grigno
  - Comune di Imèr
  - Comune di Isera
  - Comune di Lavarone
  - Comune di Lavis
  - Comune di Ledro
  - Comune di Livo
  - Comune di Lona-Lases
  - Comune di Luserna
  - Comune di Madruzzo
  - Comune di Malé
  - Comune di Massimeno
  - Comune di Mazzin
  - Comune di Mezzana
  - Comune di Mezzano
  - Comune di Mezzocorona
  - Comune di Mezzolombardo
  - Comune di Moena
  - Comune di Molveno
  - Comune di Mori
  - Comune di Nago-Torbole
  - Comune di Nogaredo
  - Comune di Nomi
  - Comune di Novaledo
  - Comune di Novella
  - Comune di Ospedaletto
  - Comune di Ossana
  - Comune di Palù del Fersina
  - Comune di Panchià
  - Comune di Peio
  - Comune di Pellizzano
  - Comune di Pelugo
  - Comune di Pergine
  - Comune di Pieve di Bono-Prezzo
  - Comune di Pieve Tesino
  - Comune di Pinzolo
  - Comune di Pomarolo
  - Comune di Porte di Rendena
  - Comune di Predaia
  - Comune di Primiero San Martino di Castrozza
  - Comune di Rabbi
  - Comune di Romeno
  - Comune di Roncegno Terme
  - Comune di Ronchi Valsugana
  - Comune di Ronzo-Chienis
  - Comune di Ronzone
  - Comune di Roverè della Luna
  - Comune di Ruffre Mendola
  - Comune di Rumo
  - Comune di Sagron Mis
  - Comune di Samone
  - Comune di San Giovanni di Fassa-Sèn Jan
  - Comune di San Lorenzo Dorsino
  - Comune di San Michele all'Adige
  - Comune di Sant'Orsola Terme
  - Comune di Sanzeno
  - Comune di Sarnonico
  - Comune di Scurelle
  - Comune di Segonzano
  - Comune di Sella Giudicarie
  - Comune di Sfruz
  - Comune di Soraga di Fassa
  - Comune di Sover
  - Comune di Spiazzo
  - Comune di Spormaggiore
  - Comune di Sporminore
  - Comune di Stenico
  - Comune di Storo
  - Comune di Strembo
  - Comune di Telve
  - Comune di Telve di Sopra
  - Comune di Tenna
  - Comune di Tenno
  - Comune di Terragnolo
  - Comune di terre dAdige
  - Comune di Terzolas
  - Comune di Tesero
  - Comune di Tione di Trento
  - Comune di Ton
  - Comune di Torcegno
  - Comune di Trambileno
  - Comune di Tre Ville
  - Comune di Trento
  - Comune di Valdaone
  - Comune di Valfloriana
  - Comune di Vallarsa
  - Comune di Vallelaghi
  - Comune di Vermiglio
  - Comune di Vignola-Falesina
  - Comune di Villa Lagarina
  - Comune di Ville d'Anaunia
  - Comune di Ville di Fiemme
  - Comune di Volano
  - Comune di Ziano di Fiemme
  - Comune di Maniago
  - Comune di Niscemi
  - Comune di Tavagnacco
  - Comune di Genova
  - Città di Monopoli
  - Comune di Seriate
  - Regione Autonoma Trentino Alto Adige
  - Comune di Bauladu
  - Comune di Bonarcado
  - Comune di Bonarcado
  - Comune di Milis
  - Comune di Santu Lussurgiu
  - Comune di Scano di Montiferro
  - Comune di Narbolia
  - Comune di Sennariolo
  - Comune di Tramatza
  - Comune di Zeddiani
  - Comune di Brescello
  - Comune di Mesagne
  - Abbanoa
  - Comune di Alba Adriatica
  - Comune di Roverbella
  - Comune di Sestu
  - Comune di Monselice
  - Comune di Pasturo
  - Comune di Aci Sant'Antonio
  - Comune di Montirone
  - Comune di Olginate
  - Comune di Bagnolo Mella
  - Comune di Trezzano sul Naviglio
  - Comune di Gottolengo
  - Comune di Marcallo con Casone
  - Azienda Provinciale per i Servizi Sociali Trento
  - Comune di Casargo
  - Comune di Nibionno
  - Comune di Cesana Brianza
  - Comune di Lodrino
  - Comune di Isorella
  - Comune di Sulzano
  - Comune di Canistro
  - Comune di Pezzaze
  - Comune di Dolzago
  - Comune di Colico
  - Comunita della Vallagarina
  - A.P.S.P. Beato De Tschiderer
  - Comune di Lecco
  - Comune di Suello
  - Comune di Erve
  - Comune di Annone di Brianza
  - Comune di Offlaga
  - Comune di Collebeato
  - Comune di Caino
  - Comune di Abbadia Lariana
  - Comune di Carenno
  - Comune di Monte Marenzo
  - Comune di Perledo
  - Comune di Vercurago
  - Comune di Viganò
  - Comune di Acquafredda
  - Comune di Alfianello
  - Comune di Collio
  - Comune di Gambara
  - Comune di Iseo
  - Comune di Lograto
  - Comune di Toscolano Maderno
  - Comune di Villachiara
  - Fondazione Edmund Mach
  - Comune di Cortina d'Ampezzo
  - Comune di Arco
  - Comune di Marcheno
  - Provincia di Barletta Andria Trani
  - Comune di Montespertoli
  - Comune di Calci
  - Comune di Apollosa
  - Provincia autonoma di Trento
  - Comune di Verbania
  - Comune di Firenze
  - Comune di Predazzo
  - Uffici Giudiziari del Trentino Alto Adige-Südtirol
  - Comune di Pineto
  - Azienda Servizi Sociali di Bolzano
  - Comune di Montecchio Precalcino
  - Città di Amalfi
  - Comune di Civezzano
  - Comune di Castel Ivano
  - Comune di Levico Terme
  - Comune di San Mango d'Aquino
  - Comune di Conflenti
  - Comune di Mineo
  - Comune di Prizzi
  - Comune di Rovereto
  - Comune di Prizzi
  - Comune di Maddaloni
  - Comune di Trani
  - Città di Asolo
  - Comune di Baradili
  - Comune di Verona
  - Comune di Tripi
  - Comune di San Michele di Ganzaria
  - Comune di Villafrati
  - Comune di Sannazzaro de' Burgondi
  - Comune di Vicenza
  - Città di San Cataldo
  - Upipa
  - Comune di Tremestieri Etneo
  - Comune di Villa Verde
  - Comune di Isca sullo Ionio
  - Iprase
  - Comune di Isca sullo Ionio
  - Comune di Specchia
  - Comune di Casamassima
  - Comune di Pula
  - Comune di Bressanone
  - Comune di Santa Domenica Talao
  - Comune di Torrecuso
  - Comune di Carpignano Salentino
  - Comune di La Valletta Brianza
  - Comune di Cellatica
  - Comune di Cortenova
  - Comune di Polaveno
  - Comune di Taceno
  - Comune di Irma
  - Comune di Rometta
  - Comune di Tollo
  - Comune di Tarquinia
  - Comune di Montesilvano
  - Comune di Primaluna
  - Comune di Brione
  - Comune di Ome
  - Comune di Laterza
  - Comune di Pontassieve
  - Comune di Follonica
  - Comune di Pinarolo Po
  - Comune di Chiusavecchia
  - Comune di Rocca Canavese
  - Comune di Pombia
  - Comune di Confienza
  - Comune di Pralormo
  - Comune di Travacò Siccomario
  - Comune di Mezzana Rabattone
  - Comune di Monticelli Pavese
  - Comune di Morano sul Po
  - Comune di Crevacuore
  - Comune di Zerbolò
  - Comune di Bogogno
  - Comune di Goito
  - Comune di Calascibetta
  - Ospedali riuniti Palermo
  - Comune di Montalbano Elicona
  - Comune di Pisa
  - Comune di Masullas
  - Comune di Torre de' Negri
  - Comune di Sersale
  - Comune di Policoro
  - Comune di Ruvo di Puglia
  - Comune di San Casciano in Val di Pesa
  - Comune di Alia
  - Comune di Ladispoli
  - Comune di San Giovanni la Punta
  - Comune di Santa Domenica Vittoria
  - Comune di Pompu
  - Comune di Fano
  - Comune di Genazzano
  - Comune di Fiumedinisi
  - Comune di Escolca
  - Comune di Patti
  - Comune di Verdellino
  - Comune di Pietrafitta
  - Comune di San Fili
  - Comune di Montalenghe
  - Comune di Desenzano
  - Comune di Empoli
  - Consiglio Regionale della Calabria
  - Comune di Madonna del Sasso
  - Comune di Vallermosa
  - Comune di Gonnoscodina
  - Comune di Rivoli
  - Comune di Sale
  - Comune di Molino dei Torti
  - Comune di Cianciana
  - Comune di Pietraperzia
  - Comune di Bordighera
  - Comune di Limbadi
  - Comune di Portopalo di Capo Passero
  - Comune di Praia a Mare
  - Regione Puglia
  - Comune di Cellara
  - Comune di Isola Dovarese
  - Comune di Montemarano
  - Comune di Paterno Calabro
  - Comune di Pessina Cremonese
  - Comune di San Vito di Leguzzano
  - Comune di Torre de' Picenardi
  - Comune di Valli del Pasubio
  - Comune di Velo d'Astico
  - Comune di Villaverla
  - Comune di Posina
  - APSP Santa Maria Cles
  - Comune di San Nicola Arcella
  - Comune di Belsito
  - Comune di Mangone
  - Comune di Grimaldi
  - Comune di Marzi
  - Comune di Rocca d'Evandro
  - Comune di San Prisco
  - Comune di Novalesa
  - Comune di Corsano
  - Comune di Crispiano
  - Fondazione Comunità di Arco
  - Comune di Canicattini Bagni
  - Comune di Villar Focchiardo
  - Comune di Chianocco
  - Comune di San Giorgio Ionico
  - Comune di Tora e Piccilli
  - Comune di Monte Porzio Catone
  - Comune di Viagrande
  - Comune di Agrigento
  - Comune di San Vincenzo
  - Comune di Cabiate
  - Comune di Udine
  - Comune di San Michele Salentino
  - Comune di Dovera
  - Comune di Cerami
  - Comune di Santa Croce Camerina
  - Comune di Travagliato
  - Comune di Sant'Andrea del Garigliano
  - Comune di Samarate
  - Comune di Monti
  - Comune di Martina Franca
  - Comune di Martano
  - Comune di Lierna
  - Comune di Golfo Aranci
  - Comune di Delia
  - Comune di Agliana
  - Comune di Pianezza
  - Comune di Sciolze
  - Comune di Cesana Torinese
  - Comune di Petrosino
  - Comune di Livorno Ferraris
  - Comune di Ginosa
  - Comune di Santa Margherita di Belice
  - Comune di Susa
  - Comune di Paceco
  - Comune di Rosignano Marittimo
  - Comune di Mojo Alcantara
  - Comune di Olgiate Molgora
  - Comune di Fermo
  - Comune di Calolziocorte
  - Comune di Claviere
  - Comune di Polignano a Mare
  - Comune di Pragelato
  - Comune di Sauze d'Oulx
  - Comune di Sauze di Cesana
  - Comune di Sestriere
  - Comune di Usseaux
  - Villa Monastero
  - Comune di Padria
  - Azienda Sanitaria Provinciale di Siracusa
  - Comune di Martirano Lombardo
  - Comune di Meta
  - Comune di Santa Cristina Daspromonte
  - Comune di Sassetta
  - Comune di Manerbio
  - Comune di Orzinuovi
  - Comune di Andezeno
  - Comune di Zungri
  - Comune di Melicucca
  - Comune di Casalvolone
  - Comune di Castellazzo Bormida
  - Comune di Arena Po
  - Comune di Brusson
  - Comune di Bussoleno
  - Comune di Carcoforo
  - Comune di Momo
  - Comune di Challand Saint Anselme
  - Comune di Romagnano Sesia
  - Comune di Vauda Canavese
  - Comune di Cavallino
  - Comune di Orzivecchi
  - Comune di Pralboino
  - Comune di Tignale
  - Comune di Casale Corte Cerro
  - Comune di Avigliana
  - Comune di Gurro
  - Comune di Pogno
  - Comune di Trapani
  - Comune di Varallo Pombia
  - Comune di Vicolungo
  - Comune di Vinzaglio
  - Comune di Assoro
  - Comune di Castelnuovo Bormida
  - Comune di Catenanuova
  - Comune di Invorio
  - Comune di Licata
  - Comune di Nissoria
  - Comune di Venaus
  - Comune di Bolzano
  - Comune di Gattico Veruno
  - Comune di Challand Saint Victor
  - Comune di Piscina
  - Comune di San Giacomo Vercellese
  - Comune di Solarino
  - Comune di Alezio
  - Comune di Cesiomaggiore
  - Comune di Bressana Bottarone
  - Comune di Pantelleria
  - Comune di Leonforte
  - Comune di Grisolia
  - Comune di Bellinzago
  - Comune di Bustogarolfo
  - Comune di Lettere
  - Comune di Cressa
  - Comune di Villadossola
  - Città Metropolitana di Genova
