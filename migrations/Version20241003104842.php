<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241003104842 extends AbstractMigration
{
  public function getDescription(): string
  {
    return '';
  }

  public function up(Schema $schema): void
  {
    // this up() migration is auto-generated, please modify it to your needs
    $this->addSql('UPDATE scheduled_action SET retry = 0 WHERE retry IS NULL ');
    $this->addSql('ALTER TABLE scheduled_action ADD execute_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
    $this->addSql('ALTER TABLE scheduled_action ALTER status SET NOT NULL');
    $this->addSql('ALTER TABLE scheduled_action ALTER retry SET NOT NULL');
  }

  public function down(Schema $schema): void
  {
    // this down() migration is auto-generated, please modify it to your needs
    $this->addSql('ALTER TABLE scheduled_action DROP execute_at');
    $this->addSql('ALTER TABLE scheduled_action ALTER status DROP NOT NULL');
    $this->addSql('ALTER TABLE scheduled_action ALTER retry DROP NOT NULL');
  }
}
