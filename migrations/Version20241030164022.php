<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241030164022 extends AbstractMigration
{
  public function getDescription(): string
  {
    return 'Add beneficiary to pratica';
  }

  public function up(Schema $schema): void
  {
    $this->addSql('ALTER TABLE pratica ADD beneficiary_id UUID DEFAULT NULL');
    $this->addSql('ALTER TABLE pratica ADD CONSTRAINT FK_448253ACECCAAFA0 FOREIGN KEY (beneficiary_id) REFERENCES utente (id) NOT DEFERRABLE INITIALLY IMMEDIATE',);
    $this->addSql('CREATE INDEX IDX_448253ACECCAAFA0 ON pratica (beneficiary_id)');
  }

  public function down(Schema $schema): void
  {
    $this->addSql('ALTER TABLE pratica DROP CONSTRAINT FK_448253ACECCAAFA0');
    $this->addSql('DROP INDEX IDX_448253ACECCAAFA0');
    $this->addSql('ALTER TABLE pratica DROP beneficiary_id');
  }
}
