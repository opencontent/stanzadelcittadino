<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240406083352 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
      // this up() migration is auto-generated, please modify it to your needs
      $this->addSql('ALTER TABLE ente ADD custom_template JSONB DEFAULT NULL');
      $this->addSql('ALTER TABLE servizio ADD custom_template JSONB DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
      // this down() migration is auto-generated, please modify it to your needs
      $this->addSql('ALTER TABLE ente DROP custom_template');
      $this->addSql('ALTER TABLE servizio DROP custom_template');
    }
}
