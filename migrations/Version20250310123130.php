<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250310123130 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE termini_utilizzo');
        $this->addSql('ALTER TABLE utente ADD terms_and_privacy_read_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE utente DROP accepted_terms');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE termini_utilizzo (id UUID NOT NULL, name VARCHAR(100) NOT NULL, text TEXT NOT NULL, mandatory BOOLEAN NOT NULL, latest_revision INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE utente ADD accepted_terms TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE utente DROP terms_and_privacy_read_at');
    }
}
