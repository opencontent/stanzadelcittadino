<?php

namespace App\AdministrativeIdentifier\Domain\Repository;

use App\Entity\AdministrativeIdentifier;
use App\Entity\CPSUser;
use Ramsey\Uuid\UuidInterface;

interface AdministrativeIdentifierRepositoryInterface
{
  public function save(AdministrativeIdentifier $administrativeIdentifier, bool $flush): void;
  public function remove(AdministrativeIdentifier $entity, bool $flush = true): void;
  public function findByIdentifiesTypeAndIdentifier(UuidInterface $identifies, string $type, string $identifier): ?AdministrativeIdentifier;
  public function findByIdentifiesTypeAndIdentifierExcludingId(UuidInterface $identifies, string $type, string $identifier, UuidInterface $excludeId): ?AdministrativeIdentifier;
  public function findById(UuidInterface $id): AdministrativeIdentifier;
  public function getList($parameters = [], $onlyCount = false, $order = 'createdAt', $sort = 'ASC', $offset = 0, $limit = 10);
}
