<?php

namespace App\AdministrativeIdentifier\Application\Command;

use App\AdministrativeIdentifier\Application\Dto\UpdateAdministrativeIdentifierDto;

class UpdateAdministrativeIdentifier
{
  private UpdateAdministrativeIdentifierDto $dto;

  public function __construct(
    UpdateAdministrativeIdentifierDto $dto
  ) {
    $this->dto = $dto;
  }

  public function getDto(): UpdateAdministrativeIdentifierDto
  {
    return $this->dto;
  }
}
