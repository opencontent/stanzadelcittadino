<?php

namespace App\Protocollo\Exception;

class AlreadySentException extends BaseException
{
    protected $message = 'Item has already been sent to protocol systems';

}
