<?php


namespace App\Model;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

class Gateway implements \JsonSerializable
{
  /**
   * @var string
   * @Serializer\Type("string")
   * @Assert\NotBlank(message="This field is mandatory: identifier")
   * @Assert\NotNull(message="This field is mandatory: identifier")
   * @OA\Property(description="Human-readable unique identifiers")
   * @Groups({"read", "write"})
   */
  private $identifier;

  /**
   * @Serializer\Type("array<string, string>")
   * @OA\Property(description="Specific parameters for gateways")
   * @Groups({"write"})
   *
   */
  private $parameters = [];

  /**
   * @return string
   */
  public function getIdentifier()
  {
    return $this->identifier;
  }

  /**
   * @param string $identifier
   */
  public function setIdentifier(string $identifier)
  {
    $this->identifier = $identifier;
  }

  /**
   * @return array
   */
  public function getParameters()
  {
    return $this->parameters;
  }

  /**
   * @param array
   */
  public function setParameters($parameters)
  {
    if (!is_array($parameters)) {
      $parameters = json_decode($parameters, true);
    }
    $this->parameters = $parameters;
  }


  public function jsonSerialize()
  {
    return get_object_vars($this);
  }

  public static function fromArray(array $data = []): self
  {
    $gateway = new self();
    $gateway->setIdentifier($data['identifier']);
    $gateway->setParameters($data['parameters']);

    return $gateway;
  }

}
