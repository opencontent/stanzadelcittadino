<?php


namespace App\Model\Calendar;


use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

class ReservationLimits implements \JsonSerializable
{

  /**
   * @OA\Property(description="Max number of meetings bookable per interval")
   * @Groups({"read", "kafka", "write"})
   */
  private int $maxMeetingsPerInterval = 0;

  /**
   * @OA\Property(description="Time intrval for max bookable meetings. Accepted values are: <code></code>, <code>daily</code>, <code>weekly</code>, <code>monthly</code>, <code>yearly</code>")
   * @Groups({"read", "kafka", "write"})
   */
  private ?string $interval = null;

  public function getMaxMeetingsPerInterval(): int
  {
    return $this->maxMeetingsPerInterval;
  }

  public function setMaxMeetingsPerInterval(int $maxMeetingsPerInterval): void
  {
    $this->maxMeetingsPerInterval = $maxMeetingsPerInterval;
  }

  public function getInterval(): ?string
  {
    return $this->interval;
  }

  public function setInterval(?string $interval): void
  {
    $this->interval = $interval;
  }


  /**
   * @return array
   */
  public function jsonSerialize(): ?array
  {

    if (empty($this->maxMeetingsPerInterval) && empty($this->interval)) {
      return null;
    }

    return [
      'max_meetings_per_interval' => $this->maxMeetingsPerInterval,
      'interval' => $this->interval,
    ];
  }


  public static function fromArray($data = []): ?ReservationLimits
  {
    if (empty($data['max_meetings_per_interval']) && empty($data['interval'])) {
      return null;
    }

    $reservationLimits = new self();
    $reservationLimits->setMaxMeetingsPerInterval($data['max_meetings_per_interval'] ?? 0);
    $reservationLimits->setInterval($data['interval'] ?? '');

    return $reservationLimits;
  }

}
