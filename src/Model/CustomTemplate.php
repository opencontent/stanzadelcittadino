<?php


namespace App\Model;


class CustomTemplate implements \JsonSerializable
{

  private ?string $header = null;

  private ?string $footer = null;

  public function getHeader(): ?string
  {
    return $this->header;
  }

  public function setHeader(?string $header): void
  {
    $this->header = $header;
  }

  public function getFooter(): ?string
  {
    return $this->footer;
  }

  public function setFooter(?string $footer): void
  {
    $this->footer = $footer;
  }


  /**
   * @return array
   */
  public function jsonSerialize()
  {
    return [
      'header' => $this->header,
      'footer' => $this->footer
    ];
  }


  public static function fromArray($data = []): CustomTemplate
  {
    $customTemplate = new self();
    $customTemplate->setHeader($data['header'] ?? null);
    $customTemplate->setFooter($data['footer'] ?? null);

    return $customTemplate;
  }

}
