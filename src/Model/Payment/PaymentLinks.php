<?php

namespace App\Model\Payment;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class PaymentLinks
{
  /**
   * @OA\Property(description="Online payment begin url", type="object", ref=@Model(type=PaymentLink::class, groups={"read", "kafka"}))
   * @Groups({"read", "write", "kafka"})
   */
  private ?PaymentLink $onlinePaymentBegin = null;

  /**
   * @OA\Property(description="Online payment landing url ", type="object", ref=@Model(type=PaymentLink::class, groups={"read", "kafka"}))
   * @Groups({"read", "write", "kafka"})
   */
  private ?PaymentLink $onlinePaymentLanding = null;

  /**
   * @OA\Property(description="Offline payment url", type="object", ref=@Model(type=PaymentLink::class, groups={"read", "kafka"}))
   * @Groups({"read", "write", "kafka"})
   */
  private ?PaymentLink $offlinePayment = null;

  /**
   * @OA\Property(description="Receipt url", type="object", ref=@Model(type=PaymentLink::class, groups={"read", "kafka"}))
   * @Groups({"read", "write", "kafka"})
   */
  private ?PaymentLink $receipt = null;

  /**
   * @OA\Property(description="Notify url", type="array", @OA\Items(ref=@Model(type=PaymentLink::class, groups={"read", "kafka"})))
   * @Groups({"read", "write", "kafka"})
   */
  private array $notify = [];

  /**
   * @OA\Property(description="Update url", type="object", ref=@Model(type=PaymentLink::class, groups={"read", "kafka"}))
   * @Groups({"read", "write", "kafka"})
   */
  private ?PaymentLink $update = null;

  /**
   * @Serializer\Since("2")
   * @OA\Property(description="Update url", type="object", ref=@Model(type=PaymentLink::class, groups={"read", "kafka"}))
   * @Groups({"write"})
   */
  private ?PaymentLink $confirm = null;

  /**
   * @Serializer\Since("2")
   * @OA\Property(description="Update url", type="object", ref=@Model(type=PaymentLink::class, groups={"read", "kafka"}))
   * @Groups({"write"})
   */
  private ?PaymentLink $cancel = null;

  /**
   * @return mixed
   */
  public function getOnlinePaymentBegin()
  {
    return $this->onlinePaymentBegin;
  }

  /**
   * @param mixed $onlinePaymentBegin
   */
  public function setOnlinePaymentBegin($onlinePaymentBegin): void
  {
    $this->onlinePaymentBegin = $onlinePaymentBegin;
  }

  /**
   * @return mixed
   */
  public function getOnlinePaymentLanding()
  {
    return $this->onlinePaymentLanding;
  }

  /**
   * @param mixed $onlinePaymentLanding
   */
  public function setOnlinePaymentLanding($onlinePaymentLanding): void
  {
    $this->onlinePaymentLanding = $onlinePaymentLanding;
  }

  /**
   * @return null
   */
  public function getOfflinePayment()
  {
    return $this->offlinePayment;
  }

  /**
   * @param null $offlinePayment
   */
  public function setOfflinePayment($offlinePayment): void
  {
    $this->offlinePayment = $offlinePayment;
  }

  /**
   * @return null
   */
  public function getReceipt()
  {
    return $this->receipt;
  }

  /**
   * @param null $receipt
   */
  public function setReceipt($receipt): void
  {
    $this->receipt = $receipt;
  }

  /**
   * @return null
   */
  public function getNotify()
  {
    return $this->notify;
  }

  /**
   * @param null $notify
   */
  public function setNotify($notify): void
  {
    $this->notify = $notify;
  }

  /**
   * @return null
   */
  public function getUpdate()
  {
    return $this->update;
  }

  /**
   * @param null $update
   */
  public function setUpdate($update): void
  {
    $this->update = $update;
  }

  public function getConfirm(): ?PaymentLink
  {
    return $this->confirm;
  }

  public function setConfirm(?PaymentLink $confirm): void
  {
    $this->confirm = $confirm;
  }

  public function getCancel(): ?PaymentLink
  {
    return $this->cancel;
  }

  public function setCancel(?PaymentLink $cancel): void
  {
    $this->cancel = $cancel;
  }

}
