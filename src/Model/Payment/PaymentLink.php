<?php

namespace App\Model\Payment;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

class PaymentLink
{
  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Online payment begin")
   * @Groups({"read", "write", "kafka"})
   */
  private $url;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Online payment begin")
   * @Groups({"read", "write", "kafka"})
   */
  private $lastOpenedAt;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Online payment begin")
   * @Groups({"read", "write", "kafka"})
   */
  private $method;

  /**
   * @return mixed
   */
  public function getUrl()
  {
    return $this->url;
  }

  /**
   * @param mixed $url
   */
  public function setUrl($url): void
  {
    $this->url = $url;
  }

  /**
   * @return mixed
   */
  public function getLastOpenedAt()
  {
    return $this->lastOpenedAt;
  }

  /**
   * @param mixed $lastOpenedAt
   */
  public function setLastOpenedAt($lastOpenedAt): void
  {
    $this->lastOpenedAt = $lastOpenedAt;
  }

  /**
   * @return mixed
   */
  public function getMethod()
  {
    return $this->method;
  }

  /**
   * @param mixed $method
   */
  public function setMethod($method): void
  {
    $this->method = $method;
  }

  public static function init($url = null, $method = 'GET', $lastOpenedAt = null): self
  {
    $paymentLink = new self();
    $paymentLink->setUrl($url);
    $paymentLink->setMethod($method);
    $paymentLink->setLastOpenedAt($lastOpenedAt);
    return $paymentLink;
  }

}
