<?php

namespace App\Model;

use App\Model\Payment\PaymentLinks;
use App\Model\Payment\PaymentTransaction;
use App\Model\Payment\Payer;
use App\Model\Payment\RemoteCollection;
use \DateTime;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Payment
{

  public const STATUS_CREATION_PENDING = 'CREATION_PENDING';
  public const STATUS_CREATION_FAILED = 'CREATION_FAILED';
  public const STATUS_PAYMENT_PENDING = 'PAYMENT_PENDING';
  public const STATUS_PAYMENT_STARTED = 'PAYMENT_STARTED';
  public const STATUS_PAYMENT_CONFIRMED = 'PAYMENT_CONFIRMED';
  public const STATUS_PAYMENT_FAILED = 'PAYMENT_FAILED';
  public const STATUS_NOTIFICATION_PENDING = 'NOTIFICATION_PENDING';
  public const STATUS_COMPLETE = 'COMPLETE';
  public const STATUS_EXPIRED = 'EXPIRED';
  public const STATUS_CANCELED = 'CANCELED';

  public const PAYMENT_STATUSES = [
    self::STATUS_CREATION_PENDING,
    self::STATUS_CREATION_FAILED,
    self::STATUS_PAYMENT_PENDING,
    self::STATUS_PAYMENT_STARTED,
    self::STATUS_PAYMENT_CONFIRMED,
    self::STATUS_PAYMENT_FAILED,
    self::STATUS_NOTIFICATION_PENDING,
    self::STATUS_COMPLETE,
    self::STATUS_EXPIRED,
    self::STATUS_CANCELED,
  ];

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment uuid")
   * @Groups({"read", "kafka"})
   */
  private $id;

  /**
   * @Serializer\Type("string")
   * @Serializer\Since("2")
   * @OA\Property(description="Payment config id")
   * @Groups({"read", "write", "kafka"})
   */
  private $configId;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment user id")
   * @Groups({"read", "write", "kafka"})
   */
  private $userId;

  /**
   * @Serializer\Type("string")
   * @Serializer\Until("1")
   * @OA\Property(description="Payment user id")
   * @Groups({"read", "write", "kafka"})
   */
  private string $type = 'PAGOPA';

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment user id")
   * @Groups({"read", "write", "kafka"})
   */
  private $tenantId;

  /**
   * @Serializer\Type("string")
   * @Serializer\Until("1")
   * @OA\Property(description="Payment user id")
   * @Groups({"read", "write", "kafka"})
   */
  private $serviceId;

  /**
   * @var RemoteCollection
   * @Serializer\Since("2")
   * @OA\Property(type="object", ref=@Model(type=RemoteCollection::class, groups={"read"}))
   * @Groups({"write"})
   */
  private $remoteCollection;

  /**
   * @Serializer\Type("string")
   * @Serializer\Until("1")
   * @OA\Property(description="Payment status")
   * @Groups({"read", "write", "kafka"})
   */
  private $status;

  /**
   * @Serializer\Type("string")
   * @Serializer\Until("1")
   * @OA\Property(description="Payment reason")
   * @Groups({"read", "write", "kafka"})
   */
  private $reason;

  /**
   * @Serializer\Type("string")
   * @Serializer\Until("1")
   * @OA\Property(description="Payment read id")
   * @Groups({"read", "write", "kafka"})
   */
  private $remoteId;

  /**
   * @var PaymentTransaction
   * @OA\Property(type="object", ref=@Model(type=PaymentTransaction::class, groups={"read"}))
   * @Groups({"read", "write", "kafka"})
   */
  private $payment;

  /**
   * @var PaymentLinks
   * @OA\Property(type="object", ref=@Model(type=PaymentLinks::class, groups={"kafka"}))
   * @Groups({"read", "write", "kafka"})
   */
  private $links;

  /**
   * @var Payer
   * @OA\Property(type="object", ref=@Model(type=Payer::class, groups={"kafka"}))
   * @Serializer\Until("1")
   * @Groups({"read", "write", "kafka"})
   */
  private $payer;

  /**
   * @var Payer
   * @OA\Property(type="object", ref=@Model(type=Payer::class, groups={"kafka"}))
   * @Serializer\Since("2")
   * @Groups({"write"})
   */
  private $debtor;

  /**
   * @Serializer\Type("bool")
   * @Serializer\Since("2")
   * @OA\Property(description="Payment is external?")
   * @Groups({"write"})
   */
  private bool $isExternal = false;

  /**
   * @var DateTime
   * @Serializer\Type("DateTime")
   * @OA\Property(description="Created at date time")
   * @Groups({"read", "write", "kafka"})
   */
  private DateTime $createdAt;

  /**
   * @var DateTime
   * @Serializer\Type("DateTime")
   * @Serializer\Until("1")
   * @OA\Property(description="Created at date time", format="date-time")
   * @Groups({"read", "kafka"})
   */
  private DateTime $updatedAt;


  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param mixed $id
   */
  public function setId($id): void
  {
    $this->id = $id;
  }

  /**
   * @return mixed
   */
  public function getConfigId()
  {
    return $this->configId;
  }

  /**
   * @param mixed $configId
   */
  public function setConfigId($configId): void
  {
    $this->configId = $configId;
  }

  /**
   * @return mixed
   */
  public function getUserId()
  {
    return $this->userId;
  }

  /**
   * @param mixed $userId
   */
  public function setUserId($userId): void
  {
    $this->userId = $userId;
  }

  /**
   * @return mixed
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * @param mixed $type
   */
  public function setType($type): void
  {
    $this->type = $type;
  }

  /**
   * @return mixed
   */
  public function getTenantId()
  {
    return $this->tenantId;
  }

  /**
   * @param mixed $tenantId
   */
  public function setTenantId($tenantId): void
  {
    $this->tenantId = $tenantId;
  }

  /**
   * @return mixed
   */
  public function getServiceId()
  {
    return $this->serviceId;
  }

  /**
   * @param mixed $serviceId
   */
  public function setServiceId($serviceId): void
  {
    $this->serviceId = $serviceId;
  }

  public function getRemoteCollection(): RemoteCollection
  {
    return $this->remoteCollection;
  }

  public function setRemoteCollection(RemoteCollection $remoteCollection): void
  {
    $this->remoteCollection = $remoteCollection;
  }

  /**
   * @return mixed
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * @param mixed $status
   */
  public function setStatus($status): void
  {
    $this->status = $status;
  }

  /**
   * @return mixed
   */
  public function getReason()
  {
    return $this->reason;
  }

  /**
   * @param mixed $reason
   */
  public function setReason($reason): void
  {
    $this->reason = $reason;
  }

  /**
   * @return mixed
   */
  public function getRemoteId()
  {
    return $this->remoteId;
  }

  /**
   * @param mixed $remoteId
   */
  public function setRemoteId($remoteId): void
  {
    $this->remoteId = $remoteId;
  }

  /**
   * @return PaymentTransaction
   */
  public function getPayment(): PaymentTransaction
  {
    return $this->payment;
  }

  /**
   * @param PaymentTransaction $payment
   */
  public function setPayment(PaymentTransaction $payment): void
  {
    $this->payment = $payment;
  }

  /**
   * @return PaymentLinks
   */
  public function getLinks(): PaymentLinks
  {
    return $this->links;
  }

  /**
   * @param PaymentLinks $links
   */
  public function setLinks(PaymentLinks $links): void
  {
    $this->links = $links;
  }

  /**
   * @return Payer
   */
  public function getPayer(): Payer
  {
    return $this->payer;
  }

  /**
   * @param Payer $payer
   */
  public function setPayer(Payer $payer): void
  {
    $this->payer = $payer;
  }

  public function getDebtor(): Payer
  {
    return $this->debtor;
  }

  public function setDebtor(Payer $debtor): void
  {
    $this->debtor = $debtor;
  }

  public function isExternal(): bool
  {
    return $this->isExternal;
  }

  public function setIsExternal(bool $isExternal): void
  {
    $this->isExternal = $isExternal;
  }

  /**
   * @return DateTime
   */
  public function getCreatedAt(): DateTime
  {
    return $this->createdAt;
  }

  /**
   * @param DateTime $createdAt
   */
  public function setCreatedAt(DateTime $createdAt): void
  {
    $this->createdAt = $createdAt;
  }

  /**
   * @return DateTime
   */
  public function getUpdatedAt(): DateTime
  {
    return $this->updatedAt;
  }

  /**
   * @param DateTime $updatedAt
   */
  public function setUpdatedAt(DateTime $updatedAt): void
  {
    $this->updatedAt = $updatedAt;
  }

}
