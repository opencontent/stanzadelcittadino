<?php

namespace App\Services\Manager;

use App\Entity\Servizio;
use App\FormIO\Schema;
use App\Services\InstanceService;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpOptions;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class PdndManager
{

  private InstanceService $instanceService;
  private HttpClientInterface $httpClient;
  private string $pdndApiUrl;

  private string $tenantId;
  private JWTTokenManagerInterface $JWTTokenManager;
  private TokenStorageInterface $tokenStorage;

  private string $JWTToken = '';
  private LoggerInterface $logger;


  public function __construct(InstanceService $instanceService, HttpClientInterface $httpClient, JWTTokenManagerInterface $JWTTokenManager, TokenStorageInterface $tokenStorage, LoggerInterface $logger, string $pdndApiUrl)
  {
    $this->instanceService = $instanceService;
    $this->httpClient = $httpClient;
    $this->pdndApiUrl = $pdndApiUrl;

    $this->tenantId = $this->instanceService->getCurrentInstance() ?? $this->instanceService->getCurrentInstance()->getId()->toString();
    $this->JWTTokenManager = $JWTTokenManager;
    $this->tokenStorage = $tokenStorage;

    $currentUser = $this->getUser();
    if ($currentUser) {
      $this->JWTToken = $JWTTokenManager->create($currentUser);
    }

    $this->logger = $logger;
  }


  /**
   * @throws TransportExceptionInterface
   * @throws ServerExceptionInterface
   * @throws RedirectionExceptionInterface
   * @throws ClientExceptionInterface
   */
  public function getTenant(): string
  {
    $options = (new HttpOptions())->setAuthBearer($this->JWTToken);
    $url = $this->pdndApiUrl . '/tenants/' . $this->instanceService->getCurrentInstance()->getId();
    return $this->httpClient->request('GET', $url, $options->toArray())->getContent();
  }

  /**
   * @throws TransportExceptionInterface
   */
  public function postTenant(): void
  {
    $url = $this->pdndApiUrl . '/tenants';
    $options = (new HttpOptions())
      ->setAuthBearer($this->JWTToken)
      ->setJson([
        'id' => $this->instanceService->getCurrentInstance()->getId(),
        'name' => $this->instanceService->getCurrentInstance()->getName(),
        'ipa_code' => $this->instanceService->getCurrentInstance()->getIpaCode(),
      ]);
    $this->httpClient->request('POST', $url, $options->toArray());

  }

  /**
   * @throws TransportExceptionInterface
   * @throws ServerExceptionInterface
   * @throws RedirectionExceptionInterface
   * @throws ClientExceptionInterface
   */
  public function getClients(): string
  {
    $url = $this->pdndApiUrl . '/tenants/' . $this->tenantId . '/clients';
    return $this->httpClient->request('GET', $url)->getContent();
  }

  public function getTenantConfigs(string $clientId = null): array
  {
    $tenant = $this->instanceService->getCurrentInstance();

    if (empty($clientId)) {
      $clientId = $tenant->getDefaultPdndClientId();
    }

    $url = $this->pdndApiUrl . '/tenants/' . $tenant->getId() .'/configs';
    $configsByEservices = [];
    $configs = $tenant->getPdndConfigIds();

    $options = (new HttpOptions())->setAuthBearer($this->JWTToken);
    $response = $this->httpClient->request('GET', $url, $options->toArray());
    $content = json_decode($response->getContent(false), true);
    // Verifico che le configurazioni ottenute siano relative a questo client e siano salvate nelle configurazioni del tenant (potrebbero esserci quelle dei servizi)
    // Todo: aggiugnere un filtro sul client per le api dei configs
    if ( isset($content['meta']) && $content['meta']['total'] > 0 ) {
      foreach ($content['data'] as $v) {
        if ($v['client_id'] === $clientId && in_array($v['id'], $configs)) {
          $configsByEservices[$v['eservice_id']] = $v;
        }
      }
    }
    return $configsByEservices;

  }

  /**
   * @throws TransportExceptionInterface
   * @throws ServerExceptionInterface
   * @throws RedirectionExceptionInterface
   * @throws ClientExceptionInterface
   */
  public function getServiceConfigs(Servizio $servizio): array
  {
    $tenant = $this->instanceService->getCurrentInstance();
    $clientId = $tenant->getDefaultPdndClientId();

    $url = $this->pdndApiUrl . '/tenants/' . $tenant->getId() .'/configs';
    $configs = [];
    $tempServiceConfigs = [];
    $tenantConfigs = $tenant->getPdndConfigIds();
    $serviceConfigs = $servizio->getPdndConfigIds();

    $options = (new HttpOptions())->setAuthBearer($this->JWTToken);
    try {
      $response = $this->httpClient->request('GET', $url, $options->toArray());
      $content = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
    } catch (\Exception $e) {
      $this->logger->error('Pdnd: error in get service configs request', [
        'exception' => $e->getMessage(),
        'service_id' => $servizio->getId()
      ]);
    }

    // Scorro l'intera lista di configurazioni associata al tenant
    if ( isset($content['meta']) && $content['meta']['total'] > 0 ) {
      foreach ($content['data'] as $v) {
        // Separo le configurazioni del tenant associate al client di default con quelle del servizio
        if ($v['client_id'] === $clientId && in_array($v['id'], $tenantConfigs)) {
          $v['related'] = 'tenant';
          $configs[$v['eservice_id']] = $v;
        }

        if (in_array($v['id'], $serviceConfigs)) {
          $v['related'] = 'service';
          $tempServiceConfigs[] = $v;
        }
      }
    }

    // Sovrascrivo le configurazioni del tenant associate al client di default con quelle del servizio
    // Le configurazioni vengono inserite nell'array con chiave e-service id
    // Posso avere una sola configurazione per e-service
    // L'array risultante dopo la sovrascrittura contiene tutte le configurazioni che un pdnd block inserito nel servizio può chiamare
    foreach ($tempServiceConfigs as $c) {
      $configs[$c['eservice_id']] = $c;
    }

    return $configs;
  }

  /**
   * @throws TransportExceptionInterface
   * @throws ServerExceptionInterface
   * @throws RedirectionExceptionInterface
   * @throws ClientExceptionInterface
   */
  public function getActiveServiceConfigs(Servizio $servizio): array
  {
    $configs = $this->getServiceConfigs($servizio);
    foreach ($configs as $k => $v) {
      if (!$v['is_active']) {
        unset($configs[$k]);
      }
    }
    return $configs;
  }

  public function isValidData($data): bool
  {
    if (empty($data['meta']['call_url']) || empty($data['meta']['signature']))
    {
      return false;
    }

    $options = (new HttpOptions())
      ->setAuthBearer($this->JWTToken)
      ->setJson($data);

    try {
      $response = $this->httpClient->request('POST', $data['meta']['call_url'], $options->toArray());
      $content = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
      return $content['result'];
    } catch (\Throwable $e) {
      $this->logger->error('Pdnd: errore durante la validazione del dato', [
        'exception' => $e->getMessage(),
      ]);
    }
    return false;
  }

  private function getUser(): ?UserInterface
  {
    if (null === $token = $this->tokenStorage->getToken()) {
      return null;
    }

    if (!is_object($user = $token->getUser())) {
      return null;
    }

    return $user;
  }

}
