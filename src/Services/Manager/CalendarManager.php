<?php

namespace App\Services\Manager;

use App\Entity\Calendar;
use App\Event\KafkaEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CalendarManager
{

  private EntityManagerInterface $entityManager;

  private EventDispatcherInterface $dispatcher;

  /**
   * CategoryManager constructor.
   * @param EntityManagerInterface $entityManager
   * @param EventDispatcherInterface $dispatcher
   */
  public function __construct(EntityManagerInterface $entityManager, EventDispatcherInterface $dispatcher)
  {
    $this->entityManager = $entityManager;
    $this->dispatcher = $dispatcher;
  }

  /**
   * @param Calendar $calendar
   */
  public function save(Calendar $calendar): void
  {
    $this->entityManager->persist($calendar);
    $this->entityManager->flush();

    $this->dispatcher->dispatch(new KafkaEvent($calendar), KafkaEvent::NAME);
  }


}
