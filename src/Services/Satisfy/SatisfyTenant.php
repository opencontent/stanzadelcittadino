<?php

namespace App\Services\Satisfy;

use App\Entity\Ente;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Client;
use GraphQL\Exception\QueryError;
use GraphQL\Mutation;
use GraphQL\Variable;
use Psr\Log\LoggerInterface;

class SatisfyTenant
{
  private string $apiUrl;
  private string $secret;
  private Client $client;
  private EntityManagerInterface $entityManager;
  private LoggerInterface $logger;

  public function __construct(string $apiUrl, string $secret, EntityManagerInterface $entityManager, LoggerInterface $logger)
  {

    $this->apiUrl = $apiUrl;
    $this->secret = $secret;

    $this->client = new Client($this->apiUrl, ['x-hasura-admin-secret' => $this->secret]);
    $this->entityManager = $entityManager;
    $this->logger = $logger;

  }

  /**
   * @throws \Exception
   */
  public function createTenant(Ente $ente, $email, $password)
  {
    $entryPoint = [
      'id' => $ente->getId(),
      'name' => $ente->getName(),
      'contact_mail' => $email,
      'max_num_entrypoints' => 5,
      'password_hash' => $password,
    ];

    $mutation = (new Mutation('insert_tenants_one'))
      ->setVariables([new Variable('entrypoint', 'tenants_insert_input', true)])
      ->setArguments(['object' => '$entrypoint'])
      ->setSelectionSet(['id']);
    $variables = ['entrypoint' => $entryPoint];

    try {
      $results = $this->client->runQuery($mutation, true, $variables);
    } catch (QueryError $exception) {
      // Catch query error and display error details
      $message = "Error when creating satisfy tenant {$ente->getname()} with id: {$ente->getId()}" . ': ' . $exception->getMessage();
      $this->logger->error($message, $exception->getErrorDetails());
      throw new \Exception($message);
    }

    // Reformat the results to an array and get the results of part of the array
    $results->reformatResults(true);
    $data = $results->getData();
    if (!empty($data['insert_tenants_one'])) {
      $entrypointId = $data['insert_tenants_one']['id'];
      $ente->setSatisfyEntrypointId($entrypointId);
      $this->entityManager->persist($ente);
      $this->entityManager->flush();
      return $data['insert_tenants_one']['id'];
    }

    return null;
  }

  /**
   * @throws \Exception
   */
  public function deleteTenant(Ente $tenant): bool
  {
    $gql = (new Mutation('delete_tenants_by_pk'))
      ->setArguments(['id' => $tenant->getId()])
      ->setSelectionSet(['id']);

    try {
      $results = $this->client->runQuery($gql);
    } catch (QueryError $exception) {
      // Catch query error and desplay error details
      $message = "Error when deleting satisfy tenant {$tenant->getname()} with id: {$tenant->getId()}" . ': ' . $exception->getMessage();
      $this->logger->error($message, $exception->getErrorDetails());
      throw new \Exception($message);
    }

    // Reformat the results to an array and get the results of part of the array
    $results->reformatResults(true);
    $data = $results->getData();

    if (!empty($data['delete_tenants_by_pk'])) {
      $tenant->setSatisfyEntrypointId(null);
      $this->entityManager->persist($tenant);
      $this->entityManager->flush();
      return true;
    }

    return false;
  }
}
