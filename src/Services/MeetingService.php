<?php


namespace App\Services;


use App\Entity\Calendar;
use App\Entity\Ente;
use App\Entity\Meeting;
use App\Entity\OpeningHour;
use App\Entity\Pratica;
use App\Event\KafkaEvent;
use App\Model\DateTimeInterval;
use App\Services\Manager\CodeCounterManager;
use App\Services\Manager\NotificationSettingsManager;
use App\Utils\DateTimeUtils;
use App\Utils\StringUtils;
use App\Utils\UploadedBase64File;
use DateInterval;
use DatePeriod;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Gedmo\Translatable\TranslatableListener;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Spatie\IcalendarGenerator\Components\Calendar as ICalendar;
use Spatie\IcalendarGenerator\Components\Event as IEvent;
use Spatie\IcalendarGenerator\Enums\EventStatus;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\Error;

class MeetingService
{
  protected EntityManagerInterface $entityManager;
  private InstanceService $instanceService;
  private MailerService $mailer;
  private string $defaultSender;
  private TranslatorInterface $translator;
  private LoggerInterface $logger;
  private EventDispatcherInterface $dispatcher;
  private CodeCounterManager $counterManager;
  private Environment $templating;

  private ?Ente $ente = null;
  private NotificationSettingsManager $notificationSettingsManager;
  private RouterInterface $router;
  private TranslatableListener $translatableListener;


  /**
   * @param EntityManagerInterface $entityManager
   * @param InstanceService $instanceService
   * @param MailerService $mailer
   * @param $defaultSender
   * @param TranslatorInterface $translator
   * @param LoggerInterface $logger
   * @param EventDispatcherInterface $dispatcher
   * @param CodeCounterManager $counterManager
   * @param Environment $templating
   * @param NotificationSettingsManager $notificationSettingsManager
   * @param RouterInterface $router
   * @param TranslatableListener $translatableListener
   */
  public function __construct(
    EntityManagerInterface      $entityManager,
    InstanceService             $instanceService,
    MailerService               $mailer,
                                $defaultSender,
    TranslatorInterface         $translator,
    LoggerInterface             $logger,
    EventDispatcherInterface    $dispatcher,
    CodeCounterManager          $counterManager,
    Environment                 $templating,
    NotificationSettingsManager $notificationSettingsManager,
    RouterInterface             $router,
    TranslatableListener        $translatableListener
  )
  {
    $this->entityManager = $entityManager;
    $this->instanceService = $instanceService;
    $this->mailer = $mailer;
    $this->defaultSender = $defaultSender;
    $this->translator = $translator;
    $this->logger = $logger;
    $this->dispatcher = $dispatcher;
    $this->counterManager = $counterManager;
    $this->templating = $templating;
    $this->notificationSettingsManager = $notificationSettingsManager;
    $this->router = $router;
    $this->translatableListener = $translatableListener;
  }

  /**
   * @param Meeting $meeting
   * @param bool $flush
   * @throws Exception
   */
  public function save(Meeting $meeting, bool $flush = true): void
  {

    try {
      // Genero un codice per il meeting se created_at è null, il calendario ha una Cgs e il meeting non ha un codice
      if ($meeting->getCreatedAt() === null && empty($meeting->getCode()) && $meeting->getCalendar() && $meeting->getCalendar()->getCodeGenerationStrategy()) {
        $meeting->setCode(
          $this->counterManager->getNextCode(
            $meeting->getCalendar()->getCodeGenerationStrategy(),
            $meeting->getFromTime()
          )
        );
      }
    } catch (Exception $e) {
      $this->logger->error('error generating code for meeting', [
        'meeting_id' => $meeting->getId(),
        'exception' => $e->getMessage(),
      ]);
    }

    if ($meeting->getStatus() == Meeting::STATUS_DRAFT) {
      $meeting->setDraftExpiration(new \DateTime('+' . ($meeting->getCalendar()->getDraftsDuration() ?? Calendar::DEFAULT_DRAFT_DURATION) . 'seconds'));
    } else {
      $meeting->setDraftExpiration(null);
    }

    $this->entityManager->persist($meeting);
    if ($flush) {
      $this->entityManager->flush();
    }
    $this->dispatcher->dispatch(new KafkaEvent($meeting), KafkaEvent::NAME);
  }

  /**
   * Checks if given slot is available
   * @param Meeting $meeting
   *
   * @return bool
   */
  public function isSlotAvailable(Meeting $meeting): bool
  {
    $meetings = $this->entityManager->createQueryBuilder()
      ->select('count(meeting) as meetingCount')
      ->from(Meeting::class, 'meeting')
      ->leftJoin('meeting.calendar', 'calendar')
      ->where('meeting.calendar = :calendar')
      ->andWhere('meeting.fromTime < :toTime AND meeting.toTime > :fromTime')
      ->andWhere('meeting.id != :id')
      ->andWhere('meeting.status != :refused')
      ->andWhere('meeting.status != :cancelled')
      ->setParameter('calendar', $meeting->getCalendar())
      ->setParameter('fromTime', $meeting->getFromTime())
      ->setParameter('toTime', $meeting->getToTime())
      ->setParameter('id', $meeting->getId())
      ->setParameter('refused', Meeting::STATUS_REFUSED)
      ->setParameter('cancelled', Meeting::STATUS_CANCELLED)
      ->getQuery()->getResult();

    return !(!empty($meetings) && $meeting->getOpeningHour() && $meetings[0]['meetingCount'] >= $meeting->getOpeningHour()->getMeetingQueue());
  }

  /**
   * @throws Exception
   */
  public function getOpeningHourByDate($calendarId, $from, $to): OpeningHour
  {
    if (!$from instanceof DateTime) {
      $from = DateTimeUtils::parseDateTime($from);
    }

    if (!$from) {
      throw new Exception('Error parsing start date: ' . var_export($from, true));
    }

    if (!$to instanceof DateTime) {
      $to = DateTimeUtils::parseDateTime($to);
    }

    if (!$to) {
      throw new Exception('Error parsing end date: ' . var_export($to, true));
    }

    $dayOfWeek = $from->format('w');


    $openingHours = $this->entityManager->createQueryBuilder()
      ->select('openingHour')
      ->from(OpeningHour::class, 'openingHour')
      ->where('openingHour.calendar = :calendar')
      ->andWhere('openingHour.startDate <= :date AND openingHour.endDate >= :date')
      ->andWhere('openingHour.beginHour <= :fromTime AND openingHour.endHour >= :toTime')
      ->setParameter('calendar', $calendarId)
      ->setParameter('date', $from)
      ->setParameter('fromTime', $from->format('H:i:s'))
      ->setParameter('toTime', $to->format('H:i:s'))
      ->getQuery()->getResult();

    if ($openingHours) {
      /** @var OpeningHour $hour */
      foreach ($openingHours as $hour) {
        if (in_array($dayOfWeek, $hour->getDaysOfWeek())) {
          return $hour;
        }
      }
    }

    throw new Exception('No opening hours compatible with slot passed');

  }

  /**
   * Checks if given slot is valid
   * @param Meeting $meeting
   *
   * @return bool
   * @throws Exception
   */
  public function isSlotValid(Meeting $meeting): bool
  {
    if (!$meeting->getCalendar()) {
      return false;
    }

    // Retrieve all meetings in the same time slot
    foreach ($this->getCalendarClosingPeriods($meeting->getCalendar()) as $closingPeriod) {
      if ($meeting->getFromTime() < $closingPeriod->getToTime() && $closingPeriod->getFromTime() < $meeting->getToTime()) {
        // closure overlap
        return false;
      }
    }

    if ($meeting->getOpeningHour() && $this->isOpeningHourValidForMeeting($meeting, $meeting->getOpeningHour())) {
      // Min duration constraint
      $duration = $this->getDifferenceInMinutes($meeting->getFromTime(), $meeting->getToTime());

      return $duration >= $meeting->getOpeningHour()->getMeetingMinutes();
    }

    if (!$meeting->getCalendar()->isAllowOverlaps()) {
      foreach ($meeting->getCalendar()->getOpeningHours() as $openingHour) {
        if ($this->isOpeningHourValidForMeeting($meeting, $openingHour)) {
          $meeting->setOpeningHour($openingHour);

          return true;
        }
      }
    }

    return false;
  }

  /**
   * @throws Exception
   */
  private function isOpeningHourValidForMeeting(Meeting $meeting, OpeningHour $openingHour): bool
  {
    $dates = $this->explodeDays($openingHour, true);
    $meetingDate = $meeting->getFromTime()->format('Y-m-d');

    // Date not available for opening hour
    if (!in_array($meetingDate, $dates, true)) {
      return false;
    }

    $isValid = false;
    if ($meeting->getCalendar()->getType() === Calendar::TYPE_TIME_VARIABLE) {
      if ($meeting->getFromTime()->format('H:i') >= $openingHour->getBeginHour()->format('H:i') && $meeting->getToTime()->format('H:i') <= $openingHour->getEndHour()->format('H:i')) {
        $isValid = true;
      }
    } else {
      $slots = $this->explodeMeetings($openingHour, $meeting->getFromTime());
      $meetingEnd = clone $meeting->getToTime();
      $slotKey = $meeting->getFromTime()->format('H:i') . '-' . $meetingEnd->format('H:i');
      if (array_key_exists($slotKey, $slots)) {
        $isValid = true;
      }
    }

    return $isValid;
  }

  /**
   * Returns array of opening hour slots by date
   *
   * @param OpeningHour $openingHour
   * @param DateTime $date
   * @return array
   * @throws Exception
   */
  public function explodeMeetings(OpeningHour $openingHour, DateTime $date): array
  {
    $closures = $this->getCalendarClosingPeriods($openingHour->getCalendar());
    $intervals = [];
    if ($openingHour->getStartDate() > $date || $openingHour->getEndDate() < $date) {
      return $intervals;
    }
    // Define the meeting interval including both meeting time and break interval
    $meetingInterval = new DateInterval('PT' . ($openingHour->getMeetingMinutes() + $openingHour->getIntervalMinutes()) . 'M');
    $dateString = $date->format('Y-m-d');

    // Set the start and end time for the period
    $begin = (new DateTime($dateString))->setTime(
      $openingHour->getBeginHour()->format('H'),
      $openingHour->getBeginHour()->format('i')
    );
    $end = (new DateTime($dateString))->setTime(
      $openingHour->getEndHour()->format('H'),
      $openingHour->getEndHour()->format('i')
    );

    // Adjust the end time to include the final interval
    $adjustedEnd = clone $end;
    $adjustedEnd->add($meetingInterval); // Extend by one interval

    $periods = new DatePeriod($begin, $meetingInterval, $adjustedEnd);

    foreach ($periods as $period) {
      $available = true;

      // Check if period falls on closure
      foreach ($closures as $closure) {
        if ($period >= $closure->getFromTime() && $period < $closure->getToTime()) {
          $available = false;
        }
      }

      $_begin = $period;
      $_end = clone $_begin;
      $_end = $_end->add($meetingInterval);

      if ($_end <= $adjustedEnd) {
        $_end->modify('- ' . $openingHour->getIntervalMinutes() . ' minutes');
        if ($_end > $end) {
          continue;
        }

        $intervals[$_begin->format('H:i') . '-' . $_end->format('H:i')] = [
          'date' => $date->format('Y-m-d'),
          'start_time' => $_begin->format('H:i'),
          'end_time' => $_end->format('H:i'),
          'slots_available' => $available ? $openingHour->getMeetingQueue() : 0,
          'opening_hour' => $openingHour->getId(),
        ];
      }
    }

    return $intervals;
  }

  /**
   * Return array of available dates
   *
   * @param OpeningHour $openingHour
   * @param bool $all
   * @param null $from
   * @param null $to
   * @return array
   * @throws Exception
   */

  public function explodeDays(OpeningHour $openingHour, ?bool $all = false, $from = null, $to = null): array
  {
    $calendar = $openingHour->getCalendar();
    $calendarClosures = $this->getCalendarClosingPeriods($calendar);
    $array = array();

    if ($all) {
      $start = $openingHour->getStartDate();
      $end = $openingHour->getEndDate();
    } else {
      if ($from) {
        $start = new DateTime($from);
        $end = new DateTime($to);
      } else {
        $noticeInterval = new DateInterval('PT' . $calendar->getMinimumSchedulingNotice() . 'H');
        $start = max((new DateTime())->add($noticeInterval), $openingHour->getStartDate());
        $rollingInterval = new DateInterval('P' . $calendar->getRollingDays() . 'D');
        $end = min((new DateTime())->add($rollingInterval), $openingHour->getEndDate());
      }
    }

    $end->setTime(23, 59, 59);
    // Variable that store the date interval of period 1 day
    $interval = new DateInterval('P1D');

    $period = new DatePeriod($start, $interval, $end);

    $closureDays = [];
    // Calcolo i giorni di chiusura del calendario formando un array del tipo ['yyyy-mm-dd' => [$closure1, $closure2]]

    foreach ($calendarClosures as $closure) {
      /** @var DateTimeInterval $closure */
      $closingPeriod = new DatePeriod($closure->getFromTime(), $interval, $closure->getToTime());
      foreach ($closingPeriod as $item) {
        $closureDays[$item->format('Y-m-d')][] = $closure;
      }
    }

    // Aggiungo all'array tutti i giorni disponibili dell'orario di apertura
    foreach ($period as $date) {
      if (!in_array($date->format('N'), $openingHour->getDaysOfWeek(), false)) {
        // Ignoro tutti i giorni che non rispettano i giorni della settimana configurati
        continue;
      }

      $dateString = $date->format('Y-m-d');
      $isDayAvailable = true;

      // Verifico se il giorno si sovrappone con un periodo di chiusura
      if (array_key_exists($dateString, $closureDays)) {
        // Per tutte le chiusure che ricoprono il giorno di apertura verifico se l'intersezione permette la prenotazione
        foreach ($closureDays[$dateString] as $closure) {
          // Calcolo inizio e fine dell'orario di apertura per il giorno di chiusura.
          $openingHourStartTime = DateTime::createFromFormat('Y-m-d:H:i', $dateString . ':' . $openingHour->getBeginHour()->format('H:i'));
          $openingHourEndTime = DateTime::createFromFormat('Y-m-d:H:i', $dateString . ':' . $openingHour->getEndHour()->format('H:i'));

          if ($closure->getFromTime() <= $openingHourStartTime && $closure->getToTime() >= $openingHourEndTime) {
            // La chiusura copre interamente l'orario di apertura, interrompo il ciclo perche il giorno non è disponibile
            $isDayAvailable = false;
            break;
          }

          if ($closure->getFromTime() <= $openingHourEndTime && $closure->getToTime() >= $openingHourStartTime) {
            // La chiusura si interseca con l'orario di apertura: verifico se c'è disponibilita per uno slot a destra e sinistra dell'intersezione
            $leftDiff = $openingHourStartTime->diff($closure->getFromTime());
            $rightDiff = $closure->getToTime()->diff($openingHourEndTime);
            $leftAvailableMinutes = $leftDiff->i + ($leftDiff->h * 60);
            $rightAvailableMinutes = $rightDiff->i + ($rightDiff->h * 60);
            $minDuration = $openingHour->getMeetingMinutes();
            // Non è possibile inserire un appuntamento perche la durata non lo consente, interrompo il ciclo
            if ($leftAvailableMinutes < $minDuration && $rightAvailableMinutes < $minDuration) {
              $isDayAvailable = false;
              break;
            }
          }
        }
      }
      if ($isDayAvailable) {
        $array[] = $dateString;
      }
    }

    return $array;
  }

  private function getCalendarEvent($title, $start, $end, $available): array
  {
    if (!$available && $title === 'Apertura') {
      $title = "Non disp";
    }

    return [
      'title' => $title,
      'start' => $start,
      'end' => $end,
      'rendering' => 'background',
      'color' => $available ? 'var(--blue)' : 'var(--200)',
    ];
  }

  /**
   * @throws Exception
   */
  public function getAbsoluteAvailabilities(OpeningHour $openingHour, $all = false, DateTime $from = null, DateTime $to = null): array
  {
    $slots = [];
    $startDate = max($from ?? new DateTime(), $openingHour->getStartDate())->format('Y-m-d');
    $endDate = min($to ?? (new DateTime())->modify('+2month'), $openingHour->getEndDate())->format('Y-m-d');
    foreach ($this->explodeDays($openingHour, $all, $startDate, $endDate) as $date) {
      $futureEvent = $date >= (new DateTime())->format('Y-m-d');
      // Monthly view availability: Check day only, without time
      $slots[] = $this->getCalendarEvent('OpeningDay', $date, $date, $futureEvent);

      if ($futureEvent) {
        $availabilities = $this->getAvailabilitiesByDate(
          $openingHour->getCalendar(),
          new DateTime($date),
          true,
          false,
          null,
          [$openingHour->getId()]
        );

        foreach ($availabilities as $availability) {
          $start = DateTime::createFromFormat(
            'Y-m-d:H:i',
            $availability['date'] . ':' . $availability['start_time']
          )->format('c');
          $end = DateTime::createFromFormat('Y-m-d:H:i', $availability['date'] . ':' . $availability['end_time'])->format(
            'c'
          );
          $slots[] = $this->getCalendarEvent('Apertura', $start, $end, $availability['availability']);
        }
      } else {
        $start = DateTime::createFromFormat('Y-m-d:H:i', $date . ':' . $openingHour->getBeginHour()->format('H:i'))->format(
          'c'
        );
        $end = DateTime::createFromFormat('Y-m-d:H:i', $date . ':' . $openingHour->getEndHour()->format('H:i'))->format(
          'c'
        );
        $slots[] = $this->getCalendarEvent('Apertura', $start, $end, false);
      }
    }

    return $slots;
  }

  /**
   * Sends email for new meeting
   *
   * @param Meeting $meeting
   * @throws Error
   */
  public function sendEmailNewMeeting(Meeting $meeting): void
  {
    $calendar = $meeting->getCalendar();
    if (!$calendar instanceof Calendar) {
      return;
    }

    if ($calendar->getDisableNotifications()) {
      return;
    }

    $locale = $meeting->getLocale();
    $this->translatableListener->setTranslatableLocale($locale);

    $calendar->setTranslatableLocale($locale);
    $this->entityManager->refresh($calendar);

    $ente = $this->getAndRefreshTenant($locale);

    if (!in_array($meeting->getStatus(), [Meeting::STATUS_APPROVED, Meeting::STATUS_PENDING])) {
      return;
    }

    if ($meeting->getEmail()) {
      $userSubject = $this->refineNotificationSubject(
        $meeting,
        $this->translator->trans('meetings.email.new_meeting.subject', [], null, $locale)
      );

      // Email nuova
      $userMessage = $this->templating->render('Emails/Meeting/user/_new_meeting.html.twig', [
        'meeting' => $meeting,
      ]);

      $this->sendNotificationToUser($meeting, $ente, $userSubject, $userMessage);
    }

    // Inizio preparazione email per moderatori e operatori, la lingua è la lingua della pratica/appuntamento in attesa degli sviluppi sulla lingua nel profilo
    $subject = $this->refineNotificationSubject(
      $meeting,
      $this->translator->trans(
        'meetings.email.operatori.new_meeting.subject',
        ['%calendar%' => $calendar->getTitle()],
        null,
        $locale
      )
    );
    $operatoreMessage = $this->templating->render('Emails/Meeting/operator/_new_meeting.html.twig', [
      'meeting' => $meeting,
    ]);

    $this->sendNotificationToCalendarContacts($meeting, $ente, $subject, $operatoreMessage);
  }

  /**
   * Sends email for updated meeting
   *
   * @param Meeting $meeting
   * @param $changeSet
   * @throws Error
   */
  public function sendEmailUpdatedMeeting(Meeting $meeting, $changeSet): void
  {
    $statusChanged = array_key_exists('status', $changeSet);
    $dateChanged = array_key_exists('fromTime', $changeSet);
    $linkChanged = array_key_exists('videoconferenceLink', $changeSet);

    $calendar = $meeting->getCalendar();
    if (!$calendar instanceof Calendar) {
      return;
    }

    if ($calendar->getDisableNotifications()) {
      return;
    }

    $locale = $meeting->getLocale();
    $this->translatableListener->setTranslatableLocale($locale);
    $status = $meeting->getStatus();
    $calendar->setTranslatableLocale($locale);
    $this->entityManager->refresh($calendar);

    $oldDate = null;
    if ($dateChanged) {
      $oldDate = $changeSet['fromTime'][0];
      if ($oldDate instanceof DateTime
        && $oldDate->format('d/m/Y') === $meeting->getFromTime()->format('d/m/Y')
        && $oldDate->format('H:i') === $meeting->getFromTime()->format('H:i')) {
        // TODO: questo if è un workaround per le mail di spostamento appuntamento non vere, il log necessario solo per rintracciare il problema alla radice
        $this->logger->warning(
          'Try to send a useless meeting message: ',
          ['meeting_id' => $meeting->getId(), 'change_set' => $changeSet]
        );
        $dateChanged = false;
      }
    }

    $oldLink = null;
    if ($linkChanged) {
      $oldLink = $changeSet['videoconferenceLink'][0];
    }
    $link = $meeting->getVideoconferenceLink();

    $ente = $this->getAndRefreshTenant($locale);

    /*
     * Invio email se:
     * l'app.to è stato rifiutato (lo stato è cambiato, non mi interessa la data)
     * Lo stato è approvato (non cambiato) ed è stata cambiata la data
     * Lo stato è cambiato in approvato e ho un cambio di data
     * L'app.to è stato approvato
     */

    if (!in_array(
      $meeting->getStatus(),
      [Meeting::STATUS_REFUSED, Meeting::STATUS_CANCELLED, Meeting::STATUS_APPROVED, Meeting::STATUS_PENDING]
    )) {
      return;
    }

    if ($meeting->getEmail()) {
      $userMessage = '';
      if ($statusChanged && $status == Meeting::STATUS_REFUSED) {
        // Meeting has been refused. Date change does not matter
        $userMessage = $this->templating->render('Emails/Meeting/user/_refused.html.twig', [
          'meeting' => $meeting,
        ]);
      }

      if ($statusChanged && $status == Meeting::STATUS_CANCELLED) {
        // Meeting has been cancelled. Date change does not matter
        $userMessage = $this->templating->render('Emails/Meeting/user/_cancelled.html.twig', [
          'meeting' => $meeting,
        ]);
      }

      // Add link for cancel meeting if meeting has status approved
      if ($status == Meeting::STATUS_APPROVED) {
        $params = [
          'meeting' => $meeting,
        ];

        if (!$statusChanged && $dateChanged) {
          // Approved meeting has been rescheduled
          $params = array_merge(
            [
              'meeting_event' => 'rescheduled',
              'old_date' => $oldDate,
            ],
            $params
          );
        } elseif ($statusChanged && $dateChanged) {
          // Auto approved meeting due to date change
          $params = array_merge(
            ['meeting_event' => 'rescheduled_and_approved'],
            $params
          );
        } elseif ($statusChanged && !$dateChanged) {
          // Approved meeting with no date change
          $params = array_merge(
            ['meeting_event' => 'approved'],
            $params
          );
        } elseif (!$statusChanged && !$dateChanged && $linkChanged) {
          // Videoconference link changed for approved meeting
          if ($link && $oldLink) {
            $params = array_merge(
              ['meeting_event' => 'link_changed'],
              $params
            );
          } elseif (!$oldLink) {
            $params = array_merge(
              ['meeting_event' => 'link_new'],
              $params
            );
          } elseif (!$link) {
            $params = array_merge(
              ['meeting_event' => 'link_removed'],
              $params
            );
          }
        }

        if (isset($params['meeting_event'])) {
          $userMessage = $this->templating->render('Emails/Meeting/user/_approved.html.twig', $params);
        }
      }

      $userSubject = $this->refineNotificationSubject(
        $meeting,
        $this->translator->trans('meetings.email.edit_meeting.subject', [], null, $locale)
      );

      $this->logger->info(
        'Sending email to ' . StringUtils::obfuscateEmail($meeting->getEmail()) . ' for updated meeting ' . $meeting->getId() . ' with status ' . $meeting->getStatusName()
      );
      $this->sendNotificationToUser($meeting, $ente, $userSubject, $userMessage);
    }

    // Inizio preparazione email per moderatori e operatori, la lingua è la lingua della pratica/appuntamento in attesa degli sviluppi sulla lingua nel profilo
    $subject = '';
    $contactMessage = '';
    if ($statusChanged && $status == Meeting::STATUS_CANCELLED) {
      $contactMessage = $this->templating->render('Emails/Meeting/operator/_cancelled.html.twig', [
        'meeting' => $meeting,
      ]);
      $subject = $this->translator->trans(
        'meetings.email.operatori.meeting_cancelled.subject',
        [
          '%calendar%' => $calendar->getTitle(),
        ],
        null,
        $locale
      );
    }

    if ($statusChanged && $status == Meeting::STATUS_REFUSED) {
      $contactMessage = $this->templating->render('Emails/Meeting/operator/_cancelled.html.twig', [
        'meeting' => $meeting,
      ]);
      $subject = $this->translator->trans(
        'meetings.email.operatori.meeting_refused.subject',
        [
          '%calendar%' => $calendar->getTitle(),
        ],
        null,
        $locale
      );
    }

    if ($statusChanged && $status == Meeting::STATUS_APPROVED) {
      $contactMessage = $this->templating->render('Emails/Meeting/operator/_approved.html.twig', [
        'meeting' => $meeting,
      ]);
      $subject = $this->translator->trans(
        'meetings.email.operatori.meeting_approved.subject',
        [
          '%calendar%' => $calendar->getTitle(),
        ],
        null,
        $locale
      );
    }

    if ($dateChanged && !$statusChanged) {
      $contactMessage = $this->templating->render('Emails/Meeting/operator/_rescheduled.html.twig', [
        'meeting' => $meeting,
        'old_date' => $oldDate,
      ]);
      $subject = $this->translator->trans(
        'meetings.email.operatori.meeting_rescheduled.subject',
        [
          '%calendar%' => $calendar->getTitle(),
        ],
        null,
        $locale
      );
    }

    $subject = $this->refineNotificationSubject($meeting, $subject);

    $this->sendNotificationToCalendarContacts($meeting, $ente, $subject, $contactMessage);

  }

  private function sendNotificationToUser(Meeting $meeting, Ente $ente, $subject, $message): void
  {
    if (empty($subject) || empty($message)) {
      return;
    }

    $file = $this->createIcsFile($meeting, $ente);

    $this->mailer->dispatchMail(
      $this->defaultSender,
      $ente->getName(),
      $meeting->getEmail(),
      $meeting->getName(),
      $message,
      $subject,
      $ente,
      [],
      [],
      $meeting->getLocale(),
      [$file]
    );
  }

  private function createIcsFile(Meeting $meeting, Ente $ente): UploadedBase64File
  {
    $name = $meeting->getName() ?? '';
    $application = $meeting->getApplication();
    if ($application instanceof Pratica) {
      $name = $application->getOggetto();
    }

    switch ($meeting->getStatus()) {
      case Meeting::STATUS_CANCELLED:
      case Meeting::STATUS_REFUSED:
      case Meeting::STATUS_MISSED:
        $status = EventStatus::cancelled();
        break;
      case Meeting::STATUS_APPROVED:
      case Meeting::STATUS_DONE:
        $status = EventStatus::confirmed();
        break;
      case Meeting::STATUS_PENDING:
      case Meeting::STATUS_DRAFT:
        $status = EventStatus::tentative();
        break;
      default:
        $status = EventStatus::tentative();
    }

    $tenantEmail = $ente->getEmailCertificata() ?? $ente->getEmail();
    $iEvent = IEvent::create()
      ->uniqueIdentifier($meeting->getId())
      ->name($name)
      ->organizer($tenantEmail, $ente->getName())
      ->description($meeting->getReason() ?? '')
      ->address($meeting->getLocation() ?? '')
      ->startsAt($meeting->getFromTime())
      ->endsAt($meeting->getToTime())
      ->status($status);

    $calendar = ICalendar::create()->event([$iEvent]);
    $icsString = $calendar->get();
    $filename = sprintf('%s.ics', $meeting->getId());

    return new UploadedBase64File(base64_encode($icsString), 'text/calendar', $filename);
  }

  private function sendNotificationToCalendarContacts(Meeting $meeting, Ente $ente, $subject, $message): void
  {
    if (empty($subject) || empty($message)) {
      return;
    }

    $calendar = $meeting->getCalendar();
    if (!$calendar instanceof Calendar) {
      return;
    }

    if ($calendar->getContactEmail()) {
      $this->logger->info(
        'Sending email to ' . StringUtils::obfuscateEmail(
          $calendar->getContactEmail()
        ) . ' (calendar contact) for updated meeting ' . $meeting->getId() . ' with status ' . $meeting->getStatusName()
      );
      $this->mailer->dispatchMail(
        $this->defaultSender,
        $ente->getName(),
        $calendar->getContactEmail(),
        'Contatto Calendario',
        $message,
        $subject,
        $ente,
        [],
        [],
        $meeting->getLocale()
      );

    }

    // Send email for each moderator
    // Todo: da ottimizzare vista l'aggiunta del controllo delle notifiche
    foreach ($calendar->getModerators() as $moderator) {
      $moderatorNotificationSettings = $this->notificationSettingsManager->getNotificationSettingsObjectIds($moderator);
      if (!in_array($calendar->getId(), $moderatorNotificationSettings)) {
        $this->logger->info(
          'Notification to  ' . StringUtils::obfuscateEmail(
            $moderator->getEmail()
          ) . ' (moderator) for updated meeting ' . $meeting->getId() . ' of calendar ' . $calendar->getId() . 'not performed due to user notification preferences'
        );
        continue;
      }

      $callToAction = [
        [
          'link' => $this->router->generate(
            'operatori_calendar_show',
            ['calendar' => $calendar->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL
          ),
          'label' => $this->translator->trans('calendars.go_calendar', [], null, $meeting->getLocale()),
        ],
      ];

      $this->mailer->dispatchMail(
        $this->defaultSender,
        $this->ente->getName(),
        $moderator->getEmail(),
        $moderator->getNome(),
        $message,
        $subject,
        $this->ente,
        $callToAction,
        [],
        $meeting->getLocale()
      );
    }
  }


  /**
   * Sends email for removed meeting
   *
   * @param Meeting $meeting
   * @throws Error
   */
  public function sendEmailRemovedMeeting(Meeting $meeting): void
  {
    if (!$meeting->getEmail()) {
      return;
    }

    if ($meeting->getStatus() == Meeting::STATUS_DRAFT) {
      // Non invio comunicazioni al cittadino all'eliminazione dell'appuntamento per scadenza della bozza
      return;
    }

    $locale = $meeting->getLocale();
    $this->translatableListener->setTranslatableLocale($locale);
    $ente = $this->getAndRefreshTenant($locale);
    $message = $this->templating->render('Emails/Meeting/user/_deleted.html.twig', [
      'meeting' => $meeting,
    ]);

    $subject = $this->refineNotificationSubject($meeting, $this->translator->trans('meetings.email.delete_meeting.subject'));
    $this->logger->info(
      'Sending email to ' . StringUtils::obfuscateEmail($meeting->getEmail()) . ' for removed meeting ' . $meeting->getId() . ' with status ' . $meeting->getStatusName()
    );

    $this->sendNotificationToUser($meeting, $ente, $subject, $message);

  }


  /**
   * Sends email for unavailable meeting
   *
   * @param Meeting $meeting
   * @throws Error
   */
  public function sendEmailUnavailableMeeting(Meeting $meeting): void
  {
    if (!$meeting->getEmail()) {
      return;
    }

    $locale = $meeting->getLocale();
    $this->translatableListener->setTranslatableLocale($locale);
    $ente = $this->getAndRefreshTenant($locale);

    $subject = $this->translator->trans('meetings.email.invalid_meeting.subject', [], null, $locale);
    $message = $this->templating->render('Emails/Meeting/user/_invalid.html.twig', [
      'meeting' => $meeting,
    ]);

    $this->logger->info(
      'Sending email to ' . StringUtils::obfuscateEmail(
        $meeting->getEmail()
      ) . ' for unavailable meeting ' . $meeting->getId() . ' with status ' . $meeting->getStatusName()
    );

    $this->sendNotificationToUser($meeting, $ente, $subject, $message);

  }


  /**
   * @throws Exception
   */
  public function getAvailabilitiesByDate(
    Calendar $calendar,
             $date,
             $ignoreMinimumSchedulingNotice = false,
             $exludeUnavailable = false,
             $excludedMeeting = null,
             $selectedOpeningHours = []
  ): array
  {
    if ($calendar->getType() === Calendar::TYPE_TIME_FIXED) {
      return $this->getSlottedAvailabilitiesByDate(
        $calendar,
        $date,
        $ignoreMinimumSchedulingNotice,
        $exludeUnavailable,
        $excludedMeeting,
        $selectedOpeningHours
      );
    }

    return $this->getVariableAvailabilitiesByDate(
      $calendar,
      $date,
      $ignoreMinimumSchedulingNotice,
      $exludeUnavailable,
      $excludedMeeting,
      $selectedOpeningHours
    );
  }


  /**
   * @throws Exception
   */
  private function getSlottedAvailabilitiesByDate(
    Calendar $calendar,
             $date,
             $all = false,
             $exludeUnavailable = false,
             $excludedMeeting = null,
             $selectedOpeningHours = []
  ): array
  {
    /** @var OpeningHour[] $openingHours */
    $openingHours = $selectedOpeningHours
      ? $this->entityManager->getRepository(OpeningHour::class)->findBy([
        'calendar' => $calendar,
        'id' => $selectedOpeningHours,
      ])
      : $calendar->getOpeningHours();

    $start = clone ($date)->setTime(0, 0, 0);
    $end = clone ($date)->setTime(23, 59, 59);

    $meetingQueryBuilder = $this->entityManager->createQueryBuilder()
      ->select('count(meeting.fromTime) as count', 'meeting.fromTime as start_time', 'meeting.toTime as end_time')
      ->from(Meeting::class, 'meeting')
      ->where('meeting.calendar = :calendar')
      ->andWhere('meeting.fromTime >= :startDate')
      ->andWhere('meeting.toTime <= :endDate')
      ->andWhere('meeting.status NOT IN (:excludedStatuses)')
      ->setParameter('excludedStatuses', [Meeting::STATUS_REFUSED, Meeting::STATUS_CANCELLED])
      ->setParameter('calendar', $calendar)
      ->setParameter('startDate', $start)
      ->setParameter('endDate', $end)
      ->groupBy('meeting.fromTime', 'meeting.toTime');

    if ($excludedMeeting) {
      $meetingQueryBuilder
        ->andWhere('meeting.id != :excluded_id')
        ->setParameter('excluded_id', $excludedMeeting);
    }
    $meetingResults = $meetingQueryBuilder->getQuery()->getResult();

    // Set meetings key (Format: start_time-end_time-count)
    $meetings = [];
    foreach ($meetingResults as $meeting) {
      $meetings[$meeting['start_time']->format('H:i') . '-' . $meeting['end_time']->format('H:i')] = $meeting;
    }

    // Retrieve calendar slots by input date
    $slots = [];
    foreach ($openingHours as $openingHour) {
      if ($openingHour->getStartDate() <= $date && $openingHour->getEndDate() >= $date && in_array($date->format('Y-m-d'), $this->explodeDays($openingHour, $all), true)) {
        $slots[] = $this->explodeMeetings($openingHour, $date);
      }
    }
    $slots = array_merge([], ...$slots);
    ksort($slots);

    $availableSlots = [];
    $noticeInterval = $all ? new DateInterval('PT0H') : new DateInterval('PT' . $calendar->getMinimumSchedulingNotice() . 'H');
    $now = (new DateTime())->add($noticeInterval)->format('Y-m-d:H:i');

    if ($selectedOpeningHours) {
      foreach ($slots as $key => $slot) {
        if (!in_array($slot["opening_hour"], $selectedOpeningHours, true)) {
          unset($slots[$key]);
        }
      }
    }

    foreach ($slots as $key => $day) {
      $slotStartTime = new DateTime($day['date'] . ' ' . $day['start_time']);
      $slotEndTime = new DateTime($day['date'] . ' ' . $day['end_time']);
      $slotKey = $slotStartTime->format('H:i') . '-' . $slotEndTime->format('H:i');
      $totalSlotsAvailable = $day['slots_available'];
      $slotsUnavailable = 0;

      if (isset($meetings[$slotKey])) {
        $totalSlotsAvailable -= $meetings[$key]['count'];
      } else {
        // Todo: trovare un modo migliore
        foreach ($meetings as $meeting) {
          $bookedStartTime = $meeting["start_time"];
          $bookedEndTime = $meeting["end_time"];
          if ($bookedEndTime > $slotStartTime && $bookedStartTime < $slotEndTime) {
            $slotsUnavailable = max($slotsUnavailable, $meeting['count'], 0);
          }
        }
      }

      // Aggiorno disponibilità e numero di slot
      $slots[$key]['slots_available'] = max($totalSlotsAvailable - $slotsUnavailable, 0);
      $slots[$key]['availability'] = ($slots[$key]['slots_available'] > 0);

      // Veerifico se lo slot è passato
      $startTimeFormatted = $slotStartTime->format('Y-m-d:H:i');
      if ($startTimeFormatted <= $now) {
        $slots[$key]['availability'] = false;
      }

      if ($slots[$key]['availability']) {
        $availableSlots[$key] = $slots[$key];
      }
    }

    if ($exludeUnavailable) {
      return $availableSlots;
    }

    return $slots;
  }

  public function getOpeningHoursOverlaps(Calendar $calendar, $selectedOpeningHours = []): array
  {

    $overlaps = [];
    /** @var OpeningHour[] $openingHours */
    $openingHours = [];

    if ($selectedOpeningHours) {
      foreach ($selectedOpeningHours as $selectedOpeningHour) {
        $openingHour = $this->entityManager->getRepository(OpeningHour::class)->findOneBy([
          'calendar' => $calendar,
          'id' => $selectedOpeningHour,
        ]);
        if ($openingHour) {
          $openingHours[] = $openingHour;
        }
      }
    } else {
      $openingHours = $calendar->getOpeningHours();
    }

    foreach ($openingHours as $index1 => $openingHour1) {
      foreach ($openingHours as $index2 => $openingHour2) {
        if ($index2 > $index1) {
          // Skip opening hours already analyzed
          $isDatesOverlapped = $openingHour1->getStartDate() < $openingHour2->getEndDate() && $openingHour1->getEndDate() > $openingHour2->getStartDate();
          $isTimesOverlapped = $openingHour1->getBeginHour() < $openingHour2->getEndHour() && $openingHour1->getEndHour() > $openingHour2->getBeginHour();
          $weekDaysOverlapped = array_intersect($openingHour1->getDaysOfWeek(), $openingHour2->getDaysOfWeek());

          if ($isTimesOverlapped && $isDatesOverlapped && !empty($weekDaysOverlapped)) {
            $overlaps[$openingHour1->getId()->toString()] = $openingHour1;
            $overlaps[$openingHour2->getId()->toString()] = $openingHour2;
          }
        }
      }
    }

    return array_values($overlaps);
  }

  /**
   * @throws Exception
   */
  private function getVariableAvailabilitiesByDate(
    Calendar $calendar,
             $date,
             $ignoreMinimumSchedulingNotice = false,
             $exludeUnavailable = false,
             $excludedMeeting = null,
             $selectedOpeningHours = []
  ): array
  {
    /** @var OpeningHour[] $openingHours */
    $openingHours = $selectedOpeningHours
      ? $this->entityManager->getRepository(OpeningHour::class)->findBy([
        'calendar' => $calendar,
        'id' => $selectedOpeningHours,
      ])
      : $calendar->getOpeningHours();


    $bookedMeetings = $this->getBookedSlotsByDate($date, $calendar, $excludedMeeting);

    if ($ignoreMinimumSchedulingNotice) {
      $noticeInterval = new DateInterval('PT0H');
    } else {
      $noticeInterval = new DateInterval('PT' . $calendar->getMinimumSchedulingNotice() . 'H');
    }

    // 5min round
    $firstAvailableDate = (new DateTime())->add($noticeInterval);
    $firstAvailableDate->setTime($firstAvailableDate->format("H"), $firstAvailableDate->format("i"), 0, 0);
    $minute = ($firstAvailableDate->format("i")) % 5;
    if ($minute != 0) {
      $firstAvailableDate->add(new DateInterval("PT" . (5 - $minute) . "M"));
    }


    $timeIntervals = [];
    foreach ($openingHours as $openingHour) {
      if ($openingHour->getStartDate() <= $date && $openingHour->getEndDate() >= $date && in_array($date->format('Y-m-d'), $this->explodeDays($openingHour, $ignoreMinimumSchedulingNotice), true)) {
        $begin = (clone $date)->setTime(
          $openingHour->getBeginHour()->format('H'),
          $openingHour->getBeginHour()->format('i'),
          0,
          0
        );
        $end = (clone $date)->setTime(
          $openingHour->getEndHour()->format('H'),
          $openingHour->getEndHour()->format('i'),
          0,
          0
        )->modify('+1minute');
        foreach (new DatePeriod($begin, new DateInterval("PT1M"), $end) as $interval) {
          $timeIntervals[$interval->format('H:i')] = [
            "availabilities" => $interval >= $firstAvailableDate ? $openingHour->getMeetingQueue() : 0,
            "opening_hour" => $openingHour,
            "datetime" => $interval,
          ];
        }
      }
    }

    ksort($timeIntervals);

    // Remove bookend meetings
    foreach ($bookedMeetings as $bookedMeeting) {
      foreach (new DatePeriod(
                 $bookedMeeting["start_time"],
                 new DateInterval("PT1M"),
                 $bookedMeeting["end_time"]->modify('+' . $bookedMeeting["interval_minutes"] . 'minutes')
               ) as $interval) {
        if (isset($timeIntervals[$interval->format('H:i')])) {
          $timeIntervals[$interval->format('H:i')]["availabilities"] = max(
            $timeIntervals[$interval->format('H:i')]["availabilities"] - $bookedMeeting["count"],
            0
          );
        }
      }
    }

    // Remove closures
    $firstTime = DateTime::createFromFormat('Y-m-d H:i', $date->format('Y-m-d') . ' ' . array_key_first($timeIntervals));
    $endTime = DateTime::createFromFormat('Y-m-d H:i', $date->format('Y-m-d') . ' ' . array_key_last($timeIntervals));

    foreach ($this->getCalendarClosingPeriods($calendar) as $closingPeriod) {
      if ($closingPeriod->getFromTime() <= $endTime && $firstTime <= $closingPeriod->getToTime()) {
        $closure = new DatePeriod(
          max($closingPeriod->getFromTime(), $firstTime),
          new DateInterval("PT1M"),
          min($closingPeriod->getToTime(), $endTime)
        );
        foreach ($closure as $closureInterval) {
          if (isset($timeIntervals[$closureInterval->format("H:i")])) {
            $timeIntervals[$closureInterval->format("H:i")]["availabilities"] = 0;
          }
        }
      }
    }

    $slots = array();
    if (empty($timeIntervals)) {
      return $slots;
    }

    // Regroup
    $slotStart = array_key_first($timeIntervals);
    $slotOpeningHour = $timeIntervals[$slotStart]["opening_hour"];
    $slotAvailability = min($timeIntervals[$slotStart]["availabilities"], 1);
    $slotEnd = $slotStart;
    $duration = null;


    foreach ($timeIntervals as $time => $interval) {
      $tmpAvailability = min($interval["availabilities"], 1);
      $tmpOpeningHour = $interval["opening_hour"];
      $slotEnd = $slotOpeningHour === $tmpOpeningHour ? $time : $slotEnd;
      if ($slotAvailability !== $tmpAvailability || $slotOpeningHour !== $tmpOpeningHour) {
        $available = $slotAvailability > 0 && $duration >= $slotOpeningHour->getMeetingMinutes();
        if ($available || !$exludeUnavailable) {
          // Check if unavailable slots should be added
          $slots[$slotStart . '-' . $slotEnd] = [
            "date" => $date->format('Y-m-d'),
            "start_time" => $slotStart,
            "end_time" => $slotEnd,
            "slots_available" => $slotAvailability,
            "availability" => $available,
            "opening_hour" => $slotOpeningHour->getId(),
            "min_duration" => $slotOpeningHour->getMeetingMinutes(),
          ];
        }

        $slotAvailability = $tmpAvailability;
        $slotOpeningHour = $tmpOpeningHour;
        $slotStart = $time;

      } else {
        $slotEnd = $time;
      }
      $duration = $this->getDifferenceInMinutes(
        $timeIntervals[$slotStart]["datetime"],
        $timeIntervals[$slotEnd]["datetime"]
      );
    }

    // Last slot
    $available = $slotAvailability > 0 && $duration >= $slotOpeningHour->getMeetingMinutes();
    if ($slotStart !== $slotEnd && ($available || !$exludeUnavailable)) {
      $slots[$slotStart . '-' . $slotEnd] = [
        "date" => $date->format('Y-m-d'),
        "start_time" => $slotStart,
        "end_time" => $slotEnd,
        "slots_available" => $slotAvailability,
        "availability" => $available,
        "opening_hour" => $slotOpeningHour->getId(),
        "min_duration" => $slotOpeningHour->getMeetingMinutes(),
      ];
    }

    return $slots;
  }

  private function getBookedSlotsByDate($date, $calendar, $excludedMeeting = null)
  {
    $start = clone ($date)->setTime(0, 0, 0);
    $end = clone ($date)->setTime(23, 59, 59);

    $builder = $this->entityManager->createQueryBuilder()
      ->select(
        'count(meeting.fromTime) as count',
        'meeting.fromTime as start_time',
        'meeting.toTime as end_time',
        'opening_hour.intervalMinutes as interval_minutes'
      )
      ->from(Meeting::class, 'meeting')
      ->join('meeting.openingHour', 'opening_hour')
      ->where('meeting.calendar = :calendar')
      ->andWhere('meeting.fromTime >= :startDate')
      ->andWhere('meeting.toTime < :endDate')
      ->andWhere('meeting.status != :refused')
      ->andWhere('meeting.status != :cancelled')
      ->setParameter('refused', Meeting::STATUS_REFUSED)
      ->setParameter('cancelled', Meeting::STATUS_CANCELLED)
      ->setParameter('calendar', $calendar)
      ->setParameter('startDate', $start)
      ->setParameter('endDate', $end)
      ->groupBy('meeting.fromTime', 'meeting.toTime', 'opening_hour.id');

    if ($excludedMeeting) {
      $builder
        ->andWhere('meeting.id != :excluded_id')
        ->setParameter('excluded_id', $excludedMeeting);
    }

    return $builder->getQuery()->getResult();
  }

  private function isUniqueActiveMeeting(Meeting $meeting): bool
  {
    $application = null;
    if ($meeting->getApplications()->count() > 0) {
      $application = $meeting->getApplications()->last();
    }
    if (!$application) {
      return true;
    }

    // Retrieve all active meetings (i.e pending of confirmed) linked to the same application
    $builder = $this->entityManager->createQueryBuilder()
      ->select('count(meeting.id)')
      ->from(Meeting::class, 'meeting')
      ->where(':applicationId MEMBER OF meeting.applications')
      ->andWhere('meeting.status IN (:activeStatuses)')
      ->setParameter(':applicationId', $application->getId())
      ->setParameter(':activeStatuses', [Meeting::STATUS_PENDING, Meeting::STATUS_APPROVED]);

    try {
      $activeMeetings = $builder->getQuery()->getSingleScalarResult();
    } catch (Exception $e) {
      $this->logger->error($e->getMessage() . ' --- ' . $e->getTraceAsString());

      return false;
    }

    return $activeMeetings <= 1;
  }

  public function isWithinReservationLimits(Meeting $meeting): bool
  {

    $email = $meeting->getEmail();
    $fiscalCode = $meeting->getFiscalCode();

    if (empty($email) && empty($meeting->getFiscalCode())) {
      return true;
    }

    $reservationLimits = $meeting->getCalendar()->getReservationLimits();
    if (!$reservationLimits) {
      return true;
    }

    $builder = $this->entityManager->createQueryBuilder()
      ->select('count(meeting.id)')
      ->from(Meeting::class, 'meeting')
      ->where('meeting.id != :id')
      ->setParameter(':id', $meeting->getId());

    if (!empty($meeting->getEmail()) && !empty($meeting->getFiscalCode())) {
      $builder
        ->andWhere($builder->expr()->orX(
          $builder->expr()->eq('meeting.email', ':email'),
          $builder->expr()->eq('meeting.fiscalCode', ':fiscalCode'),
        ))
        ->setParameter(':email', $email)
        ->setParameter(':fiscalCode', $fiscalCode);
    }

    if (!empty($meeting->getEmail())) {
      $builder->andWhere('meeting.email = :email')
        ->setParameter(':email', $email);
    }

    if (!empty($meeting->getFiscalCode())) {
      $builder->andWhere('meeting.fiscalCode = :fiscalCode')
        ->setParameter(':fiscalCode', $fiscalCode);
    }


    if ($reservationLimits->getInterval()) {
      try {
        $periodBounds = DateTimeUtils::getBoundsFromDate($meeting->getFromTime(), $reservationLimits->getInterval());

        $builder->andWhere('meeting.fromTime BETWEEN :startTime AND :endTime')
          ->setParameter(':startTime', $periodBounds['startDate'])
          ->setParameter(':endTime', $periodBounds['endDate']);
      } catch (InvalidArgumentException $exception) {
        $this->logger->error($exception->getMessage());
      }
    }

    try {
      $meetings = $builder->getQuery()->getSingleScalarResult();
    } catch (Exception $e) {
      $this->logger->error($e->getMessage() . ' --- ' . $e->getTraceAsString());
      return false;
    }

    return $meetings <= $reservationLimits->getMaxMeetingsPerInterval();

  }

  /**
   * @throws Exception
   */
  public function getMeetingErrors(Meeting $meeting): array
  {
    $errors = [];
    if (!$meeting->getOpeningHour() && $meeting->getCalendar()->isAllowOverlaps()) {
      $errors[] = $this->translator->trans('meetings.error.no_opening_hour_with_overlaps');
    } else {
      if (!$this->isSlotValid($meeting)) {
        $errors[] = $this->translator->trans('meetings.error.slot_invalid');
      }
      if ($meeting->getStatus() !== Meeting::STATUS_REFUSED && $meeting->getStatus() !== Meeting::STATUS_CANCELLED && !$this->isSlotAvailable($meeting)) {
        $errors[] = $this->translator->trans('meetings.error.slot_unavailable');
      }
    }

    // Questo controllo non è più valido vista l'introduzione della prenotazione di più meeting per pratica
    /*if (!$this->isUniqueActiveMeeting($meeting)) {
      $errors[] = $this->translator->trans('meetings.error.not_unique');
    }*/

    if (!$this->isWithinReservationLimits($meeting)) {
      $errors[] = $this->translator->trans('meetings.error.reservation_limits');
    }

    return $errors;
  }

  /**
   * @param Calendar $calendar
   * @return array
   * @description returns calendar's closing periods merged with tenants closing periods
   */
  public function getCalendarClosingPeriods(Calendar $calendar): array
  {
    $tenant = $this->instanceService->getCurrentInstance();
    return array_merge($calendar->getClosingPeriods(), $tenant->getClosingPeriods());

  }

  private function getDifferenceInMinutes(DateTime $from, DateTime $to)
  {
    $diff = $from->diff($to);

    return ($diff->h * 60) + ($diff->i);
  }

  private function refineNotificationSubject(Meeting $meeting, $subject): string
  {
    if ($meeting->getApplication()) {
      $subject .= ' - ' . $this->getMeetingServiceDetail($meeting);
    }
    if ($meeting->getCode()) {
      $subject .= ' (' . $meeting->getCode() . ')';
    }

    return $subject;
  }


  private function getMeetingServiceDetail(Meeting $meeting): string
  {
    $serviceDetail = '';
    $locale = $meeting->getLocale();
    $application = $meeting->getApplication();

    if ($application instanceof Pratica) {
      $service = $application->getServizio();
      $service->setTranslatableLocale($locale);
      $this->entityManager->refresh($service);
      $serviceName = $service->getName();
      $serviceGroup = $service->getServiceGroup();
      if ($serviceGroup) {
        $serviceDetail = $this->translator->trans(
          'meetings.email.service_detail_with_group',
          ['%service%' => $serviceName, '%group%' => $serviceGroup->getName()],
          null,
          $locale
        );
      } else {
        $serviceDetail = $this->translator->trans(
          'meetings.email.service_detail',
          ['%service%' => $serviceName],
          null,
          $locale
        );
      }
    }

    return $serviceDetail;
  }

  private function getAndRefreshTenant($locale): Ente
  {
    if ($this->ente instanceof Ente) {
      return $this->ente;
    }

    $ente = $this->instanceService->getCurrentInstance();
    $ente->setTranslatableLocale($locale);
    $this->entityManager->refresh($ente);
    $this->ente = $ente;
    return $this->ente;
  }
}
