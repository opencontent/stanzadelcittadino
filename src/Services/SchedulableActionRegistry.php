<?php

namespace App\Services;

use App\ScheduledAction\ScheduledActionHandlerInterface;

class SchedulableActionRegistry
{
  private array $services = [];

  public function registerService(ScheduledActionHandlerInterface $flow, $alias): void
  {
    $this->services[$alias] = $flow;
  }

  public function getServices(): array
  {
    return $this->services;
  }

  /**
   * @param string|null $alias
   * @return ScheduledActionHandlerInterface|null
   */
  public function getByName(string $alias = null): ?ScheduledActionHandlerInterface
  {
    if ($alias && isset($this->services[$alias])) {
      return $this->services[$alias];
    }

    return null;
  }
}
