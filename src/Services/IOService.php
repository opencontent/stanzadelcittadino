<?php


namespace App\Services;


use App\Entity\Pratica;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Client;
use League\HTMLToMarkdown\HtmlConverter;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class IOService
{

  private string $IOApiUrl;

  private LoggerInterface $logger;

  private TranslatorInterface $translator;


  public function __construct(string $IOApiUrl, LoggerInterface $logger, TranslatorInterface $translator)
  {
    $this->IOApiUrl = $IOApiUrl;
    $this->logger = $logger;
    $this->translator = $translator;
  }

  /**
   * @param $fiscalCode
   * @param $primaryKey
   * @return array
   */
  private function getProfile($primaryKey, $fiscalCode): array
  {
    $client = new Client(['base_uri' => $this->IOApiUrl]);

    $request = new Request(
      'GET',
      $client->getConfig('base_uri') . '/profiles/' . strtoupper($fiscalCode),
      ['Content-Type' => 'application/json', 'Ocp-Apim-Subscription-Key' => $primaryKey]
    );

    try {
      $response = $client->send($request);
      return json_decode($response->getBody(), true);
    } catch (\Throwable $exception) {
      $this->logger->error($exception->getMessage());
      return json_decode($exception->getResponse()->getBody()->getContents(), true);
    }
  }

  /**
   * @param $primaryKey
   * @param $fiscalCode
   * @param $subject
   * @param $markdown
   * @return array
   */
  private function sendMessage($primaryKey, $fiscalCode, $subject, $markdown): array
  {
    $converter = new HtmlConverter();
    // HTML links are converted to clickable Markdown links
    $converter->getConfig()->setOption('use_autolinks', false);
    $client = new Client(['base_uri' => $this->IOApiUrl]);

    $data = [
      'content' => [
        'subject' => $subject,
        'markdown' => $converter->convert($markdown),
      ],
      'fiscal_code' => strtoupper($fiscalCode)
    ];

    $request = new Request(
      'POST',
      $client->getConfig('base_uri') . '/messages',
      ['Content-Type' => 'application/json', 'Ocp-Apim-Subscription-Key' => $primaryKey],
      json_encode($data)
    );

    try {
      $response = $client->send($request);
      return json_decode($response->getBody(), true);
    } catch (\Throwable $exception) {
      $this->logger->error($exception->getMessage());
      return json_decode($exception->getResponse()->getBody()->getContents(), true);
    }
  }

  public function test($serviceId, $primaryKey, $secondaryKey, $fiscalCode): array
  {
    $profileResponse = $this->getProfile($primaryKey, $fiscalCode);
    if (array_key_exists("detail", $profileResponse)) {
      return ["error"=> $profileResponse["detail"]];
    }

    if (array_key_exists("message", $profileResponse)) {
      return ["error"=> $profileResponse["message"]];
    }

    if (!$profileResponse['sender_allowed']) {
      return ['error' => "Sender not allowed"];
    }

    $messageResponse =$this->sendMessage(
      $primaryKey,
      $fiscalCode,
      $this->translator->trans('app_io.test.subject'),
      $this->translator->trans('app_io.test.markdown')
    );

    if (array_key_exists("detail", $profileResponse)) {
      return ["error"=> $profileResponse["detail"]];
    }

    if (array_key_exists("message", $profileResponse)) {
      return ["error"=> $profileResponse["message"]];
    }
    return $messageResponse;
  }

  public function sendMessageForPratica(Pratica $pratica, $message, $subject): int
  {
    $sentAmount = 0;

    if (!$pratica->getEnte()->isIOEnabled() || !$pratica->getServizio()->isIOEnabled()) {
      return $sentAmount;
    }

    $primaryKey = $pratica->getServizio()->getIOServiceParameters()["primaryKey"];
    $fiscalCode = $pratica->getUser()->getCodiceFiscale();

    $profileResponse = $this->getProfile($primaryKey, $fiscalCode);
    if (array_key_exists("detail", $profileResponse)) {
      $this->logger->debug("Error in sendMessageForPratica: " . $profileResponse["detail"]);
      return $sentAmount;
    }

    if (array_key_exists("message", $profileResponse)) {
      $this->logger->debug("Error in sendMessageForPratica: " . $profileResponse["message"]);
      return $sentAmount;
    }

    if (!$profileResponse['sender_allowed']) {
      $this->logger->debug("Error in sendMessageForPratica: sender " . $fiscalCode . ' not allowed');
      return $sentAmount;
    }

    $messageResponse =$this->sendMessage(
      $primaryKey,
      $fiscalCode,
      $subject,
      $message
    );

    if (array_key_exists("detail", $messageResponse)) {
      $this->logger->debug("Error in sendMessageForPratica: " . $messageResponse["detail"]);
      return $sentAmount;
    }

    if (array_key_exists("message", $messageResponse)) {
      $this->logger->debug("Error in sendMessageForPratica: " . $messageResponse["messageA"]);
      return $sentAmount;
    }

    if (array_key_exists("id", $messageResponse)) {
      ++$sentAmount;
    }

    return $sentAmount;
  }

}
