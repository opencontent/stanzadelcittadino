<?php

namespace App\Services;

use App\Dto\UserAuthenticationData;
use App\Entity\CPSUser;
use App\Entity\User;
use App\Entity\UserSession;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserSessionService
{
  private SessionInterface $session;
  private EntityManagerInterface $entityManager;
  private EntityRepository $repository;
  private Request $request;
  private ?UserSession $currentUserSessionData = null;
  private LoggerInterface $logger;

  public function __construct(
    SessionInterface       $session,
    EntityManagerInterface $entityManager,
    LoggerInterface        $logger
  )
  {
    $this->session = $session;
    $this->entityManager = $entityManager;
    $this->repository = $this->entityManager->getRepository(UserSession::class);
    $this->request = Request::createFromGlobals();
    $this->logger = $logger;
  }

  public function getCurrentUserAuthenticationData(User $currentUser = null): ?UserAuthenticationData
  {
    $userSessionData = $this->getCurrentUserSessionData($currentUser);
    if ($userSessionData) {
      return $userSessionData->getAuthenticationData();
    }
    return null;
  }

  public function getCurrentUserSessionData(User $currentUser = null): ?UserSession
  {
    if ($this->currentUserSessionData === null) {
      if ($this->session->has('sdc_user_session_data')) {
        $this->currentUserSessionData = $this->repository->find($this->session->get('sdc_user_session_data'));
      }
      if (!$this->currentUserSessionData instanceof UserSession && $currentUser instanceof User) {
        $this->createCurrentUserSessionData($currentUser);
      }
    }
    return $this->currentUserSessionData;
  }

  public function getLastUserSessionData(User $user): ?UserSession
  {
    return $this->repository->findOneBy(['userId'=> $user->getId()], ['createdAt' => 'DESC']);
  }

  public function createCurrentUserSessionData(
    UserInterface          $currentUser,
    array                  $sessionData = [],
    UserAuthenticationData $authenticationData = null,
    bool                   $store = false
  ): bool
  {
    $userSession = new UserSession();
    $userSession->setUserId($currentUser->getId());
    $userSession->setEnvironment($this->request->headers->get('User-Agent', ''));
    $userSession->setClientIp($this->request->getClientIp() ?? '');
    $userSession->setSuspiciousActivity(false);
    $userSession->setSessionData($sessionData);

    if (!$authenticationData) {
      $authenticationData = UserAuthenticationData::fromArray([
        'authenticationMethod' => CPSUser::IDP_NONE,
      ]);
    }
    $userSession->setAuthenticationData($authenticationData);

    $this->logger->info('Creating UserSession data', ['user' => $currentUser, 'data' => $userSession]);
    $this->entityManager->persist($userSession);

    if ($store) {
      $this->entityManager->flush();
    }

    $this->currentUserSessionData = $userSession;
    $this->session->set('sdc_user_session_data', $userSession->getId());

    return true;
  }

  public function storeCurrentUserSessionData(
    UserInterface          $currentUser,
    array                  $sessionData = [],
    UserAuthenticationData $authenticationData = null
  ): bool
  {
    return $this->createCurrentUserSessionData($currentUser, $sessionData, $authenticationData, true);
  }
}
