<?php


namespace App\Services;


use App\Entity\AdminUser;
use App\Entity\Categoria;
use App\Entity\ServiceGroup;
use App\Entity\Servizio;
use App\Entity\User;
use App\Utils\LocaleUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class BreadcrumbsService
{
  /**
   * @var Breadcrumbs
   */
  private $breadcrumbs;
  /**
   * @var RouterInterface
   */
  private $router;
  /**
   * @var InstanceService
   */
  private $instanceService;

  /**
   * BreadcrumbsService constructor.
   * @param Breadcrumbs $breadcrumbs
   * @param RouterInterface $router
   * @param InstanceService $instanceService
   */
  public function __construct(Breadcrumbs $breadcrumbs, RouterInterface $router, InstanceService $instanceService)
  {
    $this->breadcrumbs = $breadcrumbs;
    $this->router = $router;
    $this->instanceService = $instanceService;
    $this->init();

  }

  private function init()
  {
    if ($this->instanceService->getCurrentInstance()->getSiteUrl()) {
      $this->breadcrumbs->addItem('Home', $this->instanceService->getCurrentInstance()->getSiteUrl());
    }

  }

  /**
   * @return Breadcrumbs
   */
  public function getBreadcrumbs(): Breadcrumbs
  {
    return $this->breadcrumbs;
  }

  public function getBackendBreadcrumbs(User $user): Breadcrumbs
  {
    $this->breadcrumbs->clear();
    if ($user instanceof AdminUser) {
      $this->breadcrumbs->addRouteItem('Dashboard', 'admin_index');
    }

    return $this->breadcrumbs;
  }

  /**
   * @param Categoria $category
   */
  public function generateCategoryBreadcrumbs(Categoria $category)
  {
    $this->breadcrumbs->addRouteItem('nav.servizi', 'servizi_list');
    $router = $this->router;
    $this->breadcrumbs->addObjectTree($category, 'name', function($object) use ($router) {
      return $router->generate('category_show', ['slug' => $object->getSlug()]);
    }, 'parent', [], 2);
  }

  /**
   * @param ServiceGroup $serviceGroup
   */
  public function generateServiceGroupBreadcrumbs(ServiceGroup $serviceGroup)
  {
    $this->breadcrumbs->addRouteItem('nav.servizi', 'servizi_list');
    if ($serviceGroup->getTopics()) {
      $router = $this->router;
      $this->breadcrumbs->addObjectTree($serviceGroup->getTopics(), 'name', function($object) use ($router) {
        return $router->generate('category_show', ['slug' => $object->getSlug()]);
      }, 'parent', [], 2);
    }
    $this->breadcrumbs->addRouteItem($serviceGroup->getName(), "service_group_show", [
      'slug' => $serviceGroup->getSlug(),
    ]);
  }

  /**
   * @param Servizio $service
   * @param Request $request
   */
  public function generateServiceBreadcrumbs(Servizio $service, Request $request)
  {
    $this->breadcrumbs->addRouteItem('nav.servizi', 'servizi_list');
    if ($service->getServiceGroup() && $service->isSharedWithGroup()) {
      $this->generateServiceGroupBreadcrumbs($service->getServiceGroup());
    } else {
      $this->generateTopicUrl($service, $request);
      if ($service->getServiceGroup()) {
        $this->breadcrumbs->addRouteItem($service->getServiceGroup()->getName(), "service_group_show", [
          'slug' => $service->getServiceGroup()->getSlug(),
        ]);
      }
      $this->breadcrumbs->addRouteItem($service->getName(), "servizi_show", [
        'slug' => $service->getSlug(),
      ]);
    }
  }

  public function generateCompileApplicationBreadcrumbs(Servizio $service, Request $request): void
  {
    $this->breadcrumbs->addRouteItem('nav.servizi', 'servizi_list');
    if ($service->getServiceGroup() && $service->isSharedWithGroup()) {
      $this->generateServiceGroupBreadcrumbs($service->getServiceGroup());
    } else {
      $this->generateTopicUrl($service, $request);
      if ($service->getServiceGroup()) {
        $this->breadcrumbs->addRouteItem($service->getServiceGroup()->getName(), "service_group_show", [
          'slug' => $service->getServiceGroup()->getSlug(),
        ]);
      }
    }
    $this->breadcrumbs->addRouteItem($service->getName(), "servizi_show", ['slug' => $service->getSlug(),]);
    $this->breadcrumbs->addItem('breadcrumbs.compile');

  }


  private function generateTopicUrl(Servizio $service, Request $request): void
  {
    if (!$service->getTopics()) {
      return;
    }

    $locale = $request->getLocale() ?? LocaleUtils::DEFAULT_LOCALE;
    $tenant = $service->getEnte();
    $catalogueIsEnabled = $tenant->isSearchAndCatalogueEnabled();
    $topics = $service->getTopics();

    if ($catalogueIsEnabled) {
      $router = $this->router;
      $this->breadcrumbs->addObjectTree($topics, 'name', function ($object) use ($router) {
        return $router->generate('category_show', ['slug' => $object->getSlug()]);
      }, 'parent', [], 2);
    } else {
      $meta = $tenant->getMetaAsArray();
      if (empty($meta['external_categories_url'])) {
        return;
      }
      $externalCategoryUrlType = $meta['external_categories_url']['type'];
      $url = '';
      switch ($externalCategoryUrlType) {
        case 'opencity':
          $url = '/Servizi/(view)/' . $topics->getName() . '?locale=' . $locale;
          break;
        case 'golem':
          $url =  '/tipo_servizio/' . $topics->getSlug();
          break;
        case 'epublic':
          // Fixme: come gestire locale?
          $url =  '/it-it/servizi/' . $topics->getSlug();
          break;
        case 'custom':
          $externalCategoryCustomStructure = $meta['external_categories_url']['custom'];
          $url = str_replace(['%category_name%', '%category_slug%', '%locale%'], [$topics->getName(), $topics->getSlug(), $locale], $externalCategoryCustomStructure);
          break;
      }
      $url = rtrim($tenant->getSiteUrl(), '/') . $url;
      $this->breadcrumbs->addItem($topics->getName(), $url);
    }
  }
}
