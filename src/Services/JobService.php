<?php

namespace App\Services;

use App\Entity\Job;
use App\Entity\ScheduledAction;
use App\Exception\DelayedScheduledActionException;
use App\Handlers\Job\JobHandlerInterface;
use App\Handlers\Job\JobHandlerRegistry;
use App\ScheduledAction\Exception\AlreadyScheduledException;
use App\ScheduledAction\ScheduledActionHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Exception\ValidatorException;

class JobService implements ScheduledActionHandlerInterface
{
  const ACTION_PROCESS_JOB = 'process_job';

  private ScheduleActionService $scheduleActionService;
  private EntityManagerInterface $entityManager;
  private JobHandlerRegistry $jobHandlerRegistry;
  private LoggerInterface $logger;


  /**
   * @param ScheduleActionService $scheduleActionService
   * @param EntityManagerInterface $entityManager
   * @param JobHandlerRegistry $jobHandlerRegistry
   * @param LoggerInterface $logger
   */
  public function __construct(
    ScheduleActionService $scheduleActionService,
    EntityManagerInterface $entityManager,
    JobHandlerRegistry $jobHandlerRegistry,
    LoggerInterface $logger
  ) {
    $this->scheduleActionService = $scheduleActionService;
    $this->entityManager = $entityManager;
    $this->jobHandlerRegistry = $jobHandlerRegistry;
    $this->logger = $logger;
  }


  /**
   * @param Job $job
   * @return void
   * @throws ValidatorException
   */
  public function processJobAsync(Job $job): void
  {

    $params = [
      'job' => $job->getId()->toString(),
    ];
    $handler = $this->jobHandlerRegistry->getByName($job->getType());
    if ($handler instanceof JobHandlerInterface) {
      $handler->validateJob($job);
    }

    try {
      $this->scheduleActionService->appendAction(
        'ocsdc.job_service',
        self::ACTION_PROCESS_JOB,
        serialize($params),
      );
    } catch (AlreadyScheduledException $e) {
      $this->logger->error('Job is already scheduled', $params);
    }
  }

  /**
   * @throws Exception|GuzzleException|DelayedScheduledActionException
   */
  public function executeScheduledAction(ScheduledAction $action)
  {
    $params = unserialize($action->getParams());
    if ($action->getType() == self::ACTION_PROCESS_JOB) {
      /** @var Job $job */
      $job = $this->entityManager->getRepository(Job::class)->find($params['job']);
      if (!$job instanceof Job) {
        throw new Exception('Not found job with id: ' . $params['job']);
      }

      $handler = $this->jobHandlerRegistry->getByName($job->getType());
      if ($handler instanceof JobHandlerInterface) {

        if (!$job->canBeRun()) {

          throw new Exception('Job cannot be started ' . $job->getId());
        }

        $job->run();
        $this->entityManager->persist($job);
        $this->entityManager->flush();

        $handler->processJob($job);

        if ($job->isSuspended()) {

          $message = sprintf('Job \'%s\' is suspended', $job->getId());
          throw new DelayedScheduledActionException($message);
        }

      }
    }
  }
}
