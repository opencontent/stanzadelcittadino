<?php

namespace App\Services;

use App\Entity\IntegrabileInterface;
use App\Entity\Message;
use App\Entity\RichiestaIntegrazioneDTO;
use App\Entity\RichiestaIntegrazioneRequestInterface;
use App\Entity\Pratica;
use App\Entity\RispostaOperatoreDTO;
use App\Entity\StatusChange;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Contracts\Translation\TranslatorInterface;

class PraticaIntegrationService
{
  private EntityManagerInterface $em;

  private PraticaStatusService $statusService;

  private ModuloPdfBuilderService $pdfBuilder;

  private TranslatorInterface $translator;

  public function __construct(
    EntityManagerInterface  $em,
    PraticaStatusService    $statusService,
    ModuloPdfBuilderService $pdfBuilder,
    TranslatorInterface     $translator
  )
  {
    $this->em = $em;
    $this->statusService = $statusService;
    $this->pdfBuilder = $pdfBuilder;
    $this->translator = $translator;
  }

  /**
   * @param Pratica $pratica
   * @param RichiestaIntegrazioneDTO $integration
   *
   * @throws Exception
   */
  public function requestIntegration(Pratica $pratica, RichiestaIntegrazioneDTO $integration)
  {
    if ($pratica instanceof IntegrabileInterface) {

      $this->statusService->validateChangeStatus($pratica, Pratica::STATUS_REQUEST_INTEGRATION);

      $message = new Message();
      $message->setApplication($pratica);
      $message->setProtocolRequired(false);
      $message->setVisibility(Message::VISIBILITY_APPLICANT);
      $message->setMessage($integration->getMessage());
      $message->setSubject($this->translator->trans('pratica.messaggi.oggetto', ['%pratica%' => $message->getApplication()]));

      $integration = $this->pdfBuilder->creaModuloProtocollabilePerRichiestaIntegrazione($pratica, $integration);
      $pratica->addRichiestaIntegrazione($integration);
      $pratica->setInstanceId(null);

      // In caso di presa in carico da parte di Giscom l'utente è in memory, non viene quindi inseriro un uuid operatore. Non va usato il messageManager
      $this->em->persist($message);

      $this->em->persist($pratica);
      $this->em->flush();

      $statusChange = new StatusChange();
      $statusChange->setMessageId($message->getId());
      $this->statusService->setNewStatus($pratica, Pratica::STATUS_REQUEST_INTEGRATION, $statusChange);
    } else {
      throw new \InvalidArgumentException("Pratica must be implements " . IntegrabileInterface::class . " interface");
    }
  }


  /**
   * @param Pratica $pratica
   * @param RispostaOperatoreDTO $risposta
   * @throws Exception
   */
  public function createRispostaOperatore(Pratica $pratica, RispostaOperatoreDTO $risposta)
  {
    $risposta = $this->pdfBuilder->creaRispostaOperatore($pratica, $risposta);
    $pratica->addRispostaOperatore($risposta);
    $pratica->setInstanceId(null);

    $this->em->persist($pratica);
    $this->em->flush();
  }
}
