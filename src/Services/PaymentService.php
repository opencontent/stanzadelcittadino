<?php

namespace App\Services;

use App\Entity\FormIO;
use App\Entity\Pratica;
use App\Entity\ScheduledAction;
use App\Form\Admin\Servizio\PaymentDataType;
use App\Model\Payment;
use App\Model\Payment\Payer;
use App\Model\Payment\PaymentTransaction;
use App\Model\Service\PaymentConfig;
use App\Payment\Gateway\Bollo;
use App\Payment\Gateway\MyPay;
use App\Payment\GatewayCollection;
use App\ScheduledAction\ScheduledActionHandlerInterface;
use DateTimeInterface;
use Exception;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpOptions;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;


class PaymentService implements ScheduledActionHandlerInterface
{
  public const SCHEDULED_CREATE_PAYMENT = 'create_payment';

  private static $detailColumns = [
    'ID',
    'USER_ID',
    'TYPE',
    'TENANT_ID',
    'SERVICE_ID',
    'CREATED_AT',
    'UPDATED_AT',
    'STATUS',
    'REASON',
    'REMOTE_ID',
    'PAYMENT_TRANSACTION_ID',
    'PAYMENT_PAID_AT',
    'PAYMENT_EXPIRE_AT',
    'PAYMENT_AMOUNT',
    'PAYMENT_CURRENCY',
    'PAYMENT_NOTICE_CODE',
    'PAYMENT_IUD',
    'PAYMENT_IUV',
    'ONLINE_PAYMENT_BEGIN_URL',
    'ONLINE_PAYMENT_BEGIN_LAST_OPENED_AT',
    'ONLINE_PAYMENT_BEGIN_METHOD',
    'ONLINE_PAYMENT_LANDING_URL',
    'ONLINE_PAYMENT_LANDING_LAST_OPENED_AT',
    'ONLINE_PAYMENT_LANDING_METHOD',
    'OFFLINE_PAYMENT_URL',
    'OFFLINE_PAYMENT_LAST_OPENED_AT',
    'OFFLINE_PAYMENT_METHOD',
    'RECEIPT_URL',
    'RECEIPT_LAST_OPENED_AT',
    'RECEIPT_METHOD',
    'UPDATE_URL',
    'UPDATE_LAST_CHECK_AT',
    'UPDATE_NEXT_CHECK_AT',
    'UPDATE_METHOD',
    'PAYER_TYPE',
    'PAYER_TAX_IDENTIFICATION_NUMBER',
    'PAYER_NAME',
    'PAYER_FAMILY_NAME',
    'PAYER_STREET_NAME',
    'BUILDING_NUMBER',
    'POSTAL_CODE',
    'TOWN_NAME',
    'COUNTRY_SUBDIVISION',
    'COUNTRY',
    'EMAIL',
    'EVENT_ID',
    'EVENT_VERSION',
    'EVENT_CREATED_AT',
    'APP_ID',
  ];

  private static $activeColumns = [
    'ID',
    'TENANT_ID',
    'USER_ID',
    'SERVICE_ID',
    'REMOTE_ID',
    'STATUS',
    'REASON',
    'PAYER_NAME',
    'PAYER_FAMILY_NAME ',
    'PAYER_TAX_IDENTIFICATION_NUMBER',
    'PAYER_STREET_NAME',
    'BUILDING_NUMBER',
    'POSTAL_CODE',
    'TOWN_NAME',
    'COUNTRY_SUBDIVISION',
    'COUNTRY',
    'PAYMENT_IUD',
    'PAYMENT_IUV',
    'PAYMENT_NOTICE_CODE',
    'PAYMENT_AMOUNT',
    'PAYMENT_EXPIRE_AT',
    'PAYMENT_EXPIRE_AT_EPOCH',
    'CREATED_AT',
    'CREATED_AT_EPOCH',
    'UPDATED_AT',
    'UPDATED_AT_EPOCH',
    'ONLINE_PAYMENT_BEGIN_URL',
    'ONLINE_PAYMENT_BEGIN_METHOD',
    'OFFLINE_PAYMENT_URL',
    'OFFLINE_PAYMENT_METHOD',
  ];

  private static $completeColumns = [
    'ID',
    'TENANT_ID',
    'USER_ID',
    'SERVICE_ID',
    'REMOTE_ID',
    'STATUS',
    'REASON',
    'PAYER_NAME',
    'PAYER_FAMILY_NAME ',
    'PAYER_TAX_IDENTIFICATION_NUMBER',
    'PAYER_STREET_NAME',
    'BUILDING_NUMBER',
    'POSTAL_CODE',
    'TOWN_NAME',
    'COUNTRY_SUBDIVISION',
    'COUNTRY',
    'PAYMENT_IUD',
    'PAYMENT_IUV',
    'PAYMENT_NOTICE_CODE',
    'PAYMENT_AMOUNT',
    'PAYMENT_EXPIRE_AT',
    'PAYMENT_EXPIRE_AT_EPOCH',
    'CREATED_AT',
    'CREATED_AT_EPOCH',
    'UPDATED_AT',
    'UPDATED_AT_EPOCH',
    'PAYMENT_PAID_AT',
    'PAYMENT_PAID_AT_EPOCH',
    'RECEIPT_URL',
    'RECEIPT_METHOD',
  ];

  private RouterInterface $router;

  private LoggerInterface $logger;

  private HttpClientInterface $httpClient;

  private array $gateways = [];

  private string $ksqlDBUrl;
  private string $paymentDispatcherApiUrl;
  private GatewayCollection $gatewayCollection;
  private ScheduleActionService $scheduleActionService;
  private InstanceService $instanceService;


  /**
   * @param RouterInterface $router
   * @param LoggerInterface $logger
   * @param GatewayCollection $gatewayCollection
   * @param HttpClientInterface $httpClient
   * @param string $ksqlDBUrl
   * @param string $paymentDispatcherApiUrl
   * @param ScheduleActionService $scheduleActionService
   * @param InstanceService $instanceService
   */
  public function __construct(
    RouterInterface       $router,
    LoggerInterface       $logger,
    GatewayCollection     $gatewayCollection,
    HttpClientInterface   $httpClient,
    string                $ksqlDBUrl,
    string                $paymentDispatcherApiUrl,
    ScheduleActionService $scheduleActionService,
    InstanceService       $instanceService
  )
  {
    $this->router = $router;
    $this->logger = $logger;
    $this->ksqlDBUrl = $ksqlDBUrl;
    $this->httpClient = $httpClient;

    // Recupero la lista dei gateway payment proxy disponibili
    foreach ($gatewayCollection->getAvailablePaymentGateways() as $k => $g) {
      if (!in_array($k, [Bollo::IDENTIFIER, MyPay::IDENTIFIER], true)) {
        $this->gateways[$k] = $g;
      }
    }
    $this->paymentDispatcherApiUrl = $paymentDispatcherApiUrl;
    $this->gatewayCollection = $gatewayCollection;
    $this->scheduleActionService = $scheduleActionService;
    $this->instanceService = $instanceService;
  }

  /**
   * @param Pratica $pratica
   * @param $amount
   * @return array
   * @throws Exception
   */
  public function createDefferedPaymentData(Pratica $pratica, $amount): array
  {
    $paymentData = $this->createPaymentData($pratica);
    $paymentData['deferred'] = true;
    $paymentData['amount'] = $amount;
    return $paymentData;
  }

  /**
   * @param FormIO $pratica
   * @return array
   */
  public function createPaymentData(FormIO $pratica): array
  {
    $paymentData = [];
    $data = $pratica->getDematerializedForms();

    // Amount
    $paymentData['amount'] = $pratica->getPaymentAmount();
    if ($paymentData['amount'] && isset($data['data'][PaymentDataType::PAYMENT_FINANCIAL_REPORT])) {
      $paymentData['split'] = $data['data'][PaymentDataType::PAYMENT_FINANCIAL_REPORT];
    }

    // Reason
    if (isset($data['flattened'][PaymentDataType::PAYMENT_DESCRIPTION]) && !empty($data['flattened'][PaymentDataType::PAYMENT_DESCRIPTION])) {
      $paymentData['reason'] = $data['flattened'][PaymentDataType::PAYMENT_DESCRIPTION];
    } else {
      $paymentData['reason'] = $pratica->getId() . ' - ' . $pratica->getUser()->getCodiceFiscale();
    }

    if (!isset($paymentData['split'])) {
      $paymentData['split'] = null;
    }

    $paymentDayLifeTime = 90;
    $paymentData['expire_at'] = (new \DateTime())->modify('+' . $paymentDayLifeTime . 'days')->format(DateTimeInterface::W3C);
    $paymentData['notify'] = [
      'url' => $this->generateNotifyUrl($pratica),
      'method' => 'POST',
    ];

    $paymentData['landing'] = [
      'url' => $this->generateCallbackUrl($pratica),
      'method' => 'GET',
    ];
    return $paymentData;
  }

  /**
   * @throws Exception
   */
  public function createPayment(FormIO $pratica): void
  {
    $data = $pratica->getDematerializedForms();
    $amount = $pratica->getFormPaymentAmount();
    $split = null;
    $reason = null;

    // Split
    if (isset($data['data'][PaymentDataType::PAYMENT_FINANCIAL_REPORT])) {
      $split = $data['data'][PaymentDataType::PAYMENT_FINANCIAL_REPORT];
    }

    // Reason
    if (isset($data['flattened'][PaymentDataType::PAYMENT_DESCRIPTION]) && !empty($data['flattened'][PaymentDataType::PAYMENT_DESCRIPTION])) {
      $reason = $data['flattened'][PaymentDataType::PAYMENT_DESCRIPTION];
    }

    $requestPaymentConfigs = $pratica->getServizio()->getRequestPaymentConfigs();

    if (empty($requestPaymentConfigs)) {
      return;
    }

    $user = $pratica->getUser();
    $payer = Payer::fromUser($user);
    $paymentLinks = $this->createPaymentLinks($pratica);

    $serializer = SerializerBuilder::create()->build();

    /** @var PaymentConfig $config */
    foreach ($requestPaymentConfigs as $config) {
      $context = $this->createSerializationContext();
      $payment = new Payment();
      $payment->setConfigId($config->getId());
      $payment->setUserId($user->getId());
      $payment->setTenantId($pratica->getEnte()->getId());
      $payment->setRemoteCollection(Payment\RemoteCollection::init($pratica->getId(), 'application'));
      $payment->setLinks($paymentLinks);
      $payment->setCreatedAt(new \DateTime());
      $paymentTransaction = $this->generatePaymentTransaction($amount, $reason, $split);
      $payment->setPayment($paymentTransaction);
      $payment->setDebtor($payer);

      $payload = $serializer->toArray($payment, $context);
      $this->postPayment($pratica, $config->getId(), $payload);
    }
  }

  public function createDeferredPayment(FormIO $pratica, array $paymentConfigs): void
  {

    if (empty($paymentConfigs)) {
      return;
    }

    $user = $pratica->getUser();
    $payer = Payer::fromUser($user);
    $paymentLinks = $this->createPaymentLinks($pratica);

    $serializer = SerializerBuilder::create()->build();

    /** @var PaymentConfig $config */
    foreach ($paymentConfigs as $config) {
      $context = $this->createSerializationContext();
      $payment = new Payment();
      $payment->setConfigId($config->getId());
      $payment->setUserId($user->getId());
      $payment->setTenantId($pratica->getEnte()->getId());
      $payment->setRemoteCollection(Payment\RemoteCollection::init($pratica->getId(), 'application'));
      $payment->setLinks($paymentLinks);
      $payment->setCreatedAt(new \DateTime());
      $paymentTransaction = $this->generatePaymentTransaction();
      $payment->setPayment($paymentTransaction);
      $payment->setDebtor($payer);

      $payload = $serializer->toArray($payment, $context);
      $this->postPayment($pratica, $config->getId(), $payload);
    }
  }

  private function postPayment(FormIO $pratica, string $paymentConfigId, $payload): void
  {
    try {
      $url = $this->paymentDispatcherApiUrl . '/payment';
      $options = (new HttpOptions())->setJson($payload);
      $this->httpClient->request('POST', $url, $options->toArray());
    } catch (\Throwable $e) {
      $this->logger->error('Error during payment creation', [
        'application_id' => $pratica->getId(),
        'config_id' => $paymentConfigId,
        'error' => $e->getMessage(),
      ]);
      $this->createPaymentAsync($payload);
    }
  }


  /**
   * @param Pratica $application
   * @param array $filters
   * @return array
   * @throws Exception
   */
  public function getPaymentStatusByApplication(Pratica $application, array $filters = []): array
  {
    $data = [];
    $curl = curl_init();
    $query = "SELECT id, reason, status, created_at, updated_at, online_payment_begin_url, online_payment_begin_method,
              online_payment_landing_url, online_payment_landing_method, offline_payment_url, offline_payment_method,
              cancel_url, cancel_method
              FROM payments_detail WHERE remote_id = '{$application->getId()}'";

    if (!empty($filters['status'])) {
      $statuses = explode(',', $filters['status']);
      $upperCasedStatuses = array_map('strtoupper', $statuses);
      $statuses = implode("','", $upperCasedStatuses);
      $query .= " AND status IN ('" . $statuses . "')";
    }

    $payload = [
      'ksql' => $query . ';',
    ];

    $url = $this->ksqlDBUrl . '/query';
    curl_setopt_array($curl, [
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($payload),
      CURLOPT_HTTPHEADER => [
        "Accept: application/vnd.ksql.v1+json",
        "Content-Type: application/json",
      ],
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
      throw new Exception($err);
    }

    $responseData = json_decode($response, true, 512, JSON_THROW_ON_ERROR);

    if (empty($responseData[1])) {
      return $data;
    }

    $data = [
      "id" => $responseData[1]['row']['columns'][0],
      "reason" => $responseData[1]['row']['columns'][1],
      "status" => $responseData[1]['row']['columns'][2],
      "created_at" => $responseData[1]['row']['columns'][3],
      "updated_at" => $responseData[1]['row']['columns'][4],
    ];

    if (strtoupper($data['status']) === 'PAYMENT_PENDING' || strtoupper($data['status']) === 'PAYMENT_STARTED') {
      $data['links'] = [
        'online_payment_begin' => [
          'url' => $responseData[1]['row']['columns'][5],
          'method' => $responseData[1]['row']['columns'][6],
        ],
        'online_payment_landing' => [
          'url' => $responseData[1]['row']['columns'][7],
          'method' => $responseData[1]['row']['columns'][8],
        ],
        'offline_payment' => [
          'url' => $responseData[1]['row']['columns'][9],
          'method' => $responseData[1]['row']['columns'][10],
        ],
        'cancel_payment' => [
          'url' => $responseData[1]['row']['columns'][11],
          'method' => $responseData[1]['row']['columns'][12],
        ],
      ];
    }

    return $data;
  }

  /**
   * @param array $criteria
   * @return array
   * @throws Exception
   */
  public function getPayments(array $criteria): array
  {

    $columns = implode(', ', self::$detailColumns);
    $query = "SELECT {$columns} FROM payments_detail";

    if (!empty($criteria)) {
      $query .= ' WHERE ';
      $filters = [];

      // after|before|strictly_after|strictly_before
      $this->addDateFilters($filters, $criteria, 'created_at', 'created_at');
      $this->addDateFilters($filters, $criteria, 'paid_at', 'payment_paid_at');
      $this->addDateFilters($filters, $criteria, 'expire_at', 'payment_expire_at');

      if (!empty($criteria['status'])) {
        $statuses = explode(',', $criteria['status']);
        $upperCasedStatuses = array_map('strtoupper', $statuses);
        $statuses = implode("','", $upperCasedStatuses);
        $filters [] = "status IN ('" . $statuses . "')";
        unset($criteria['status']);
      }

      foreach ($criteria as $key => $value) {
        $filters [] = "{$key} = '{$value}'";
      }

      $query .= implode(' AND ', $filters);
    }

    $query .= ';';

    return $this->selectQuery($query, self::$detailColumns);
  }

  /**
   * @param array $criteria
   * @return array
   * @throws Exception
   */
  public function getPaymentsToPay(array $criteria): array
  {

    $columns = implode(', ', self::$activeColumns);
    $query = "SELECT {$columns} FROM payments_active";

    $criteria['tenant_id'] = (string)$this->instanceService->getCurrentInstance()->getId();

    if (!empty($criteria)) {
      $query .= ' WHERE ';
      $filters = [];

      // after|before|strictly_after|strictly_before
      $this->addDateMillisecondFilters($filters, $criteria, 'expire_at', 'payment_expire_at_epoch');

      foreach ($criteria as $key => $value) {
        $filters [] = "{$key} = '{$value}'";
      }

      $query .= implode(' AND ', $filters);
    }
    $query .= ';';

    return $this->selectQuery($query, self::$activeColumns);
  }

  /**
   * @throws Exception
   */
  public function getPaymentsPaid(array $criteria): array
  {
    $columns = implode(', ', self::$completeColumns);
    $query = "SELECT {$columns} FROM payments_complete";

    $criteria['tenant_id'] = (string)$this->instanceService->getCurrentInstance()->getId();

    if (!empty($criteria)) {
      $query .= ' WHERE ';
      $filters = [];

      // after|before|strictly_after|strictly_before
      $this->addDateMillisecondFilters($filters, $criteria, 'paid_at', 'payment_paid_at_epoch');

      foreach ($criteria as $key => $value) {
        $filters [] = "{$key} = '{$value}'";
      }
      $query .= implode(' AND ', $filters);
    }
    $query .= ';';

    return $this->selectQuery($query, self::$completeColumns);
  }

  /**
   * @throws Exception
   */
  public function getPayment($id): array
  {

    $columns = implode(', ', self::$detailColumns);
    $query = "SELECT {$columns} FROM payments_detail WHERE id = '{$id}';";

    return $this->selectQuery($query, self::$detailColumns);
  }

  /**
   * @throws Exception
   */
  private function selectQuery($query, $columns): array
  {
    $data = [];
    $curl = curl_init();

    $payload = [
      'ksql' => $query,
    ];

    $url = $this->ksqlDBUrl . '/query';
    curl_setopt_array($curl, [
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($payload),
      CURLOPT_HTTPHEADER => [
        "Accept: application/vnd.ksql.v1+json",
        "Content-Type: application/json",
      ],
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
      throw new Exception($err);
    }

    $responseData = json_decode($response, true, 512, JSON_THROW_ON_ERROR);

    if (empty($responseData[1])) {
      return $data;
    }

    // Elimino la riga con l'header
    unset($responseData[0]);
    foreach ($responseData as $v) {
      $data [] = $this->mapPaymentFromKsqlDBRow($v['row']['columns'], $columns);
    }
    return $data;
  }

  private function mapPaymentFromKsqlDBRow(array $row, array $columns): array
  {
    $mappedColumns = [];
    foreach ($row as $key => $value) {
      $mappedColumns[$columns[$key]] = $value;
    }
    return [
      'id' => $mappedColumns['ID'] ?? null,
      'user_id' => $mappedColumns['USER_ID'] ?? null,
      'type' => $mappedColumns['TYPE'] ?? null,
      'tenant_id' => $mappedColumns['TENANT_ID'] ?? null,
      'service_id' => $mappedColumns['SERVICE_ID'] ?? null,
      'created_at' => $mappedColumns['CREATED_AT'] ?? null,
      'updated_at' => $mappedColumns['UPDATED_AT'] ?? null,
      'status' => $mappedColumns['STATUS'] ?? null,
      'reason' => $mappedColumns['REASON'] ?? null,
      'remote_id' => $mappedColumns['REMOTE_ID'] ?? null,
      'payment' => [
        'transaction_id' => $mappedColumns['PAYMENT_TRANSACTION_ID'] ?? null,
        'paid_at' => $mappedColumns['PAYMENT_PAID_AT'] ?? null,
        'expire_at' => $mappedColumns['PAYMENT_EXPIRE_AT'] ?? null,
        'amount' => $mappedColumns['PAYMENT_AMOUNT'] ?? null,
        'currency' => $mappedColumns['PAYMENT_CURRENCY'] ?? null,
        'notice_code' => $mappedColumns['PAYMENT_NOTICE_CODE'] ?? null,
        'iud' => $mappedColumns['PAYMENT_IUD'] ?? null,
        'iuv' => $mappedColumns['PAYMENT_IUV'] ?? null,
        'split' => [],
      ],
      'links' => [
        'online_payment_begin' => [
          'url' => $mappedColumns['ONLINE_PAYMENT_BEGIN_URL'] ?? null,
          'last_opened_at' => $mappedColumns['ONLINE_PAYMENT_BEGIN_LAST_OPENED_AT'] ?? null,
          'method' => $mappedColumns['ONLINE_PAYMENT_BEGIN_METHOD'] ?? null,
        ],
        'online_payment_landing' => [
          'url' => $mappedColumns['ONLINE_PAYMENT_LANDING_URL'] ?? null,
          'last_opened_at' => $mappedColumns['ONLINE_PAYMENT_LANDING_LAST_OPENED_AT'] ?? null,
          'method' => $mappedColumns['ONLINE_PAYMENT_LANDING_METHOD'] ?? null,
        ],
        'offline_payment' => [
          'url' => $mappedColumns['OFFLINE_PAYMENT_URL'] ?? null,
          'last_opened_at' => $mappedColumns['OFFLINE_PAYMENT_LAST_OPENED_AT'] ?? null,
          'method' => $mappedColumns['OFFLINE_PAYMENT_METHOD'] ?? null,
        ],
        'receipt' => [
          'url' => $mappedColumns['RECEIPT_URL'] ?? null,
          'last_opened_at' => $mappedColumns['RECEIPT_LAST_OPENED_AT'] ?? null,
          'method' => $mappedColumns['RECEIPT_METHOD'] ?? null,
        ],
        'notify' => [],
        'update' => [
          'url' => $mappedColumns['UPDATE_URL'] ?? null,
          'method' => $mappedColumns['UPDATE_METHOD'] ?? null,
          'last_check_at' => $mappedColumns['UPDATE_LAST_CHECK_AT'] ?? null,
          'next_check_at' => $mappedColumns['UPDATE_NEXT_CHECK_AT'] ?? null,
        ],
      ],
      'payer' => [
        'type' => $mappedColumns['PAYER_TYPE'] ?? null,
        'tax_identification_number' => $mappedColumns['PAYER_TAX_IDENTIFICATION_NUMBER'] ?? null,
        'name' => $mappedColumns['PAYER_NAME'] ?? null,
        'family_name' => $mappedColumns['PAYER_FAMILY_NAME'] ?? null,
        'street_name' => $mappedColumns['PAYER_STREET_NAME'] ?? null,
        'building_number' => $mappedColumns['BUILDING_NUMBER'] ?? null,
        'postal_code' => $mappedColumns['POSTAL_CODE'] ?? null,
        'town_name' => $mappedColumns['TOWN_NAME'] ?? null,
        'country_subdivision' => $mappedColumns['COUNTRY_SUBDIVISION'] ?? null,
        'country' => $mappedColumns['EMAIL'] ?? null,
        'email' => $mappedColumns['PAYMENT_PAID_AT'] ?? null,
      ],
      'event_id' => $mappedColumns['EVENT_ID'] ?? null,
      'event_version' => $mappedColumns['EVENT_VERSION'] ?? null,
      'event_created_at' => $mappedColumns['EVENT_CREATED_AT'] ?? null,
      'app_id' => $mappedColumns['APP_ID'] ?? null,
    ];
  }

  public function cancelApplicationPendingPayment(Pratica $application): void
  {
    $filters['status'] = implode(',', [Payment::STATUS_PAYMENT_PENDING, Payment::STATUS_PAYMENT_STARTED]);

    try {
      $payment = $this->getPaymentStatusByApplication($application, $filters);
      if (!empty($payment) && !(empty($payment['links']['cancel_payment'])) && $gateway = $this->getApplicationGatewayDetail($application)) {
        $url = $gateway['url'] . '/payments/' . $payment['id'];
        $options = (new HttpOptions())->setJson(['status' => 'CANCELED']);
        $this->httpClient->request('PATCH', $url, $options->toArray());
      }
    } catch (\Throwable $e) {
      $this->logger->error('Error cencelling pending payment for application: ' . $application->getId(), [
        'application_id' => $application->getId(),
        'message' => $e->getMessage()
      ]);
    }

  }


  public function createPaymentAsync($payload): void
  {
    $params = serialize([
      'payload' => $payload
    ]);
    try {
      $this->scheduleActionService->appendAction(
        'ocsdc.payment.service',
        self::SCHEDULED_CREATE_PAYMENT,
        $params
      );
    } catch (Exception $e) {
      $this->logger->error('Create payment async error:' . $e->getMessage());
    }
  }

  public function executeScheduledAction(ScheduledAction $action)
  {
    $params = unserialize($action->getParams(), [self::class]);
    if ($action->getType() === self::SCHEDULED_CREATE_PAYMENT) {
      $this->postPayment($params['payload']);
    }
  }


  private function getApplicationGatewayDetail(Pratica $application)
  {
    return $this->gateways[$application->getPaymentType()] ?? null;
  }

  private function createPaymentLinks(FormIO $pratica): Payment\PaymentLinks
  {
    $paymentLinks = new Payment\PaymentLinks();
    $paymentLinks->setOnlinePaymentLanding(Payment\PaymentLink::init($this->generateCallbackUrl($pratica)));
    $paymentLinks->setNotify([Payment\PaymentLink::init($this->generateNotifyUrl($pratica))]);
    return $paymentLinks;
  }

  private function createSerializationContext(): SerializationContext
  {
    $context = new SerializationContext();
    $context->setSerializeNull(true);
    $context->setVersion('2');
    $context->setGroups('write');
    return $context;
  }

  /**
   * @param Pratica $pratica
   * @return string
   */
  private function generateNotifyUrl(Pratica $pratica): string
  {
    return $this->router->generate('applications_payment_api_post', [
      'id' => $pratica->getId(),
    ], UrlGeneratorInterface::ABSOLUTE_URL);
  }

  /**
   * @param Pratica $pratica
   * @return string
   */
  private function generateCallbackUrl(Pratica $pratica): string
  {
    return $this->router->generate('pratiche_payment_callback', [
      'pratica' => $pratica->getId(),
    ], UrlGeneratorInterface::ABSOLUTE_URL);
  }

  private function generatePaymentTransaction($amount = null, $reason = null, $split = null): PaymentTransaction
  {
    $paymentTransaction = new PaymentTransaction();
    if ($amount) {
      $paymentTransaction->setAmount($amount);
    }

    if ($reason) {
      $paymentTransaction->setReason($reason);
    }

    if ($split) {
      $splits = [];
      foreach ($split as $s) {
        $splits [] = Payment\PaymentSplit::init($s['code'], $s['amount'], $s['meta']);
      }
      $paymentTransaction->setSplit($splits);
    }

    return $paymentTransaction;
  }

  private function addDateFilters(&$filters, &$criteria, $key, $column): void
  {
    if (!isset($criteria[$key])) {
      return;
    }

    $conditions = [
      'strictly_after' => '>',
      'after' => '>=',
      'strictly_before' => '<',
      'before' => '<='
    ];

    foreach ($conditions as $param => $operator) {
      if (isset($criteria[$key][$param])) {
        $filters[] = "$column $operator '{$criteria[$key][$param]}'";
      }
    }

    unset($criteria[$key]);
  }

  private function addDateMillisecondFilters(&$filters, &$criteria, $key, $column): void
  {
    if (!isset($criteria[$key])) {
      return;
    }

    $conditions = [
      'strictly_after' => '>',
      'after' => '>=',
      'strictly_before' => '<',
      'before' => '<='
    ];

    foreach ($conditions as $param => $operator) {
      if (isset($criteria[$key][$param])) {

        $dateTimeCriteria = $criteria[$key][$param];
        $milliseconds = $dateTimeCriteria->getTimestamp() * 1000 + (int)($dateTimeCriteria->format('v'));

        $filters[] = "$column $operator {$milliseconds}";
      }
    }

    unset($criteria[$key]);
  }

}
