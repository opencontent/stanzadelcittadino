<?php

namespace App\Services;

use App\Helpers\MunicipalityConverter;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CadastralCodeConverter
{
  private HttpClientInterface $client;
  private string $municipalityApiUrl;
  private LoggerInterface $logger;

  public function __construct(
    HttpClientInterface $client,
    LoggerInterface     $logger,
    string              $municipalityApiUrl
  )
  {
    $this->client = $client;
    $this->logger = $logger;
    $this->municipalityApiUrl = $municipalityApiUrl;
  }

  public function convert(?string $codiceCatastale)
  {
    if (empty($codiceCatastale)) {
      return null;
    }

    try {

      return MunicipalityConverter::translate($codiceCatastale);

    } catch (\Exception $e) {

      $this->logger->warning(
        "MunicipalityConverter failed to convert code, I will try with geo/comuni API", [
        'codiceCatastale' => $codiceCatastale,
      ]);

    }

    return $this->getFromGeoComuni($codiceCatastale);
  }

  private function getFromGeoComuni(string $code): string
  {
    if (!preg_match('/[A-Z][\d]{3}/m', $code)) {
      return $code;
    }

    try {

      $response = $this->client->request(
        'GET',
        $this->municipalityApiUrl . '?cod_catastale=' . $code,
      );

      if ($response->getStatusCode() !== Response::HTTP_OK) {

        $this->logger->error("geo/comuni API response with not valid status code",
          ['code' => $response->getStatusCode()]
        );

        return $code;
      }

      $content = $response->getContent();
      $municipalityInfo = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

      if (isset($municipalityInfo[0])) {
        return $municipalityInfo[0]['comune'] . " (EE)";
      }

      $this->logger->error("geo/comuni API response with not valid response");

    } catch (\Throwable $e) {

      $this->logger->error("Generic error on geo/comuni API request", [
        'message' => $e->getMessage()
      ]);
    }

    return $code;
  }
}
