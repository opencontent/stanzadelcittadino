<?php

namespace App\Doctrine\DBAL;

class Connection extends \Doctrine\DBAL\Connection
{
  use ConnectionTrait;
}
