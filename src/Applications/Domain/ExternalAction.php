<?php

declare(strict_types=1);

namespace App\Applications\Domain;

use App\Applications\Exceptions\ApplicationLiteException;
use JsonSerializable;

final class ExternalAction implements JsonSerializable
{
  private string $action;
  private string $url;

  private const ALLOWED_ACTIONS = [
    'view',
    'request_integration',
  ];

  /** @throws ApplicationLiteException */
  public static function fromArray($externalAction): ExternalAction
  {
    return new self($externalAction['action'], $externalAction['url']);
  }

  /**
   * @param string $action Action type (e.g. 'view', 'request_integration')
   * @param string $url    URL associated with the action
   *
   * @throws ApplicationLiteException
   */
  public function __construct(string $action, string $url)
  {
    if (!in_array($action, self::ALLOWED_ACTIONS, true)) {
      throw new ApplicationLiteException('Invalid action: ' . $action);
    }

    if (!filter_var($url, FILTER_VALIDATE_URL)) {
      throw new ApplicationLiteException('Invalid URL: ' . $url);
    }

    $this->action = $action;
    $this->url = $url;
  }

  public function action(): string
  {
    return $this->action;
  }

  public function url(): string
  {
    return $this->url;
  }

  public function equals(ExternalAction $otherAction): bool
  {
    return $this->action === $otherAction->action()
      && $this->url === $otherAction->url();
  }

  public function toArray(): array
  {
    return [
      'action' => $this->action,
      'url' => $this->url,
    ];
  }

  public function jsonSerialize(): array
  {
    return $this->toArray();
  }
}
