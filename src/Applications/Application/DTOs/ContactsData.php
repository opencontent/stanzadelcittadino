<?php

declare(strict_types=1);

namespace App\Applications\Application\DTOs;

use JMS\Serializer\Annotation\Groups;

class ContactsData
{
  /** @Groups({"read", "write", "kafka"}) */
  public ?string $mobile = null;
  /** @Groups({"read", "write", "kafka"}) */
  public ?string $phone = null;
  /** @Groups({"read", "write", "kafka"}) */
  public ?string $email = null;
  /** @Groups({"read", "write", "kafka"}) */
  public ?string $pec = null;
}
