<?php

declare(strict_types=1);

namespace App\Applications\Application\DTOs;

use JMS\Serializer\Annotation\Groups;

class IdentifiersData
{
  /** @Groups({"read", "write", "kafka"}) */
  public ?string $taxCode = null;

  /** @Groups({"read", "write", "kafka"}) */
  public ?string $spidCode = null;

  /** @Groups({"read", "write", "kafka"}) */
  public ?string $idEidas = null;

  /** @Groups({"read", "write", "kafka"}) */
  public ?string $anprId = null;

  /** @Groups({"read", "write", "kafka"}) */
  public ?string $inpsId = null;

  /** @Groups({"read", "write", "kafka"}) */
  public ?string $ioWalletId = null;
}
