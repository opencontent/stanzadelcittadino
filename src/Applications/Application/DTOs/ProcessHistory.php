<?php

declare(strict_types=1);

namespace App\Applications\Application\DTOs;

use DateTime;
use JMS\Serializer\Annotation as Serializer;

class ProcessHistory
{
  /** @Serializer\Groups({"read", "write"}) */
  public int $status;

  /** @Serializer\Groups({"read", "write"}) */
  public DateTime $statusChangedAt;
}
