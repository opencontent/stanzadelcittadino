<?php

declare(strict_types=1);

namespace App\Applications\Infrastructure;

use App\Applications\Domain\ApplicationLiteRepositoryInterface;
use App\Entity\ApplicationLite;
use App\Event\KafkaEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class DbApplicationLiteRepository implements ApplicationLiteRepositoryInterface
{
  private EntityManagerInterface $entityManager;
  private EventDispatcherInterface $dispatcher;

  public function __construct(
    EventDispatcherInterface $dispatcher,
    EntityManagerInterface $entityManager
  ) {
    $this->dispatcher = $dispatcher;
    $this->entityManager = $entityManager;
  }

  public function save(ApplicationLite $applicationLite): ApplicationLite
  {
    try {
      $this->entityManager->persist($applicationLite);
      $this->entityManager->flush();
      $this->entityManager->refresh($applicationLite);

      $this->dispatcher->dispatch(new KafkaEvent($applicationLite), KafkaEvent::NAME);
    } catch (\Throwable $e) {
      // TODO saverio
    }

    return $applicationLite;
  }
}
