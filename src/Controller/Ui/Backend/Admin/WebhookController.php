<?php

namespace App\Controller\Ui\Backend\Admin;

use App\Entity\Pratica;
use App\Entity\Webhook;
use App\Form\Admin\Ente\WebhookType;
use App\Services\InstanceService;
use App\Services\WebhookService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class WebhookController
 * @Route("/admin/webhook")
 */
class WebhookController extends AbstractController
{

  private EntityManagerInterface $entityManager;

  private InstanceService $instanceService;

  private WebhookService $webhookService;

  private TranslatorInterface $translator;

  /**
   * @param EntityManagerInterface $entityManager
   * @param InstanceService $instanceService
   * @param WebhookService $webhookService
   * @param TranslatorInterface $translator
   */
  public function __construct(EntityManagerInterface $entityManager, InstanceService $instanceService, WebhookService $webhookService, TranslatorInterface $translator)
  {
    $this->entityManager = $entityManager;
    $this->instanceService = $instanceService;
    $this->webhookService = $webhookService;
    $this->translator = $translator;
  }


  /**
   * @Route("/", name="admin_webhook_index", methods={"GET"})
   */
  public function indexWebhooksAction(): Response
  {

    $servizi = $this->instanceService->getServices();
    $services = [];
    $services ['all'] = $this->translator->trans('tutti');
    foreach ($servizi as $s) {
      $services[$s->getId()] = $s->getName();
    }

    $items = $this->entityManager->getRepository(Webhook::class)->findAll();

    return $this->render( 'Admin/indexWebhook.html.twig', [
      'user'  => $this->getUser(),
      'statuses' => Webhook::TRIGGERS,
      'services' => $services,
      'items' => $items
    ]);
  }

  /**
   * @Route("/new", name="admin_webhook_new", methods={"GET", "POST"})
   * @param Request $request
   * @return Response
   */
  public function newWebhookAction(Request $request): Response
  {
    $webhook = new Webhook();
    $form = $this->createForm(WebhookType::class, $webhook);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $webhook->setEnte($this->instanceService->getCurrentInstance());
      $this->entityManager->persist($webhook);
      $this->entityManager->flush();

      $this->addFlash('feedback', $this->translator->trans('operatori.create_webhook_success'));
      return $this->redirectToRoute('admin_webhook_index');
    }

    return $this->render( 'Admin/editWebhook.html.twig', [
      'user'  => $this->getUser(),
      'item' => $webhook,
      'form' => $form->createView(),
    ]);
  }

  /**
   * @Route("/{id}/edit", name="admin_webhook_edit", methods={"GET", "POST"})
   * @param Request $request
   * @param Webhook $webhook
   * @return Response
   * @throws GuzzleException
   */
  public function editWebhookAction(Request $request, Webhook $webhook): Response
  {
    $form = $this->createForm(WebhookType::class, $webhook);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $this->entityManager->flush();
      try {
        $this->testWebhook($request, $webhook);
      } catch (Exception $e) {
        $error = new FormError($e->getMessage());
        $form->addError($error);
      }

      //return $this->redirectToRoute('admin_webhook_edit', array('id' => $webhook->getId()));
    }

    $templateVariables = [
      'user'  => $this->getUser(),
      'item' => $webhook,
      'form' => $form->createView(),
      'test' => true
    ];

    if ($request->request->has('application_id')) {
      $templateVariables ['application_id'] = $request->request->get('application_id');
    }

    return $this->render( 'Admin/editWebhook.html.twig', $templateVariables);
  }

  /**
   * @Route("/{id}/delete", name="admin_webhook_delete", methods={"GET", "POST", "DELETE"})
   */
  public function deleteServiceGroupAction(Request $request, Webhook $webhook): RedirectResponse
  {
    try {
      $this->entityManager->remove($webhook);
      $this->entityManager->flush();
      $this->addFlash('feedback', $this->translator->trans('operatori.delete_webhook_success'));
      return $this->redirectToRoute('admin_webhook_index');

    } catch (ForeignKeyConstraintViolationException $exception) {
      $this->addFlash('warning', $this->translator->trans('operatori.delete_webhook_error'));
      return $this->redirectToRoute('admin_service_group_index');
    }
  }


  /**
   * @throws GuzzleException|RuntimeException
   */
  private function testWebhook(Request $request, Webhook $webhook): void
  {

    if ($request->request->has('test')) {
      try {
        $applicationRepo = $this->entityManager->getRepository(Pratica::class);
        $application = $applicationRepo->find($request->request->get('application_id'));
        if ($application instanceof Pratica) {
          $this->webhookService->applicationWebhook(
            [
              'pratica' => $application->getId(),
              'webhook' => $webhook->getId()
            ]
          );
        }
      } catch (Exception $e) {
        throw new RuntimeException($e->getMessage());
      }
    }
  }
}
