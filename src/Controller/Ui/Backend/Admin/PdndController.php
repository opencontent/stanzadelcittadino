<?php

namespace App\Controller\Ui\Backend\Admin;

use App\Services\BreadcrumbsService;
use App\Services\InstanceService;
use App\Services\Manager\PdndManager;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;

/**
 * Class AdminController
 * @Route("/admin")
 */
class PdndController extends AbstractController
{

  private InstanceService $instanceService;
  private string $pdndApiUrl;
  private HttpClientInterface $httpClient;
  private PdndManager $pdndManager;
  private BreadcrumbsService $breadcrumbsService;

  public function __construct(InstanceService $instanceService, HttpClientInterface $httpClient, string $pdndApiUrl, PdndManager $pdndManager, BreadcrumbsService $breadcrumbsService)
  {
    $this->instanceService = $instanceService;
    $this->pdndApiUrl = $pdndApiUrl;
    $this->httpClient = $httpClient;
    $this->pdndManager = $pdndManager;
    $this->breadcrumbsService = $breadcrumbsService;
  }


  /**
   * @Route("/ente/pdnd/clients", name="admin_pdnd_clients")
   * @param Request $request
   * @return Response
   * @throws RedirectionExceptionInterface
   * @throws ServerExceptionInterface
   */
  public function pdndClientsAction(Request $request): Response
  {
    $user = $this->getUser();
    $this->breadcrumbsService->getBackendBreadcrumbs($user)->addRouteItem('nav.admin.pdnd', 'admin_pdnd_clients');
    try {
      $this->pdndManager->getTenant();
    } catch (ClientExceptionInterface $e) {
      // Non è presente un tenant sul pdnd connector, viene creato automaticamente
      try {
        $this->pdndManager->postTenant();
      } catch (\Throwable $e) {
        $this->addFlash('error', $e->getMessage());
        return $this->redirectToRoute('admin_edit_ente');
      }
    } catch (\Throwable $e) {
      // Todo: aggiungere log
    }

    return $this->render('Admin/pdnd/pdndClients.html.twig', [
      'user' => $user,
      'default_pdnd_client_id' => $this->instanceService->getCurrentInstance()->getDefaultPdndClientId(),
      'url' => $this->pdndApiUrl . '/tenants/' . $this->instanceService->getCurrentInstance()->getId() .'/clients',
      'tenant_identifier' => $this->instanceService->getCurrentInstance()->getSlug()
    ]);
  }

  /**
   * @Route("/ente/pdnd/clients/new", name="admin_pdnd_clients_new")
   * @param Request $request
   * @return Response
   */
  public function pdndClientsNewAction(Request $request): Response
  {

    $user = $this->getUser();
    $this->breadcrumbsService->getBackendBreadcrumbs($user)
      ->addRouteItem('nav.admin.pdnd', 'admin_pdnd_clients')
      ->addRouteItem('Nuovo client', 'admin_pdnd_clients_new');

    return $this->render('Admin/pdnd/pdndClientsNew.html.twig', [
      'user' => $user,
      'keys_url' => $this->pdndApiUrl . '/tenants/' . $this->instanceService->getCurrentInstance()->getId() .'/keys',
      'post_url' => $this->pdndApiUrl . '/tenants/' . $this->instanceService->getCurrentInstance()->getId() .'/clients'
    ]);
  }

  /**
   * @Route("/ente/pdnd/clients/{id}", name="admin_pdnd_clients_detail")
   * @param Request $request
   * @param $id
   * @return Response
   */
  public function pdndClientsDetailAction(Request $request, $id): Response
  {
    $user = $this->getUser();
    $this->breadcrumbsService->getBackendBreadcrumbs($user)
      ->addRouteItem('nav.admin.pdnd', 'admin_pdnd_clients')
      ->addItem($id);

    $tenant = $this->instanceService->getCurrentInstance();
    $tenantId = $tenant->getId();
    $configsByEservices = $this->pdndManager->getTenantConfigs($id);

    return $this->render('Admin/pdnd/pdndClientsDetail.html.twig', [
      'user' => $this->getUser(),
      'id' => $id,
      'tenant_id' => $tenantId,
      'tenant_identifier' => $tenant->getSlug(),
      'url' => $this->pdndApiUrl . '/tenants/' . $tenantId .'/clients/' . $id,
      'configs_url' => $this->pdndApiUrl . '/tenants/' . $tenantId .'/configs',
      'configs_by_eservices' => json_encode($configsByEservices),
      'configs' => json_encode($tenant->getPdndConfigIds()),
      'eservices_url' => $this->pdndApiUrl . '/e-services'
    ]);
  }
}
