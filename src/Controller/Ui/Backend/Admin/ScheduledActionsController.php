<?php

declare(strict_types=1);

namespace App\Controller\Ui\Backend\Admin;

use App\DataTable\ScheduledActionTableType;
use App\DataTable\Traits\FiltersTrait;
use App\Entity\ScheduledAction;
use Doctrine\ORM\EntityManagerInterface;
use Omines\DataTablesBundle\DataTableFactory;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;


/**
 * Class ScheduledActionsController
 * @Route("/admin")
 */
class ScheduledActionsController extends AbstractController
{
  use FiltersTrait;

  private TranslatorInterface $translator;

  private DataTableFactory $dataTableFactory;

  private LoggerInterface $logger;

  private EntityManagerInterface $entityManager;

  /**
   * @param TranslatorInterface $translator
   * @param DataTableFactory $dataTableFactory
   * @param LoggerInterface $logger
   * @param EntityManagerInterface $entityManager
   */
  public function __construct(
    TranslatorInterface           $translator,
    DataTableFactory              $dataTableFactory,
    LoggerInterface               $logger,
    EntityManagerInterface        $entityManager
  )
  {
    $this->translator = $translator;
    $this->dataTableFactory = $dataTableFactory;
    $this->logger = $logger;
    $this->entityManager = $entityManager;
  }

  /**
   * Lists all scheduled actions entities.
   * @Route("/scheduled-actions", name="admin_scheduled_actions_index", methods={"GET", "POST"})
   */
  public function indexScheduledActionsAction(Request $request): Response
  {

    $statuses = [
      ScheduledAction::STATUS_PENDING => [
        'label' => $this->translator->trans('STATUS_WAITING'),
        'count' => 0,
      ],
      ScheduledAction::STATUS_DONE => [
        'label' => $this->translator->trans('STATUS_DONE'),
        'count' => 0,
      ],
      ScheduledAction::STATUS_INVALID => [
        'label' => $this->translator->trans('STATUS_INVALID'),
        'count' => 0,
      ],
    ];

    $sql = "SELECT count(id) as count, status FROM scheduled_action GROUP BY status";
    try {
      $stmt = $this->entityManager->getConnection()->prepare($sql);
      $result = $stmt->executeQuery()->fetchAllAssociative();
      foreach ($result as $r) {
        $statuses[$r['status']]['count'] = $r['count'];
      }

    } catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }

    $options = self::getFiltersFromRequest($request);
    $table = $this->dataTableFactory->createFromType(ScheduledActionTableType::class, $options)
      ->handleRequest($request);

    if ($table->isCallback()) {
      return $table->getResponse();
    }

    return $this->render('Admin/indexScheduledActions.html.twig', [
      'user' => $this->getUser(),
      'datatable' => $table,
      'filters' => $options['filters'] ?? [],
      'statuses' => $statuses,
    ]);
  }

  /**
   * Reset retry of a scheduled action
   * @Route("/scheduled-actions/{id}/retry", name="admin_scheduled_actions_retry", methods={"GET"})
   */
  public function retryScheduledActionsAction(Request $request, ScheduledAction $scheduledAction): JsonResponse
  {
    try {
      $scheduledAction->setRetry(0);
      $scheduledAction->setHostname(null);
      $this->entityManager->persist($scheduledAction);
      $this->entityManager->flush();

      return new JsonResponse(['status' => 'success']);
    } catch (\Exception $e) {
      return new JsonResponse([
          'status' => 'error',
          'message' => $e->getMessage(),
        ]
      );
    }

  }
}
