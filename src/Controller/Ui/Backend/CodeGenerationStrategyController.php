<?php

namespace App\Controller\Ui\Backend;

use App\Entity\CodeGenerationStrategy;
use App\Entity\Place;
use App\Form\Api\CodeGenerationStrategyApiType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CodeGenerationStrategyController
 * @Route("/admin/code-generation-strategies")
 */
class CodeGenerationStrategyController extends AbstractController
{

  private EntityManagerInterface $entityManager;
  /**
   * @var TranslatorInterface
   */
  private TranslatorInterface $translator;


  public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator)
  {
    $this->entityManager = $entityManager;
    $this->translator = $translator;
  }


  /**
   * @Route("/", name="admin_cgs_index", methods={"GET"})
   */
  public function indexPlacesAction(): Response
  {
    $items = $this->entityManager->getRepository(CodeGenerationStrategy::class)->findBy([], ['name' => 'asc']);
    return $this->render('Admin/indexCgs.html.twig', [
      'user'  => $this->getUser(),
      'items' => $items
    ]);
  }


  /**
   * @Route("/new", name="admin_cgs_new", methods={"GET", "POST"})
   * @param Request $request
   * @return RedirectResponse|Response|null
   */
  public function newStrategyAction(Request $request)
  {
    $item = new CodeGenerationStrategy();
    $form = $this->createForm(CodeGenerationStrategyApiType::class, $item);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $this->entityManager->persist($item);
      $this->entityManager->flush();

      $this->addFlash('feedback', $this->translator->trans('general.flash.created'));
      return $this->redirectToRoute('admin_cgs_edit', ['id' => $item->getId()]);
    }
    //dd($form->createView());
    return $this->render( 'Admin/editCgs.html.twig', [
      'user'  => $this->getUser(),
      'item' => $item,
      'form' => $form->createView(),
    ]);
  }


  /**
   * @Route("/{id}/edit", name="admin_cgs_edit", methods={"GET", "POST"})
   * @param Request $request
   * @param CodeGenerationStrategy $item
   * @return Response|null
   */
  public function editPlaceAction(Request $request, CodeGenerationStrategy $item): ?Response
  {
    $form = $this->createForm(CodeGenerationStrategyApiType::class, $item);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $this->entityManager->flush();
      return $this->redirectToRoute('admin_cgs_edit', ['id' => $item->getId()]);
    }

    return $this->render( 'Admin/editCgs.html.twig',
      [
        'user'  => $this->getUser(),
        'item' => $item,
        'form' => $form->createView()
      ]);
  }


  /**
   * @Route("/{id}/delete", name="admin_cgs_delete", methods={"GET", "POST"})
   */
  public function deletePlaceAction(Request $request, CodeGenerationStrategy $item): RedirectResponse
  {
    try {
      $this->entityManager->remove($item);
      $this->entityManager->flush();
      $this->addFlash('feedback', $this->translator->trans('code_generation.strategies.delete_success'));
      return $this->redirectToRoute('admin_cgs_index');

    } catch (ForeignKeyConstraintViolationException $exception) {
      $this->addFlash('warning', $this->translator->trans('code_generation.strategies.delete_error'));
      return $this->redirectToRoute('admin_cgs_index');
    }
  }


}
