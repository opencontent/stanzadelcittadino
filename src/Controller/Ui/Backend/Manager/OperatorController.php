<?php

namespace App\Controller\Ui\Backend\Manager;

use App\DataTable\OperatorTableType;
use App\DataTable\Traits\FiltersTrait;
use App\Entity\User;
use App\Entity\OperatoreUser;
use App\Entity\Servizio;
use App\Security\Voters\UserVoter;
use App\Services\InstanceService;
use App\Services\MailerService;
use App\Services\Manager\UserManager;
use App\Utils\StringUtils;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Omines\DataTablesBundle\DataTableFactory;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Form\OperatoreUserType;


class OperatorController extends AbstractController
{
  use FiltersTrait;

  private InstanceService $instanceService;

  private TranslatorInterface $translator;

  private DataTableFactory $dataTableFactory;

  private LoggerInterface $logger;

  private MailerService $mailer;

  private EntityManagerInterface $entityManager;

  private UserManager $userManager;
  private RouterInterface $router;


  public function __construct(
    InstanceService               $instanceService,
    TranslatorInterface           $translator,
    RouterInterface               $router,
    DataTableFactory              $dataTableFactory,
    LoggerInterface               $logger,
    UserManager                   $userManager,
    MailerService                 $mailer,
    EntityManagerInterface        $entityManager
  )
  {
    $this->instanceService = $instanceService;
    $this->translator = $translator;
    $this->dataTableFactory = $dataTableFactory;
    $this->logger = $logger;
    $this->userManager = $userManager;
  }


  /**
   * @Route("/operatori/operatore", name="admin_operatore_index", methods={"GET", "POST"})
   */
  public function indexOperatoreAction(Request $request): Response
  {
    if (!$this->isGranted(User::ROLE_ADMIN) && !$this->isGranted(User::ROLE_MANAGER)) {
      throw $this->createAccessDeniedException();
    }


    $options = self::getFiltersFromRequest($request);
    $table = $this->dataTableFactory
      ->createFromType(OperatorTableType::class, $options)
      ->handleRequest($request);

    if ($table->isCallback()) {
      return $table->getResponse();
    }

    return $this->render('Admin/indexOperatore.html.twig', [
      'user' => $this->getUser(),
      'datatable' => $table,
      'filters' => $options['filters'] ?? [],
    ]);
  }

  /**
   * @Route("/operatori/operatore/new", name="admin_operatore_new", methods={"GET", "POST"})
   * @param Request $request
   * @param UserPasswordHasherInterface $passwordHasher
   * @return Response
   */
  public function newOperatoreAction(Request $request, UserPasswordHasherInterface $passwordHasher): Response
  {

    if (!$this->isGranted(User::ROLE_ADMIN) && !$this->isGranted(User::ROLE_MANAGER)) {
      throw $this->createAccessDeniedException();
    }

    /** @var User $user */
    $user = $this->getUser();
    $systemUserParameter = $request->get('system_user', false);

    $operatoreUser = new Operatoreuser();
    $operatoreUser->setSystemUser($systemUserParameter);

    $form = $this->createForm(OperatoreUserType::class, $operatoreUser);
    $form->handleRequest($request);

    try {
      if ($form->isSubmitted() && $form->isValid()) {
        $ente = $this->instanceService->getCurrentInstance();

        $operatoreUser
          ->setEnte($ente)
          ->setPlainPassword(md5(time()))
          ->setEnabled(true);

        $operatoreUser->setPassword(
          $passwordHasher->hashPassword(
            $operatoreUser,
            StringUtils::randomPassword()
          )
        );

        $this->userManager->resetPassword($operatoreUser, $user);

        $this->userManager->save($operatoreUser);

        $this->addFlash('feedback', $this->translator->trans('admin.create_operator_notify'));

        return $this->redirectToRoute('admin_operatore_edit', array('id' => $operatoreUser->getId()));
      }
    } catch (UniqueConstraintViolationException $exception) {
      $this->addFlash('error', $this->translator->trans('operatori.error_duplicate_operator_user') . ' ' . $operatoreUser->getUsername());
      return $this->redirectToRoute('admin_operatore_new');
    } catch (Exception $exception) {
      $this->addFlash('error', $this->translator->trans('operatori.create_calendar_error'));
      return $this->redirectToRoute('admin_operatore_new');
    }

    return $this->render('Admin/editOperatore.html.twig', [
      'user' => $user,
      'operatoreUser' => $operatoreUser,
      'form' => $form->createView(),
    ]);
  }

  /**
   * @Route("/operatori/operatore/{id}", name="admin_operatore_show", methods={"GET"})
   */
  public function showOperatoreAction(OperatoreUser $operatoreUser): Response
  {

    $this->denyAccessUnlessGranted(UserVoter::VIEW, $operatoreUser);

    if ($operatoreUser->getServiziAbilitati()->count() > 0) {
      $serviziAbilitati = $this->getDoctrine()
        ->getRepository(Servizio::class)
        ->findBy(['id' => $operatoreUser->getServiziAbilitati()->toArray()]);
    } else {
      $serviziAbilitati = [];
    }

    return $this->render('Admin/showOperatore.html.twig', [
      'user' => $this->getUser(),
      'operatoreUser' => $operatoreUser,
      'servizi_abilitati' => $serviziAbilitati,
    ]);
  }

  /**
   * @Route("/operatori/operatore/{id}/edit", name="admin_operatore_edit", methods={"GET", "POST"})
   */
  public function editOperatoreAction(Request $request, OperatoreUser $operatoreUser)
  {

    $this->denyAccessUnlessGranted(UserVoter::EDIT, $operatoreUser);

    $form = $this->createForm(OperatoreUserType::class, $operatoreUser);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $operatoreUser = $form->getData();
      $this->userManager->save($operatoreUser);

      return $this->redirectToRoute('admin_operatore_edit', array('id' => $operatoreUser->getId()));
    }

    return $this->render('Admin/editOperatore.html.twig', [
      'user' => $this->getUser(),
      'operatoreUser' => $operatoreUser,
      'form' => $form->createView(),
    ]);
  }

  /**
   * @Route("/operatori/operatore/{id}/resetpassword", name="admin_operatore_reset_password", methods={"GET", "POST"})
   */
  public function resetPasswordOperatoreAction(Request $request, OperatoreUser $operatoreUser): RedirectResponse
  {

    $this->denyAccessUnlessGranted(UserVoter::EDIT, $operatoreUser);

    $this->userManager->resetPassword($operatoreUser);
    $this->userManager->save($operatoreUser);

    return $this->redirectToRoute('admin_operatore_edit', array('id' => $operatoreUser->getId()));
  }

  /**
   * @Route("/operatori/operatore/{id}/delete", name="admin_operatore_delete", methods={"GET", "POST", "DELETE"})
   */
  public function deleteOperatoreAction(Request $request, OperatoreUser $operatoreUser): RedirectResponse
  {

    $this->denyAccessUnlessGranted(UserVoter::DELETE, $operatoreUser);

    try {
      $this->userManager->remove($operatoreUser);
      $this->addFlash('feedback', $this->translator->trans('admin.delete_operator_notify'));
    } catch (ForeignKeyConstraintViolationException $exception) {
      $this->addFlash('warning', $this->translator->trans('admin.error_delete_operator_notify'));
    }
    return $this->redirectToRoute('admin_operatore_index');
  }

  /**
   * @Route("/operatori/operatore/{id}/toggle-manager", name="admin_toggle_manager", methods={"GET", "POST", "DELETE"})
   */
  public function toggleManagerAction(OperatoreUser $operatoreUser): RedirectResponse
  {
    if ($operatoreUser->hasRole(User::ROLE_MANAGER)) {
      $operatoreUser->setRoles([User::ROLE_OPERATORE]);
    } else {
      $operatoreUser->setRoles([User::ROLE_MANAGER]);
    }
    $this->userManager->save($operatoreUser);
    $this->addFlash('success', $this->translator->trans('admin.edit_operator_notify'));
    return $this->redirectToRoute('admin_operatore_index');
  }
}
