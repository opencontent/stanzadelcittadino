<?php

namespace App\Controller\Ui\Frontend;

use App\Http\TransparentPixelResponse;
use App\Services\Manager\MessageManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Message;


/**
 * Class TrackingController
 */
class TrackingController extends AbstractController
{
  /** @var EntityManagerInterface */
  private EntityManagerInterface $entityManager;

  /** @var LoggerInterface */
  private LoggerInterface $logger;

  /** @var MessageManager  */
  private MessageManager $messageManager;

  /**
   * TrackingController constructor.
   * @param EntityManagerInterface $entityManager
   * @param LoggerInterface $logger
   * @param MessageManager $messageManager
   */
  public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger, MessageManager $messageManager)
  {
    $this->entityManager = $entityManager;
    $this->logger = $logger;
    $this->messageManager = $messageManager;
  }


  /**
   * @Route("track-message.gif", name="track_message")
   * @param Request $request
   * @return TransparentPixelResponse
   */
  public function trackMessageEmailAction(Request $request): TransparentPixelResponse
  {
    $id = $request->query->get('id');
    if ($id && Uuid::isValid($id)) {
      $message = $this->entityManager->getRepository(Message::class)->find($id);
      if ($message and !$message->getReadAt()) {
        $message->setReadAt(time());
        try {
          $this->messageManager->save($message);
        } catch (\Exception $e) {
          $this->logger->error("Impossible to update message read time for message {$message->getId()}: {$e->getMessage()}");
        }
      }
    }

    return new TransparentPixelResponse();
  }
}
