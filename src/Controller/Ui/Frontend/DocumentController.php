<?php


namespace App\Controller\Ui\Frontend;


use App\Security\Voters\DocumentVoter;
use App\Services\BreadcrumbsService;
use App\Services\FileService\DocumentFileService;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Entity\Folder;
use App\Entity\Document;

/**
 * Class DocumentController
 * @Route("documenti")
 */
class DocumentController extends AbstractController
{
  /**
   * @var EntityManagerInterface
   */
  private EntityManagerInterface $em;

  /**
   * @var LoggerInterface
   */
  private LoggerInterface $logger;
  /**
   * @var TranslatorInterface
   */
  private TranslatorInterface $translator;
  /**
   * @var BreadcrumbsService
   */
  private BreadcrumbsService $breadcrumbsService;
  /**
   * @var DocumentFileService
   */
  private DocumentFileService $fileService;

  /**
   * DocumentController constructor.
   * @param TranslatorInterface $translator
   * @param EntityManagerInterface $em
   * @param LoggerInterface $logger
   * @param BreadcrumbsService $breadcrumbsService
   * @param DocumentFileService $fileService
   */
  public function __construct(TranslatorInterface $translator, EntityManagerInterface $em, LoggerInterface $logger, BreadcrumbsService $breadcrumbsService, DocumentFileService $fileService)
  {
    $this->translator = $translator;
    $this->em = $em;
    $this->logger = $logger;
    $this->breadcrumbsService = $breadcrumbsService;
    $this->fileService = $fileService;

    $this->breadcrumbsService->getBreadcrumbs()->addRouteItem(ucfirst($this->translator->trans('personal_area')), 'user_dashboard');
    $this->breadcrumbsService->getBreadcrumbs()->addRouteItem($this->translator->trans('nav.documenti'), 'folders_list_cpsuser');

  }

  /**
   * @Route("/", name="folders_list_cpsuser")
   * @throws Exception|\Doctrine\DBAL\Exception
   */
  public function cpsUserListFoldersAction(): Response
  {

    $user = $this->getUser();
    // Get all user's folders
    $folderRepository = $this->em->getRepository(Folder::class);
    $folders = $folderRepository->findBy(['owner' => $user]);

    // Get folders with shared documents
    $sql = 'SELECT DISTINCT folder.id from document JOIN folder  on document.folder_id = folder.id where (readers_allowed)::jsonb @> \'"' . $user->getCodiceFiscale() . '"\'';
    $stmt = $this->em->getConnection()->prepare($sql);
    $result = $stmt->executeQuery();
    $sharedIds = $result->fetchAllAssociative();

    foreach ($sharedIds as $id) {
      $folders[] = $folderRepository->find($id);
    }

    return $this->render('Document/cpsUserListFolders.html.twig', [
      'folders' => $folders,
      'user' => $this->getUser(),
    ]);
  }

  /**
   * @Route("/{folder}", name="documenti_list_cpsuser")
   * @param Request $request
   * @param Folder $folder
   * @ParamConverter("folder", class="App\Entity\Folder")
   * @return Response
   * @throws \Doctrine\DBAL\Exception
   */
  public function cpsUserListDocumentsAction(Request $request, Folder $folder): Response
  {
    $user = $this->getUser();
    $documents = [];

    $this->breadcrumbsService->getBreadcrumbs()->addRouteItem($folder->getTitle(), 'documenti_list_cpsuser', ['folder' => $folder]);

    $documentRepository = $this->em->getRepository(Document::class);
    if ($folder->getOwner() === $user)
      $documents = $documentRepository->findBy(['folder' => $folder]);
    else {
      try {
        $sql = 'SELECT document.id from document JOIN folder  on document.folder_id = folder.id where (readers_allowed)::jsonb @> \'"' . $user->getCodiceFiscale() . '"\' and folder.id = \'' . $folder->getId() . '\'';

        $stmt = $this->em->getConnection()->prepare($sql);
        $sharedDocuments = $stmt->executeQuery()->fetchAllAssociative();

        foreach ($sharedDocuments as $id) {
          $documents[] = $documentRepository->find($id);
        }
      } catch (Exception $exception) {
        $this->addFlash('warning', $this->translator->trans('documenti.document_search_error'));
      }
    }

    return $this->render('Document/cpsUserListDocuments.html.twig', [
      'documents' => $documents,
      'folder' => $folder,
      'user' => $user
    ]);
  }

  /**
   * @Route("/{folder}/{document}", name="documento_show_cpsuser")
   * @param Request $request
   * @param Folder $folder
   * @param Document $document
   * @ParamConverter("folder", class="App\Entity\Folder")
   * @ParamConverter("document", class="App\Entity\Document")
   * @return RedirectResponse|Response
   */
  public function cpsUserShowDocumentoAction(Request $request, Folder $folder, Document $document)
  {
    $this->breadcrumbsService->getBreadcrumbs()->addRouteItem($folder->getTitle(), 'documenti_list_cpsuser', ['folder' => $folder]);
    $this->breadcrumbsService->getBreadcrumbs()->addItem($document->getTitle());

    if (!$this->isGranted(DocumentVoter::VIEW, $document)) {
      $this->addFlash('warning', $this->translator->trans('documenti.no_document_permissions'));
      return $this->redirectToRoute('documenti_list_cpsuser', ['folder' => $folder]);
    }

    return $this->render('Document/cpsUserShowDocumento.html.twig', [
      'document' => $document,
      'user' => $this->getUser(),
    ]);
  }

  /**
   * Download a document
   * @Route("/{folder}/{document}/download", name="document_download_cpsuser")
   * @param Request $request
   * @param Folder $folder
   * @param Document $document
   * @return Response
   * @ParamConverter("folder", class="App\Entity\Folder")
   * @ParamConverter("document", class="App\Entity\Document")
   */
  public function downloadDocumentAction(Request $request, Folder $folder, Document $document): Response
  {
    $this->denyAccessUnlessGranted(DocumentVoter::VIEW, $document);

    try {
      $response = $this->fileService->download($document);
      $document->setLastReadAt(new \DateTime());
      $document->setDownloadsCounter($document->getDownloadsCounter() + 1);
      $this->em->persist($document);
      $this->em->flush();

      return  $response;
    } catch (\Exception $e) {
      $this->logger->error("An error occurred while downloading document {$document->getId()}: {$e->getMessage()}");
      throw new BadRequestHttpException($this->translator->trans('documenti.download_error'));
    }
  }
}
