<?php

declare(strict_types=1);

namespace App\Controller\Rest;

use App\ApplicationLiteImport\Application\ApplicationLiteBulkImportJob;
use App\ApplicationLiteImport\Application\ApplicationLiteImportService;
use App\ApplicationLiteImport\Application\DataTransformer;
use App\ApplicationLiteImport\Application\ImportCommand\ImportData;
use App\ApplicationLiteImport\Domain\ApplicationImportResult;
use App\ApplicationLiteImport\Infrastructure\HttpRequestToImportBulkCommandMapper;
use App\ApplicationLiteImport\Infrastructure\HttpRequestToImportCommandMapper;
use App\ApplicationLiteImport\Infrastructure\InvalidJsonException;
use App\Entity\Job;
use App\Entity\User;
use App\Services\InstanceService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class ApplicationLiteImportController extends AbstractFOSRestController
{
  private InstanceService $instanceService;

  public function __construct(
    InstanceService $instanceService
  ) {
    $this->instanceService = $instanceService;
  }

  /**
   * Import a single ApplicationLite.
   *
   * @Rest\Post("/applications-lite/import", name="application_lite_import")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The application to import",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=ImportData::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Ok",
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=ImportData::class, groups={"read"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request. Return a description of problem occurred",
   *     @OA\MediaType(
   *       mediaType="application/json",
   *       @OA\Schema(
   *         type="string",
   *         description="Description of problem occurred",
   *         example="The provided JSON is not valid: The property status is required (application.status)",
   *       )
   *   )
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=500,
   *     description="Internal server error"
   * )
   *
   * @OA\Tag(
   *     name="applications-lite",
   *     description="Applications Lite"
   * )
   */
  public function importApplicationLite(
    ApplicationLiteImportService $applicationLite,
    HttpRequestToImportCommandMapper $commandMapper,
    LoggerInterface $logger,
    Request $request
  ): View {
    if (
      !$this->isGranted(User::ROLE_OPERATORE)
      && !$this->isGranted(User::ROLE_ADMIN)
    ) {
      throw $this->createAccessDeniedException();
    }

    $content = $request->getContent();
    /** @var User $requester */
    $requester = $this->getUser();

    try {
      $ente = $this->instanceService->getCurrentInstance();
      $importCommand = $commandMapper->map($content, $ente, $requester);

      $result = $applicationLite->import($importCommand);

      return $this->presentResult($result);
    } catch (InvalidJsonException $e) {
      return $this->view($e->getMessage(), Response::HTTP_BAD_REQUEST);
    } catch (Throwable $e) {
      $logger->error($e->getMessage(), $e->getTrace());

      return $this->view('Generic error', Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  private function presentResult(ApplicationImportResult $result): View
  {
    if ($result->isCompletedWithoutError()) {
      $model = DataTransformer::toApplicationLiteImportApiModel($result->applicationLiteImported());

      return $this->view($model, Response::HTTP_OK);
    } else {
      return $this->view([
        'error' => $result->getError(),
      ], Response::HTTP_BAD_REQUEST);
    }
  }

  /**
   * Import multiple ApplicationLite in bulk.
   * The endpoint accepts a JSON array where each element represents an import unit as an ApplicationLite.
   * The ApplicationLite are queued for asynchronous processing and the API returns a
   * unique identifier (importId) that can be used to track the import progress.
   *
   * @Rest\Post("/applications-lite/import/bulk", name="application_lite_import_bulk")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The applications to import",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="array",
   *             @OA\Items(ref=@Model(type=ImportData::class, groups={"write"}))
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=201,
   *     description="Ok",
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="string",
   *             format="uuid"
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request. Return a description of problem occurred",
   *     @OA\MediaType(
   *       mediaType="application/json",
   *       @OA\Schema(
   *         type="string",
   *         description="Description of problem occurred",
   *         example="The provided JSON is not valid: The property status is required ([0].application.status)",
   *       )
   *     )
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=500,
   *     description="Internal server error"
   * )
   *
   * @OA\Tag(name="applications-lite")
   */
  public function importAllApplicationLite(
    ApplicationLiteBulkImportJob $applicationLite,
    HttpRequestToImportBulkCommandMapper $commandMapper,
    LoggerInterface $logger,
    Request $request
  ): View {
    if (
      !$this->isGranted(User::ROLE_OPERATORE)
      && !$this->isGranted(User::ROLE_ADMIN)
    ) {
      throw $this->createAccessDeniedException();
    }

    $content = $request->getContent();
    /** @var User $requester */
    $requester = $this->getUser();

    try {
      $ente = $this->instanceService->getCurrentInstance();
      $importCommand = $commandMapper->map($content, $ente, $requester);

      $result = $applicationLite->createJob($importCommand);

      return $this->importAllApplicationLitePresentResult($result);

    } catch (InvalidJsonException $e) {
      return $this->view($e->getMessage(), Response::HTTP_BAD_REQUEST);
    } catch (Throwable $e) {
      $logger->error($e->getMessage(), $e->getTrace());

      return $this->view('Generic error', Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  private function importAllApplicationLitePresentResult(Job $job): View
  {
    return $this->view(
      [
        'job_id' => $job->getId(),
        'job_status' => $job->getStatus(),
      ],
      Response::HTTP_CREATED,
    );
  }
}
