<?php

namespace App\Controller\Rest;

use App\Entity\Job;
use App\Entity\User;
use App\Form\Api\JobApiType;
use App\Services\InstanceService;
use App\Services\JobService;
use App\Utils\FormUtils;
use App\Utils\UploadedBase64File;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class JobsAPIController.
 *
 * @Route("/jobs")
 */
class JobsAPIController extends AbstractFOSRestController
{
  public const CURRENT_API_VERSION = '1.0';

  private EntityManagerInterface $entityManager;

  private InstanceService $instanceService;
  private JobService $jobService;

  private LoggerInterface $logger;
  private TranslatorInterface $translator;

  public function __construct(
    EntityManagerInterface $entityManager,
    InstanceService $instanceService,
    JobService $jobService,
    LoggerInterface $logger,
    TranslatorInterface $translator
  ) {
    $this->entityManager = $entityManager;
    $this->instanceService = $instanceService;
    $this->jobService = $jobService;
    $this->logger = $logger;
    $this->translator = $translator;
  }

  /**
   * List all Jobs.
   *
   * @Rest\Get("", name="jobs_api_list")
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve list of jobs",
   *     @OA\JsonContent(
   *         type="array",
   *         @OA\Items(ref=@Model(type=Job::class, groups={"read"}))
   *     )
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="jobs")
   */
  public function getJobsAction(): View
  {
    if (!$this->isGranted(User::ROLE_OPERATORE) && !$this->isGranted(User::ROLE_ADMIN)) {
      throw $this->createAccessDeniedException();
    }
    $result = $this->entityManager->getRepository(Job::class)->findBy([], ['createdAt' => 'asc']);

    return $this->view($result, Response::HTTP_OK);
  }

  /**
   * Retrieve a Job by id.
   *
   * @Rest\Get("/{id}", name="job_api_get")
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve a Job",
   *     @Model(type=Job::class, groups={"read"})
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Job not found"
   * )
   * @OA\Tag(name="jobs")
   */
  public function getJobAction(string $id): View
  {
    if (!$this->isGranted(User::ROLE_OPERATORE) && !$this->isGranted(User::ROLE_ADMIN)) {
      throw $this->createAccessDeniedException();
    }
    try {
      $repository = $this->entityManager->getRepository(Job::class);
      $result = $repository->find($id);
    } catch (\Exception $e) {
      return $this->view(['Object not found'], Response::HTTP_NOT_FOUND);
    }

    if (null === $result) {
      return $this->view(['Object not found'], Response::HTTP_NOT_FOUND);
    }

    return $this->view($result, Response::HTTP_OK);
  }

  /**
   * Create a Job.
   *
   * @Rest\Post(name="jobs_api_post")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The job to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=Job::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=201,
   *     description="Create a Job"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="jobs")
   */
  public function postJobAction(Request $request): View
  {
    if (!$this->isGranted(User::ROLE_OPERATORE) && !$this->isGranted(User::ROLE_ADMIN)) {
      throw $this->createAccessDeniedException();
    }

    $item = new Job();
    $form = $this->createForm(JobApiType::class, $item);
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors,
      ];

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {
      $item
        ->setStatus(Job::STATUS_PENDING)
        ->setTenant($this->instanceService->getCurrentInstance());

      $attachment = $request->request->get('attachment', false);
      if ($attachment) {
        $attachmentMimeType = $attachment['mime_type'];
        if ('text/csv' !== $attachmentMimeType) {
          $data = [
            'type' => 'error',
            'title' => $this->translator->trans('servizio.invalid_file_type'),
            'description' => $this->translator->trans('servizio.invalid_csv_file'),
          ];

          return $this->view($data, Response::HTTP_BAD_REQUEST);
        }

        if (filter_var($attachment['file'], FILTER_VALIDATE_URL)) {
          $fileContent = file_get_contents($attachment['file']);
          $md5 = md5($fileContent);
          $base64Content = base64_encode($fileContent);
          $file = new UploadedBase64File($base64Content, $attachmentMimeType);
        } else {
          $file = new UploadedBase64File($attachment['file'], $attachmentMimeType);
          $md5 = md5(base64_decode($attachment['file'], true));
        }

        if (isset($attachment['md5']) && $md5 !== $attachment['md5']) {
          $data = [
            'type' => 'error',
            'title' => $this->translator->trans('servizio.upload_error'),
            'description' => $this->translator->trans('servizio.error_md5_file_check'),
          ];

          return $this->view($data, Response::HTTP_BAD_REQUEST);
        }

        $item->setFile($file);
        $item->setOriginalFilename($attachment['name']);
      }

      $this->entityManager->persist($item);
      $this->entityManager->flush();

      $this->jobService->processJobAsync($item);
    } catch (ValidatorException $e) {
      $data = [
        'type' => 'error',
        'title' => $this->translator->trans('servizio.invalid_file_type'),
        'description' => $this->translator->trans('servizio.invalid_job'),
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request],
      );

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request],
      );

      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view($item, Response::HTTP_CREATED);
  }

  /**
   * Edit full Job.
   *
   * @Rest\Put("/{id}", name="jobs_api_put")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The job to update",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=Job::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Edit full Job"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="jobs")
   *
   * @param $id
   */
  public function putJobAction($id, Request $request): View
  {
    if (!$this->isGranted(User::ROLE_OPERATORE) && !$this->isGranted(User::ROLE_ADMIN)) {
      throw $this->createAccessDeniedException();
    }

    $repository = $this->entityManager->getRepository(Job::class);
    $item = $repository->find($id);

    if (!$item) {
      return $this->view(['Object not found'], Response::HTTP_NOT_FOUND);
    }

    $form = $this->createForm('App\Form\Api\JobApiType', $item);
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'put_validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors,
      ];

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {
      $this->entityManager->persist($item);
      $this->entityManager->flush();
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request],
      );

      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view(['Object Modified Successfully'], Response::HTTP_OK);
  }

  /**
   * Patch a Job.
   *
   * @Rest\Patch("/{id}", name="jobs_api_patch")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The job to update",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=Job::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Patch a Job"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="jobs")
   *
   * @param $id
   */
  public function patchJobAction($id, Request $request): View
  {
    if (!$this->isGranted(User::ROLE_OPERATORE) && !$this->isGranted(User::ROLE_ADMIN)) {
      throw $this->createAccessDeniedException();
    }

    $repository = $this->entityManager->getRepository(Job::class);
    $item = $repository->find($id);

    if (!$item) {
      return $this->view(['Object not found'], Response::HTTP_NOT_FOUND);
    }

    $form = $this->createForm('App\Form\Api\JobApiType', $item);
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors,
      ];

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {
      $this->entityManager->persist($item);
      $this->entityManager->flush();
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request],
      );

      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view(['Object Patched Successfully'], Response::HTTP_OK);
  }

  /**
   * Delete a Job.
   *
   * @Rest\Delete("/{id}", name="job_api_delete")
   *
   * @OA\Response(
   *     response=204,
   *     description="The resource was deleted successfully."
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Tag(name="jobs")
   *
   * @param $id
   */
  public function deleteJobAction($id): View
  {
    if (!$this->isGranted(User::ROLE_OPERATORE) && !$this->isGranted(User::ROLE_ADMIN)) {
      throw $this->createAccessDeniedException();
    }

    if (!Uuid::isValid($id)) {
      return $this->view(['Parameter id must be a valid uuid'], Response::HTTP_BAD_REQUEST);
    }
    $item = $this->entityManager->getRepository(Job::class)->find($id);
    if ($item) {
      $this->entityManager->remove($item);
      $this->entityManager->flush();
    }

    return $this->view(null, Response::HTTP_NO_CONTENT);
  }

  private function processForm(Request $request, FormInterface $form): void
  {
    $data = json_decode($request->getContent(), true);

    // Todo: find better way
    if (isset($data['args']) && count($data['args']) > 0) {
      $data['args'] = \json_encode($data['args']);
    }

    $clearMissing = 'PATCH' != $request->getMethod();
    $form->submit($data, $clearMissing);
  }
}
