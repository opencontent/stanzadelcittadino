<?php

declare(strict_types=1);

namespace App\Controller\Rest;

use App\Entity\Servizio;
use DateTime;
use Exception;
use App\Entity\Pratica;
use App\Services\InstanceService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;


/**
 * Class APIController
 * @property EntityManager em
 * @property InstanceService is
 * @package App\Controller
 */
class APIController extends AbstractFOSRestController
{
  /**
   * @var string
   */
  const CURRENT_API_VERSION = 'v1.0';

  /**
   * @var string
   */
  const SCHEDA_INFORMATIVA_REMOTE_PARAMETER = 'remote';

  private EntityManagerInterface $em;

  private InstanceService $is;

  public function __construct(EntityManagerInterface $em, InstanceService $is)
  {
    $this->em = $em;
    $this->is = $is;
  }

  /**
   * @Route("/v1.0/status",name="api_status")
   */
  public function statusAction(): JsonResponse
  {
    return new JsonResponse([
      'version' => self::CURRENT_API_VERSION,
      'status' => 'ok',
    ]);
  }

  /**
   * @Route("/v1.0/usage",name="api_usage")
   * @return JsonResponse
   * @throws \Exception
   */
  public function usageAction(): JsonResponse
  {
    $repo = $this->em->getRepository(Pratica::class);
    $pratiche = $repo->findSubmittedPraticheByEnte($this->is->getCurrentInstance());
    $serviziRepository = $this->getDoctrine()->getRepository(Servizio::class);
    $servizi = $serviziRepository->findBy(
      [
        'status' => [1],
      ]
    );

    $count = array_reduce($pratiche, static function ($acc, $el) {
        $year = (new DateTime())->setTimestamp($el->getSubmissionTime())->format('Y');
        try {
          ++$acc[$year];
        } catch (Exception $exception) {
          $acc[$year] = 1;
        }
        return $acc;
    }, []);

    return new JsonResponse([
      'version' => self::CURRENT_API_VERSION,
      'status' => 'ok',
      'servizi' => count($servizi),
      'pratiche' => $count,

    ]);
  }

  /**
   * @Route("/v1.0/user/{pratica}/notes",name="api_set_notes_for_pratica", methods={"POST"})
   */
  public function postNotesAction(Request $request, Pratica $pratica): Response
  {
    $user = $this->getUser();
    if ($pratica->getUser() !== $user) {
      return new Response(null, Response::HTTP_NOT_FOUND);
    }

    $newNote = $request->getContent();
    $pratica->setUserCompilationNotes($newNote);
    $this->getDoctrine()->getManager()->flush();

    return new Response();
  }

  /**
   * @Route("/v1.0/user/{pratica}/notes",name="api_get_notes_for_pratica", methods={"GET"})
   * @return JsonResponse|Response
   */
  public function getNotesAction(Pratica $pratica)
  {
    $user = $this->getUser();
    if ($pratica->getUser() !== $user) {
      return new JsonResponse(null, Response::HTTP_NOT_FOUND);
    }

    return new Response($pratica->getUserCompilationNotes());
  }

  /**
   * @Route("",name="api_base", methods={"GET"})
   */
  public function ping(): JsonResponse
  {
    return new JsonResponse(['Pong'], Response::HTTP_OK);
  }

}
