<?php

namespace App\Controller\Rest;

use App\Dto\AbstractUser;
use App\Dto\Admin;
use App\Dto\Operator;
use App\Dto\User;
use App\Entity\CPSUser;
use App\Entity\OperatoreUser;
use App\Entity\User as EntityUser;
use App\Form\Api\AdminApiType;
use App\Form\Api\OperatorApiType;
use App\Form\Api\UserApiType;
use App\Security\Voters\UserVoter;
use App\Services\Manager\UserManager;
use App\Utils\FormUtils;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/users")
 */
class UsersAPIController extends AbstractFOSRestController
{
  public const CURRENT_API_VERSION = '1.0';

  private LoggerInterface $logger;
  private EntityManagerInterface $entityManager;
  private UserManager $userManager;
  private UserPasswordHasherInterface $passwordHasher;

  /**
   * @param EntityManagerInterface $entityManager
   * @param LoggerInterface $logger
   * @param UserManager $userManager
   * @param UserPasswordHasherInterface $passwordHasher
   */
  public function __construct(
    EntityManagerInterface $entityManager,
    LoggerInterface $logger,
    UserManager $userManager,
    UserPasswordHasherInterface $passwordHasher
  ) {
    $this->logger = $logger;
    $this->entityManager = $entityManager;
    $this->userManager = $userManager;
    $this->passwordHasher = $passwordHasher;
  }

  /**
   * List all Users
   * @Rest\Get("", name="users_api_list")
   *
   * @Security(name="Bearer")
   *
   * @OA\Parameter(
   *     name="roles",
   *     description="Comma separated user's roles. Available roles are user, operator and admin",
   *     in="query",
   *      @OA\Schema(
   *          type="string",
   *          default="user"
   *      )
   * )
   *
   * @OA\Parameter(
   *     name="cf",
   *     in="query",
   *      @OA\Schema(
   *          type="string",
   *          example="XXXXXX00XZ00X000X"
   *      ),
   *     description="Fiscal code of the user. This filter is only available if the user role is selected"
   * )
   *
   * @OA\Parameter(
   *     name="username",
   *     in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *     description="User's username. This filter is only available if the operator role or the admin role is selected"
   * )
   *
   * @OA\Parameter(
   *     name="user_group_id",
   *     in="query",
   *      @OA\Schema(
   *          type="string",
   *          format="uuid"
   *      ),
   *     description="Operator's user group. This filter is only available if the operator role is selected"
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve list of users",
   *     @OA\JsonContent(
   *         type="array",
   *         @OA\Items(
   *          oneOf={
   *              @OA\Schema(ref=@Model(type=User::class, groups={"read"})),
   *              @OA\Schema(ref=@Model(type=Operator::class, groups={"read"})),
   *              @OA\Schema(ref=@Model(type=Admin::class, groups={ "read"}))
   *            }
   *        )
   *     )
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Tag(name="users")
   * @param Request $request
   * @return View
   */
  public function getUsersAction(Request $request): View
  {
    $user = $this->getUser();

    $roles = $request->query->get('roles', AbstractUser::USER_TYPE_CPS);
    $roles = explode(',', $roles);

    $fiscalCodeParameter = $request->query->get('cf', false);
    $userGroupId = $request->query->get('user_group_id', false);
    $username = $request->query->get('username', false);

    // Validate parameters with roles
    if ($fiscalCodeParameter and !in_array(AbstractUser::USER_TYPE_CPS, $roles)) {
      $data = [
        'type' => 'error',
        'title' => 'Invalid cf query parameter',
        'description' => 'You cannot search for a user by cf if you do not select the user role ',
      ];

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    if ($userGroupId && !in_array(AbstractUser::USER_TYPE_OPERATORE, $roles)) {
      $data = [
        'type' => 'error',
        'title' => 'Invalid user_group_id query parameter',
        'description' => 'You cannot search for a user by user_group_id if you do not select the operator role ',
      ];

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    if (
      $username
      && !in_array(AbstractUser::USER_TYPE_OPERATORE, $roles)
      && !in_array(AbstractUser::USER_TYPE_ADMIN, $roles)
    ) {
      $data = [
        'type' => 'error',
        'title' => 'Invalid username query parameter',
        'description' => 'You cannot search for a user by username if you do not select the operator or the admin role ',
      ];

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    $result = [];

    // USERS
    if (in_array(AbstractUser::USER_TYPE_CPS, $roles)) {
      $qb = $this->entityManager
        ->createQueryBuilder()
        ->select('user')
        ->from('App:CPSUser', 'user');

      // If requester user is a user return only itself
      if ($user instanceof CPSUser) {
        $qb
          ->andWhere('user.username = :username')
          ->setParameter('username', $user->getUsername());
      }

      if ($fiscalCodeParameter) {
        $qb
          ->andWhere('lower(user.codiceFiscale) = :cf')
          ->setParameter('cf', strtolower($fiscalCodeParameter));
      }

      $users = $qb->getQuery()->getResult();
      foreach ($users as $u) {
        $result[] = User::fromEntity($u);
      }
    }

    // OPERATORS
    if (in_array(AbstractUser::USER_TYPE_OPERATORE, $roles)) {
      // Only operators or admins can query operators
      if (!$this->isGranted(\App\Entity\User::ROLE_OPERATORE) && !$this->isGranted(EntityUser::ROLE_ADMIN)) {
        throw $this->createAccessDeniedException();
      }

      $qb = $this->entityManager
        ->createQueryBuilder()
        ->select('operator')
        ->from('App:OperatoreUser', 'operator');

      if ($username) {
        $qb
          ->andWhere('operator.username = :username')
          ->setParameter('username', $username);
      }

      if ($userGroupId) {
        $qb
          ->andWhere(':userGroupId MEMBER OF operator.userGroups')
          ->setParameter('userGroupId', $userGroupId);
      }

      $operators = $qb->getQuery()->getResult();
      foreach ($operators as $o) {
        $result[] = Operator::fromEntity($o);
      }
    }

    // ADMINS
    if (in_array(AbstractUser::USER_TYPE_ADMIN, $roles)) {
      // Only operators or admins can query admins
      if (!$this->isGranted(EntityUser::ROLE_OPERATORE) && !$this->isGranted(EntityUser::ROLE_ADMIN)) {
        throw $this->createAccessDeniedException();
      }

      $qb = $this->entityManager
        ->createQueryBuilder()
        ->select('admin')
        ->from('App:AdminUser', 'admin');

      if ($username) {
        $qb
          ->andWhere('admin.username = :username')
          ->setParameter('username', $username);
      }

      $admins = $qb->getQuery()->getResult();
      foreach ($admins as $a) {
        $result[] = Admin::fromEntity($a);
      }
    }

    $context = new Context();
    $context->setGroups(['read']);

    return $this->view($result, Response::HTTP_OK)->setContext($context);
  }

  /**
   * Retrieve a User by id
   * @Rest\Get("/{id}", name="user_api_get")
   *
   * @Security(name="Bearer")
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve a User",
   *     @OA\JsonContent(
   *       oneOf={
   *          @OA\Schema(ref=@Model(type=User::class, groups={"read"})),
   *          @OA\Schema(ref=@Model(type=Operator::class, groups={"read"})),
   *          @OA\Schema(ref=@Model(type=Admin::class, groups={ "read"}))
   *       }
   *    )
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="User not found"
   * )
   * @OA\Tag(name="users")
   *
   * @param Request $request
   * @param string $id
   * @return View
   */
  public function getUserAction(Request $request, string $id): View
  {
    try {
      $repository = $this->entityManager->getRepository(EntityUser::class);
      $result = $repository->find($id);
    } catch (\Exception $e) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    if (!$result) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    $this->denyAccessUnlessGranted(UserVoter::VIEW, $result);

    try {
      if ($result instanceof CPSUser) {
        $user = User::fromEntity($result);
      } elseif ($result instanceof OperatoreUser) {
        $user = Operator::fromEntity($result);
      } else {
        $user = Admin::fromEntity($result);
      }

      $context = new Context();
      $context->setGroups(['read']);

      return $this->view($user, Response::HTTP_OK)->setContext($context);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error',
        'description' => $e->getMessage(),
      ];

      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Create a User
   * @Rest\Post(name="users_api_post")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The user to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             oneOf={
   *                @OA\Schema(ref=@Model(type=User::class, groups={"write"})),
   *                @OA\Schema(ref=@Model(type=Operator::class, groups={"write"})),
   *                @OA\Schema(ref=@Model(type=Admin::class, groups={ "write"}))
   *            }
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=201,
   *     description="Create a User"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="users")
   *
   * @param Request $request
   * @return View
   */
  public function postUserAction(Request $request): View
  {

    $role = $request->request->get('role', 'user');
    $password = $request->request->get('password', '');

    if (!in_array($role, AbstractUser::USER_TYPES)) {
      $data = [
        'title' => 'Role specified is incorrect',
        'detail' => 'Role specified is incorrect, allowed values are: ' . implode(', ', AbstractUser::USER_TYPES),
        'status' => Response::HTTP_BAD_REQUEST,
      ];

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {
      $userDto = new User();
      switch ($role) {
        case AbstractUser::USER_TYPE_CPS:
          $userDto = new User();
          $form = $this->createForm(UserApiType::class, $userDto);
          break;
        case AbstractUser::USER_TYPE_OPERATORE:
          $userDto = new Operator();
          $form = $this->createForm(OperatorApiType::class, $userDto);
          break;
        case AbstractUser::USER_TYPE_ADMIN:
          $userDto = new Admin();
          $form = $this->createForm(AdminApiType::class, $userDto);
          break;
      }

      $this->processForm($request, $form);
      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'title' => 'There was a validation error',
          'errors' => $errors,
        ];

        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      // Adesso che ho un entità Utente verifico i permessi tramite voter
      $user = $userDto->toEntity();
      $this->denyAccessUnlessGranted(UserVoter::EDIT, $user);

      $user
        ->setEnabled(true)
        ->setPassword($this->passwordHasher->hashPassword($user, $password))
        ->setLastChangePassword(new \DateTime());


      $this->userManager->save($user);
    } catch (UniqueConstraintViolationException $e) {
      $data = [
        'type' => 'error',
        'title' => 'Duplicate user',
        'description' => 'An user with this passed fiscal code / username is already present',
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request],
      );

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request],
      );

      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    $context = new Context();
    $context->setGroups(['read']);

    return $this->view(AbstractUser::modelFromEntity($user), Response::HTTP_CREATED)->setContext($context);
  }

  /**
   * Edit full User
   * @Rest\Put("/{id}", name="users_api_put")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="Edit full user",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             oneOf={
   *                @OA\Schema(ref=@Model(type=User::class, groups={"write"})),
   *                @OA\Schema(ref=@Model(type=Operator::class, groups={"write"})),
   *                @OA\Schema(ref=@Model(type=Admin::class, groups={ "write"}))
   *            }
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Edit full User"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="users")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function putUserAction($id, Request $request): View
  {
    $repository = $this->entityManager->getRepository(EntityUser::class);
    $user = $repository->find($id);

    if (!$user) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    $this->denyAccessUnlessGranted(UserVoter::EDIT, $user);

    if ($user instanceof CPSUser) {
      $userDto = User::fromEntity($user);
      $form = $this->createForm(UserApiType::class, $userDto);
    } elseif ($user instanceof OperatoreUser) {
      $userDto = Operator::fromEntity($user);
      $form = $this->createForm(OperatorApiType::class, $userDto);
    } else {
      $userDto = Admin::fromEntity($user);
      $form = $this->createForm(AdminApiType::class, $userDto);
    }

    try {
      $this->processForm($request, $form);

      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'put_validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors,
        ];

        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $user = $userDto->toEntity($user);

      $this->userManager->save($user);
    } catch (\Exception $e) {

      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request],
      );

      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view(["Object Modified Successfully"], Response::HTTP_OK);
  }

  /**
   * Patch a user
   * @Rest\Patch("/{id}", name="users_api_patch")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="Patch an user",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             oneOf={
   *                @OA\Schema(ref=@Model(type=User::class, groups={"write"})),
   *                @OA\Schema(ref=@Model(type=Operator::class, groups={"write"})),
   *                @OA\Schema(ref=@Model(type=Admin::class, groups={ "write"}))
   *            }
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Patch a User"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="users")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function patchUserAction($id, Request $request): View
  {

    $repository = $this->entityManager->getRepository(EntityUser::class);
    $user = $repository->find($id);
    if (!$user) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    $this->denyAccessUnlessGranted(UserVoter::EDIT, $user);

    if ($user instanceof CPSUser) {
      $userDto = User::fromEntity($user);
      $form = $this->createForm(UserApiType::class, $userDto);
    } elseif ($user instanceof OperatoreUser) {
      $userDto = Operator::fromEntity($user);
      $form = $this->createForm(OperatorApiType::class, $userDto);
    } else {
      $userDto = Admin::fromEntity($user);
      $form = $this->createForm(AdminApiType::class, $userDto);
    }

    try {
      $this->processForm($request, $form);

      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors,
        ];

        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $user = $userDto->toEntity($user);

      $this->userManager->save($user);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request],
      );

      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view(["Object Patched Successfully"], Response::HTTP_OK);
  }

  /**
   * Delete a User
   * @Rest\Delete("/{id}", name="users_api_delete", methods={"DELETE"})
   *
   * @OA\Response(
   *     response=204,
   *     description="The resource was deleted successfully."
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   *
   * @OA\Tag(name="users")
   *
   * @param $id
   * @return View
   */
  public function deleteAction($id): View
  {
    $user = $this->entityManager->getRepository(EntityUser::class)->find($id);

    if (!$user) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    $this->denyAccessUnlessGranted(UserVoter::DELETE, $user);
    $this->userManager->remove($user);

    return $this->view(null, Response::HTTP_NO_CONTENT);
  }

  /**
   * @param Request $request
   * @param FormInterface $form
   */
  private function processForm(Request $request, FormInterface $form)
  {
    $data = json_decode($request->getContent(), true);

    $clearMissing = $request->getMethod() !== 'PATCH';
    $form->submit($data, $clearMissing);
  }
}
