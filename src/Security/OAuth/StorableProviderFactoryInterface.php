<?php

namespace App\Security\OAuth;

interface StorableProviderFactoryInterface
{
  public function storeConfiguration(string $providerIdentifier, Configuration $configuration): void;
}
