<?php

namespace App\Security\OAuth;

use App\Entity\UserSession;

interface LogoutProviderInterface
{
  public function setUrlLogout(string $logoutUrl): void;

  public function getUrlLogout(?UserSession $userSession): string;
}
