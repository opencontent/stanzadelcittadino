<?php


namespace App\Security\Voters;


use App\Entity\Calendar;
use App\Entity\OperatoreUser;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class CalendarVoter extends Voter
{
  public const VIEW = 'view';
  public const EDIT = 'edit';
  public const DELETE = 'delete';


  private Security $security;

  /**
   * @param Security $security
   */
  public function __construct(Security $security)
  {
    $this->security = $security;
  }

  /**
   * @param $attribute
   * @param $subject
   * @return bool
   */
  protected function supports($attribute, $subject): bool
  {
    // if the attribute isn't one we support, return false
    if (!in_array($attribute, [self::VIEW, self::EDIT, self::DELETE])) {
      return false;
    }

    // only vote on `Calendar` objects
    if ($subject && !$subject instanceof Calendar) {
      return false;
    }

    return true;
  }

  /**
   * @param $attribute
   * @param $subject
   * @param TokenInterface $token
   * @return bool
   */
  protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
  {
    $user = $token->getUser();

    if (!$user instanceof UserInterface) {
      // the user must be logged in; if not, deny access
      return false;
    }

    // you know $subject is a Calendar object, thanks to `supports()`
    /** @var Calendar $calendar */
    $calendar = $subject;

    switch ($attribute) {
      case self::VIEW:
        return $this->canView($calendar, $user);
      case self::EDIT:
        return $this->canEdit($calendar, $user);
      case self::DELETE:
        return $this->canDelete($calendar, $user);

    }

    throw new \LogicException('This code should not be reached!');
  }

  /**
   * @param Calendar $calendar
   * @param UserInterface $user
   * @return bool
   */
  private function canView(Calendar $calendar, UserInterface $user): bool
  {
    // if they can edit, they can view
    if ($this->canEdit($calendar, $user)) {
      return true;
    }

    return false;
  }

  /**
   * @param Calendar $calendar
   * @param UserInterface $user
   * @return bool
   */
  private function canEdit(Calendar $calendar, UserInterface $user): bool
  {
    if ($this->security->isGranted('ROLE_ADMIN')) {
      return true;
    }
    if ($this->security->isGranted('ROLE_OPERATORE')) {
      /** @var OperatoreUser $user */
      if ($user->getId() == $calendar->getOwnerId() || in_array($user->getId(), $calendar->getModeratorsId())) {
        return true;
      }
    }

    return false;
  }

  /**
   * @param Calendar $calendar
   * @param UserInterface $user
   * @return bool
   */
  private function canDelete(Calendar $calendar, UserInterface $user): bool
  {
    if ($this->security->isGranted('ROLE_ADMIN')) {
      return true;
    }
    if ($this->security->isGranted('ROLE_OPERATORE')) {
      /** @var OperatoreUser $user */
      if ($user->getId() == $calendar->getOwnerId()) {
        return true;
      }
    }

    return false;
  }
}
