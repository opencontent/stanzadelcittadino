<?php


namespace App\Security\Voters;


use App\Entity\Allegato;
use App\Entity\CPSUser;
use App\Entity\OperatoreUser;
use App\Entity\Pratica;
use App\Entity\RichiestaIntegrazione;
use App\Entity\RispostaOperatore;
use App\Services\Manager\PraticaManager;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class AttachmentVoter extends Voter
{

  public const DOWNLOAD = 'download';
  public const EDIT = 'edit';
  public const UPLOAD = 'upload';
  public const DELETE = 'delete';

  private Security $security;
  private SessionInterface $session;
  private $hashValidity;

  /**
   * AttachmentVoter constructor.
   * @param Security $security
   * @param SessionInterface $session
   * @param $hashValidity
   */
  public function __construct(Security $security, SessionInterface $session, $hashValidity)
  {
    $this->security = $security;
    $this->session = $session;
    $this->hashValidity = $hashValidity;
  }

  /**
   * @param string $attribute
   * @param mixed $subject
   * @return bool
   */
  protected function supports($attribute, $subject): bool
  {
    if (!in_array($attribute, [
      self::DOWNLOAD,
      self::EDIT,
      self::UPLOAD,
      self::DELETE
    ])) {
      return false;
    }

    if ($subject && !$subject instanceof Allegato) {
      return false;
    }

    return true;
  }

  protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
  {
    $user = $token->getUser();

    if (!$user instanceof UserInterface) {
      // the user must be logged in; if not, deny access
      return false;
    }

    // you know $subject is a Pratica object, thanks to `supports()`
    /** @var Allegato $attachment */
    $attachment = $subject;

    switch ($attribute) {
      case self::UPLOAD:
      case self::EDIT:
        return $this->canEdit($attachment, $user);
      case self::DOWNLOAD:
        return $this->canDownload($attachment, $user);
      case self::DELETE:
        return $this->canDelete($attachment, $user);

    }

    throw new \LogicException('This code should not be reached!');
  }

  /**
   * @param Allegato $attachment
   * @param UserInterface $user
   * @return bool
   */
  private function canDownload(Allegato $attachment, UserInterface $user): bool
  {
    if ($this->security->isGranted('ROLE_ADMIN')) {
      return true;
    }

    if ($this->security->isGranted('ROLE_OPERATORE')) {
      /** @var OperatoreUser $user */
      foreach ($attachment->getPratiche() as $pratica) {
        if (PraticaManager::isOperatorEnabled($pratica, $user) || PraticaManager::isOperatorAssigned($pratica, $user)) {
          return true;
        }
      }

      if ($attachment instanceof RichiestaIntegrazione) {
        $pratica = $attachment->getPratica();
        if (!$pratica instanceof Pratica) {
          return false;
        }
        if (PraticaManager::isOperatorEnabled($pratica, $user) || PraticaManager::isOperatorAssigned($pratica, $user)) {
          return true;
        }
      }

      // Fixme: permetto sempre il download della risposta da parte degli operatori. Da sistemare una volta riorganizzati gli allegati
      if ($attachment instanceof RispostaOperatore) {
        return true;
      }

    }

    if ($attachment->getOwner() === $user) {
      return true;
    }

    $canDownload = false;
    $pratica = $attachment->getPratiche()->first();
    if ($pratica instanceof Pratica) {
      if ($user instanceof CPSUser) {
        $relatedCFs = $pratica->getRelatedCFs();
        $canDownload = ((is_array($relatedCFs) && in_array($user->getCodiceFiscale(), $relatedCFs)) || $pratica->getUser() == $user);
      } elseif ($this->session->isStarted() && $this->session->has(Pratica::HASH_SESSION_KEY)) {
        $canDownload = $pratica->isValidHash($this->session->get(Pratica::HASH_SESSION_KEY), $this->hashValidity);
      }
    }

    return $canDownload;

  }

  /**
   * @param Allegato $attachment
   * @param UserInterface $user
   * @return bool
   */
  private function canEdit(Allegato $attachment, UserInterface $user): bool
  {
    if ($this->security->isGranted('ROLE_ADMIN')) {
      return true;
    }

    if ($this->security->isGranted('ROLE_OPERATORE')) {
      return true;
    }

    if ($attachment->getOwner() === $user) {
      return true;
    }

    if (
      $this->session->isStarted()
      && $attachment->getHash() === hash('sha256', $this->session->getId())
    ) {
      return true;
    }

    return false;
  }

  private function canDelete(Allegato $attachment, UserInterface $user): bool
  {
    /** @var Pratica $pratica */
    $pratica = $attachment->getPratiche()->first();

    // Se sono operatore o il proprietario della pratica  e l'allegato non è ancora stato agganciato ad una pratica posso eliminarlo
    // Todo: da rivedere
    if (($attachment->getOwner() === $user || $this->security->isGranted('ROLE_OPERATORE')) && !$pratica) {
      return true;
    }

    if ($pratica  && $pratica->getStatus() != Pratica::STATUS_DRAFT) {
      return false;
    }

    if ($attachment->getOwner() === $user) {
      return true;
    }

    if (
      $this->session->isStarted()
      && $attachment->getHash() === hash('sha256', $this->session->getId())
    ) {

      return true;
    }

    return false;
  }
}
