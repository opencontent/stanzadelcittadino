<?php

namespace App\Security;

use App\Dto\UserAuthenticationData;
use App\Entity\CPSUser;
use App\Services\InstanceService;
use App\Services\UserSessionService;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class PatAuthenticator extends AbstractAuthenticator
{
  private $shibboletServerVarNames;
  private LoggerInterface $logger;


  /**
   * OpenLoginAuthenticator constructor.
   * @param UrlGeneratorInterface $urlGenerator
   * @param array $shibboletServerVarNames
   * @param $loginRoute
   * @param UserSessionService $userSessionService
   * @param JWTTokenManagerInterface $JWTTokenManager
   * @param LoggerInterface $logger
   */
  public function __construct(
    UrlGeneratorInterface $urlGenerator,
    $shibboletServerVarNames,
    $loginRoute,
    UserSessionService $userSessionService,
    JWTTokenManagerInterface $JWTTokenManager,
    LoggerInterface $logger
  ) {
    $this->urlGenerator = $urlGenerator;
    $this->shibboletServerVarNames = $shibboletServerVarNames;
    $this->loginRoute = $loginRoute;
    $this->userSessionService = $userSessionService;
    $this->JWTTokenManager = $JWTTokenManager;
    $this->logger = $logger;
  }

  public function supports(Request $request): bool
  {
    try {
      $this->checkLoginRoute();
    } catch (\Exception $e) {
      return false;
    }

    return $request->attributes->get('_route') === 'login_pat' && $this->checkShibbolethUserData($request);
  }


  private function checkShibbolethUserData(Request $request): bool
  {

    $spid = [];
    $request->server->add($spid);

    if ( !$request->server->get($this->shibboletServerVarNames['shibSessionId']) ||
         !$request->server->get($this->shibboletServerVarNames['shibAuthenticationIstant']) ||
         !$request->server->get($this->shibboletServerVarNames['shibSessionIndex'])
    ) {
      return false;
    }
    return true;
  }

  protected function getLoginRouteSupported(): array
  {
    return ['login_pat'];
  }

  protected function getRequestDataToStoreInUserSession(Request $request): array
  {
    return $this->createUserDataFromRequest($request);
  }

  /**
   * @param Request $request
   * @return array
   */
  protected function createUserDataFromRequest(Request $request): array
  {
    $userDataKeys = array_flip($this->shibboletServerVarNames);
    $serverProps = $request->server->all();
    $data = [];
    foreach ($userDataKeys as $shibbKey => $ourKey) {
      $data[$ourKey] = isset($serverProps[$shibbKey]) ? $serverProps[$shibbKey] : null;
    }

    // Fallback on session
    if ($data[self::KEY_PARAMETER_NAME] == null) {
      $data = $request->getSession()->get('user_data');
    }

    return $data;
  }

  /**
   * @param Request $request
   * @param UserInterface $user
   * @return UserAuthenticationData
   * @throws \Exception
   */
  protected function getUserAuthenticationData(Request $request, UserInterface $user): UserAuthenticationData
  {

    // Spid
    if ($request->server->has($this->shibboletServerVarNames['spidCode']) && !empty($request->server->get($this->shibboletServerVarNames['spidCode']))) {
      $data = [
        'authenticationMethod' => CPSUser::IDP_SPID,
        'sessionId' => $request->server->get($this->shibboletServerVarNames['shibSessionId']),
        'spidCode' => $request->server->get($this->shibboletServerVarNames['spidCode']),
        'instant' => $request->server->get($this->shibboletServerVarNames['shibAuthenticationIstant']),
        'sessionIndex' => $request->server->get($this->shibboletServerVarNames['shibSessionIndex']),
        'spidLevel' => $request->server->get($this->shibboletServerVarNames['spidLevel'] ?? ''),
      ];
      return UserAuthenticationData::fromArray($data);
    }

    // Cps
    if (
      $request->server->get($this->shibboletServerVarNames['x509certificate_issuerdn']) &&
      !empty($request->server->get($this->shibboletServerVarNames['x509certificate_issuerdn'])) &&
      $request->server->get($this->shibboletServerVarNames['x509certificate_subjectdn']) &&
      !empty($request->server->get($this->shibboletServerVarNames['x509certificate_subjectdn'])) &&
      $request->server->get($this->shibboletServerVarNames['x509certificate_base64']) &&
      !empty($request->server->get($this->shibboletServerVarNames['x509certificate_base64']))
    ) {
      $data = [
        'authenticationMethod' => CPSUser::IDP_CPS_OR_CNS,
        'sessionId' => $request->server->get($this->shibboletServerVarNames['shibSessionId']),
        'certificateIssuer' => $request->server->get($this->shibboletServerVarNames['x509certificate_issuerdn']),
        'certificateSubject' => $request->server->get($this->shibboletServerVarNames['x509certificate_subjectdn']),
        'certificate' => $request->server->get($this->shibboletServerVarNames['x509certificate_base64']),
        'instant' => $request->server->get($this->shibboletServerVarNames['shibAuthenticationIstant']),
        'sessionIndex' => $request->server->get($this->shibboletServerVarNames['shibSessionIndex'])
      ];
      return UserAuthenticationData::fromArray($data);
    }

    // Sirac Other
    if (
      !$request->server->has($this->shibboletServerVarNames['spidCode']) &&
      $request->server->has($this->shibboletServerVarNames['idpType']) &&
      strtolower($request->server->get($this->shibboletServerVarNames['idpType'])) === CPSUser::IDP_OTHER
    ) {
      $data = [
        'authenticationMethod' => CPSUser::IDP_OTHER,
        'sessionId' => $request->server->get($this->shibboletServerVarNames['shibSessionId']),
        'instant' => $request->server->get($this->shibboletServerVarNames['shibAuthenticationIstant']),
        'sessionIndex' => $request->server->get($this->shibboletServerVarNames['shibSessionIndex']),
        'spidLevel' => $request->server->get($this->shibboletServerVarNames['shibAuthnContextClass'] ?? ''),
      ];
      return UserAuthenticationData::fromArray($data);
    }

    // Sirac Cns
    if (
      !$request->server->has($this->shibboletServerVarNames['spidCode']) &&
      $request->server->has($this->shibboletServerVarNames['idpType']) &&
      strtolower($request->server->get($this->shibboletServerVarNames['idpType'])) === CPSUser::IDP_CNS
    ) {
      $data = [
        'authenticationMethod' => CPSUser::IDP_CPS_OR_CNS,
        'sessionId' => $request->server->get($this->shibboletServerVarNames['shibSessionId']),
        'instant' => $request->server->get($this->shibboletServerVarNames['shibAuthenticationIstant']),
        'sessionIndex' => $request->server->get($this->shibboletServerVarNames['shibSessionIndex']),
        'spidLevel' => $request->server->get($this->shibboletServerVarNames['shibAuthnContextClass'] ?? ''),
      ];
      return UserAuthenticationData::fromArray($data);
    }

    // Cie L1
    if ( $request->server->has($this->shibboletServerVarNames['shibAuthnContextClass'])
      && $request->server->get($this->shibboletServerVarNames['shibAuthnContextClass']) === 'https://www.spid.gov.it/SpidL1'
      && (!$request->server->has($this->shibboletServerVarNames['spidCode']) || empty($request->server->get($this->shibboletServerVarNames['spidCode'])))
    ) {
      $data = [
        'authenticationMethod' => CPSUser::IDP_CIE,
        'sessionId' => $request->server->get($this->shibboletServerVarNames['shibSessionId']),
        'instant' => $request->server->get($this->shibboletServerVarNames['shibAuthenticationIstant']),
        'sessionIndex' => $request->server->get($this->shibboletServerVarNames['shibSessionIndex']),
      ];
      return UserAuthenticationData::fromArray($data);
    }

    // Cie L2
    if ( $request->server->has($this->shibboletServerVarNames['shibAuthnContextClass'])
      && ($request->server->get($this->shibboletServerVarNames['shibAuthnContextClass']) === 'urn:oasis:names:tc:SAML:2.0:ac:classes:TimeSyncToken')
      && (!$request->server->has($this->shibboletServerVarNames['spidCode']) || empty($request->server->get($this->shibboletServerVarNames['spidCode'])))
      && (!$request->server->has($this->shibboletServerVarNames['x509certificate_base64']) || empty($request->server->get($this->shibboletServerVarNames['x509certificate_base64'])))
    ) {
      $data = [
        'authenticationMethod' => CPSUser::IDP_CIE,
        'sessionId' => $request->server->get($this->shibboletServerVarNames['shibSessionId']),
        'instant' => $request->server->get($this->shibboletServerVarNames['shibAuthenticationIstant']),
        'sessionIndex' => $request->server->get($this->shibboletServerVarNames['shibSessionIndex']),
      ];
      return UserAuthenticationData::fromArray($data);
    }

    // Cie L3
    if ( $request->server->has($this->shibboletServerVarNames['shibAuthnContextClass'])
         && ($request->server->get($this->shibboletServerVarNames['shibAuthnContextClass']) === 'urn:oasis:names:tc:SAML:2.0:ac:classes:Smartcard' || $request->server->get($this->shibboletServerVarNames['shibAuthnContextClass']) === 'https://www.spid.gov.it/SpidL2')
         && (!$request->server->has($this->shibboletServerVarNames['spidCode']) || empty($request->server->get($this->shibboletServerVarNames['spidCode'])))
         && (!$request->server->has($this->shibboletServerVarNames['x509certificate_base64']) || empty($request->server->get($this->shibboletServerVarNames['x509certificate_base64'])))
      ) {
      $data = [
        'authenticationMethod' => CPSUser::IDP_CIE,
        'sessionId' => $request->server->get($this->shibboletServerVarNames['shibSessionId']),
        'instant' => $request->server->get($this->shibboletServerVarNames['shibAuthenticationIstant']),
        'sessionIndex' => $request->server->get($this->shibboletServerVarNames['shibSessionIndex']),
      ];
      return UserAuthenticationData::fromArray($data);
    }

    // Fallback: preserva il login che è avvenuto ma non riesco a capire idp type
    /*$data = [
      'authenticationMethod' => CPSUser::IDP_OTHER,
      'sessionId' => $request->server->get($this->shibboletServerVarNames['shibSessionId']),
      'instant' => $request->server->get($this->shibboletServerVarNames['shibAuthenticationIstant']),
      'sessionIndex' => $request->server->get($this->shibboletServerVarNames['shibSessionIndex']),
    ];
    return UserAuthenticationData::fromArray($data);*/

    $this->logger->error('PatAuthenticator - insufficient authentication data', [
      'sessionId' => $request->server->get($this->shibboletServerVarNames['shibSessionId']),
      'spidCode' => $request->server->get($this->shibboletServerVarNames['spidCode'] ?? ''),
      'instant' => $request->server->get($this->shibboletServerVarNames['shibAuthenticationIstant']),
      'sessionIndex' => $request->server->get($this->shibboletServerVarNames['shibSessionIndex']),
      'spidLevel' => $request->server->get($this->shibboletServerVarNames['spidLevel'] ?? ''),
      'shibAuthnContextClass' => $request->server->get($this->shibboletServerVarNames['shibAuthnContextClass'] ?? ''),
      'certificateIssuer' => $request->server->get($this->shibboletServerVarNames['x509certificate_issuerdn'] ?? ''),
      'certificateSubject' => $request->server->get($this->shibboletServerVarNames['x509certificate_subjectdn'] ?? ''),
      'certificate' => $request->server->get($this->shibboletServerVarNames['x509certificate_base64'] ?? ''),
    ]);

    throw new \Exception('PatAuthenticator - insufficient authentication data');
  }
}
