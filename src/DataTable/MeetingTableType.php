<?php

namespace App\DataTable;


use App\Entity\Meeting;
use App\Entity\OperatoreUser;
use App\Entity\Servizio;
use DateTime;
use Doctrine\ORM\QueryBuilder;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\Column\MapColumn;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\TwigColumn;
use Omines\DataTablesBundle\DataTable;
use Omines\DataTablesBundle\DataTableTypeInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class MeetingTableType implements DataTableTypeInterface
{
  public function configure(DataTable $dataTable, array $options): void
  {
    $dataTable
      ->add('fromTime', DateTimeColumn::class, [
        'label' => 'date',
        'format' => 'd/m/y H:i',
        'orderable' => true,
        'searchable' => false,
      ])
      ->add('slots', TwigColumn::class, [
        'label' => 'schedule',
        'orderable' => false,
        'searchable' => false,
        'template' => 'Admin/table/calendars/_schedule.html.twig',
      ])
      ->add('user', TwigColumn::class, [
        'label' => 'meetings.labels.user',
        'orderable' => false,
        'searchable' => false,
        'template' => 'Admin/table/calendars/_user.html.twig',
      ])
      ->add('email', TextColumn::class, [
        'label' => 'general.email',
        'orderable' => false,
        'searchable' => false
      ])
      ->add('fiscalCode', TextColumn::class, [
        'label' => 'user.profile.codice_fiscale',
        'orderable' => false,
        'searchable' => false,
      ])
      ->add('phoneNumber', TextColumn::class, [
        'label' => 'pratica.pdf.telefono',
        'orderable' => false,
        'searchable' => false,
      ])
      ->add('code', TextColumn::class, [
        'label' => 'code_generation.code',
        'orderable' => false,
        'searchable' => false,
      ])
      ->add('getStatusName', TextColumn::class, [
        'label' => 'servizio.stato',
        'orderable' => false,
        'searchable' => false,
      ])
      ->add('rescheduled', TextColumn::class, [
        'label' => '<i class="fa fa-refresh" aria-hidden="true"></i>',
        'orderable' => false,
        'searchable' => false,
      ])
      ->createAdapter(ORMAdapter::class, [
        'entity' => Meeting::class,
        'query' => function (QueryBuilder $builder) use ($options): void {
          $qb = $builder
            ->select('meeting', 'user')
            ->from(Meeting::class, 'meeting')
            ->leftJoin('meeting.user', 'user')
            ->leftJoin('meeting.calendar', 'calendar')
            ->where('meeting.calendar = :calendar')
            ->andWhere('meeting.status != :draft')
            //->andWhere('meeting.fromTime >= :rangeLimit')
            ->setParameter('calendar', $options['calendar'])
            ->setParameter('draft', Meeting::STATUS_DRAFT)
            //->setParameter('rangeLimit', (new DateTime())->modify('-1 months'));
          ;
        },
      ])
      ;
  }
}
