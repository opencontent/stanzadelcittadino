<?php

namespace App\DataTable;


use App\Entity\ScheduledAction;
use Doctrine\ORM\QueryBuilder;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\Column\MapColumn;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\TwigColumn;
use Omines\DataTablesBundle\DataTable;
use Omines\DataTablesBundle\DataTableTypeInterface;
use Omines\DataTablesBundle\Filter\ChoiceFilter;
use Symfony\Contracts\Translation\TranslatorInterface;

class ScheduledActionTableType implements DataTableTypeInterface
{

  public const TYPE_MAPPER = [
    'createForPratica' => 'scheduled_actions.create_pdf',
    'updateForPratica' => 'scheduled_actions.update_pdf',
    'protocollo.sendPratica' => 'scheduled_actions.protocollo_send_pratice',
    'protocollo.sendAllegati' => 'scheduled_actions.protocollo_send_attach',
    'protocollo.sendRitiro' => 'scheduled_actions.protocollo_send_withdraw',
    'protocollo.sendRichiesteIntegrazione' => 'scheduled_actions.protocollo_send_integrations',
    'protocollo.refreshPratica' => 'scheduled_actions.protocollo_refresh_pratice',
    'protocollo.uploadFile' => 'scheduled_actions.protocollo_upload_file',
    'giscom.sendPratica' => 'scheduled_actions.giscom_send_pratice',
    'giscom.askCFs' => 'scheduled_actions.giscom_askCFs',
    'application_webhook' => 'scheduled_actions.application_webhook',
    'application_payment_reminder' => 'scheduled_actions.application_payment_reminder',
    'produce_message' => 'scheduled_actions.produce_message',
    'process_job' => 'scheduled_actions.process_job',
    'meeting_reminder' => 'scheduled_actions.meeting_reminder',
    'meeting_delete_cancelled' => 'scheduled_actions.meeting_delete_cancelled',
  ];

  private array $translatedType = [];

  public function __construct(TranslatorInterface $translator)
  {
    foreach (self::TYPE_MAPPER as $k => $v) {
      $this->translatedType[$k] = $translator->trans($v);
    }

  }

  public function configure(DataTable $dataTable, array $options): void
  {

    $filter = new ChoiceFilter();
    $filter->set([
      'operator' => '=',
      'choices' => ['', '1', '3', '4']
    ]);

    $dataTable
      ->add('id', TwigColumn::class, [
        'className' => 'overflow-table',
        'label' => 'Id',
        'orderable' => false,
        'searchable' => false,
        'template' => 'Admin/table/scheduledActions/_id.html.twig',
      ])
      ->add('type', TextColumn::class, [
        'label' => 'type',
        'orderable' => false,
        'searchable' => true,
        'render' => function ($value) {
          return $this->translatedType[$value];
        }
      ])
      ->add('params', TwigColumn::class, [
        'className' => 'overflow-table',
        'label' => 'parameters',
        'orderable' => false,
        'searchable' => true,
        'operator' => 'LIKE',
        'rightExpr' => function ($value) {
          return '%' . $value . '%';
        },
        'template' => 'Admin/table/scheduledActions/_params.html.twig',
      ])
      ->add('hostname', TextColumn::class, [
        'label' => 'Host',
        'orderable' => false,
        'searchable' => false,
        'visible' => false
      ])
      ->add('retry', TextColumn::class, [
        'label' => '#',
        'orderable' => true,
        'searchable' => false,
      ])
      ->add('status', TwigColumn::class, [
        'label' => 'general.stato',
        'globalSearchable' => false,
        'orderable' => false,
        'searchable' => true,
        'field' => 'scheduled_action.status',
        'filter' => $filter,
        'template' => 'Admin/table/scheduledActions/_status.html.twig',
      ])
      ->add('log', TextColumn::class, [
        'label' => 'Log',
        'visible' => false,
        'orderable' => false,
        'searchable' => false,
      ])
      ->add('createdAt', DateTimeColumn::class, [
        'label' => 'created_at',
        'format' => 'd/m/y H:i',
        'orderable' => true,
        'searchable' => false,
      ])
      ->add('updatedAt', DateTimeColumn::class, [
        'label' => 'updated_at',
        'format' => 'd/m/y H:i',
        'orderable' => true,
        'searchable' => false,
      ])
      ->add('executeAt', DateTimeColumn::class, [
        'label' => 'execute_at',
        'format' => 'd/m/y H:i',
        'orderable' => true,
        'searchable' => false,
      ])
      ->add('actions', TwigColumn::class, [
        'label' => '',
        'orderable' => false,
        'searchable' => false,
        'template' => 'Admin/table/scheduledActions/_actions.html.twig',
      ])
      ->createAdapter(ORMAdapter::class, [
        'entity' => ScheduledAction::class,
        'query' => function (QueryBuilder $builder) use ($options): void {

          $qb = $builder
            ->select('scheduled_action')
            ->from(ScheduledAction::class, 'scheduled_action');

          if (isset($options['filters'])) {
            if (!empty($options['filters'][7])) {
              $range = explode('|', $options['filters'][7]);
              if (count($range) === 2) {
                //$from = \DateTime::createFromFormat('d/m/Y', $options['filters']['date_from']);
                $builder
                  ->andWhere('scheduled_action.createdAt >= :from')
                  ->setParameter('from', $range[0]);
                $builder
                  ->andWhere('scheduled_action.createdAt <= :to')
                  ->setParameter('to', $range[1]);
              }
            }
          }

          $qb->getQuery()->getSQL();

        },
      ])
      ->addOrderBy('updatedAt', DataTable::SORT_DESCENDING);
  }
}
