<?php

declare(strict_types=1);

namespace App\Twig;

use App\Twig\Runtime\RouteExtensionRuntime;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class RouteExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_application_detail_url', [RouteExtensionRuntime::class, 'getApplicationDetailUrl']),
        ];
    }
}
