<?php


namespace App\Dto;

use App\Dto\Admin as AdminDto;
use App\Dto\Operator as OperatorDto;
use App\Dto\User as UserDto;
use App\Entity\AdminUser;
use App\Entity\CPSUser;
use App\Entity\OperatoreUser;
use App\Entity\User;
use DateTime;
use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;
use JMS\Serializer\Annotation\Groups;

abstract class AbstractUser
{

  const USER_TYPE_CPS = 'user';
  const USER_TYPE_OPERATORE = 'operator';
  const USER_TYPE_ADMIN = 'admin';

  const USER_TYPES = [self::USER_TYPE_CPS, self::USER_TYPE_OPERATORE, self::USER_TYPE_ADMIN];

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="User's uuid", type="string", format="uuid")
   * @Groups({"read", "kafka"})
   */
  protected $id;

  /**
   * @var string
   *
   * @Serializer\Type("string")
   * @OA\Property(description="Role of the user", type="string", default="user", enum={"user", "operator", "admin"})
   * @Groups({"read", "write", "kafka"})
   */
  protected string $role;


  /**
   * @var string
   *
   * @Serializer\Type("string")
   * @OA\Property(description="User's name")
   * @Groups({"read", "write"})
   */
  protected $nome;

  /**
   * @var string
   *
   * @Serializer\Type("string")
   * @OA\Property(description="User's surname")
   * @Groups({"read", "write"})
   */
  protected $cognome;

  /**
   * @var string
   *
   * @Serializer\Type("string")
   * @OA\Property(description="Operator's fullname")
   * @Groups({"read", "kafka"})
   *
   * https://w3id.org/italia/onto/CPV/fullName
   */
  protected string $fullName;


  /**
   * @var string
   *
   * @Serializer\Type("string")
   * @OA\Property(description="User's email", type="string", format="email")
   * @Groups({"read", "write", "kafka"})
   */
  protected $email;

  /**
   * @Serializer\Type("DateTime")
   * @OA\Property(description="User's creation datetime", type="string", format="date-time")
   * @Groups({"read", "kafka"})
   */
  protected $createdAt;

  /**
   * @Serializer\Type("DateTime")
   * @OA\Property(description="Datetime of lasted user's update", type="string", format="date-time")
   * @Groups({"read", "kafka"})
   */
  protected $updatedAt;

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param mixed $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getRole(): string
  {
    return $this->role;
  }

  /**
   * @param string $role
   */
  public function setRole(string $role)
  {
    $this->role = $role;
  }

  /**
   * @return string
   */
  public function getNome(): ?string
  {
    return $this->nome;
  }

  /**
   * @param string $nome
   */
  public function setNome(string $nome)
  {
    $this->nome = $nome;
  }

  /**
   * @return string
   */
  public function getCognome(): ?string
  {
    return $this->cognome;
  }

  /**
   * @param string $cognome
   */
  public function setCognome(string $cognome)
  {
    $this->cognome = $cognome;
  }

  /**
   * @return string
   */
  public function getFullName(): string
  {
    return $this->fullName;
  }

  /**
   * @param string $fullName
   */
  public function setFullName(string $fullName)
  {
    $this->fullName = $fullName;
  }

  /**
   * @return string
   */
  public function getEmail(): ?string
  {
    return $this->email;
  }

  /**
   * @param string|null $email
   */
  public function setEmail(?string $email): void
  {
    $this->email = $email;
  }

  /**
   * @return DateTime|null
   */
  public function getCreatedAt(): ?DateTime
  {
    return $this->createdAt;
  }

  /**
   * @param DateTime $createdAt
   */
  public function setCreatedAt(DateTime $createdAt)
  {
    $this->createdAt = $createdAt;
  }

  /**
   * @return DateTime|null
   */
  public function getUpdatedAt(): ?DateTime
  {
    return $this->updatedAt;
  }

  /**
   * @param DateTime $updatedAt
   */
  public function setUpdatedAt(DateTime $updatedAt)
  {
    $this->updatedAt = $updatedAt;
  }

  /**
   * @Serializer\VirtualProperty(name="given_name")
   * @Serializer\Type("string")
   * @Serializer\SerializedName("given_name")
   * @Groups({"kafka"})
   *
   * @return string|null
   *
   * https://schema.gov.it/lodview/onto/CPV/givenName
   */
  public function getGivenName(): ?string
  {
    return $this->getNome();
  }

  /**
   * @Serializer\VirtualProperty(name="family_name")
   * @Serializer\Type("string")
   * @Serializer\SerializedName("family_name")
   * @Groups({"kafka"})
   *
   * @return string|null
   *
   * https://w3id.org/italia/onto/CPV/familyName
   */
  public function getFamilyName(): ?string
  {
    return $this->getCognome();
  }

  public static function modelFromEntity(User $user)
  {
    if ($user instanceof CPSUser) {
      return UserDto::fromEntity($user);
    } elseif ($user instanceof OperatoreUser) {
      return OperatorDto::fromEntity($user);
    } elseif ($user instanceof AdminUser) {
      return AdminDto::fromEntity($user);
    }
  }
}
