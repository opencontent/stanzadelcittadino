<?php


namespace App\Dto;


use App\BackOffice\CalendarsBackOffice;
use App\Entity\Ente as TenantEntity;
use App\Model\DateTimeInterval;
use App\Model\Gateway;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Validator\Constraints as Assert;

class Tenant
{

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Message's uuid", type="string")
   * @Groups({"read"})
   */
  private $id;


  /**
   * @var string
   * @Serializer\Type("string")
   * @OA\Property(description="Tenant name")
   * @Groups({"read", "write"})
   */
  private $name;

  /**
   * @var string
   * @Serializer\Type("string")
   * @OA\Property(description="Tenant slug")
   * @Groups({"read"})
   */
  private $slug;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Tenant IPA code")
   * @Groups({"read", "write"})
   */
  private ?string $ipaCode;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Tenant cadastral code")
   * @Groups({"read", "write"})
   */
  private ?string $cadastralCode;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Tenant AOO code")
   * @Groups({"read", "write"})
   */
  private ?string $aooCode;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Tenant mechanographic code")
   * @Groups({"read", "write"})
   */
  private ?string $mechanographicCode;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Tenant administrative code")
   * @Groups({"read", "write"})
   */
  private ?string $administrativeCode;

  /**
   * @Assert\NotBlank(message="Site url is mandatory")
   * @Assert\NotNull(message="Site url is mandatory")
   * @Serializer\Type("string")
   * @OA\Property(description="Link on the title of each page")
   * @Groups({"read", "write"})
   */
  private $siteUrl;

  /**
   * @Serializer\Type("array")
   * @OA\Property(description="Tenant graphic appearance")
   * @Groups({"read", "write"})
   */
  private $meta;

  /**
   * @Serializer\Type("bool")
   * @OA\Property(description="Enable App IO integration")
   * @Groups({"read", "write"})
   */
  private $ioEnabled;

  /**
   * @Serializer\Type("array")
   * @OA\Property(property="gateways", description="List of payment gateways and related parameters", type="array", @OA\Items(type="object", ref=@Model(type=Gateway::class)))
   * @Groups({"read", "write"})
   */
  private $gateways;

  /**
   * @Serializer\Type("array")
   * @OA\Property(property="backoffice_enabled_integrations", description="List of backoffices, available options are 'operatori_subscription-service_index', 'operatori_calendars_index'", type="array", @OA\Items(type="string"))
   * @Groups({"read", "write"})
   */
  private $backofficeEnabledIntegrations;

  /**
   * @Serializer\Type("bool")
   * @OA\Property(description="Enable linkable application meetings")
   * @Groups({"read", "write"})
   */
  private $linkableApplicationMeetings;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Configure entry point for user satisfaction")
   * @Groups({"read", "write"})
   */
  private $satisfyEntrypointId;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Configure Default pdnd client id")
   * @Groups({"read", "write"})
   */
  private $defaultPdndClientId = null;


  /**
   * @Serializer\Type("array")
   * @OA\Property(property="pdnd_config_ids", description="List of pdnd config ids", type="array", @OA\Items(type="string"))
   * @Groups({"read", "write"})
   */
  private array $pdndConfigIds = [];

  /**
   * @var DateTimeInterval[]
   *
   * @OA\Property(description="Calendar's closing periods", type="array", @OA\Items(ref=@Model(type=DateTimeInterval::class)))
   * @Groups({"read", "write"})
   */
  private array $closingPeriods = [];

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param mixed $id
   */
  public function setId($id): void
  {
    $this->id = $id;
  }

  /**
   * @return string|null
   */
  public function getMechanographicCode()
  {
    return $this->mechanographicCode;
  }

  /**
   * @param mixed $mechanographicCode
   */
  public function setMechanographicCode($mechanographicCode): void
  {
    $this->mechanographicCode = $mechanographicCode;
  }

  /**
   * @return string|null
   */
  public function getAdministrativeCode()
  {
    return $this->administrativeCode;
  }

  /**
   * @param mixed $administrativeCode
   */
  public function setAdministrativeCode($administrativeCode): void
  {
    $this->administrativeCode = $administrativeCode;
  }


  /**
   * @return ?string
   */
  public function getIpaCode(): ?string
  {
    return $this->ipaCode;
  }

  /**
   * @param ?string $ipaCode
   *
   * @return Tenant
   */
  public function setIpaCode($ipaCode): Tenant
  {
    $this->ipaCode = $ipaCode;

    return $this;
  }

  /**
   * @return ?string
   */
  public function getCadastralCode(): ?string
  {
    return $this->cadastralCode;
  }

  /**
   * @param ?string $cadastralCode
   *
   * @return Tenant
   */
  public function setCadastralCode(?string $cadastralCode): Tenant
  {
    $this->cadastralCode = $cadastralCode;

    return $this;
  }

  /**
   * @return ?string
   */
  public function getAooCode(): ?string
  {
    return $this->aooCode;
  }

  /**
   * @param ?string $aooCode
   *
   * @return Tenant
   */
  public function setAooCode(?string $aooCode): Tenant
  {
    $this->aooCode = $aooCode;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getSiteUrl()
  {
    return $this->siteUrl;
  }

  /**
   * @param mixed $siteUrl
   */
  public function setSiteUrl($siteUrl): void
  {
    $this->siteUrl = $siteUrl;
  }

  /**
   * @return mixed
   */
  public function getMeta()
  {
    return $this->meta;
  }

  /**
   * @param mixed $meta
   */
  public function setMeta($meta): void
  {
    $this->meta = $meta;
  }

  /**
   * @return bool
   */
  public function isIoEnabled(): ?bool
  {
    return $this->ioEnabled;
  }

  /**
   * @param bool $ioEnabled
   */
  public function setIoEnabled(?bool $ioEnabled)
  {
    $this->ioEnabled = $ioEnabled;
  }

  /**
   * @return bool
   */
  public function isLinkableApplicationMeetings(): ?bool
  {
    return $this->linkableApplicationMeetings;
  }

  /**
   * @param bool $linkableApplicationMeetings
   */
  public function setLinkableApplicationMeetings(?bool $linkableApplicationMeetings)
  {
    $this->linkableApplicationMeetings = $linkableApplicationMeetings;
  }

  /**
   * @return array
   */
  public function getGateways()
  {
    if (is_array($this->gateways)) {
      return $this->gateways;
    } else {
      return json_decode($this->gateways);
    }
  }

  /**
   * @param Gateway[] $gateways
   * @return $this
   */
  public function setGateways($gateways)
  {
    $this->gateways = $gateways;
    return $this;
  }

  /**
   * @return array
   */
  public function getBackofficeEnabledIntegrations()
  {
    return $this->backofficeEnabledIntegrations;
  }

  /**
   * @param array $backofficeEnabledIntegrations
   * @return $this
   */
  public function setBackofficeEnabledIntegrations($backofficeEnabledIntegrations)
  {
    $this->backofficeEnabledIntegrations = $backofficeEnabledIntegrations;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getSatisfyEntrypointId()
  {
    return $this->satisfyEntrypointId;
  }

  /**
   * @param mixed $satisfyEntrypointId
   */
  public function setSatisfyEntrypointId($satisfyEntrypointId): void
  {
    $this->satisfyEntrypointId = $satisfyEntrypointId;
  }

  /**
   * @return string
   */
  public function getName(): string
  {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName(string $name): void
  {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getSlug(): string
  {
      return $this->slug;
  }

  /**
   * @param string $slug
   */
  public function setSlug(string $slug): void
  {
      $this->slug = $slug;
  }

  /**
   * @return null
   */
  public function getDefaultPdndClientId()
  {
    return $this->defaultPdndClientId;
  }

  /**
   * @param null $defaultPdndClientId
   */
  public function setDefaultPdndClientId($defaultPdndClientId): void
  {
    $this->defaultPdndClientId = $defaultPdndClientId;
  }

  /**
   * @return array
   */
  public function getPdndConfigIds(): array
  {
    return $this->pdndConfigIds ?? [];
  }

  /**
   * @param array $pdndConfigIds
   */
  public function setPdndConfigIds(array $pdndConfigIds): void
  {
    $this->pdndConfigIds = $pdndConfigIds;
  }

  public function getClosingPeriods(): array
  {
    return $this->closingPeriods;
  }

  public function setClosingPeriods(array $closingPeriods): void
  {
    $this->closingPeriods = $closingPeriods;
  }


  /**
   * @param TenantEntity $tenant
   * @return Tenant
   */
  public static function fromEntity(TenantEntity $tenant): Tenant
  {
    $dto = new self();
    $dto->id = $tenant->getId();
    $dto->name = $tenant->getName();
    $dto->slug = $tenant->getSlug();
    $dto->ipaCode = $tenant->getIpaCode();
    $dto->aooCode = $tenant->getAooCode();
    $dto->cadastralCode = $tenant->getCadastralCode();
    $dto->mechanographicCode = $tenant->getCodiceMeccanografico();
    $dto->administrativeCode = $tenant->getCodiceAmministrativo();
    $dto->siteUrl = $tenant->getSiteUrl();
    $dto->meta = $tenant->getMeta();
    $dto->ioEnabled = $tenant->isIOEnabled();
    $dto->gateways = [];
    $dto->backofficeEnabledIntegrations = $tenant->getBackofficeEnabledIntegrations();
    $dto->linkableApplicationMeetings = $tenant->isLinkableApplicationMeetings();
    $dto->satisfyEntrypointId = $tenant->getSatisfyEntrypointId();
    $dto->defaultPdndClientId = $tenant->getDefaultPdndClientId();
    $dto->pdndConfigIds = $tenant->getPdndConfigIds();
    $dto->closingPeriods = $tenant->getClosingPeriods();

    foreach ($tenant->getGateways() as $gateway) {
      $g = new Gateway();
      $g->setIdentifier($gateway['identifier']);
      // Todo: Display parameters only to admin users
      $g->setParameters(null);
      $dto->gateways[] = $g;
    }

    return $dto;
  }

  /**
   * @param TenantEntity|null $entity
   * @return TenantEntity
   */
  public function toEntity(TenantEntity $entity = null)
  {
    if (!$entity) {
      $entity = new TenantEntity();
    }

    $entity->setCodiceMeccanografico($this->mechanographicCode);
    $entity->setName($this->name);
    $entity->setIpaCode($this->ipaCode);
    $entity->setCadastralCode($this->cadastralCode);
    $entity->setAooCode($this->aooCode);
    $entity->setCodiceAmministrativo($this->administrativeCode);
    $entity->setSiteUrl($this->siteUrl);
    $entity->setMeta($this->meta);
    $entity->setIOEnabled($this->ioEnabled);
    $entity->setGateways($this->gateways);
    $entity->setBackofficeEnabledIntegrations($this->backofficeEnabledIntegrations);
    $entity->setSatisfyEntrypointId($this->satisfyEntrypointId);
    $entity->setDefaultPdndClientId($this->defaultPdndClientId);
    $entity->setPdndConfigIds($this->pdndConfigIds);
    $entity->setClosingPeriods($this->closingPeriods);

    if (!in_array(CalendarsBackOffice::PATH, $entity->getBackofficeEnabledIntegrations())) {
      // disable if integration is not set
      $entity->setLinkableApplicationMeetings(false);
    }

    return $entity;
  }

  /**
   * @param $data
   * @return mixed
   */
  public static function normalizeData($data)
  {

    // Todo: find better way
    if (isset($data['gateways']) && count($data['gateways']) > 0) {
      $temp = [];
      foreach ($data['gateways'] as $f) {
        $f['parameters'] = \json_encode($f['parameters']);
        $temp[$f['identifier']]= $f;
      }
      $data['gateways'] = $temp;
    }

    // Todo: find better way
    if (isset($data['meta'])) {
      $data['meta'] = \json_encode($data['meta']);
    }
    return $data;
  }

}
