<?php

namespace App\Dto;

use App\Model\Service\PaymentConfig;
use Ramsey\Uuid\Uuid;

class ApplicationOutcome
{
  /**
   * @var string Uuid
   */
  private $applicationId;

  /**
   * @var int
   */
  private $outcome;

  /**
   * @var string
   */
  private $message;

  private $mainAttachment;

  /**
   * @var string
   */
  private $attachments;

  /**
   * @var float
   */
  private $paymentAmount = 0;

  private array $stamps = [];

  private $paymentConfigs;

  /**
   * @return string
   */
  public function getApplicationId()
  {
    return $this->applicationId;
  }

  /**
   * @param Uuid $applicationId
   * @return ApplicationOutcome
   */
  public function setApplicationId($applicationId): self
  {
    $this->applicationId = $applicationId;

    return $this;
  }

  /**
   * @return int
   */
  public function getOutcome()
  {
    return (int)$this->outcome;
  }

  /**
   * @param int $outcome
   * @return ApplicationOutcome
   */
  public function setOutcome(int $outcome): self
  {
    $this->outcome = $outcome;

    return $this;
  }

  /**
   * @return string
   */
  public function getMessage()
  {
    return (string)$this->message;
  }

  /**
   * @param string|null $message
   * @return ApplicationOutcome
   */
  public function setMessage(?string $message): self
  {
    $this->message = $message;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getMainAttachment()
  {
    try {
      return json_decode($this->mainAttachment, true, 512, JSON_THROW_ON_ERROR) ?? [];
    } catch (\JsonException $e) {

    }
    return [];
  }

  /**
   * @param mixed $mainAttachment
   */
  public function setMainAttachment($mainAttachment): self
  {
    $this->mainAttachment = $mainAttachment;
    return $this;
  }

  /**
   * @return array
   */
  public function getAttachments(): array
  {
    try {
      return json_decode($this->attachments, true, 512, JSON_THROW_ON_ERROR) ?? [];
    } catch (\JsonException $e) {

    }
    return [];
  }

  /**
   * @param array $attachments
   * @return ApplicationOutcome
   */
  public function setAttachments($attachments): self
  {
    $this->attachments = $attachments;
    return $this;
  }

  public function getPaymentAmount(): ?float
  {
    return $this->paymentAmount;
  }

  public function setPaymentAmount(?float $paymentAmount): void
  {
    $this->paymentAmount = $paymentAmount;
  }

  public function getStamps(): array
  {
    return $this->stamps;
  }

  /**
   * @param mixed $stamps
   */
  public function setStamps($stamps): void
  {
    $this->stamps = $stamps;
  }

  public function getPaymentConfigs(): array
  {
    $paymentConfigs = [];
    try {
      $configs =  json_decode($this->paymentConfigs, true, 512, JSON_THROW_ON_ERROR) ?? [];
      if ($configs) {
        foreach ($configs as $data) {
          if (is_array($data)) {
            $config = PaymentConfig::fromArray($data);
            $paymentConfigs[$config->getId()] = $config;
          } else {
            $paymentConfigs[$data->getId()] = $data;
          }
        }
      }
      return $paymentConfigs;
    } catch (\JsonException $e) {

    }
    return [];
  }

  public function setPaymentConfigs($paymentConfigs): self
  {
    $this->paymentConfigs = $paymentConfigs;

    return $this;
  }

}
