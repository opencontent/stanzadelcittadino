<?php

namespace App\Handlers\Job;


class JobHandlerRegistry
{
  private array $handlers = [];

  public function registerHandler(JobHandlerInterface $jobHandler, $alias)
  {
    $this->handlers[$alias] = $jobHandler;
  }

  /**
   * @param string|null $alias
   * @return JobHandlerInterface
   */
  public function getByName(string $alias = null): ?JobHandlerInterface
  {
    if ($alias && isset($this->handlers[$alias])) {
      return $this->handlers[$alias];
    }

    return null;
  }
}
