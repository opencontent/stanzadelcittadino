<?php

namespace App\Handlers\Job;

use App\Entity\Job;

interface JobHandlerInterface
{
  public function processJob(Job $job);

  public function validateJob(Job $job);
}
