<?php

declare(strict_types=1);

namespace App\Handlers\Servizio;

use Symfony\Component\HttpFoundation\Response;
use App\Entity\CPSUser;
use App\Entity\DematerializedFormPratica;
use App\Entity\Ente;
use App\Entity\Pratica;
use App\Entity\Servizio;
use App\Form\Base\PraticaFlow;
use App\Logging\LogConstants;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DuePaymentHandler extends DefaultHandler
{

  /**
   * @var string
   */
  const IDENTIFIER = 'due_payment';

  // Todo: fare override del can access e verificare che l'utente sia loggato
  public function execute(Servizio $servizio)
  {
    $this->getUser();

    return new RedirectResponse(
      $this->router->generate('payments_list_cpsuser', [
        'service_id' => $servizio->getId(),
        'status' => 'to-pay'
      ]),
      Response::HTTP_FOUND
    );
  }

}
