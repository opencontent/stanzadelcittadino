<?php

declare(strict_types=1);

namespace App\ApplicationLiteImport\Infrastructure;

use App\ApplicationLiteImport\Application\ImportCommand\ImportCommand;
use App\ApplicationLiteImport\Application\ImportCommand\ImportData;
use App\Entity\Ente;
use App\Entity\User;
use JMS\Serializer\SerializerInterface;
use JsonSchema\Constraints\Factory;
use JsonSchema\SchemaStorage;
use JsonSchema\Validator;

class HttpRequestToImportCommandMapper
{
  private SerializerInterface $serializer;

  public function __construct(
    SerializerInterface $serializer
  ) {
    $this->serializer = $serializer;
  }

  /** @throws InvalidJsonException */
  public function map(string $content, Ente $ente, User $requester): ImportCommand
  {
    $this->validate($content);
    /** @var ImportData $importData */
    $importData = $this->serializer->deserialize($content, ImportData::class, 'json');

    $importCommand = new ImportCommand();
    $importCommand->ente = $ente;
    $importCommand->importData = $importData;
    $importCommand->requester = $requester;

    return $importCommand;
  }

  /** @throws InvalidJsonException */
  public function validate(string $json): bool
  {
    $jsonSchema = $this->getSchema();
    $jsonSchemaObject = json_decode($jsonSchema);

    $schemaStorage = new SchemaStorage();
    $schemaStorage->addSchema('file://mySchema', $jsonSchemaObject);

    $jsonValidator = new Validator(new Factory($schemaStorage));

    $jsonToValidateObject = json_decode($json);
    $jsonValidator->validate($jsonToValidateObject, $jsonSchemaObject);

    $jsonValidator->getErrors();

    if (!$jsonValidator->isValid()) {

      throw InvalidJsonException::becauseThereAreUnmetConstraints($jsonValidator);
    }

    return true;
  }

  private function getSchema(): string
  {
    return <<<'JSON'
      {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "type": "object",
        "properties": {
          "application": {
            "type": "object",
            "properties": {
              "external_id": {
                "type": "string",
                "description": "Identificativo pratica sul sistema esterno",
                "minLength": 1
              },
              "subject": {
                "type": "string",
                "description": "Oggetto della pratica",
                "minLength": 3,
                "maxLength": 255
              },
              "applicant": {
                "$ref": "#/definitions/person",
                "description": "Richiedente della pratica"
              },
              "beneficiary": {
                "anyOf": [
                  { "type": "null" },
                  { "$ref": "#/definitions/person" }
                ],
                "description": "Beneficiario della pratica"
              },
              "status": {
                "type": "integer",
                "description": "Stato della pratica. Valori possibili: 2000 (Inviata), 4000 (Presa in carico), 4200 (In attesa di integrazioni), 7000 (Approvata), 9000 (Rifiutata), 20000 (Ritirata), 50000 (Annullata)",
                "enum": [
                  2000,
                  4000,
                  4200,
                  7000,
                  9000,
                  20000,
                  50000
                ]
              },
              "service": {
                "type": "object",
                "description": "Servizio correlato alla pratica",
                "properties": {
                  "id": {
                    "type": "string",
                    "format": "uuid",
                    "pattern": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$",
                    "description": "Identificativo interno del servizio"
                  }
                },
                "required": [
                  "id"
                ]
              },
              "actions": {
                "type": "array",
                "description": "Azioni che è possibile effettuare sulla Pratica con relativo URL",
                "items": {
                  "type": "object",
                  "properties": {
                    "action": {
                      "type": "string",
                      "description": "Azione associata al link (es. view, request integration)",
                      "enum": [
                        "view",
                        "request_integration"
                      ]
                    },
                    "url": {
                      "type": "string",
                      "format": "uri",
                      "description": "URL per accedere alla risorsa"
                    }
                  },
                  "required": [
                    "action",
                    "url"
                  ]
                }
              },
              "process_history": {
                "type": "array",
                "description": "An array representing the workflow of the process. Each entry includes the status and the date and time when the status was changed.",
                "items": {
                  "properties": {
                    "status": {
                      "type": "integer",
                      "description": "Stato della pratica. Valori possibili: 2000 (Inviata), 4000 (Presa in carico), 4200 (In attesa di integrazioni), 7000 (Approvata), 9000 (Rifiutata), 20000 (Ritirata), 50000 (Annullata)",
                      "enum": [
                        2000,
                        4000,
                        4200,
                        7000,
                        9000,
                        20000,
                        50000
                      ]
                    },
                    "status_changed_at": {
                      "type": "string",
                      "format": "date-time",
                      "description": "Date and time when the status change occurred, formatted using ISO 8601 standard",
                      "examples": [
                        "2025-01-20T14:30:00Z",
                        "2025-01-20T14:30:00+02:00"
                      ]
                    }
                  }
                }
              }
            },
            "required": [
              "external_id",
              "subject",
              "applicant",
              "status",
              "service",
              "actions",
              "process_history"
            ]
          }
        },
        "required": [
          "application"
        ],
        "definitions": {
          "person": {
            "type": "object",
            "properties": {
              "given_name": {
                "type": "string",
                "description": "Nome della persona"
              },
              "family_name": {
                "type": "string",
                "description": "Cognome della persona"
              },
              "identifiers": {
                "type": "object",
                "description": "Sistemi di identificazione digitale della persona (attualmente supportiamo solo il codice fiscale)",
                "properties": {
                  "tax_code": {
                    "type": "string",
                    "description": "Codice fiscale",
                    "minLength": 16,
                    "maxLength": 16
                  },
                  "spid_code": {
                    "type": "string",
                    "description": "Codice SPID"
                  },
                  "id_eidas": {
                    "type": "string",
                    "description": "Codice eIDAS"
                  },
                  "anpr_id": {
                    "type": "string",
                    "description": "Codice unico dell'anagrafe"
                  },
                  "inps_id": {
                    "type": "string",
                    "description": "Codice INPS"
                  },
                  "io_wallet_id": {
                    "type": "string",
                    "description": "Codice di PagoPA"
                  },
                  "email": {
                    "type": "string",
                    "description": "Email se usata come identificativo utente"
                  }
                },
                "required": [ "tax_code" ],
                "anyOf": [
                  { "required": [ "tax_code" ] },
                  { "required": [ "spid_code" ] },
                  { "required": [ "id_eidas" ] },
                  { "required": [ "anpr_id" ] },
                  { "required": [ "inps_id" ] },
                  { "required": [ "io_wallet_id" ] },
                  { "required": [ "email" ] }
                ]
              },
              "contacts": {
                "type": "object",
                "properties": {
                  "mobile": {
                    "type": "string",
                    "description": "Numero di cellulare"
                  },
                  "phone": {
                    "type": "string",
                    "description": "Numero di telefono"
                  },
                  "email": {
                    "type": "string",
                    "description": "Email"
                  },
                  "pec": {
                    "type": "string",
                    "description": "PEC"
                  }
                }
              }
            },
            "required": [
              "given_name",
              "family_name",
              "identifiers"
            ]
          }
        }
      }
      JSON;
  }
}
