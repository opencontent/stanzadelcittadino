<?php

declare(strict_types=1);

namespace App\ApplicationLiteImport\Application\ImportCommand;

class MainDocument
{
  public string $name;
}
