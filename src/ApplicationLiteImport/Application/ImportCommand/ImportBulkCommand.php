<?php

declare(strict_types=1);

namespace App\ApplicationLiteImport\Application\ImportCommand;

use App\ApplicationLiteImport\Domain\RequesterId;
use App\Entity\Ente;
use JMS\Serializer\Annotation\Type;

class ImportBulkCommand
{
  public Ente $ente;
  public RequesterId $requester;
  /**
   * @var ImportData[]
   * @Type("array<App\ApplicationLiteImport\Application\ImportCommand\ImportData>")
   */
  public array $importData;
}
