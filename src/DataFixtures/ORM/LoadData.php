<?php

namespace App\DataFixtures\ORM;

use App\Entity\Categoria;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Persistence\ObjectManager;
use App\Utils\Csv;


class LoadData extends AbstractFixture
{

  private $counters = [
    'servizi' => [
      'new' => 0,
      'updated' => 0,
    ],
    'enti' => [
      'new' => 0,
      'updated' => 0,
    ],
    'categorie' => [
      'new' => 0,
      'updated' => 0,
    ],
    'payment_gateways' => [
      'new' => 0,
      'updated' => 0,
    ],
    'privacy' => [
      'new' => 0,
      'updated' => 0,
    ],
  ];

  private $data = [];

  public function load(ObjectManager $manager)
  {
    $this->loadCategories($manager);
  }

  private function getData($key)
  {

    if (empty($this->data)) {
      // Fixme
      $dataDir = dirname(__FILE__) . '/../../../data';

      $categories = Csv::csvToArray($dataDir . '/categories.csv');
      $paymentGateways = Csv::csvToArray($dataDir . '/payment-gateways.csv');
      $privacy = Csv::csvToArray($dataDir . '/privacy.csv');

      $this->data = [
        'categories'       => $categories,
        'payment_gateways' => $paymentGateways,
        'privacy' => $privacy,
      ];
    }

    if (isset($this->data[$key])) {
      return $this->data[$key];
    }

    return [];
  }


  public function loadCategories(ObjectManager $manager)
  {
    $data = $this->getData('categories');
    $categoryRepo = $manager->getRepository('App\Entity\Categoria');
    foreach ($data as $item) {
      $category = $categoryRepo->findOneBySlug($item['slug']);
      if (!$category) {
        $this->counters['categorie']['new']++;
        $category = new Categoria();
        $category
          ->setName($item['name'])
          ->setDescription($item['description']);

        $manager->persist($category);

      } else {
        // Update name
        if ($item['name'] != $category->getName()) {
          $category->setName($item['name']);
        }
        // Update Description
        if ($item['description'] != $category->getDescription()) {
          $category->setDescription($item['description']);
        }
        $manager->persist($category);
        $this->counters['categorie']['updated']++;
      }

      $manager->flush();
    }
  }

  public function getCounters()
  {
    return $this->counters;
  }
}
