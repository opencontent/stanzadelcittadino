<?php

namespace App\Form;

use App\Entity\Meeting;
use App\Form\Security\NewPasswordType;
use App\Utils\HaveIBeenPwned;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Entity\User;


class ChangePasswordType extends AbstractType
{
  /**
   * @var TranslatorInterface
   */
  private $translator;

  public function __construct(TranslatorInterface $translator)
  {
    $this->translator = $translator;
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {

    $builder
      ->add('currentPassword', PasswordType::class, [
        'label' => 'security.current_password',
        'mapped' => false,
        'constraints' => [new UserPassword(['message' => $this->translator->trans('security.invalid_password')])]
      ])
      ->add(
        'plainPassword',
        NewPasswordType::class,
        [
          'mapped' => false,
          'label' => false
        ]
      )
      ->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
  }

  public function onPreSubmit(FormEvent $event): void
  {
    $data = $event->getData();
    $newPassword = $data['plainPassword']['plainPassword']['first'];

    if ($data['currentPassword'] === $newPassword) {
      $event->getForm()->addError(new FormError($this->translator->trans('security.equal_to_current_password')));
      return;
    }

    try {
      if (!HaveIBeenPwned::idValid($newPassword)) {
        $event->getForm()->addError(new FormError($this->translator->trans('security.pwned_password')));
      }
    } catch (\Exception $e) {

    }
  }


  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults(array(
      'data_class' => User::class
    ));
  }


}
