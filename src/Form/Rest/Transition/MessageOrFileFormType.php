<?php

namespace App\Form\Rest\Transition;


use App\Entity\Allegato;
use App\Form\Rest\FileType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageOrFileFormType extends AbstractType
{

  private array $allowedExtensions;
  private EntityManagerInterface $entityManager;

  public function __construct(EntityManagerInterface $entityManager, $allowedExtensions)
  {
    $this->entityManager = $entityManager;
    $this->allowedExtensions = array_merge(...$allowedExtensions);
  }

  /**
   * @param FormBuilderInterface $builder
   * @param array $options
   */
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $builder
      ->add('message', TextareaType::class, [
        'required' => false,
        'purify_html' => true,
      ])
      ->add('attachment', FileType::class, [
        'required' => false
      ]);

    $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
  }

  public function onPreSubmit(FormEvent $event)
  {
    $data = $event->getData();

    if (isset($data['attachment'])) {
      $attachment = $data['attachment'];
      if (!in_array($attachment['mime_type'], $this->allowedExtensions, true)) {
        return $event->getForm()->addError(
          new FormError('Invalid mime type, valid mime types are: ' . implode(', ', $this->allowedExtensions)),
        );
      }

      $extension = explode('.', $attachment['name']);
      if (count($extension) < 2) {
        return $event->getForm()->addError(
          new FormError("It is mandatory to specify the file extension le name field")
        );

      }

      if (!array_key_exists(end($extension), $this->allowedExtensions)) {
        return $event->getForm()->addError(new FormError('Invalid extension, valid extensions are:' . implode(', ', array_keys($this->allowedExtensions))));
      }

      if (empty($attachment['file'])) {
        return $event->getForm()->addError(new FormError('The file field is mandatory'));
      }
      if (isset($attachment['external_id']) && $attachment['external_id']) {
        // Verifico external ids duplicati
        $attachmentsByExternalId = $this->entityManager->getRepository(Allegato::class)->findBy(['externalId' => $attachment['external_id']]);
        if (!empty($attachmentsByExternalId)) {
          return $event->getForm()->addError(new FormError("An attachment with external id '{$attachment['external_id']}' already exists"));
        }
      }

    }
  }

  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults(array(
      'allow_extra_fields' => true,
      'csrf_protection' => false,
    ));
  }

}
