<?php

namespace App\Form\Rest;


use App\Dto\Message;
use App\Entity\Pratica;
use App\Entity\Servizio;
use App\Form\Base\StampsCollectionType;
use App\Form\Base\StampType;
use App\Form\Rest\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Model\Application;

class ApplicationFormType extends AbstractType
{

  /**
   * @param FormBuilderInterface $builder
   * @param array $options
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {

    $prioritiesChoices = [];
    foreach (Pratica::PRIORITIES as $priority) {
      $prioritiesChoices[$priority] = $priority;
    }

    $builder
      ->add('user')
      ->add('service')
      ->add('data')
      ->add('protocol_folder_number')
      ->add('protocol_folder_code')
      ->add('protocol_number')
      ->add('protocol_document_id')
      ->add('protocolled_at')
      ->add('outcome')
      ->add('outcome_motivation')
      ->add('outcome_protocol_number')
      ->add('outcome_protocol_document_id')
      ->add('outcome_protocolled_at')
      ->add('payment_type')
      ->add('payment_data')
      ->add('status')
      ->add('external_id')
      ->add('user_compilation_notes')
      ->add('priority', ChoiceType::class, [
        'choices' => $prioritiesChoices,
        'required' => false,
        'empty_data' => 'low'
      ])
      ->add('stamps', CollectionType::class, [
        'required' => false,
        'entry_type' => StampType::class,
        'allow_add' => true,
        'allow_delete' => true,
        'label' => false
      ])
      ->add('created_at', DateTimeType::class, [
        'widget' => 'single_text',
        'required' => false,
        'empty_data' => ''
      ])
    ;
  }

  /**
   * @param OptionsResolver $resolver
   */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => Application::class,
      'allow_extra_fields' => true,
      'csrf_protection' => false
    ));
  }

}
