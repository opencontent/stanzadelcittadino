<?php

namespace App\Form\Admin\Servizio;

use App\Entity\FormIO;
use App\Entity\SciaPraticaEdilizia;
use App\Entity\Servizio;
use App\Form\Extension\TestiAccompagnatoriProcedura;
use App\FormIO\SchemaFactoryInterface;
use App\Services\FormServerApiAdapterService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Form\FormError;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\GuzzleException;


class PdndType extends AbstractType
{
  private EntityManagerInterface $em;

  private FormServerApiAdapterService $formServerService;
  private SchemaFactoryInterface $schemaFactory;

  /**
   * ChooseAllegatoType constructor.
   *
   * @param EntityManagerInterface $entityManager
   * @param FormServerApiAdapterService $formServerService
   * @param SchemaFactoryInterface $schemaFactory
   */
  public function __construct(EntityManagerInterface $entityManager, SchemaFactoryInterface $schemaFactory)
  {
    $this->em = $entityManager;
    $this->schemaFactory = $schemaFactory;
  }

  /**
   * @param FormBuilderInterface $builder
   * @param array $options
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {

    /** @var Servizio $servizio */
    $servizio = $builder->getData();
    $formId = $servizio->getFormIoId();
    $builder
      ->add('form_id', HiddenType::class,
        [
          'attr' => ['value' => $formId],
          'mapped' => false,
          'required' => false,
        ]
      )
      ->add('form_schema', HiddenType::class,
        [
          'attr' => ['value' => ''],
          'mapped' => false,
          'required' => false,
        ]
      );
    $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
  }

  public function onPreSubmit(FormEvent $event)
  {
    /** @var Servizio $servizio */
    $servizio = $event->getForm()->getData();

    if (isset($event->getData()['form_schema']) && !empty($event->getData()['form_schema'])) {
      $schema = \json_decode($event->getData()['form_schema'], true);
      $response = $this->formServerService->editForm($schema);

      if ($response['status'] != 'success') {
        $event->getForm()->addError(
          new FormError($response['message'])
        );
      }

      $schema = $this->schemaFactory->createFromFormJson($event->getData()['form_schema']);
      $servizio->setProfileBlocks($schema->getProfileBlocks());
    }

    $this->em->persist($servizio);
  }

  public function getBlockPrefix()
  {
    return 'pdnd';
  }

}
