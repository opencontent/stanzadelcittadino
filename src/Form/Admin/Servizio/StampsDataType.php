<?php


namespace App\Form\Admin\Servizio;


use App\Form\Base\StampType;
use App\Services\Manager\ServiceManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Servizio;
use Symfony\Contracts\Translation\TranslatorInterface;


class StampsDataType extends AbstractType
{
  private TranslatorInterface $translator;
  private ServiceManager $serviceManager;

  public function __construct(ServiceManager $serviceManager, TranslatorInterface $translator)
  {
    $this->translator = $translator;
    $this->serviceManager = $serviceManager;
  }

  public function buildForm(FormBuilderInterface $builder, array $options):void
  {
    $builder->add('stamps', CollectionType::class, [
      'label' => false,
      'entry_type' => StampType::class,
      'entry_options' => ['view' => 'backend'],
      'allow_add' => true,
      'allow_delete' => true
    ]);

    $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
  }

  public function onPreSubmit(FormEvent $event): void
  {
    /** @var Servizio $service */
    $service = $event->getForm()->getData();
    $data = $event->getData();

    try {
      $this->serviceManager->validateStamps($data);
    } catch (\Exception $e) {
      $event->getForm()->addError(
        new FormError($e->getMessage())
      );
    }
  }

  public function configureOptions(OptionsResolver $resolver):void
  {
    $resolver->setDefaults(array(
      'data_class' => Servizio::class
    ));
  }

  public function getBlockPrefix():string
  {
    return 'stamps_data';
  }
}
