<?php

namespace App\Form;

use App\Entity\Pratica;
use App\Form\Base\StampType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Model\Application;

class ApplicationType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {

    $prioritiesChoices = [];
    foreach (Pratica::PRIORITIES as $priority) {
      $prioritiesChoices[$priority] = $priority;
    }

    $builder
      ->add('protocol_folder_number', TextType::class, ['label' => false])
      ->add('protocol_folder_code', TextType::class, ['label' => false])
      ->add('protocol_number', TextType::class, ['label' => false])
      ->add('protocol_document_id', TextType::class, ['label' => false])
      ->add('protocolled_at', DateTimeType::class, [
        'widget' => 'single_text',
        'required' => false,
        'empty_data' => ''
      ])
      ->add('outcome_protocol_number', TextType::class, ['label' => false])
      ->add('outcome_protocol_document_id', TextType::class, ['label' => false])
      ->add('outcome_protocolled_at', DateTimeType::class, [
        'widget' => 'single_text',
        'required' => false,
        'empty_data' => ''
      ])
      ->add('external_id', TextType::class, ['label' => false])
      ->add('user_compilation_notes', TextType::class, ['label' => false])
      ->add('priority', ChoiceType::class, [
        'choices' => $prioritiesChoices,
        'required' => false
      ])
      ->add('stamps', CollectionType::class, [
        'required' => false,
        'entry_type' => StampType::class,
        'allow_add' => true,
        'allow_delete' => true,
        'label' => false
      ])
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => Application::class,
      'csrf_protection' => false,
      'allow_extra_fields' => true
    ));
  }

  public function getBlockPrefix()
  {
    return 'app_bundle_application_type';
  }
}
