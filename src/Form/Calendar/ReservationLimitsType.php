<?php


namespace App\Form\Calendar;


use App\Model\Calendar\ReservationLimits;
use App\Utils\DateTimeUtils;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReservationLimitsType extends AbstractType
{
  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $builder
      ->add('max_meetings_per_interval', IntegerType::class, [
        'label' => 'calendars.reservation_limits.max_meetings_per_inteval',
        'property_path' => 'maxMeetingsPerInterval',
      ])
      ->add('interval', ChoiceType::class, [
        'label' => 'calendars.reservation_limits.interval',
        'required' => false,
        'choices' => [
          'code_generation.strategies.temporal_reset_choices.daily' => DateTimeUtils::PERIOD_DAILY,
          'code_generation.strategies.temporal_reset_choices.weekly' => DateTimeUtils::PERIOD_WEEKLY,
          'code_generation.strategies.temporal_reset_choices.monthly' => DateTimeUtils::PERIOD_MONTHLY,
          'code_generation.strategies.temporal_reset_choices.yearly' => DateTimeUtils::PERIOD_YEARLY,
        ],
        'empty_data' => null
      ])
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults(array(
      'data_class' => ReservationLimits::class,
      'csrf_protection' => false
    ));
  }
}
