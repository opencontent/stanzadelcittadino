<?php

namespace App\Form\Base;

use App\Entity\FormIO;
use App\Entity\Pratica;
use App\Form\Extension\TestiAccompagnatoriProcedura;
use App\Services\Manager\PraticaManager;
use App\Services\PraticaStatusService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Contracts\Translation\TranslatorInterface;


class StampsCollectionType extends AbstractType
{

  private PraticaStatusService $statusService;
  private TranslatorInterface $translator;
  private PraticaManager $praticaManager;


  /**
   * @param PraticaStatusService $statusService
   * @param TranslatorInterface $translator
   * @param PraticaManager $praticaManager
   */
  public function __construct(PraticaStatusService $statusService, TranslatorInterface $translator, PraticaManager $praticaManager)
  {
    $this->statusService = $statusService;
    $this->translator = $translator;
    $this->praticaManager = $praticaManager;
  }

  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    /** @var TestiAccompagnatoriProcedura $helper */
    $helper = $options["helper"];

    $helper->setStepTitle('steps.common.stamps.title', true);
    $helper->setDescriptionText('', true);
    $helper->setGuideText('', true);

    /** @var FormIO $application */
    $application = $builder->getData();
    $data = $application->getStamps();

    $builder->add('stamps', CollectionType::class, [
      'label' => false,
      'entry_type' => StampType::class,
      'data' => $data
    ]);

    $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
    $builder->addEventListener(FormEvents::POST_SUBMIT, array($this, 'postSubmit'));
  }

  public function onPreSubmit(FormEvent $event): void
  {
    $stamps = $event->getData()['stamps'];
    $firstAvailableDate = new \DateTime('2002-01-01');
    $stampNumbers = [];
    foreach ($stamps as $stamp) {
      try {
        $emittedAt = new \DateTime($stamp['emitted_at']);
      } catch (\Exception $e) {
        $event->getForm()->get('stamps')->addError(new FormError($this->translator->trans('servizio.stamps.invalid_date_format')));
        return;
      }
      // Creo un datetime per ogni iterazione altrimenti il check fallisce per i pochi millisecondi che dividono 'now' dalla data di emissione se
      // viene inserita data e ora corrente o se non specificata
      if ($emittedAt < $firstAvailableDate || $emittedAt > new \DateTime()) {
        $event->getForm()->get('stamps')->addError(new FormError(
          $this->translator->trans('servizio.stamps.invalid_emission_date', ['%emitted_at%' => $emittedAt->format('d/m/Y, H:i')])));
      }
      $number = $stamp['number'];
      if (!preg_match('/^[0-9]{14}$/', $stamp['number'])) {
        $event->getForm()->get('stamps')->addError(new FormError(
          $this->translator->trans('servizio.stamps.invalid_number', ['%number%' => $number])
        ));
      }

      if (in_array($number, $stampNumbers)) {
        $event->getForm()->get('stamps')->addError(new FormError(
          $this->translator->trans('servizio.stamps.duplicate_number')
        ));
      } else {
        $stampNumbers [] = $number;
      }

    }
  }

  /**
   * @throws \Exception
   */
  public function postSubmit(FormEvent $event): void
  {

    if (!$event->getForm()->isValid()) {
      return;
    }

    /** @var FormIO $application */
    $application = $event->getForm()->getData();
    $this->praticaManager->save($application);


    try {
      // Se la pratica ha un pagamento in fase di richiesta configurato
      if ($application->getStatus() === Pratica::STATUS_STAMPS_PAYMENT_PENDING && $application->hasImmediatePayment()) {
        $this->praticaManager->selectPaymentGateway($application);
      } else if ($application->getStatus() === Pratica::STATUS_STAMPS_PAYMENT_PENDING && $application->getEsito()) {

        // Se la pratica ha un pagamento posticipato configurato
        if ($application->hasPaymentDeferred()) {
          $this->statusService->setNewStatus($application, Pratica::STATUS_PAYMENT_PENDING);
          return;
        }

        // Se il servizio prevede protocollazione
        if ($application->getServizio()->isProtocolRequired()) {
          $this->statusService->setNewStatus($application, Pratica::STATUS_COMPLETE_WAITALLEGATIOPERATORE);
          return;
        }
        // Non sono previste ulteriori interazioni sulla pratica
        $this->statusService->setNewStatus($application, Pratica::STATUS_COMPLETE);
        return;
      }
    } catch (\Exception $e) {
      $event->getForm()->addError(new FormError($e->getMessage()));
    }
  }

  public function getBlockPrefix(): string
  {
    return 'pratica_stamps';
  }
}
