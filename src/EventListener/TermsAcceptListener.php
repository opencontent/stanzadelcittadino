<?php

namespace App\EventListener;

use App\Entity\CPSUser;
use App\Services\InstanceService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class TermsAcceptListener
{
  private InstanceService $instanceService;
  private RouterInterface $router;

  private TokenStorageInterface $tokenStorage;

  private array $termsAcceptRoutes = [
    'user_dashboard',
    'user_profile',
    'pratiche',
    'allegati_list_cpsuser',
    'folders_list_cpsuser',
    'subscriptions_list_cpsuser',
    //'payments_list_cpsuser',
  ];


  public function __construct(InstanceService $instanceService, RouterInterface $router, TokenStorageInterface $tokenStorage)
  {
    $this->instanceService = $instanceService;
    $this->router = $router;
    $this->tokenStorage = $tokenStorage;

  }


  public function onKernelRequest(RequestEvent $event): void
  {

    $currentRoute = $event->getRequest()->get('_route');
    if (!in_array($currentRoute, $this->termsAcceptRoutes, true)){
      return;
    }

    if (null === $this->instanceService->getCurrentInstance()->getPrivacyInfo()) {
      return;
    }

    $user = $this->getUser();
    if (!empty($currentRoute) && $currentRoute !== 'terms' && $user instanceof CPSUser && !$user->isAnonymous() && !$user->areTermsAndPrivacyRead()) {
      $redirectParameters = ['r' => $event->getRequest()->getUri()];
      $redirectUrl = $this->router->generate('terms', $redirectParameters);
      $event->setResponse(new RedirectResponse($redirectUrl));
    }
  }

  protected function getUser(): ?UserInterface
  {
    if (null === $token = $this->tokenStorage->getToken()) {
      return null;
    }

    if (!is_object($user = $token->getUser())) {
      return null;
    }

    return $user;
  }
}
