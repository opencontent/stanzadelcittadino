<?php

namespace App\EventListener;

use App\Event\KafkaEvent;
use App\Services\KafkaService;
use GuzzleHttp\Exception\GuzzleException;
use ReflectionException;

class KafkaListener
{
  private KafkaService $kafkaService;

  /**
   * WebhookApplicationListener constructor.
   * @param KafkaService $kafkaService
   */
  public function __construct(KafkaService $kafkaService)
  {
    $this->kafkaService = $kafkaService;
  }

  /**
   * @param KafkaEvent $event
   * @return void
   * @throws GuzzleException|ReflectionException
   */
  public function produce(KafkaEvent $event): void
  {
    // Todo: è necessario un try catch?
    $item = $event->getItem();
    $this->kafkaService->produceMessage($item);
  }
}
