<?php

namespace App\EventListener;

use App\Entity\AdminUser;
use App\Entity\CPSUser;
use App\Entity\OperatoreUser;
use App\Services\CPSUserProvider;
use App\Validator\FiscalCodeValidator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;


class CheckUserProfileListener
{
  private RouterInterface $router;
  private TokenStorageInterface $tokenStorage;

  private CPSUserProvider $userProvider;

  private string $passwordLifeTime;

  private array $ignoreListenerRoutes = [
    'attachment_upload',
    'attachment_upload_finalize'
  ];
  private FlashBagInterface $flashBag;
  private TranslatorInterface $translator;

  /**
   * CheckUserProfileListener constructor.
   *
   * @param RouterInterface $router
   * @param TokenStorageInterface $tokenStorage
   * @param CPSUserProvider $userProvider
   * @param string $passwordLifeTime
   * @param FlashBagInterface $flashBag
   * @param TranslatorInterface $translator
   */
  public function __construct(RouterInterface $router, TokenStorageInterface $tokenStorage, CPSUserProvider $userProvider, string $passwordLifeTime, FlashBagInterface $flashBag, TranslatorInterface $translator)
  {
    $this->router = $router;
    $this->tokenStorage = $tokenStorage;
    $this->userProvider = $userProvider;
    $this->passwordLifeTime = $passwordLifeTime;
    $this->flashBag = $flashBag;
    $this->translator = $translator;
  }

  public function onKernelRequest(RequestEvent $event): void
  {

    $request = $event->getRequest();
    if (strpos($request->getRequestUri(), '/api/') !== false) {
      return;
    }

    // Check if the request is the master request (not a sub-request)
    if (!$event->isMainRequest()) {
      return;
    }

    $user = $this->getUser();
    if ($user instanceof CPSUser && !$user->isAnonymous()) {
      $currentRoute = $event->getRequest()->get('_route');
      $fiscalCodevalidator = new FiscalCodeValidator($user->getUsername());
      // Esistono utenti sulla piattaforma che si loggano con ldap esterni e non hanno cf come username
      if ($currentRoute !== 'error' && $fiscalCodevalidator->isFormallyValid() && !$fiscalCodevalidator->isOlderThan18()) {
        $redirectUrl = $this->router->generate('error');
        $event->setResponse(new RedirectResponse($redirectUrl));
      }
    }

    // L'utente non deve essere mai portato nella pagina di completamento del profilo, l'unico dato necessario per adesso è l'autenticazione
    // Todo: da verificare se vogliamo intercettare la rotta specifica di compilazione
    /*if ($user instanceof CPSUser && !$user->isAnonymous()) {
      $currentRoute = $event->getRequest()->get('_route');
      if (in_array($currentRoute, $this->ignoreListenerRoutes)) {
        return;
      }
      $currentRouteParams = $event->getRequest()->get('_route_params');
      $currentRouteQuery = $event->getRequest()->query->all();

      if (!$this->userProvider->userHasEnoughData($user)
        && $currentRoute !== ''
        && $currentRoute !== 'user_profile'
        && $currentRoute !== 'terms'
      ) {

        $redirectParameters = [
          'r' => $event->getRequest()->getUri()
        ];

        $redirectUrl = $this->router->generate('user_profile', $redirectParameters);
        $event->setResponse(new RedirectResponse($redirectUrl));
      }
    }*/

    if ($user instanceof OperatoreUser || $user instanceof AdminUser) {

      // Redirect al security_change_password se lastChangePassword è più vecchio della data odierna - $passwordLifeTime
      $currentRoute = $event->getRequest()->get('_route');
      $currentRouteParams = $event->getRequest()->get('_route_params');
      $currentRouteQuery = $event->getRequest()->query->all();
      $isPasswordExpired = $user->getLastChangePassword() && $user->getLastChangePassword()->getTimestamp() < strtotime('-' . $this->passwordLifeTime . ' day');
      $needsToChangePassword = $user instanceof AdminUser || ($user instanceof OperatoreUser && !$user->isSystemUser());

      if ($currentRoute !== 'security_change_password' && ($user->getLastChangePassword() == null || ($needsToChangePassword && $isPasswordExpired))) {
        $this->flashBag->add('info', $this->translator->trans('security.expired_password'));

        $redirectParameters['r'] = $currentRoute;
        if ($currentRouteParams) {
          $redirectParameters['p'] = serialize($currentRouteParams);
        }
        if ($currentRouteParams) {
          $redirectParameters['q'] = serialize($currentRouteQuery);
        }

        $redirectUrl = $this->router->generate('security_change_password', $redirectParameters);
        $event->setResponse(new RedirectResponse($redirectUrl));
      }

    }

  }

  protected function getUser()
  {
    if (null === $token = $this->tokenStorage->getToken()) {
      return null;
    }

    if (!is_object($user = $token->getUser())) {
      return null;
    }

    return $user;
  }
}

