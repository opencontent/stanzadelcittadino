<?php

namespace App\EventListener;

use App\Entity\OperatoreUser;
use App\Security\OAuth\BackendConfigurationProviderFactory;
use App\Security\OAuth\LogoutProviderInterface;
use App\Services\UserSessionService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Event\LogoutEvent;

class LogoutSubscriber implements EventSubscriberInterface
{
  private UrlGeneratorInterface $router;
  private UserSessionService $userSessionService;
  private BackendConfigurationProviderFactory $providerFactory;
  private bool $isEnabledBackendOauth;

  public function __construct(UrlGeneratorInterface $router, UserSessionService $userSessionService, BackendConfigurationProviderFactory $providerFactory, $isEnabledBackendOauth)
  {
    $this->router = $router;
    $this->userSessionService = $userSessionService;
    $this->providerFactory = $providerFactory;
    $this->isEnabledBackendOauth = $isEnabledBackendOauth;
  }

  public static function getSubscribedEvents(): array
  {
    return [LogoutEvent::class => 'onLogout'];
  }

  public function onLogout(LogoutEvent $event): void
  {

    $token = $event->getToken();
    
    if (!$token || !$this->isEnabledBackendOauth || !($token->getUser()) instanceof OperatoreUser) {
      return;
    }

    $logoutUrl = null;
    $provider = $this->providerFactory->instanceProvider();
    if ($provider instanceof LogoutProviderInterface) {
      $user = $token->getUser();
      $logoutUrl = $provider->getUrlLogout($this->userSessionService->getLastUserSessionData($user));
    }

    if (empty($logoutUrl)) {
      $logoutUrl = $this->router->generate('security_login');
    }

    $response = new RedirectResponse($logoutUrl, Response::HTTP_SEE_OTHER);
    $event->setResponse($response);
  }
}
