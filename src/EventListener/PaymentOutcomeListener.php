<?php

namespace App\EventListener;

use App\BackOffice\BackOfficeInterface;
use App\Entity\DematerializedFormPratica;
use App\Entity\Pratica;
use App\Event\GenerateApplicationReceiptEvent;
use App\Event\PraticaOnChangeStatusEvent;
use App\ScheduledAction\Exception\AlreadyScheduledException;
use App\Services\ModuloPdfBuilderService;
use App\Services\PraticaStatusService;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class PaymentOutcomeListener
{

  private PraticaStatusService $statusService;

  private LoggerInterface $logger;

  private EventDispatcherInterface $dispatcher;

  public function __construct(PraticaStatusService $statusService,  LoggerInterface $logger, EventDispatcherInterface $dispatcher)
  {
    $this->statusService = $statusService;
    $this->logger = $logger;
    $this->dispatcher = $dispatcher;
  }

  public function onStatusChange(PraticaOnChangeStatusEvent $event)
  {
    $application = $event->getPratica();
    if ( $event->getNewStateIdentifier() == Pratica::STATUS_PAYMENT_SUCCESS) {
      // Se la pratica ha già un esito significa che è una pratica con pagamento differito
      if ($application->getEsito()) {
        if ($application->getServizio()->isProtocolRequired()) {
          $this->statusService->setNewStatus($application, Pratica::STATUS_COMPLETE_WAITALLEGATIOPERATORE);
        } else {
          $this->statusService->setNewStatus($application, Pratica::STATUS_COMPLETE);
        }
      } else {
        // Invio la pratica
        $application->setSubmissionTime(time());
        $this->statusService->setNewStatus($application, Pratica::STATUS_PRE_SUBMIT);

        // Emette l'evento per la generazione del pdf
        $this->dispatcher->dispatch(new GenerateApplicationReceiptEvent($application), GenerateApplicationReceiptEvent::NAME);
      }
    }
  }
}
