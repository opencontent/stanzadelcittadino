<?php

namespace App\EventListener;

use App\Entity\Pratica;
use App\Entity\Servizio;
use App\Event\GenerateApplicationReceiptEvent;
use App\ScheduledAction\Exception\AlreadyScheduledException;
use App\Services\ModuloPdfBuilderService;
use App\Services\PraticaStatusService;
use Psr\Log\LoggerInterface;

class CreatePdfPraticaListener
{

  private ModuloPdfBuilderService $pdfBuilder;
  private PraticaStatusService $statusService;

  private LoggerInterface $logger;

  public function __construct(ModuloPdfBuilderService $pdfBuilder, PraticaStatusService $statusService, LoggerInterface $logger)
  {
    $this->pdfBuilder = $pdfBuilder;
    $this->statusService = $statusService;
    $this->logger = $logger;
  }

  public function onStatusChange(GenerateApplicationReceiptEvent $event): void
  {

    $pratica = $event->getApplication();



    if ($pratica->getServizio()->getReceipt() != Servizio::RECEIPT_COMPILED_MODULE_ON_DEMAND) {
      if ($pratica->getStatus() == Pratica::STATUS_PRE_SUBMIT && $pratica->getModuliCompilati()->isEmpty()) {
        try {
          $this->pdfBuilder->createForPraticaAsync($pratica);
          $this->logger->info('Pdf generation for application scheduled', ['application' => $pratica->getId(), 'status' => $pratica->getStatus()]);
        } catch (AlreadyScheduledException $e) {
          $this->logger->error('Pdf generation is already scheduled', ['application' => $pratica->getId(), 'status' => $pratica->getStatus()]);
        }
      } elseif ($pratica->getStatus() == Pratica::STATUS_REGISTERED) {
        try {
          $this->pdfBuilder->updateForPraticaAsync($pratica);
          $this->logger->info('Pdf regeneration for registered application scheduled', ['application' => $pratica->getId(), 'status' => $pratica->getStatus()]);
        } catch (AlreadyScheduledException $e) {
          $this->logger->error('Pdf regeneration is already scheduled', ['application' => $pratica->getId(), 'status' => $pratica->getStatus()]);
        }
      } elseif ($pratica->getStatus() > Pratica::STATUS_SUBMITTED && $pratica->getModuliCompilati()->isEmpty()) {
        try {
          $this->pdfBuilder->createForPraticaAsync($pratica, $this->pdfBuilder::WITHOUT_CHANGE_STATUS);
          $this->logger->info('Pdf generation for application scheduled', ['application' => $pratica->getId(), 'status' => $pratica->getStatus()]);
        } catch (AlreadyScheduledException $e) {
          $this->logger->error('Pdf generation is already scheduled', ['application' => $pratica->getId(), 'status' => $pratica->getStatus()]);
        }
      }
    } else {
      // Todo: modificare il paticaStatusService, deve determinare il automatico lo stato in cui deve andare una pratica.
      // Solo nel caso di pratica inviata passo intervengo sugli stati e passo allo stato successivo
      if ($pratica->getStatus() == Pratica::STATUS_PRE_SUBMIT) {
        $this->statusService->setNewStatus($pratica, Pratica::STATUS_SUBMITTED);
      }
    }
  }
}
