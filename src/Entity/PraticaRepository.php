<?php

namespace App\Entity;

use App\FormIO\SchemaComponent;
use App\Model\Transition;
use App\Utils\GeoUtils;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use PHPCoord\Exception\UnknownCoordinateReferenceSystemException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * PraticaRepository
 *
 */
class PraticaRepository extends EntityRepository
{
  public const OPERATORI_LOWER_STATE = Pratica::STATUS_STAMPS_PAYMENT_PENDING;

  private $classConstants;


  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   */
  public function findRelatedPraticaForUser(CPSUser $user)
  {
    $sql = 'SELECT id from pratica where (related_cfs)::jsonb @> \'"' . $user->getCodiceFiscale() . '"\'';


    $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
    $result = $stmt->executeQuery()->fetchAllAssociative();

    $ids = [];

    foreach ($result as $id) {
      $ids[] = $id['id'];
    }

    return $this->findById($ids);
  }

  public function findPendingPraticaForUser(CPSUser $user): array
  {
    return $this->findBy(
      [
        'user' => $user,
        'status' => [
          Pratica::STATUS_PRE_SUBMIT,
          Pratica::STATUS_PAYMENT_PENDING,
          Pratica::STATUS_SUBMITTED,
          Pratica::STATUS_REGISTERED,
          Pratica::STATUS_PENDING,
          Pratica::STATUS_PENDING_AFTER_INTEGRATION,
          Pratica::STATUS_COMPLETE_WAITALLEGATIOPERATORE,
          Pratica::STATUS_REQUEST_INTEGRATION,
          Pratica::STATUS_REGISTERED_AFTER_INTEGRATION,
          Pratica::STATUS_CANCELLED_WAITALLEGATIOPERATORE,
          Pratica::STATUS_PAYMENT_OUTCOME_PENDING
        ],
      ],
      [
        'updatedAt' => 'DESC',
      ]
    );
  }

  public function findEvidencePraticaForUser(CPSUser $user)
  {
    $timeDiff = "-7 days";
    $qb = $this->createQueryBuilder('p');
    $qb
      ->where('p.user = :user')
      ->andWhere('p.status IN (:statues)')
      ->orWhere('p.latestStatusChangeTimestamp >= :timediff AND p.user = :user')
      ->setParameter('user', $user->getId())
      ->setParameter('timediff', strtotime($timeDiff))
      ->setParameter('statues', [Pratica::STATUS_PAYMENT_PENDING, Pratica::STATUS_DRAFT_FOR_INTEGRATION, Pratica::STATUS_PAYMENT_OUTCOME_PENDING])
      ->orderBy('p.status', 'ASC')
      ->addOrderBy('p.latestStatusChangeTimestamp', 'DESC')
      ->setMaxResults(10);
    return $qb->getQuery()->getResult();
  }

  public function findSubmittedPraticheByEnte(Ente $ente)
  {
    $qb = $this->createQueryBuilder('p');
    $qb->where('p.status >= ' . Pratica::STATUS_SUBMITTED)
      ->andWhere('p.ente = :ente')
      ->setParameter('ente', $ente);


    return $qb->getQuery()->getResult();
  }

  public function findPraticheByOperatore(OperatoreUser $user, $filters, $limit, $offset)
  {
    $serviziAbilitati = $this->getServizioIdListByOperatore($user);
    if (empty($serviziAbilitati)) {
      return [];
    }

    $qb = $this->getPraticheByOperatoreQueryBuilder($filters, $user);
    if (isset($filters['sort'], $filters['order'])) {
      $qb->orderBy('pratica.' . $filters['sort'], strtolower($filters['order']));
    } else {
      $qb->orderBy('pratica.submissionTime', 'desc');
    }
    // Todo: a cosa serve?
    $qb->addOrderBy('pratica.id', 'desc');

    return $qb->setFirstResult($offset)
      ->setMaxResults($limit)
      ->getQuery()->execute();
  }

  /**
   * @throws Exception
   * @throws NonUniqueResultException
   * @throws \Doctrine\DBAL\Exception
   * @throws NoResultException
   */
  public function countPraticheByOperatore(OperatoreUser $user, $filters)
  {
    $servicesByOperatore = $this->getServizioIdListByOperatore($user);
    if (empty($servicesByOperatore)) {
      return 0;
    }

    $qb = $this->getPraticheByOperatoreQueryBuilder($filters, $user);
    return $qb->select('count(pratica.id)')->getQuery()->getSingleScalarResult();
  }

  /**
   * @throws Exception
   * @throws \Doctrine\DBAL\Exception
   */
  public function findStatesPraticheByOperatore(OperatoreUser $user, $filters = []): array
  {
    $servicesByOperatore = $this->getServizioIdListByOperatore($user);
    if (empty($servicesByOperatore)) {
      return [];
    }

    $qb = $this->getPraticheByOperatoreQueryBuilder($filters, $user, null, 'DISTINCT pratica.status');
    $qb->addOrderBy('pratica.status', 'asc');

    $result = $qb->getQuery()->execute();
    $states = [];
    $states[] = ['id' => '', 'name' => 'Tutti'];
    foreach ($result as $s) {
      foreach ($this->getClassConstants() as $name => $value) {
        if ($value == $s['status'] && strpos($name, 'STATUS_') === 0) {
          $states[] = ['id' => $s['status'], 'name' => $name];
        }
      }
    }
    return $states;
  }

  /**
   * @param Pratica $pratica
   * @return UuidInterface|null
   * @throws \Exception
   */
  public function getFolderForApplication(Pratica $pratica)
  {
    $serviceGroup = $pratica->getServizio()->getServiceGroup();
    if (!$serviceGroup) {
      return null;
    }

    $result = $this->createQueryBuilder('pratica')
      ->where('pratica.user = :user')->setParameter('user', $pratica->getUser())
      ->andWhere('pratica.serviceGroup = :group')->setParameter('group', $serviceGroup)
      ->andWhere('pratica.folderId IS NOT NULL')
      ->orderBy('pratica.creationTime', 'DESC')
      ->setFirstResult(0)
      ->setMaxResults(1)
      ->getQuery()->execute();

    if (!empty($result)) {
      return $result[0]->getFolderId();
    }
    return Uuid::uuid4();
  }

  /**
   * @param SchemaComponent[] $fields
   * @param OperatoreUser $user
   * @param $filters
   * @return array|int|string
   */
  public function getSumFieldsInPraticheByOperatore($fields, OperatoreUser $user, $filters)
  {
    $servicesByOperatore = $this->getServizioIdListByOperatore($user);
    if (empty($servicesByOperatore)) {
      return 'n/a';
    }
    $sqlSelectFields = [];
    $fieldAliases = [];
    foreach ($fields as $index => $field) {
      $formField = "pratica.dematerializedForms " . $field->getName();
      $formFieldAlias = 'df_' . $index;
      $fieldAliases[$formFieldAlias] = $field;
      $sqlSelectFields[] = "SUM(FORMIO_JSON_FIELD($formField, DECIMAL)) as $formFieldAlias";
    }

    $result = array_fill_keys($fields, 0);

    if (!empty($sqlSelectFields)) {
      $data = $this->getPraticheByOperatoreQueryBuilder($filters, $user, FormIO::class)
        ->select($sqlSelectFields)
        ->getQuery()->execute();
      if (isset($data[0])) {
        foreach ($fieldAliases as $alias => $field) {
          if ((int)$data[0][$alias] === 0 && $field->getType() !== 'number') {
            $result[$field->getName()] = 'n/a';
          } else {
            $result[$field->getName()] = number_format($data[0][$alias], 2, ',', '.');
          }
        }
      }
    }

    return $result;
  }

  /**
   * @param SchemaComponent[] $fields
   * @param OperatoreUser $user
   * @param $filters
   * @return array|int|string
   */
  public function getAvgFieldsInPraticheByOperatore($fields, OperatoreUser $user, $filters)
  {
    $servicesByOperatore = $this->getServizioIdListByOperatore($user);
    if (empty($servicesByOperatore)) {
      return 'n/a';
    }
    $sqlSelectFields = [];
    $fieldAliases = [];
    foreach ($fields as $index => $field) {
      $formField = "pratica.dematerializedForms " . $field->getName();
      $formFieldAlias = 'df_' . $index;
      $fieldAliases[$formFieldAlias] = $field;
      $sqlSelectFields[] = "AVG(FORMIO_JSON_FIELD($formField, DECIMAL)) as $formFieldAlias";
    }

    $result = array_fill_keys($fields, 0);

    if (!empty($sqlSelectFields)) {
      $data = $this->getPraticheByOperatoreQueryBuilder($filters, $user, FormIO::class)
        ->select($sqlSelectFields)
        ->getQuery()->execute();

      if (isset($data[0])) {
        foreach ($fieldAliases as $alias => $field) {
          if ((int)$data[0][$alias] === 0 && $field->getType() !== 'number') {
            $result[$field->getName()] = 'n/a';
          } else {
            $result[$field->getName()] = number_format($data[0][$alias], 2, ',', '.');
          }
        }
      }
    }

    return $result;
  }

  /**
   * @param SchemaComponent[] $fields
   * @param OperatoreUser $user
   * @param $filters
   * @return array|string
   */
  public function getCountNotNullFieldsInPraticheByOperatore($fields, OperatoreUser $user, $filters)
  {
    $servicesByOperatore = $this->getServizioIdListByOperatore($user);
    if (empty($servicesByOperatore)) {
      return 'n/a';
    }
    $sqlSelectFields = [];
    $fieldAliases = [];
    foreach ($fields as $index => $field) {
      $formFieldAlias = 'df_' . $index;
      $fieldAliases[$formFieldAlias] = $field->getName();
    }

    $result = array_fill_keys($fields, 0);

    if (!empty($fieldAliases)) {
      foreach ($fieldAliases as $alias => $field) {
        $data = $this->getPraticheByOperatoreQueryBuilder($filters, $user, FormIO::class)
          ->select("count(FORMIO_JSON_FIELD(pratica.dematerializedForms $field)) as $alias")
          ->andWhere("FORMIO_JSON_FIELD(pratica.dematerializedForms $field) IS NOT NULL")
          ->andWhere("FORMIO_JSON_FIELD(pratica.dematerializedForms $field) != '[]'")
          ->getQuery()->execute();
        if (isset($data[0])) {
          $result[$field] = number_format($data[0][$alias], 0, ',', '.');
        }
      }
    }

    return $result;
  }

  /**
   * @param $filters
   * @param OperatoreUser $user
   * @param null $entity
   * @param string $fields
   * @return QueryBuilder
   * @throws Exception
   * @throws \Doctrine\DBAL\Exception
   */
  private function getPraticheByOperatoreQueryBuilder($filters, OperatoreUser $user, $entity = null, $fields = 'pratica')
  {
    if (!$entity) {
      $entity = Pratica::class;
    }

    $qb = $this->getEntityManager()->createQueryBuilder()
      ->select($fields)
      ->from($entity, 'pratica')
      ->leftJoin('pratica.servizio', 'servizio');

    $orStatements = $this->getEnabledServicesQuery($qb, $user);

    $qb->andWhere($orStatements);

    if (!empty($filters['servizio'])) {
      $qb->andWhere('pratica.servizio = :servizio')
        ->setParameter('servizio', $filters['servizio']);
    } else {
      $qb->orWhere('pratica.operatore = :operatore')
        ->setParameter('operatore', $user);
    }

    if (!empty($filters['gruppo'])) {
      $groupRepository = $this->getEntityManager()->getRepository('App\Entity\ServiceGroup');
      $serviceGroup = $groupRepository->find($filters['gruppo']);
      if ($serviceGroup instanceof ServiceGroup && $serviceGroup->getServicesCount() > 0) {
        $serviceIds = [];
        /** @var Servizio $service */
        foreach ($serviceGroup->getServices() as $service) {
          $serviceIds[] = $service->getId();
        }
        $qb->andWhere('pratica.servizio IN (:group)')
          ->setParameter('group', $serviceIds);
      }

    }


    if (!empty($filters['stato'])) {
      $qb->andWhere('pratica.status = :stato')
        ->setParameter('stato', $filters['stato']);
    } else {
      $qb->andWhere('pratica.status >= :stato')
        ->setParameter('stato', self::OPERATORI_LOWER_STATE);
    }

    if (!empty($filters['geographic_area'])) {
      $qb->join('pratica.geographicAreas', 'geographicAreas')
        ->andWhere('geographicAreas IN (:geographic_area)')
        ->setParameter('geographic_area', $filters['geographic_area']);
    }

    if ($filters['query_field'] && !empty($filters['query'])) {
      switch ($filters['query_field']) {
        case 1:
//          @todo must must must refactor
//          Non è possibile usare una JOIN in dql!
//          [Semantical Error] Error: Class App\Entity\User has no field or association named codiceFiscale
//          $qb->andWhere('LOWER(user.codiceFiscale) LIKE LOWER(:searchTerm)')
//            ->leftJoin('pratica.user', 'user')
//            ->setParameter('searchTerm', '%'.$filters['query'].'%');
//          break;

          $qb->andWhere("LOWER(FORMIO_JSON_FIELD(pratica.dematerializedForms applicant.fiscal_code.fiscal_code)) LIKE LOWER(:searchTerm)")
            ->setParameter('searchTerm', '%' . $filters['query'] . '%');

          break;
        case 2:
          $qb->andWhere('LOWER(user.nome) LIKE LOWER(:searchTerm) OR LOWER(user.cognome) LIKE LOWER(:searchTerm) OR CONCAT(LOWER(user.nome), \' \', LOWER(user.cognome)) LIKE LOWER(:searchTerm) OR CONCAT(LOWER(user.cognome), \' \', LOWER(user.nome)) LIKE LOWER(:searchTerm)')
            ->leftJoin('pratica.user', 'user')
            ->setParameter('searchTerm', '%' . $filters['query'] . '%');
          break;
        case 3:
          $qb->andWhere('pratica.id = :searchTerm');
          $qb->setParameter('searchTerm', $filters['query']);
          break;
      }
    }

    $workflowFilterParts = explode('@', (string)$filters['workflow']);

    $workflowFilterRequest = $workflowFilterParts[0];
    $workflowFilterRequestParam = $workflowFilterParts[1] ?? null;

    switch ($workflowFilterRequest) {
      case 'owned':
        $qb->andWhere('pratica.operatore = :operatore')
          ->setParameter('operatore', $user);
        break;
      case 'unassigned':
        $qb->andWhere('pratica.operatore IS NULL')
          ->andWhere('servizio.workflow = :workflow_forward')
          ->setParameter('workflow_forward', Servizio::WORKFLOW_APPROVAL);
        break;
      case 'user_group':
        if ($workflowFilterRequestParam) {
          $qb->andWhere('pratica.userGroup = :userGroup')
            ->setParameter('userGroup', $workflowFilterRequestParam);
        }
    }


    if ($filters['collate']) {
      $qb->andWhere('pratica.parent IS NULL');
      $qb->andWhere('pratica.id IN (:grouped)')
        ->setParameter('grouped', $this->getApplicationsCollectionsId($filters['servizio']));
    }

    if (!empty($filters['last_status_change']) && count($filters['last_status_change']) == 2) {
      $qb->andWhere('pratica.latestStatusChangeTimestamp >= :start AND pratica.latestStatusChangeTimestamp <= :end');
      $qb->setParameter('start', (int)$filters['last_status_change'][0]);
      $qb->setParameter('end', (int)$filters['last_status_change'][1]);
    }

    if (!empty($filters['priority'])) {
      $qb->andWhere('pratica.priority = :priority');
      $qb->setParameter('priority', (int)$filters['priority']);
    }

    return $qb;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   */
  private function getApplicationsCollectionsId($filterService)
  {
    $data = [];
    /*
      SELECT json_agg(id) as ids, folder_id
      FROM pratica
      WHERE status != 1000 AND folder_id IS NOT null
      GROUP BY folder_id, created_at
      order by created_at asc
      limit 1
    */
    // Per migliorare le performance mostriamo solo la prima delle pratiche che hanno stesso folder_id
    $dql = 'SELECT json_agg(id) as ids, folder_id FROM pratica WHERE status != 1000 AND folder_id IS NOT null';
    if (!empty($filterService)) {
      $dql .= " AND servizio_id = '$filterService'";
    }
    $dql .= ' GROUP BY folder_id, created_at ORDER BY created_at ASC LIMIT 1';

    $stmt = $this->getEntityManager()->getConnection()->prepare($dql);
    $result = $stmt->executeQuery()->fetchAllAssociative();

    foreach ($result as $r) {
      $temp = \json_decode($r['ids']);
      $data[] = $temp[0];
    }

    // Elimino perchè la singola pratica non deve costituire un fascicolo, da verificare
    /*$dql = 'SELECT id FROM pratica WHERE folder_id IS NULL';
    $stmt = $this->getEntityManager()->getConnection()->prepare($dql);
    $result = $stmt->executeQuery()->fetchAllAssociative();
    foreach ($result as $r) {
      if (!in_array($r['id'], $data)) {
        $data[] = $r['id'];
      }
    }*/
    return $data;
  }

  /**
   * @throws Exception
   * @throws NonUniqueResultException
   * @throws NoResultException
   * @throws \Doctrine\DBAL\Exception
   */
  public function getApplications($parameters = [], $onlyCount = false, $order = 'creationTime', $sort = 'ASC', $offset = 0, $limit = 10)
  {

    $qb = $this->createQueryBuilder('pratica');
    if (isset($parameters['status'])) {
      $qb->where('pratica.status = :status')->setParameter('status', $parameters['status']);
    } else {
      $qb->where('pratica.status != :status')->setParameter('status', Pratica::STATUS_DRAFT);
    }

    if (isset($parameters['service'])) {
      $qb->andWhere('pratica.servizio IN (:services)')->setParameter('services', $parameters['service']);
    }

    if (isset($parameters['operatore'])) {
      if (isset($parameters['get_only_assigned_applications'])) {
        if ($parameters['get_only_assigned_applications']) {
          $qb->andWhere('pratica.operatore = :operatore')->setParameter('operatore', $parameters['operatore']);
        }
      } else {
        $qb->orWhere('pratica.operatore = :operatore')->setParameter('operatore', $parameters['operatore']);
      }
    }

    if (isset($parameters['service_identifier'])) {
      $qb
        ->leftJoin('pratica.servizio', 'service')
        ->andWhere('service.identifier = :identifier')
        ->setParameter('identifier', $parameters['service_identifier']);
    }

    // after|before|strictly_after|strictly_before
    if (isset($parameters['createdAt'])) {
      if (isset($parameters['createdAt']['strictly_after'])) {
        $qb->andWhere('pratica.createdAt > :createdAtAfter')->setParameter('createdAtAfter', $parameters['createdAt']['strictly_after']);
      } else if (isset($parameters['createdAt']['after'])) {
        $qb->andWhere('pratica.createdAt >= :createdAtAfter')->setParameter('createdAtAfter', $parameters['createdAt']['after']);
      }

      if (isset($parameters['createdAt']['strictly_before'])) {
        $qb->andWhere('pratica.createdAt < :createdAtBefore')->setParameter('createdAtBefore', $parameters['createdAt']['strictly_before']);
      } else if (isset($parameters['createdAt']['before'])) {
        $qb->andWhere('pratica.createdAt <= :createdAtBefore')->setParameter('createdAtBefore', $parameters['createdAt']['before']);
      }
    }

    if (isset($parameters['updatedAt'])) {
      if (isset($parameters['updatedAt']['strictly_after'])) {
        $qb->andWhere('pratica.updatedAt > :updatedAtAfter')->setParameter('updatedAtAfter', $parameters['updatedAt']['strictly_after']);
      } else if (isset($parameters['updatedAt']['after'])) {
        $qb->andWhere('pratica.updatedAt >= :updatedAtAfter')->setParameter('updatedAtAfter', $parameters['updatedAt']['after']);
      }

      if (isset($parameters['updatedAt']['strictly_before'])) {
        $qb->andWhere('pratica.updatedAt < :updatedAtBefore')->setParameter('updatedAtBefore', $parameters['updatedAt']['strictly_before']);
      } else if (isset($parameters['updatedAt']['before'])) {
        $qb->andWhere('pratica.updatedAt <= :updatedAtBefore')->setParameter('updatedAtBefore', $parameters['updatedAt']['before']);
      }
    }
    // after|before|strictly_after|strictly_before
    if (isset($parameters['submittedAt'])) {
      if (isset($parameters['submittedAt']['strictly_after'])) {
        $qb->andWhere('pratica.submissionTime > :submittedAtAfter')->setParameter('submittedAtAfter', (new \DateTime($parameters['submittedAt']['strictly_after']))->getTimestamp());
      } else if (isset($parameters['submittedAt']['after'])) {
        $qb->andWhere('pratica.submissionTime >= :submittedAtAfter')->setParameter('submittedAtAfter', (new \DateTime($parameters['submittedAt']['after']))->getTimestamp());
      }

      if (isset($parameters['submittedAt']['strictly_before'])) {
        $qb->andWhere('pratica.submissionTime < :submittedAtBefore')->setParameter('submittedAtBefore', (new \DateTime($parameters['submittedAt']['strictly_before']))->getTimestamp());
      } else if (isset($parameters['submittedAt']['before'])) {
        $qb->andWhere('pratica.submissionTime <= :submittedAtBefore')->setParameter('submittedAtBefore', (new \DateTime($parameters['submittedAt']['before']))->getTimestamp());
      }
    }

    if (isset($parameters['user'], $parameters['beneficiary'])) {
      $qb->andWhere(
        $qb->expr()->orX(
          $qb->expr()->eq('pratica.user', ':user'),
          $qb->expr()->eq('pratica.beneficiary', ':beneficiary')
        )
      )
        ->setParameter(':user', $parameters['user'])
        ->setParameter(':beneficiary', $parameters['beneficiary']);

    } elseif (isset($parameters['user'])) {
      $qb->andWhere('pratica.user = :user')->setParameter('user', $parameters['user']);
    } elseif (isset($parameters['beneficiary'])) {
      $qb->andWhere('pratica.beneficiary = :beneficiary')->setParameter('beneficiary', $parameters['beneficiary']);
    }

    if (isset($parameters['bounding_box'])) {
      $ids = $this->getApplicationsInBoundingBox($parameters['bounding_box']);
      $qb->andWhere('pratica.id IN (:bounded_applications)')->setParameter('bounded_applications', $ids);
    }

    if (isset($parameters['field'])) {
      foreach ($parameters['field'] as $field => $value) {
        // Remove data elements
        $field = str_replace('.data.', '.', $field);
        // Remove dots from parameter name
        $fieldValueKey = str_replace('.', '', $field);
        $qb->andWhere("(LOWER(FORMIO_JSON_FIELD(pratica.dematerializedForms $field)) = :{$fieldValueKey} OR LOWER(FORMIO_JSON_NO_DATA_FIELD(pratica.dematerializedForms $field)) = :{$fieldValueKey})")
          ->setParameter($fieldValueKey, strtolower($value));
      }
    }

    if (isset($parameters['backoffice_field'])) {
      foreach ($parameters['backoffice_field'] as $field => $value) {
        // Remove data elements
        $field = str_replace('.data.', '.', $field);
        // Remove dots from parameter name
        $fieldValueKey = str_replace('.', '', $field);
        $qb->andWhere("(LOWER(FORMIO_JSON_FIELD(pratica.backofficeFormData $field)) = :{$fieldValueKey} OR LOWER(FORMIO_JSON_NO_DATA_FIELD(pratica.backofficeFormData $field)) = :{$fieldValueKey})")
          ->setParameter($fieldValueKey, strtolower($value));
      }
    }

    if ($onlyCount) {
      $qb->select('COUNT(pratica.id)');
      return $qb->getQuery()->getSingleScalarResult();
    }

    if (isset($parameters['geojson']) && $parameters['geojson']) {
      $qb->select('pratica.id');
    } else {
      $qb
        ->orderBy('pratica.' . $order, $sort)
        ->setFirstResult($offset)
        ->setMaxResults($limit);
    }

    return $qb->getQuery()->execute();
  }

  /**
   * @throws UnknownCoordinateReferenceSystemException
   * @throws Exception
   * @throws \Doctrine\DBAL\Exception
   */
  public function getClusteredApplications($parameters): array
  {
    // Eseguo prima la query per recuperare le pratiche in base ai parametri passati, solo se ho risultati eseguo la query per recuperare la posizione
    $applications = $this->getApplications($parameters);

    if (empty($applications)) {
      return [];
    }

    $ids = [];
    foreach ($applications as $application) {
      $ids [] = $application['id'];
    }

    $clusterDistance = 100 * 1000;
    if (isset($parameters['bounding_box'])) {
      $clusterDistance = GeoUtils::getClusterDistance($parameters['bounding_box']);
    }

    $where = '';
    if (!empty($ids)) {
      $where = "WHERE id IN ('" . implode("', '", $ids) . "')";
    }
    $sql = sprintf("SELECT dbscan_cid, count(projected_point) as point_number,
                           ST_AsGeoJSON(ST_Transform(ST_AsGeoJSON(ST_Centroid(st_union(projected_point))), 4326)) as center,
                           string_agg(id::text, ',') as application_ids
                           FROM (
                            SELECT
                            ST_ClusterDBSCAN(projected_point, %f, 1) OVER () dbscan_cid,
                            projected_point,
                            id
                            FROM geo_application
                            %s
                           ) kmeans GROUP BY dbscan_cid;", $clusterDistance, $where);
    $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
    $result = $stmt->executeQuery();
    return $result->fetchAllAssociative();
  }

  /**
   * @throws Exception
   * @throws \Doctrine\DBAL\Exception
   */
  private function getApplicationsInBoundingBox(array $boundingBox)
  {
    $ids = [];
    $sql = sprintf("SELECT id FROM geo_application WHERE geo_point @ ST_MakeEnvelope(%s, 4326);", implode(', ', $boundingBox));
    $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
    $result = $stmt->executeQuery()->fetchAllAssociative();
    if (!empty($result)) {
      foreach ($result as $item) {
        $ids [] = $item['id'];
      }
    }
    return $ids;
  }

  /**
   * @param Pratica $pratica
   * @return Pratica[]
   */
  public function getApplicationsInFolder(Pratica $pratica): array
  {
    $serviceGroup = $pratica->getServizio()->getServiceGroup();
    $folderId = $pratica->getFolderId();
    $applications = [];
    if ($serviceGroup && $folderId) {
      $applications = $this->createQueryBuilder('pratica')
        ->where('pratica.user = :user')->setParameter('user', $pratica->getUser())
        ->andWhere('pratica.status >= :status')->setParameter('status', Pratica::STATUS_PRE_SUBMIT)
        ->andWhere('pratica.folderId = :folderId')->setParameter('folderId', $folderId)
        ->andWhere('pratica.parent IS NULL')
        ->orderBy('pratica.submissionTime', 'DESC')
        ->getQuery()->execute();
    }
    if (count($applications) == 0 && $pratica->getParent() !== null) {
      $applications = $this->getApplicationsInFolder($pratica->getRootParent());
    }

    if (count($applications) == 0 && $pratica->getChildren()->count() > 0) {
      $applications [] = $pratica;
    }
    return $applications;
  }

  /**
   * Get services list by operatore
   * It contains both the services to which the operatore is enabled
   * and the services where the operatore has at least one assigned application
   *
   * @param OperatoreUser $user
   * @param int|null $minStatus
   * @return array
   */
  public function getServizioIdListByOperatore(OperatoreUser $user, $minStatus = null): array
  {
    $qb = $this->createQueryBuilder('pratica')
      ->select('distinct identity(pratica.servizio)');

    if (!$user->isSystemUser()) {
      $orStatements = $this->getEnabledServicesQuery($qb, $user);

      $qb->andWhere($orStatements);
    }

    if ($minStatus) {
      $qb->andWhere('pratica.status >= :minStatus')
        ->setParameter('minStatus', (int)$minStatus);
    }

    $qb->orderBy('pratica.servizio', 'ASC');
    return $qb->getQuery()->getSingleColumnResult();
  }

  /**
   * Get applications statuses list by operatore
   *
   * @param OperatoreUser $user
   * @param int|null $minStatus
   * @return array
   */
  public function getStateListByOperatore(OperatoreUser $user, $minStatus = null): array
  {
    $qb = $this->createQueryBuilder('pratica')
      ->select('pratica.status');

    $orStatements = $this->getEnabledServicesQuery($qb, $user);

    $qb->andWhere($orStatements);

    if ($minStatus) {
      $qb->andWhere('pratica.status >= :minStatus')
        ->setParameter('minStatus', (int)$minStatus);
    }

    $qb->orderBy('pratica.servizio', 'ASC');
    $stateIdList = $qb->getQuery()->getSingleColumnResult();

    $states = [];
    foreach ($stateIdList as $id) {
      foreach ($this->getClassConstants() as $name => $value) {
        if ($value == $id && strpos($name, 'STATUS_') === 0) {
          $states[] = ['id' => $id, 'name' => $name];
        }
      }
    }

    return $states;
  }

  /**
   * Get applications geographic areas list by operatore
   *
   * @param OperatoreUser $user
   * @return array
   */
  public function getGeographicAreasListByOperatore(OperatoreUser $user): array
  {
    $qb = $this->createQueryBuilder('pratica')
      ->join('pratica.geographicAreas', 'g')
      ->select('g.id, g.name');

    $orStatements = $this->getEnabledServicesQuery($qb, $user);

    $qb->andWhere($orStatements)
      ->groupBy('g.id')
      ->orderBy('g.name', 'ASC');;

    return $qb->getQuery()->getResult();
  }

  private function getClassConstants()
  {
    if (null === $this->classConstants) {
      $class = new \ReflectionClass(Pratica::class);
      $this->classConstants = $class->getConstants();
    }

    return $this->classConstants;
  }

  public function findRecentlySubmittedPraticheByUser(Pratica $pratica, CPSUser $user, $limit)
  {
    $qb = $this->createQueryBuilder('p');
    $qb->where('p.status >= ' . Pratica::STATUS_SUBMITTED)
      ->andWhere('p.user = :user')
      ->setParameter('user', $user)
      ->andWhere('p.id NOT IN (:tree)')
      ->setParameter('tree', $pratica->getTreeIdList())
      ->orderBy('p.submissionTime', 'DESC')
      ->setMaxResults($limit);

    return $qb->getQuery()->getResult();
  }

  public function findPraticheByUser(CPSUser $user, $filters, $limit, $offset)
  {
    $qb = $this->getPraticheByUserQueryBuilder($filters, $user);
    if (isset($filters['sort'], $filters['order'])) {
      $qb->orderBy('pratica.' . $filters['sort'], strtolower($filters['order']));
    } else {
      $qb->orderBy('pratica.submissionTime', 'desc');
    }
    $qb->addOrderBy('pratica.id', 'desc');

    return $qb->setFirstResult($offset)
      ->setMaxResults($limit)
      ->getQuery()->execute();
  }

  public function countPraticheByUser(CPSUser $user, $filters)
  {
    return $this->getPraticheByUserQueryBuilder($filters, $user)->select('count(pratica.id)')
      ->getQuery()->getSingleScalarResult();
  }

  private function getPraticheByUserQueryBuilder($filters, CPSUser $user)
  {
    if (!empty($filters['data'])) {
      $entity = FormIO::class;
    } else {
      $entity = Pratica::class;
    }

    $qb = $this->getEntityManager()->createQueryBuilder()
      ->select('pratica')
      ->from($entity, 'pratica');

    if (!empty($filters['status'])) {
      $qb->andWhere('pratica.status IN (:status)')
        ->setParameter('status', (array)$filters['status']);
    }

    $qb->andWhere('pratica.user = :user')
      ->setParameter('user', $user);

    if (!empty($filters['service'])) {
      $qb->andWhere('servizio.slug in (:service)')
        ->leftJoin('pratica.servizio', 'servizio')
        ->setParameter('service', (array)$filters['service']);
    }

    if (!empty($filters['data'])) {
      foreach ($filters['data'] as $field => $value) {
        $fieldValueKey = str_replace('.', '', $field);
        $qb->andWhere("LOWER(FORMIO_JSON_FIELD(pratica.dematerializedForms $field)) = :{$fieldValueKey}")
          ->setParameter($fieldValueKey, strtolower($value));
      }
    }

    return $qb;
  }

  /**
   * @param $filters
   * @param Pratica $pratica
   * @return float|int|mixed|string
   */
  public function getMessages($filters, Pratica $pratica, $count = false)
  {
    $qb = $this->getEntityManager()->createQueryBuilder()
      ->from('App:Message', 'message')
      ->where('message.application = :application')
      ->setParameter('application', $pratica);

    if (!empty($filters['visibility'])) {
      $qb->andWhere('message.visibility = :visibility')
        ->setParameter('visibility', (array)$filters['visibility']);
    }

    // Todo: questo modo di passare le date è deprecato, usare quello successivo
    if (isset($filters['from_date'])) {
      $qb->andWhere('message.createdAt >= :from_date')->setParameter('from_date', $filters['from_date']->getTimestamp());
    }
    if (isset($filters['to_date'])) {
      $qb->andWhere('message.createdAt <= :to_date')->setParameter('to_date', $filters['to_date']->getTimestamp());
    }

    // after|before|strictly_after|strictly_before
    if (isset($filters['created_at'])) {
      if (isset($filters['created_at']['strictly_after'])) {
        $qb->andWhere('message.createdAt > :createdAt')->setParameter('createdAt', $filters['created_at']['strictly_after']->getTimestamp());
      } else if (isset($filters['created_at']['after'])) {
        $qb->andWhere('message.createdAt >= :createdAt')->setParameter('createdAt', $filters['created_at']['after']->getTimestamp());
      }

      if (isset($filters['created_at']['strictly_before'])) {
        $qb->andWhere('message.createdAt < :createdAt')->setParameter('createdAt', $filters['created_at']['strictly_before']->getTimestamp());
      } else if (isset($filters['created_at']['before'])) {
        $qb->andWhere('message.createdAt <= :createdAt')->setParameter('createdAt', $filters['created_at']['before']->getTimestamp());
      }
    }

    if ($count) {
      $qb->select('count(message.id)');
      try {
        return $qb->getQuery()->getSingleScalarResult();
      } catch (NoResultException|NonUniqueResultException $e) {
        return 0;
      }
    }

    $qb->select('message');

    if (!empty($filters['offset'])) {
      $qb->setFirstResult($filters['offset']);
    }

    if (!empty($filters['limit'])) {
      $qb->setMaxResults($filters['limit']);
    }

    $qb->orderBy('message.createdAt', 'ASC');

    return $qb->getQuery()->getResult();
  }

  /**
   * @param Pratica $pratica
   * @return void
   */
  public function getLastMessageByApplicationOwner(Pratica $pratica)
  {
    $qb = $this->getEntityManager()->createQueryBuilder()
      ->select('message')
      ->from('App:Message', 'message')
      ->where('message.application = :application')
      ->setParameter('application', $pratica)
      ->andWhere('message.author = :user')
      ->setParameter('user', $pratica->getUser())
      ->orderBy('message.createdAt', 'DESC')
      ->setMaxResults(1);

    try {
      return $qb->getQuery()->getSingleResult();
    } catch (NoResultException $e) {
    } catch (NonUniqueResultException $e) {
      return null;
    }
  }


  public function getMessageAttachments($filters, Pratica $pratica)
  {
    $qb = $this->getEntityManager()->createQueryBuilder()
      ->select('attachment')
      ->from('App:AllegatoMessaggio', 'attachment')
      ->join('attachment.messages', 'message');

    if (!empty($filters['visibility'])) {
      $qb->andWhere('message.visibility = :visibility')
        ->setParameter('visibility', (array)$filters['visibility']);
    }

    if (!empty($filters['author'])) {
      $qb->andWhere('message.author = :author')
        ->setParameter('author', (array)$filters['author']);
    }

    $qb->andWhere('message.application = :application')
      ->setParameter('application', $pratica)
      ->orderBy('message.createdAt', 'asc');


    return $qb->getQuery()->getResult();
  }

  public function findOrderedMeetings(Pratica $pratica)
  {
    $qb = $this->getEntityManager()->createQueryBuilder()
      ->select('meeting')
      ->from('App:Meeting', 'meeting')
      ->where('meeting.id IN (:meetings)')
      ->setParameter(':meetings', $pratica->getMeetings())
      ->orderBy('meeting.fromTime', 'desc');

    return $qb->getQuery()->getResult();
  }

  public function findIncomingMeetings(Pratica $pratica)
  {
    $qb = $this->getEntityManager()->createQueryBuilder()
      ->select('meeting')
      ->from('App:Meeting', 'meeting')
      ->where('meeting.id IN (:meetings)')
      ->andWhere('meeting.fromTime >= :now')
      ->andWhere('meeting.status IN (:statuses)')
      ->setParameter(':meetings', $pratica->getMeetings())
      ->setParameter(':statuses', [Meeting::STATUS_APPROVED, Meeting::STATUS_PENDING])
      ->setParameter(':now', new \DateTime())
      ->orderBy('meeting.fromTime', 'asc');

    return $qb->getQuery()->getResult();
  }

  public function findStatusMessagesByStatus(Pratica $pratica, $status): array
  {
    $messages = [];
    $messageRepo = $this->getEntityManager()->getRepository('App\Entity\Message');

    foreach ($pratica->getHistory() as $transition) {
      /** @var Transition $status */
      if ($transition->getStatusCode() === $status && $transition->getMessageId()) {
        $message = $messageRepo->find($transition->getMessageId());
        $messages[] = $message;
      }
    }

    return $messages;
  }

  /**
   * @param QueryBuilder $qb
   * @param OperatoreUser $user
   * @return \Doctrine\ORM\Query\Expr\Orx
   */
  private function getEnabledServicesQuery(QueryBuilder $qb, OperatoreUser $user): \Doctrine\ORM\Query\Expr\Orx
  {
    $orStatements = $qb->expr()->orX();
    $orStatements->add(
      $qb->expr()->eq('pratica.operatore', $qb->expr()->literal($user->getId()))
    );

    /*
      * Filtro tutte le pratiche il cui servizio:
      * - è gestito direttamente dall'operatore
      * - è gestito dall'ufficio a cui appartiene l'operatore
      * - è gestito dall'ufficio di cui è responsabile l'operatore
      */
    $enabledServices = $user->getAllEnabledServicesIds();
    if (!empty($enabledServices)) {
      $orStatements->add(
        $qb->expr()->in('pratica.servizio', $enabledServices)
      );
    }

    if (!$user->getUserGroups()->isEmpty()) {
      $orStatements->add(
        $qb->expr()->in('pratica.userGroup', $user->getUserGroups())
      );
    }

    if (!$user->getUserGroupsManager()->isEmpty()) {
      $orStatements->add(
        $qb->expr()->in('pratica.userGroup', $user->getUserGroupsManager())
      );
    }
    return $orStatements;
  }

  public function findApplicationLiteByExternalId(string $externalId): ?ApplicationLite
  {
    $qb = $this->getEntityManager()->createQueryBuilder();
    $qb
      ->select('a')
      ->from(ApplicationLite::class, 'a')
      ->where('a.externalId = :externalId');

    $qb
      ->setParameter('externalId', $externalId);

    $applicationLite = $qb->getQuery()->getResult();

    if (count($applicationLite) === 0) {
      return null;
    } else {
      return $applicationLite[0];
    }
  }

  public function groupByCalendarMeetings($meetings): array
  {
    $calendarsGroups = [];
    foreach ($meetings as $element) {
      if (is_object($element)) {
        $key = $element->getCalendar()->getTitle();
        $calendarsGroups[$key][] = $element;
      }
    }
    return $calendarsGroups;
  }


}
