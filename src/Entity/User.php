<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class User
 *
 * @ORM\Entity
 * @ORM\Table(name="utente")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"operatore" = "OperatoreUser", "cps" = "CPSUser", "admin" = "AdminUser"})
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="email", column=@ORM\Column(type="string", name="email", length=255, unique=false, nullable=true)),
 *      @ORM\AttributeOverride(name="emailCanonical", column=@ORM\Column(type="string", name="email_canonical", length=255, unique=false, nullable=true))
 * })
 * @package App\Entity
 */
abstract class User implements UserInterface, PasswordAuthenticatedUserInterface
{

  use TimestampableEntity;

  public const ROLE_MANAGER = 'ROLE_MANAGER';

  public const ROLE_OPERATORE = 'ROLE_OPERATORE';
  public const ROLE_USER = 'ROLE_USER';
  public const ROLE_ADMIN = 'ROLE_ADMIN';
  public const ROLE_CPS_USER = 'ROLE_CPS_USER';

  public const USER_TYPE_OPERATORE = 'operatore';
  public const USER_TYPE_CPS = 'cps';
  public const USER_TYPE_ADMIN = 'admin';

  public const FAKE_EMAIL_DOMAIN = 'cps.didnt.have.my.email.tld';
  public const ANONYMOUS_FAKE_EMAIL_DOMAIN = 'anonymous.fake.email.tld';


  /**
   * @ORM\Column(type="guid")
   * @ORM\Id
   */
  protected $id;

  /**
   * @var string
   * @ORM\Column(type="string", length=180, unique=true)
   */
  protected $username;

  /**
   * @var string
   * @ORM\Column(type="string", length=180, unique=true)
   */
  protected $usernameCanonical;

  /**
   * @var string
   * @ORM\Column(type="string", name="email", length=255, unique=false, nullable=true)
   */
  protected $email;

  /**
   * @var string
   * @ORM\Column(type="string", name="email_canonical", length=255, unique=false, nullable=true)
   */
  protected $emailCanonical;

  /** @ORM\Column(type="string", name="pec", length=255, unique=false, nullable=true) */
  protected ?string $pec = null;

  /**
   * @ORM\Column(name="enabled", type="boolean")
   */
  protected $enabled;

  /**
   * The salt to use for hashing.
   *
   * @var string
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  protected $salt;

  /**
   * Encrypted password. Must be persisted.
   *
   * @var string
   * @ORM\Column(type="string", length=255)
   */
  protected $password;

  /**
   * Plain password. Used for model validation. Must not be persisted.
   *
   * @var string
   */
  protected $plainPassword;

  /**
   * @var DateTime
   * @ORM\Column(name="last_login", type="datetime", nullable=true)
   */
  protected $lastLogin;

  /**
   * Random string sent to the user email address in order to verify it.
   *
   * @var string
   * @ORM\Column(type="string", length=180, unique=true, nullable=true)
   */
  protected $confirmationToken;

  /**
   * @var DateTime
   * @ORM\Column(name="password_requested_at", type="datetime", nullable=true)
   */
  protected $passwordRequestedAt;

  /**
   * @var DateTime
   *
   * @ORM\Column(name="last_change_password", type="datetime", nullable=true)
   */
  protected $lastChangePassword;

  /**
   * @ORM\Column(type="array")
   */
  protected $roles;

  /**
   * @var string
   *
   * @ORM\Column(name="nome", type="string", nullable=true)
   */
  private $nome;

  /**
   * @var string
   *
   * @ORM\Column(name="cognome", type="string", nullable=true)
   */
  private $cognome;

  /** @var string */
  protected $type;

  /** @var string */
  private $fullName;

  /**
   * @var string
   *
   * @ORM\Column(type="string", nullable=true)
   */
  protected $emailContatto;

  /**
   * @var string
   *
   * @ORM\Column(type="string", nullable=true)
   */
  protected $cellulareContatto;

  /**
   * @ORM\OneToMany(targetEntity=NotificationSetting::class, mappedBy="owner", cascade={"persist", "remove"}, orphanRemoval=true)
   */
  private Collection $notificationSettings;

  /**
   * User constructor.
   *
   */
  public function __construct()
  {
    $this->id = Uuid::uuid4();
    $this->enabled = false;
    $this->roles = array();
    $this->notificationSettings = new ArrayCollection();
  }

  /**
   * @param $role
   * @return $this
   */
  public function addRole($role): User
  {
    $role = strtoupper($role);
    if ($role === self::ROLE_USER) {
      return $this;
    }

    if (!in_array($role, $this->roles, true)) {
      $this->roles[] = $role;
    }

    return $this;
  }

  /**
   * @return array
   */
  public function getRoles(): array
  {
    $roles = $this->roles;
    // guarantee every user at least has ROLE_USER
    $roles[] = self::ROLE_USER;

    return array_unique($roles);
  }

  /**
   * @param $role
   * @return bool
   */
  public function hasRole($role): bool
  {

    return in_array(strtoupper($role), $this->getRoles(), true);
  }

  /**
   * @param $role
   * @return $this
   */
  public function removeRole($role): User
  {
    if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
      unset($this->roles[$key]);
      $this->roles = array_values($this->roles);
    }

    return $this;
  }

  /**
   * @param array $roles
   * @return $this
   */
  public function setRoles(array $roles): User
  {
    $this->roles = array();

    foreach ($roles as $role) {
      $this->addRole($role);
    }

    return $this;
  }

  /**
   * @return UuidInterface
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getUsernameCanonical(): string
  {
    return $this->usernameCanonical;
  }

  /**
   * @param string $usernameCanonical
   */
  public function setUsernameCanonical(string $usernameCanonical): void
  {
    $this->usernameCanonical = $usernameCanonical;
  }

  /**
   * @return string
   */
  public function getEmailCanonical(): ?string
  {
    return $this->emailCanonical;
  }

  /**
   * @param string $emailCanonical
   */
  public function setEmailCanonical(string $emailCanonical): void
  {
    $this->emailCanonical = $emailCanonical;
  }

  /**
   * @return bool
   */
  public function isEnabled(): bool
  {
    return $this->enabled;
  }


  public function setEnabled(bool $enabled): self
  {
    $this->enabled = $enabled;
    return $this;
  }

  /**
   * @return string
   */
  public function getPlainPassword(): string
  {
    return $this->plainPassword;
  }

  public function setPlainPassword(string $plainPassword): self
  {
    $this->plainPassword = $plainPassword;
    return $this;
  }

  public function getLastLogin(): ?DateTime
  {
    return $this->lastLogin;
  }

  public function setLastLogin(DateTime $lastLogin): self
  {
    $this->lastLogin = $lastLogin;
    return $this;
  }

  /**
   * @return string
   */
  public function getConfirmationToken(): ?string
  {
    return $this->confirmationToken;
  }

  public function setConfirmationToken(?string $confirmationToken): self
  {
    $this->confirmationToken = $confirmationToken;
    return $this;
  }

  public function getPasswordRequestedAt(): ?DateTime
  {
    return $this->passwordRequestedAt;
  }

  public function setPasswordRequestedAt(DateTime $passwordRequestedAt): self
  {
    $this->passwordRequestedAt = $passwordRequestedAt;
    return $this;
  }

  /**
   * @return DateTime
   */
  public function getLastChangePassword()
  {
    return $this->lastChangePassword;
  }

  /**
   * @param DateTime|null $lastChangePassword
   */
  public function setLastChangePassword(DateTime $lastChangePassword = null): self
  {
    if ($lastChangePassword == null) {
      $lastChangePassword = new DateTime();
    }
    $this->lastChangePassword = $lastChangePassword;
    return $this;
  }


  public function setId(UuidInterface $id): self
  {
    $this->id = $id;
    return $this;
  }

  public function hasPassword(): bool
  {
    return $this->password !== null;
  }

  public function getNome()
  {
    return $this->nome;
  }

  public function setNome($nome): self
  {
    $this->nome = $nome;
    return $this;
  }

  public function getCognome()
  {
    return $this->cognome;
  }

  public function setCognome($cognome): self
  {
    $this->cognome = $cognome;
    return $this;
  }

  public function getFullName(): string
  {
    $fullName = $this->nome ?? '';
    $fullName .= $this->cognome ? ' ' . $this->cognome : '';
    return trim($fullName);
  }

  public function getType()
  {
    return $this->type;
  }

  public function getEmailContatto()
  {
    return $this->emailContatto;
  }

  public function setEmailContatto($emailContatto): self
  {
    $this->emailContatto = $emailContatto;
    return $this;
  }

  public function getCellulareContatto()
  {
    return $this->cellulareContatto;
  }

  public function setCellulareContatto($cellulareContatto): self
  {
    $this->cellulareContatto = $cellulareContatto;
    return $this;
  }

  public function getPassword(): ?string
  {
    return (string)$this->password;
  }

  public function setPassword(string $password): self
  {
    $this->password = $password;
    return $this;
  }

  public function getUsername()
  {
    return $this->username;
  }

  public function setUsername(string $username): self
  {
    $this->username = $username;
    $this->usernameCanonical = $username;
    return $this;
  }

  public function getEmail(): ?string
  {
    return $this->email;
  }


  public function setEmail(?string $email): self
  {
    $this->email = $email;

    return $this;
  }

  public function getPec(): ?string
  {
    return $this->pec;
  }

  public function setPec(?string $pec): self
  {
    $this->pec = $pec;

    return $this;
  }

  public function eraseCredentials()
  {
    $this->plainPassword = null;
  }

  /**
   * @see UserInterface
   */
  public function getSalt()
  {
    // not needed when using the "bcrypt" algorithm in security.yaml
  }

  /**
   * @return Collection<int, NotificationSetting>
   */
  public function getNotificationSettings(): Collection
  {
    return $this->notificationSettings;
  }

  public function addNotificationSetting(NotificationSetting $notificationSetting): self
  {
    if (!$this->notificationSettings->contains($notificationSetting)) {
      $this->notificationSettings[] = $notificationSetting;
      $notificationSetting->setOwner($this);
    }

    return $this;
  }

  public function removeNotificationSetting(NotificationSetting $notificationSetting): self
  {
    if ($this->notificationSettings->removeElement($notificationSetting)) {
      // set the owning side to null (unless already changed)
      if ($notificationSetting->getOwner() === $this) {
        $notificationSetting->setOwner(null);
      }
    }

    return $this;
  }

}
