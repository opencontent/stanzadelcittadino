<?php

namespace App\Entity;

use App\AdministrativeIdentifier\Infrastructure\Repository\DoctrineAdministrativeIdentifierRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Table(name="administrative_identifier",
 *      uniqueConstraints={
 *           @ORM\UniqueConstraint(name="administrative_identifier_unique", columns={"identifies", "type", "identifier"})
 *      }
 *  )
 * @ORM\Entity(repositoryClass=DoctrineAdministrativeIdentifierRepository::class)
 */
class AdministrativeIdentifier
{
  use TimestampableEntity;

  /**
   * @ORM\Id
   * @ORM\Column(type="guid")
   * @OA\Property(description="Administrative identifier Uuid")
   * @Groups({"read"})
   */
  private $id = null;

  /**
   * @ORM\ManyToOne(targetEntity=CPSUser::class, inversedBy="administrativeIdentifiers")
   * @ORM\JoinColumn(name="identifies", referencedColumnName="id", nullable=false)
   * @Serializer\Exclude()
   */
  private CPSUser $identifies;

  /**
   * @ORM\Column(type="string", length=255)
   * @Groups({"read", "write"})
   */
  private string $type;

  /**
   * @ORM\Column(type="string", length=255)
   * @Groups({"read", "write"})
   */
  private string $identifier;

  /**
   * @var DateTime
   * @Gedmo\Timestampable(on="create")
   * @ORM\Column(type="datetime")
   * @Groups({"read"})
   */
  protected $createdAt;

  /**
   * @var DateTime
   * @Gedmo\Timestampable(on="update")
   * @ORM\Column(type="datetime")
   * @Groups({"read"})
   */
  protected $updatedAt;

  public function __construct(CPSUser $identifies, string $type, string $identifier)
  {
    if (empty($type)) {
      throw new \InvalidArgumentException('Type cannot be empty or null');
    }

    if (empty($identifier)) {
      throw new \InvalidArgumentException('Identifier cannot be empty or null');
    }


    $this->id = Uuid::uuid4();
    $this->identifies = $identifies;
    $this->type = $type;
    $this->identifier = $identifier;
  }

  /**
   * @Serializer\VirtualProperty()
   * @Serializer\Type("string")
   * @Serializer\SerializedName("identifies")
   * @OA\Property(description="User to whom the administrative identifier belongs (uuid)")
   * @Groups({"read"})
   */
  public function getIdentifiesId(): string
  {
    return $this->getIdentifies()->getId();
  }

  public function getId()
  {
    return $this->id;
  }

  public function getIdentifies(): CPSUser
  {
    return $this->identifies;
  }

  public function getType(): ?string
  {
    return $this->type;
  }

  public function getIdentifier(): ?string
  {
    return $this->identifier;
  }

  public function update(string $type, string $identifier): void
  {
    $this->type = $type;
    $this->identifier = $identifier;
  }

  public function identifiesUser(CPSUser $user): bool
  {
    return $this->identifies === $user;
  }

}
