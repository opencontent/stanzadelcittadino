<?php

namespace App\Entity;

use App\Repository\ProfileBlockRepository;
use DateTime;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="profile_block", uniqueConstraints={
 *        @UniqueConstraint(name="unique_profile_block",
 *            columns={"user_id", "key", "unique_id"})
 *    }))
 * @ORM\Entity(repositoryClass=ProfileBlockRepository::class)
 */
class ProfileBlock
{

  use TimestampableEntity;

  /**
   * @ORM\Id
   * @ORM\Column(type="guid")
   * @OA\Property(description="Uuid of the profile block", type="string")
   * @Groups({"read"})
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity=CPSUser::class, inversedBy="profileBlocks")
   * @ORM\JoinColumn(nullable=false)
   * @Serializer\Exclude()
   */
  private ?CPSUser $user;

  /**
   * @ORM\Column(type="string", length=255)
   * @OA\Property(description="Key of the profile block", type="string")
   * @Groups({"read"})
   */
  private ?string $key;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   * @OA\Property(description="Unique identifier of the profile block", type="string")
   * @Groups({"read"})
   */
  private ?string $uniqueId;

  /**
   * @ORM\Column(type="json", options={"jsonb":true}, nullable=true)
   * @Serializer\Type("array")
   * @OA\Property(description="Value of the profile block")
   * @Groups({"read"})
   */
  private ?array $value;

  /**
   * @ORM\Column(type="string", length=255)
   * @OA\Property(description="Url of the related nested form", type="string")
   * @Groups({"read"})
   */
  private ?string $subformUrl;

  /**
   * @ORM\Column(type="json", options={"jsonb":true})
   * @Serializer\Type("array")
   * @OA\Property(description="Information on the objects that originated the profile block")
   * @Groups({"read"})
   */
  private array $origin = [];

  /**
   * @ORM\Column(type="string", length=255)
   * @OA\Property(description="Profile block hash", type="string")
   * @Groups({"read"})
   */
  private ?string $hash;

  /**
   * @var DateTime
   * @Gedmo\Timestampable(on="create")
   * @ORM\Column(type="datetime")
   * @Groups({"read"})
   */
  protected $createdAt;

  /**
   * @var DateTime
   * @Gedmo\Timestampable(on="update")
   * @ORM\Column(type="datetime")
   * @Groups({"read"})
   */
  protected $updatedAt;


  public function __construct()
  {
    if (!$this->id) {
      $this->id = Uuid::uuid4();
    }
  }

  public function getId()
  {
    return $this->id;
  }

  public function getUser(): ?CPSUser
  {
    return $this->user;
  }

  public function setUser(?CPSUser $user): self
  {
    $this->user = $user;

    return $this;
  }

  public function getKey(): ?string
  {
    return $this->key;
  }

  public function setKey(string $key): self
  {
    $this->key = $key;

    return $this;
  }

  public function getUniqueId(): ?string
  {
    return $this->uniqueId;
  }

  public function setUniqueId(?string $uniqueId): self
  {
    $this->uniqueId = $uniqueId;

    return $this;
  }

  public function getValue(): ?array
  {
    return $this->value;
  }

  public function setValue(array $value): self
  {
    $this->value = $value;

    return $this;
  }

  public function getSubformUrl(): ?string
  {
    return $this->subformUrl;
  }

  public function setSubformUrl(string $subformUrl): self
  {
    $this->subformUrl = $subformUrl;

    return $this;
  }

  public function getOrigin(): ?array
  {
      return $this->origin;
  }

  public function setOrigin(array $origin): self
  {
      $this->origin = $origin;

      return $this;
  }

  public function getHash(): ?string
  {
      return $this->hash;
  }

  public function setHash(string $hash): self
  {
      $this->hash = $hash;

      return $this;
  }
}
