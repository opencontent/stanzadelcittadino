<?php

namespace App\Entity;

use App\Form\Admin\Servizio\PaymentDataType;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class FormIO
 * @ORM\Entity
 */
class FormIO extends Pratica implements DematerializedFormPratica
{
  public const FIELD_SUBJECT = 'application_subject';


  public function __construct()
  {
    parent::__construct();
    $this->type = self::TYPE_FORMIO;
    $this->dematerializedForms = [];
  }

  public function getPaymentAmount()
  {
    $formAmount = $this->getFormPaymentAmount();
    if ($formAmount !== null) {
      return $formAmount;
    }

    $serviceAmount = $this->getServicePaymentAmount();
    if ($serviceAmount !== null) {
      return $serviceAmount;
    }

    return 0;
  }

  public function getFormPaymentAmount(): ?float
  {
    $formAmount = $this->dematerializedForms['flattened'][PaymentDataType::PAYMENT_AMOUNT] ?? null;
    return $formAmount ? (float)str_replace(',', '.', $formAmount) : null;
  }

  private function getServicePaymentAmount(): ?float
  {
    $serviceAmount = $this->getServizio()->getPaymentParameters()['total_amounts'] ?? null;
    return $serviceAmount ? (float)str_replace(',', '.', $serviceAmount) : null;
  }

  public function setPaymentAmount($amount): void
  {
    $dematerializedForms = $this->dematerializedForms;
    $dematerializedForms['data'][PaymentDataType::PAYMENT_AMOUNT] = $amount;
    $dematerializedForms['flattened'][PaymentDataType::PAYMENT_AMOUNT] = $amount;
    $this->dematerializedForms = $dematerializedForms;
  }

  public function isPaymentExempt(): bool
  {
    $data = $this->dematerializedForms;
    return isset($data['flattened'][PaymentDataType::PAYMENT_AMOUNT]) && $data['flattened'][PaymentDataType::PAYMENT_AMOUNT] <= 0;
  }

  public function hasImmediatePayment(): bool
  {
    return $this->getPaymentAmount() || $this->getServizio()->isPaymentRequired();
  }

  public function hasPaymentDeferred(): bool
  {
    $paymentData = $this->getPaymentData();
    return ($this->getPaymentAmount() > 0 && !empty($paymentData['deferred'])) || (!empty($paymentData['deferred']) && !empty($paymentData['amount']));
  }

  public function getType(): string
  {
    return Pratica::TYPE_FORMIO;
  }

  public function generateSubject(): string
  {
    $data = $this->dematerializedForms;
    if (isset($data['flattened'][self::FIELD_SUBJECT]) && !empty($data['flattened'][self::FIELD_SUBJECT])) {
      return $data['flattened'][self::FIELD_SUBJECT];
    }
    return parent::generateSubject();
  }

}
