<?php

namespace App\Traits;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

trait AnonymizableEntity
{


  /**
   * @var DateTime|null
   * @ORM\Column(type="datetime", nullable=true)
   */
  protected ?DateTime $anonymizedAt;

  public function getAnonymizedAt(): ?DateTime
  {
    return $this->anonymizedAt;
  }

  public function setAnonymizedAt(?DateTime $anonymizedAt): void
  {
    $this->anonymizedAt = $anonymizedAt;
  }


}
