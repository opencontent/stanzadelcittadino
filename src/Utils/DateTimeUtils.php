<?php

namespace App\Utils;

use DateTime;
use InvalidArgumentException;

class DateTimeUtils
{

  public const PERIOD_NONE = 'none';
  public const PERIOD_DAILY = 'daily';
  public const PERIOD_WEEKLY = 'weekly';
  public const PERIOD_MONTHLY = 'monthly';
  public const PERIOD_YEARLY = 'yearly';

  public static function getBoundsFromDate(\DateTime $date, $boundary): array
  {
    // Clone the given date to avoid modifying the original DateTime object
    $startDate = clone $date;
    $endDate = clone $date;

    switch ($boundary) {
      case self::PERIOD_DAILY:
        // Set the time to the start and end of the day
        $startDate->setTime(0, 0, 0);
        $endDate->setTime(23, 59, 59);
        break;

      case self::PERIOD_WEEKLY:
        // Modify to get the first day of the week (Monday) and last day (Sunday)
        $startDate->modify('this week')->setTime(0, 0, 0);
        $endDate->modify('this week +6 days')->setTime(23, 59, 59);
        break;

      case self::PERIOD_MONTHLY:
        // Modify to get the first and last day of the month
        $startDate->modify('first day of this month')->setTime(0, 0, 0);
        $endDate->modify('last day of this month')->setTime(23, 59, 59);
        break;

      case self::PERIOD_YEARLY:
        // Modify to get the first and last day of the month
        $startDate->modify('first day of this year')->setTime(0, 0, 0);
        $endDate->modify('last day of this year')->setTime(23, 59, 59);
        break;

      default:
        throw new InvalidArgumentException('Invalid boundary type. Use "daily", "week", or "month".');
    }

    // Return both dates as an associative array
    return [
      'startDate' => $startDate,
      'endDate' => $endDate
    ];
  }

  public static function parseDateTime(string $dateTime): ?DateTime
  {
    $formats = ['Y-m-d H:i:s', DATE_W3C, 'Y-m-d\TH:i:s.v\Z'];

    foreach ($formats as $format) {
      $date = DateTime::createFromFormat($format, $dateTime);
      if ($date && $date->format($format) === $dateTime) {
        return $date;
      }
    }

    // If none of the formats match, return false or handle the error as needed
    return null;
  }

  public static function parseDate(string $dateString, $format = 'd/m/Y'): ?DateTime
  {
    return $dateString ? DateTime::createFromFormat($format, $dateString) : null;
  }

}
