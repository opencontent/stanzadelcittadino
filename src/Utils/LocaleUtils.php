<?php

namespace App\Utils;

use IntlDateFormatter;

class LocaleUtils
{
  public const DEFAULT_LOCALE = 'it';

  public static function getFormatLocale($locale, $formatDate, $formatTime, $datetime)
  {
    return (new IntlDateFormatter($locale, $formatDate, $formatTime))->format($datetime);
  }
}
