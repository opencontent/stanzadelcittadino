<?php

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

class NotificationEvent extends Event
{

  public const NAME = 'ocsdc.notification';

  private $item;

  public function __construct($item)
  {
    $this->item = $item;
  }

  public function getItem()
  {
    return $this->item;
  }

}
