<?php

namespace App\Command;

use App\Entity\Meeting;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteDraftMeetingsCommand extends Command
{
  protected static $defaultName = 'ocsdc:delete-draft-meetings';

  private EntityManagerInterface $entityManager;
  private LoggerInterface $logger;

  public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
  {
    parent::__construct();
    $this->entityManager = $entityManager;
    $this->logger = $logger;
  }

  protected function configure(): void
  {
    $this
      ->setDescription('Delete expired draft meetings.')
      ->setHelp('This command removes meetings in draft status that have expired.');
  }

  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $this->logger->info('Starting the deletion of expired draft meetings.');

    try {
      // Perform the deletion with one query
      $queryBuilder = $this->entityManager->createQueryBuilder();
      $query = $queryBuilder
        ->delete(Meeting::class, 'meeting')
        ->where('meeting.status = :status')
        ->andWhere('meeting.draftExpiration <= :date')
        ->setParameter('status', Meeting::STATUS_DRAFT)
        ->setParameter('date', new \DateTime())
        ->getQuery();

      $deletedCount = $query->execute(); // Returns the number of rows deleted

      $this->logger->info("Successfully deleted {$deletedCount} expired draft meetings.");
    } catch (\Exception $exception) {
      $this->logger->error(
        'An error occurred during the deletion of expired draft meetings: ' . $exception->getMessage(),
        ['exception' => $exception]
      );
      return Command::FAILURE;
    }

    $this->logger->info('Finished the deletion of expired draft meetings.');
    return Command::SUCCESS;
  }
}
