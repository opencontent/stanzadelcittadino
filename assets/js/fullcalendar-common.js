import axios from "axios";
import moment from "moment/moment";
import printJS from "print-js";

export function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)===' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}

export function enableDisableAllInput(isDisabled) {

  $("#modalDate").prop('disabled', isDisabled);
  $("#modalSlot").prop('disabled', isDisabled);
  $("#modalReason").prop('disabled', isDisabled);
  $("#modalDescription").prop('disabled', isDisabled);
  $("#modalLocation").prop('disabled', isDisabled);
  $("#modalVideoconferenceLink").prop('disabled', isDisabled);
  $("#modalEmail").prop('disabled', isDisabled);
  $("#modalPhone").prop('disabled', isDisabled);
  $("#modalMotivationOutcome").prop('disabled', isDisabled);
  $("#modalLocale").prop('disabled', isDisabled);
  $("#modalSave").prop('disabled', isDisabled);
}

/**
 * Workaround perché c'è un bug su come è stata configurata la gestione delle date nel calendario
 * @param meetingEnd Date
 */
export function isMeetingOutdated(meetingEnd) {

  const shortMeetingEnd = meetingEnd.toISOString().slice(0, 19)
  const now = formatDate(new Date())

  return shortMeetingEnd < now
}

function formatDate(date) {

  const year = date.getFullYear();
  const month = ('' + (date.getMonth() + 1)).padStart(2, '0');
  const day = ('' + date.getDate()).padStart(2, '0');
  const hours = ('' + date.getHours()).padStart(2, '0');
  const minutes = ('' + date.getMinutes()).padStart(2, '0');
  const seconds = ('' + date.getSeconds()).padStart(2, '0');

  return `${year}-${month}-${day}T${hours}:${minutes}:${seconds}`
}

/**
 * Get status as string
 * @param status
 */
export function getStatus(status) {
  const language = document.documentElement.lang.toString();

  switch (status) {
    case 0:
      return Translator.trans('meetings.status.pending', {}, 'messages', language);
    case 1:
      return Translator.trans('meetings.status.approved', {}, 'messages', language);
    case 2:
      return Translator.trans('meetings.status.refused', {}, 'messages', language);
    case 3:
      return Translator.trans('meetings.status.missed', {}, 'messages', language);
    case 4:
      return Translator.trans('meetings.status.concluded', {}, 'messages', language);
    case 5:
      return Translator.trans('meetings.status.cancelled', {}, 'messages', language);
    case 6:
      return Translator.trans('meetings.status.draft', {}, 'messages', language);
    default:
      return Translator.trans('status_error', {}, 'messages', language);
  }
}

/**
 * Delete draft modal
 * @param info
 */
export function deleteDraftModal(info) {
  $('#modalDraftId').html(info.event.id);
  let draftExpireTime = new Date(info.event.extendedProps.draftExpireTime)

  let date = draftExpireTime.toLocaleDateString();
  let time = draftExpireTime.getHours()+":"+draftExpireTime.getMinutes();

  let description = $('#modalDraftDescription').html()
  description = description.replace("%expire_time%", time).replace("%expire_date%", date)
  $('#modalDraftDescription').html(description)

  $('#modalDeleteDraft').modal('show');
}


export function setCookie(key, value) {
  document.cookie = key+"="+value;
}

export function printCalendar() {
  printJS({
    printable: "container-fullcalendar",
    type: "html",
    scanStyles: false,
    css: [
      "https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.css",
      "https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.css",
      "https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.min.css",
      "https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/list/main.min.css",
      "https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/bootstrap/main.min.css",
    ],
  });
}

export function exportCalendarEventsToICS(calendar) {
  const view = calendar.view;
  const start = view.currentStart;
  const end = view.currentEnd;
  const url = $('#hidden').data('url-export');

  axios.get(url, {
    params: {
      start: start.toISOString(),
      end: end.toISOString(),
      exclude_availabilities: true
    }
  })
    .then(function (response) {

      // Create ICS file content
      let icsContent = 'BEGIN:VCALENDAR\n';
      icsContent += 'VERSION:2.0\n';
      icsContent += 'CALSCALE:GREGORIAN\n';

      response.data.forEach(event => {

        const eventStart = moment(event.start).utc();
        const eventEnd = moment(event.end).utc();
        icsContent += 'BEGIN:VEVENT\n';
        icsContent += `SUMMARY:${event.title}\n`;
        icsContent += `DTSTART:${eventStart.format('YYYYMMDDTHHmmss[Z]')}\n`;
        icsContent += `DTEND:${eventEnd.format('YYYYMMDDTHHmmss[Z]')}\n`;
        icsContent += `DESCRIPTION:${event.description || ''}\n`;
        icsContent += `LOCATION:${event.location || ''}\n`;
        icsContent += 'END:VEVENT\n';
      });
      icsContent += 'END:VCALENDAR\n';

      // Create a blob and download the file
      const blob = new Blob([icsContent], { type: 'text/calendar' });
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'calendar.ics';
      link.click();
    })
    .catch(function (error) {
      console.log(error)
    })
}

export function disableBtnOnClick(btn) {
  let spinner = document.createElement('i');
  spinner.id = 'btn-spinner';
  spinner.classList.add("fa", "fa-circle-o-notch", "fa-spin", "fa-fw");
  $(btn).prepend(spinner)
  $(btn).addClass("disabled");
}

export function enableBtn(btn) {
  let spinner = document.getElementById('btn-spinner');
  spinner.remove();
  $(btn).removeClass("disabled");
}
