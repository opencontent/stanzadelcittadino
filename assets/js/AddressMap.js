import Base from "formiojs/components/_classes/component/Component";
import editForm from "./AddressMap/AddressMap.form";
import _ from "lodash";
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';


export default class FormioAddressMap extends Base {
  constructor(component, options, data) {
    super(component, options, data);
    this.date = false;
    this.slot = false;
    this.container = false;
    this.$language = document.documentElement.lang.toString();
    this.countdown = false;
    this.position = null;
    this.mapProvider = window.OC_MAP_SEARCH_PROVIDER ? window.OC_MAP_SEARCH_PROVIDER : "nominatim.openstreetmap.org";
    this.loaderTpl = `<div id="loader" class="text-center"><i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw"></i><span class="sr-only">${Translator.trans(
      "loading",
      {},
      "messages",
      this.$language
    )}</span></div>`;
    this.map = null;
    this.marker = null;
    this.geographic_areas_ids = null;
    this.markerPosition = null;
    this.allBounds = L.latLngBounds();
    this.boundingBox = null;
  }

  static schema() {
    return Base.schema({
      type: "address_map",
    });
  }

  static builderInfo = {
    title: "Address Map",
    group: "basic",
    icon: "fa fa-map-location-dot",
    weight: 70,
    schema: FormioAddressMap.schema(),
  };

  static editForm = editForm;

  locationNameFormatter(data) {
    var properties = data.address;
    var parts = [];
    if (data.addresstype === 'county' && properties['country']) {
      return properties['county'];
    } else if (data.addresstype === 'village' && properties['village']) {
      return properties['village'];
    }
    if (properties['name']) {
      parts.push(properties['name']);
    }
    if (properties['road']) {
      parts.push(properties['road']);
    } else if (properties['pedestrian']) {
      parts.push(properties['pedestrian']);
    } else if (properties['suburb']) {
      parts.push(properties['suburb']);
    } else if (properties['neighbourhood']) {
      parts.push(properties['neighbourhood']);
    }
    if (properties['house_number']) {
      parts.push(properties['house_number']);
    }
    if (properties['postcode']) {
      parts.push(properties['postcode']);
    }
    if (properties['town']) {
      parts.push(properties['town']);
    } else if (properties['city']) {
      parts.push(properties['city']);
    } else if (properties['village']) {
      parts.push(properties['village']);
    }
    var name = parts.join(', ').substr(0, 150);
    // console.log('Default -> '+ data.display_name)
    // console.log('Pretty -> '+ name)
    return name;
  };

  /**
   * Render returns a html string of the fully rendered component.
   *
   * @param children - If this class is extended, the sub string is passed as children.
   * @returns {string}
   */
  render(children) {
    // To make this dynamic, we could call this.renderTemplate('templatename', {}).
    let calendarClass = "";
    let content = this.renderTemplate("input", {
      input: {
        type: "input",
        ref: `${this.component.key}-selected`,
        attr: {
          id: `${this.component.key}`,
          class: "form-control",
          type: "hidden",
        },
      },
    });

    let translateLabel = this.options.i18n.resources[this.$language]
    // Calling super.render will wrap it html as a component.
    return super.render(`
      <style>
      .autocomplete__list {
        list-style: none;
        padding: 0;
        margin: 0;
        border: 1px solid #ddd;
        background-color: white;
        max-height: 200px;
        overflow-y: auto;
        position: absolute;
        z-index: 1000;
        width: 100%;
      }
      .autocomplete__item {
        padding: 10px;
        cursor: pointer;
      }
      .autocomplete__item:hover {
        background-color: #f0f0f0;
      }
      .autocomplete__item.focused {
        background-color: #ddd;
        outline: none;
      }

      .cta-button {
            padding: 10px 20px;
            background-color: #007bff;
            color: white;
            cursor: pointer;
            border: none;
            border-radius: 5px;
            font-size: 16px;
        }

        /* Modal Styles */
        .modal-adress-map {
            display: none;
            background-color: rgba(0, 0, 0, 0.5);
        }

        .modal-content-adress-map {
            border-radius: 0.3rem;
            width: 100%;
            max-width: 900px;
            height: 75%;
            margin: 6rem auto;
        }


        .close-adress-map {
            color: #aaa;
            font-size: 28px;
            font-weight: bold;
            position: absolute;
            right: 20px;
        }

        .close-adress-map:hover,
        .close-adress-map:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .modal-header-adress-map {
          padding: 1.25rem;
          position: relative;
        }

        .modal-body-adress-map {
          padding: 0 1.25rem;
          flex-grow: 0.8;
        }

        /* Map Container Style */
        .map {
            height: 100%;
            width: 100%;
        }
      </style>
      <div id="address-map-container-${this.id}" class="">
        <div class="select-wrapper form-group">
          <label class="col-form-label active" for="accessibleAutocomplete">${translateLabel?.translation[this.component.label] || this.component.label}</label>
          <input aria-expanded="false" aria-owns="accessibleAutocomplete__listbox" aria-autocomplete="list" aria-activedescendant="" aria-controls="accessibleAutocomplete__listbox" autocomplete="off" class="autocomplete__input form-control" id="accessibleAutocompleteInput" name="" placeholder="" type="text" role="combobox" data-focus-mouse="false" aria-describedby="accessibleAutocomplete__assistiveHint">
          <ul id="autocompleteResults" class="autocomplete__list d-none" role="listbox" aria-labelledby="accessibleAutocomplete"></ul>
        </div>
      </div>

<div class="link-wrapper mt-3"><a class="list-item active icon-left p-0 cursor-pointer nav-link" id="open-modal-${this.id}"><span class="list-item-title-icon-wrapper">
<svg class="icon icon-primary">
    <use  href="/bootstrap-italia/dist/svg/sprite.svg#it-map-marker-circle"></use>
</svg>
<span class="list-item-title t-primary">${Translator.trans("address_map.locate_on_map", {}, "messages", this.$language)}</span></span></a></div>

    <!-- The Modal -->
    <div id="map-modal-${this.id}" class="modal modal-adress-map">
        <div class="modal-content modal-content-adress-map">
          <div class="modal-header-adress-map">
            <div  id="close-modal-${this.id}" class="close-adress-map">&times;</div>
            <h5 class="modal-title">${Translator.trans("address_map.open_map", {}, "messages", this.$language)}</h5>
            <div class="title-xsmall u-grey-dark mb-1">${Translator.trans("address_map.desc_map_point", {}, "messages", this.$language)}</div>
          </div>
          <div class="modal-body-adress-map">
            <div id="map-${this.id}" class="map"></div>
            <div id="geographic_area_name_it"></div>
            <div id="geographic_area_geofence"></div>
            <div class="modal-footer"><button id="close-btn-modal-${this.id}" type="button" class="btn btn-secondary">${Translator.trans("address_map.close", {}, "messages", this.$language)}</button><button id="salve-btn-modal-${this.id}" type="button" class="btn btn-primary">${Translator.trans("address_map.save_address", {}, "messages", this.$language)}</button></div>
          </div>
        </div>
    </div>
  
    `);
  }

  /**
   * After the html string has been mounted into the dom, the dom element is returned here. Use refs to find specific
   * elements to attach functionality to.
   *
   * @param element
   * @returns {Promise}
   */
  attach(element) {
    var self = this;
    var debounceTimer;
    var inputElement = document.querySelector(`#address-map-container-${this.id} #accessibleAutocompleteInput`);
    var resultList = document.querySelector(`#address-map-container-${this.id} #autocompleteResults`);
    var currentSuggestions = [];
    var focusedIndex = -1;


    let location = window.location,
      explodedPath = location.pathname.split("/");
    let urlGeographicAreas = `${location.origin}/${explodedPath[1]}/api/geographic-areas`;
    fetch(urlGeographicAreas)
      .then(response => response.json())
      .then(data => {
        if (data) {
          self.geographic_areas_ids = [];
          self.geographic_areas = [];
          self.areasLayer = [];
          data.forEach((geo) => {
            self.geographic_areas_ids.push(geo.id);
            const geofence = JSON.parse(geo.geofence);
            self.geographic_areas.push(geofence);
            const geoJsonLayer = L.geoJSON(geofence, {
              style: {
                color: '#3478bd',
                fillColor: '#87CEEB',
                fillOpacity: 0.2,
                weight: 1.5
              },
              filter: function (feature) {
                return feature.geometry.type === "Polygon" || feature.geometry.type === "MultiPolygon";
              }
            })
            self.allBounds.extend(geoJsonLayer.getBounds());
            self.areasLayer.push(geoJsonLayer)
          });
          const bbNE = self.allBounds.getNorthEast();
          const bbSW = self.allBounds.getSouthWest();

          self.boundingBox = [ bbNE.lng, bbNE.lat, bbSW.lng, bbSW.lat ]
        } else {
          console.log('No geographic_areas_ids found.');
        }
      })
      .catch(error => {
        console.error('Error with reverse geocoding:', error);
      });

    if (inputElement) {
      inputElement.addEventListener('input', function (e) {
        var query = this.value;

        clearTimeout(debounceTimer);
        debounceTimer = setTimeout(function () {
          if (query.length > 2) {
            fetchLocations(query);
          } else {
            resultList.classList.add('d-none');
            currentSuggestions = [];
            focusedIndex = -1;
            inputElement.setAttribute('aria-expanded', 'false');
          }
        }, 300);
      });



      inputElement.addEventListener('keydown', function (e) {
        if (resultList.classList.contains('d-none')) return;
        if (e.key === 'ArrowDown') {
          focusedIndex = (focusedIndex + 1) % currentSuggestions.length;
          updateFocus();
        } else if (e.key === 'ArrowUp') {
          focusedIndex = (focusedIndex > 0) ? focusedIndex - 1 : currentSuggestions.length - 1;
          updateFocus();
        } else if (e.key === 'Enter') {
          if (focusedIndex > -1) {
            selectSuggestion(focusedIndex);
          }
        } else if (e.key === 'Escape') {
          resultList.classList.add('d-none');
          inputElement.setAttribute('aria-expanded', 'false');
        }
      });

      inputElement.addEventListener('blur', function () {
        setTimeout(function () {
          if (!currentSuggestions.includes(inputElement.value)) {
            inputElement.value = self.position?.label || self.position?.display_name || '';
          }
          resultList.classList.add('d-none');
          inputElement.setAttribute('aria-expanded', 'false');
        }, 200);
      });
    }
    function fetchLocations(query) {
      const language = 'it'; // Assuming Italian, can be passed dynamically
      const apiUrl = `https://nominatim.openstreetmap.org/search?q=${encodeURIComponent(query)}&countrycodes=it&viewbox=${self.boundingBox}&bounded=1&addressdetails=1&format=jsonv2&accept-language=${language}`;
      
      fetch(apiUrl)
        .then(response => response.json())
        .then(data => {
          currentSuggestions = [];
          
          if (data.length) {
            const windowLocation = window.location;
            const explodedPath = windowLocation.pathname.split("/");
            // Create an array of fetch promises for geographic area checks
            const fetchPromises = data.map((location, index) => {
              const { lat, lon } = location;
              
              const url = `${windowLocation.origin}/${explodedPath[1]}/api/geographic-areas/contains?lat=${lat}&lon=${lon}&geographic_areas_ids=${self.geographic_areas_ids.join(",")}`;
              
              return fetch(url)
                .then(areaResponse => areaResponse.json())
                .then(areaData => {
                  if (areaData.result) {
                    location.label = self.locationNameFormatter(location);
                    return location; // Return the location object
                  }
                  return null; // Return null if the geographic check fails
                });
            });
    
            // Wait for all promises to resolve
            return Promise.all(fetchPromises);
          } else {
            // Handle no results from the initial API call
            resultList.innerHTML = '';
            var noResultItem = document.createElement('li');
            noResultItem.className = 'autocomplete__item';
            noResultItem.textContent = `${Translator.trans("address_map.no_results", {}, "messages", self.$language)}`;
            noResultItem.setAttribute('role', 'option');
            resultList.appendChild(noResultItem);
            resultList.classList.remove('d-none');
            inputElement.setAttribute('aria-expanded', 'true');
          }
        })
        .then(validLocations => {
          validLocations = validLocations ? validLocations.filter(location => location !== null) : [];
          resultList.innerHTML = '';
          if (validLocations.length > 0) {
            validLocations.forEach((location, index) => {
              validLocations[index].label = self.locationNameFormatter(location);
            });
            validLocations = _.uniqWith(validLocations, (a, c) => a.label === c.label);
            validLocations.forEach((location, index) => {
              var listItem = document.createElement('li');
              listItem.className = 'autocomplete__item';
              listItem.textContent = location.label;
              listItem.setAttribute('data-value', location.label);
              listItem.setAttribute('role', 'option');
              listItem.setAttribute('id', `autocomplete-option-${index}`);
              listItem.addEventListener('click', function () {
                inputElement.value = location.label;
                resultList.classList.add('d-none');
                self.position = location;
                self.updateValue();
                updateMapPosition();
              });
              resultList.appendChild(listItem);
              currentSuggestions.push(location.label);
            });
            resultList.classList.remove('d-none');
          } else {
            var noResultItem = document.createElement('li');
            noResultItem.className = 'autocomplete__item';
            noResultItem.textContent = `${Translator.trans("address_map.no_results", {}, "messages", self.$language)}`;
            noResultItem.setAttribute('role', 'option');
            resultList.appendChild(noResultItem);
            resultList.classList.remove('d-none');
            inputElement.setAttribute('aria-expanded', 'true');
          }
        })
        .catch(error => {
          console.error('Error fetching locations:', error);
        });
    }

    function updateFocus() {
      var items = resultList.querySelectorAll('.autocomplete__item');

      items.forEach((item, index) => {
        if (index === focusedIndex) {
          item.classList.add('focused');
          inputElement.setAttribute('aria-activedescendant', item.id);
          ensureItemVisible(item);
        } else {
          item.classList.remove('focused');
        }
      });
    }

    function selectSuggestion(index) {
      inputElement.value = currentSuggestions[index];
      resultList.classList.add('d-none');
      inputElement.setAttribute('aria-expanded', 'false');
      self.position = location;
      self.updateValue();
      updateMapPosition();
    }

    function updateMapPosition() {
      const newLatLng = [self.position.lat, self.position.lon];
      if (self.marker) {
        self.marker.setLatLng(newLatLng).update();
        self.map.setView(newLatLng, 13);
      }
    }

    function ensureItemVisible(item) {
      const listRect = resultList.getBoundingClientRect();
      const itemRect = item.getBoundingClientRect();

      if (itemRect.bottom > listRect.bottom) {
        resultList.scrollTop += itemRect.bottom - listRect.bottom;
      } else if (itemRect.top < listRect.top) {
        resultList.scrollTop -= listRect.top - itemRect.top;
      }
    }
    self.renderData();




    delete L.Icon.Default.prototype._getIconUrl;

    L.Icon.Default.mergeOptions({
      iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
      iconUrl: require('leaflet/dist/images/marker-icon.png'),
      shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
    });

    const modal = document.getElementById(`map-modal-${this.id}`);
    const btn = document.getElementById(`open-modal-${this.id}`);
    const close = document.getElementById(`close-modal-${this.id}`);
    const close2 = document.getElementById(`close-btn-modal-${this.id}`);
    const save = document.getElementById(`salve-btn-modal-${this.id}`);

    if(!btn) {
      return
    }

    btn.onclick = function () {
      modal.style.display = "block";
      setTimeout(initializeMap, 100);
      save.setAttribute('disabled', true); 
    };

    close.onclick = function () {
      closeModal();
    };

    close2.onclick = function () {
      closeModal();
    };

    save.onclick = function () {
      onSaveMapLocation();
    }

    function closeModal() {
      modal.style.display = "none";
      self.markerPosition = null;
    };

    window.onclick = function (event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    };

    function initializeMap() {
      if (!self.map) {
        self.map = L.map(`map-${self.id}`).setView([41.5, 12.3], 13);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          attribution: '© OpenStreetMap contributors'
        }).addTo(self.map);
        
        self.areasLayer.map(layer => {layer.addTo(self.map)});

        const centerMarker = self.position ? [`${self.position.lat}`, `${self.position.lon}`] : self.allBounds.getCenter();
        
        self.marker = L.marker(centerMarker, { draggable: true }).addTo(self.map);

        self.marker.on('moveend', function (event) {
          const position = event.target.getLatLng();
          fetchReverseGeocoding(position.lat, position.lng);
        });

        if (!self.position) {
          self.map.fitBounds(self.allBounds);
        } else {
          updateMapPosition();
        }
      } else {
        self.map.invalidateSize();
      }
    }

    function fetchReverseGeocoding(lat, lng) {
      const url = `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${lat}&lon=${lng}`;

      fetch(url)
        .then(response => response.json())
        .then(data => {
          if (data && data.display_name) {
            data.label = self.locationNameFormatter(data)
            console.log(`${data.label}`);
            self.marker.bindPopup(`${data.label}`).openPopup();
            self.markerPosition = data;
            save.removeAttribute('disabled');
          } else {
            console.log('No address found.');
          }
        })
        .catch(error => {
          console.error('Error with reverse geocoding:', error);
        });
    }

    function onSaveMapLocation() {
      if (self.markerPosition) {
        const url = `${location.origin}/${explodedPath[1]}/api/geographic-areas/contains?lat=${self.markerPosition.lat}&lon=${self.markerPosition.lon}&geographic_areas_ids=${self.geographic_areas_ids.join(",")}`

        fetch(url)
          .then(response => response.json())
          .then(data => {
            if (data) {
              console.log(data)
              if (data.result === false) {
                self.showModalPositionOutOfAreas();
              } else if (data.result === true) {
                self.position = self.markerPosition;
                inputElement.value = self.markerPosition.label;
                self.updateValue();
                closeModal();
              }

            } else {
              console.log('No geographic-areas/contains .');
            }
          })
          .catch(error => {
            console.error('Error with geographic-areas/contains:', error);
          });
      }
    }

    return super.attach(element);
  }

  renderData() {
    const inputElement = document.querySelector(`#address-map-container-${this.id} #accessibleAutocompleteInput`);
    const btn = document.getElementById(`open-modal-${this.id}`);
    if (this.position) {
      inputElement.value = this.position?.label || this.position?.display_name
    }
    if (this.disabled) {
      inputElement.setAttribute('disabled', true);
      btn.classList.add('d-none');
    }
  }

  showModalPositionOutOfAreas() {
    alert("Posizione fuori area");
  }

  /**
   * Get the value of the component from the dom elements.
   *
   * @returns {String}
   */
  getValue() {
    if (!(this.position)) {
      // Unset value (needed for calendars with 'required' option"
      return null;
    }
    return this.position
  }

  /**
   * Set the value of the component into the dom elements.
   *
   * @param value
   * @returns {boolean}
   */
  setValue(value) {
    if (!value) {
      return;
    }
    this.position = value;
    this.renderData();
  }
}
