import React, {useEffect, useState} from 'react';
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import {Button} from "design-react-kit"
import "react-dates/lib/css/_datepicker.css";
import "../../../styles/components/DateRangePicker/react_dates_overrides.scss";
const lang = document.documentElement.lang.toString() || 'it'
import moment from "moment";
import {Default, Mobile} from "../../utils/Responsive";
import {STATUS_ALL, STATUS_PAID, STATUS_TO_PAY} from "../../pages/payments/Main";
moment.locale(lang)

function DateRangePickerWrapper({childToParent,detectChanges,status, loading}) {

  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [focusedInput, setFocusedInput] = useState(null);

  const onDatesChange = (startDate, endDate) => {
    if(startDate && endDate){
      setStartDate(startDate)
      setEndDate(endDate)
      childToParent({ startDate, endDate })
    }

  }

  const onDetectDatesChange = (startDate, endDate) => {
    if(startDate && endDate) {
      detectChanges({startDate, endDate})
    }
  }

  useEffect(() => {
    //onDatesChange(startDate,endDate)
    handleClickShortcutButtons()
  }, []);

  useEffect(() => {
      if(status === STATUS_PAID){
        setStartDate(moment().subtract(6, 'months'))
        setEndDate(moment())
      }else if(status === STATUS_ALL){
        setStartDate(moment().subtract(6, 'months'))
        setEndDate(moment())
      }else if(status === STATUS_TO_PAY){
        setStartDate(moment().subtract(3, 'months'))
        setEndDate(moment().add(3,'months'))
      }

  }, [status]);

  useEffect(() => {
    onDatesChange(startDate,endDate)
  }, [startDate,endDate]);


  const handleClickShortcutButtons = () => {
    document.addEventListener('click', function (event) {

      // If the clicked element doesn't have the right selector, bail
      if (!event.target.matches('.DayPicker_calendarInfo__horizontal button')) return;

      // Don't follow the link
      event.preventDefault();

      // Log the clicked element in the console
      setFocusedInput(null)

    }, false);
  }

  function renderDatePresets() {
    const presets = [
      {
        text: Translator.trans('time.last_month', {}, 'messages', lang),
        start: moment().subtract(1, 'month'),
        end: moment(),
      },
      {
        text: Translator.trans('time.last_3_month', {}, 'messages', lang),
        start: moment().subtract(3, 'months'),
        end: moment(),
      },
      {
        text: Translator.trans('time.last_6_month', {}, 'messages', lang),
        start: moment().subtract(6, 'months'),
        end: moment(),
      },
      {
        text: Translator.trans('time.last_year', {}, 'messages', lang),
        start: moment().subtract(12, 'months'),
        end: moment(),
      }
    ]


    return (
      <div className="d-block d-md-flex flex-row flex-md-column">
        {presets.map(({ text, start, end }) => {
          return (
            <Button
              size='xs'
              outline
              color={'primary'}
              key={text}
              type="button"
              onClick={() => onDatesChange(start,end)}
              className={'my-3 mx-2'}
            >
              {text}
            </Button>
          );
        })}
      </div>
    );
  }

  const isOutsideRange = (day) => {
    day.isAfter(moment().subtract(3, 'months')) || day.isBefore(moment().add(3,'months'));
  }


  return (
        <div className='mx-0 mt-5 mt-md-0'>
          <Mobile>
            <p className={'mb-0 text-button-sm'}>
              {status === STATUS_PAID ? (
                <strong className="date-range-picker-label">{Translator.trans('pratica.selected_period_pay', {}, 'messages', lang)}</strong>
              ):status === STATUS_TO_PAY ?
                <strong className="date-range-picker-label">{Translator.trans('pratica.selected_period_expired', {}, 'messages', lang)}</strong>
                :
                <strong className="date-range-picker-label">{Translator.trans('pratica.selected_period', {}, 'messages', lang)}</strong>}
            </p>
            <DateRangePicker
              customArrowIcon={' '}
              calendarInfoPosition="before"
              renderCalendarInfo={renderDatePresets}
              startDatePlaceholderText={Translator.trans('iscrizioni.data_inizio', {}, 'messages', lang)}
              endDatePlaceholderText={Translator.trans('iscrizioni.data_fine', {}, 'messages', lang)}
              displayFormat={() => moment.localeData().longDateFormat('L')}
              minimumNights={0}
              minDate={moment().subtract(12, 'months')}
              enabledOutsideDays={false}
              isOutsideRange={status === STATUS_TO_PAY ? isOutsideRange : day => (moment().diff(day) < 0)}
              startDate={startDate}
              startDateId="start_date_id_mobile"
              endDate={endDate}
              endDateId="end_date_id_mobile"
              monthFormat="MMMM YYYY"
              onDatesChange={({ startDate, endDate }) => {
                onDatesChange(startDate, endDate)
               // onDetectDatesChange(startDate, endDate)
              }} // PropTypes.func.isRequired,
              focusedInput={focusedInput}
              onFocusChange={e => {setFocusedInput(e)}} // PropTypes.func.isRequired,
              orientation="vertical"
              verticalHeight={568}
              withFullScreenPortal
              disabled={loading}
            />
          </Mobile>
          <Default>
            <p className={'mb-0 text-button-sm'}>
            {status === STATUS_PAID ? (
              <strong className="date-range-picker-label">{Translator.trans('pratica.selected_period_pay', {}, 'messages', lang)}</strong>
            ):status === STATUS_TO_PAY ?
              <strong className="date-range-picker-label">{Translator.trans('pratica.selected_period_expired', {}, 'messages', lang)}</strong>
              :
              <strong className="date-range-picker-label">{Translator.trans('pratica.selected_period', {}, 'messages', lang)}</strong>}
              </p>

              <DateRangePicker
            closeOnOutsideClick={true}
            anchorDirection="right"
            customArrowIcon={'-'}
            calendarInfoPosition="before"
            renderCalendarInfo={renderDatePresets}
            startDatePlaceholderText={Translator.trans('iscrizioni.data_inizio', {}, 'messages', lang)}
            endDatePlaceholderText={Translator.trans('iscrizioni.data_fine', {}, 'messages', lang)}
            displayFormat={() => moment.localeData().longDateFormat('L')}
            minimumNights={0}
            minDate={moment().subtract(12, 'months')}
            enabledOutsideDays={false}
            isOutsideRange={status === STATUS_TO_PAY ?isOutsideRange : day => (moment().diff(day) < 0)}
            startDate={startDate}
            startDateId="start_date_id"
            endDate={endDate}
            endDateId="end_date_id"
            monthFormat="MMMM YYYY"
            onDatesChange={({ startDate, endDate }) => {
              onDatesChange(startDate, endDate)
              //onDetectDatesChange(startDate, endDate)
            }} // PropTypes.func.isRequired,
            focusedInput={focusedInput}
            onFocusChange={e => {setFocusedInput(e)}} // PropTypes.func.isRequired,
            disabled={loading}
          />
          </Default>
        </div>
    )
}



export default DateRangePickerWrapper;
