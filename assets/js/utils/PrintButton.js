export class PrintButton {

  static init() {

    const urlPrint = $('#download_module').data('print')

    $('#download_module').on('click', () => {
      let printWindow = window.open(urlPrint)
      const intervalId = setInterval(() => {
        if (printWindow.document.readyState === 'complete' && printWindow.status === 'ready') {
            clearInterval(intervalId);
            printWindow.print();
        }
      }, 100);

      //Close window once print is finished
      printWindow.onafterprint = function(){
        printWindow.close()
      };
    })
  }
}
