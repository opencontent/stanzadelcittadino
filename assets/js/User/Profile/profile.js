$(document).ready(function ($) {
  $('#form_save').on('click', function (element, child) {
    var form = document.querySelector('form')
    var errorsFieldForm = [];

    function createListErr(ulList) {
      errorsFieldForm.forEach(el => {
          var listItem = $('<li></li>').text(el);
          listItem.appendTo(ulList)
        }
      )
    }

    form.addEventListener('invalid', (e) => {
      if (e.target) {
        errorsFieldForm.push(e.target.previousSibling.innerText)
      }

      if ($('.alert.alert-danger').length > 0) {
        $("#edit_user_profile").find('.alert.alert-danger').remove()
        const errors = $('#edit_user_profile').data('error')
        $('#edit_user_profile').prepend(`<div class="alert alert-danger alert-dismissible fade show" role="alert">${errors}:
<ul class='list-error'></ul><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`
        );
        createListErr($('ul.list-error'))
      } else {
        const errors = $('#edit_user_profile').data('error')
        $('#edit_user_profile').prepend(`<div class="alert alert-danger alert-dismissible fade show" role="alert">${errors}:
<ul class='list-error'></ul><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>`
        );
        createListErr($('ul.list-error'))
      }
    }, true)
  });

  $('#copia_domicilio').on('click', (event) =>{
    copia_domicilio(event);
  })


  $('#copia_residenza').on('click', (event) =>{
    copia_residenza(event);
  })


  function copia_residenza(event) {
    event.preventDefault()
    $('#form_sdc_indirizzo_domicilio').val($('#form_sdc_indirizzo_residenza').val());
    $('#form_sdc_civico_domicilio').val($('#form_sdc_civico_residenza').val());
    $('#form_sdc_cap_domicilio').val($('#form_sdc_cap_residenza').val());
    $('#form_sdc_citta_domicilio').val($('#form_sdc_citta_residenza').val());
    $('#form_sdc_provincia_domicilio').val($('#form_sdc_provincia_residenza').val());
    $('[data-id=form_sdc_provincia_domicilio]').attr('title', $('#form_sdc_provincia_residenza').val());
    $('[data-id=form_sdc_provincia_domicilio]').find(".filter-option-inner-inner").html($('#form_sdc_provincia_residenza option:selected').text())
    $('#form_sdc_stato_domicilio').val($('#form_sdc_stato_residenza').val());
  }

  function copia_domicilio(event) {
    event.preventDefault()
    $('#form_sdc_indirizzo_residenza').val($('#form_sdc_indirizzo_domicilio').val());
    $('#form_sdc_civico_residenza').val($('#form_sdc_civico_domicilio').val());
    $('#form_sdc_cap_residenza').val($('#form_sdc_cap_domicilio').val());
    $('#form_sdc_citta_residenza').val($('#form_sdc_citta_domicilio').val());
    $('#form_sdc_provincia_residenza').val($('#form_sdc_provincia_domicilio').val());
    $('[data-id=form_sdc_provincia_residenza]').attr('title', $('#form_sdc_provincia_domicilio').val());
    $('[data-id=form_sdc_provincia_residenza]').find(".filter-option-inner-inner").html($('#form_sdc_provincia_domicilio option:selected').text())
    $('#form_sdc_stato_residenza').val($('#form_sdc_stato_domicilio').val());
  }

  // Se non è presete il codice catastale esterno non è necessario recuperarne il valore via API ne
  // prepopolare i dati relativi al luogo di nascita in modalità readonly
  const codCatastale = $('input[data-ee]').val();
  if (!codCatastale) {
    return
  }

  //Stato estero
  if($('input[data-ee]') && $('input[data-api]')){
    getMunicipality($('input[data-ee]').val()).then(e => {
      $('#form_luogo_nascita').val(e[0].comune)
      $('#form_luogo_nascita').prop('readonly', true);
      $('#provincia_nascita').val(e[0].pr)
      $('#provincia_nascita').prop('readonly', true);
      $('#form_stato_nascita').val(e[0].regione)
      $('#form_stato_nascita').prop('readonly', true);
    }).catch(err => console.log(err))
  }

  async function getMunicipality(cod_catastale) {
    const apiUrl = $('input[data-api]').data('api')
    const response = await fetch(`${apiUrl}?cod_catastale=${cod_catastale}`);
    return await response.json();
  }

});



