import "../styles/vendor/formio.scss";
import Calendar from "./Calendar";
import DynamicCalendar from "./DynamicCalendar";
import PageBreak from "./PageBreak";
import FinancialReport from "./FinancialReport";
import SdcFile from "./SdcFile";
import AddressMap from "./AddressMap";
import "formiojs/dist/formio.form.min.css";
import {Formio, Utils} from "formiojs";
import FormioI18n from "./utils/FormioI18n";

Formio.registerComponent("calendar", Calendar);
Formio.registerComponent("dynamic_calendar", DynamicCalendar);
Formio.registerComponent("pagebreak", PageBreak);
Formio.registerComponent("financial_report", FinancialReport);
Formio.registerComponent("sdcfile", SdcFile);
Formio.registerComponent("address_map", AddressMap);

const language = document.documentElement.lang.toString();

function initPrintable(containerId) {
  const $container = $("#" + containerId);
  const formUrl = $container.data("formserver_url") + "/printable/" + $container.data("form_id");
  const baseUrl = $container.data("formserver_url") + "/printable"


  $.getJSON(
    $container.data("formserver_url") + "/form/" + $container.data("form_id") + "/i18n?lang=" + $container.data("locale"),
    function (data) {
      Formio.icons = "fontawesome";
      Formio.setBaseUrl(baseUrl);

      Formio.createForm(document.getElementById(containerId), formUrl, {
        noAlerts: true,
        language: $container.data("locale"),
        i18n: data,
        readOnly: true,
        buttonSettings: {showCancel: false},
        //renderMode: 'html'
      }).then(function (form) {
        form.submission = {
          data: $container.data("submission"),
        };

        // Called when the form has completed the render, attach, and one initialization change event loop
        // Da migliorare il controllo sulla fine del rendering del form
        form.on('initialized', () => {
          setTimeout(function () {
            console.log('initialized');
            window.status = 'ready'
          }, 3000)
        });

      });
    }
  );
}

function getFormSchema(formUrl) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: formUrl,
      dataType: 'json',
      type: 'GET',
      success: function (form) {
        form = wizardConverter(form)
        resolve(form)
      },
      error: function (error) {
        reject(error)
      }
    })
  })
}

function wizardConverter(form) {
  if (form.display === 'wizard') {
    // Metodo 1: conversione semplice: espansione delle pagine in verticale
    form.display = 'form';
  }
  return disableForm(form);
}

function disableForm(form) {
  let disabledForm = JSON.parse(JSON.stringify(form))

  if (disabledForm.components) {
    // Disable form components
    disabledForm.components.forEach(function (component, index) {
      disabledForm.components[index] = disableForm(component);
    })
  } else if (disabledForm.columns) {
    // Disable columns components
    disabledForm.columns.forEach(function (column, index) {
      disabledForm.columns[index] = disableForm(column);
    })
  } else {
    // Simple component
    // Disable JS custom calculated values
    if (disabledForm.type === 'select' && ['custom', 'url'].includes(disabledForm['dataSrc'])) {
      disabledForm.dataSrc = 'custom';
      if (disabledForm.data && disabledForm.data.custom) {
        // Disable custom js
        disabledForm.data.custom = '';
      }
    }
    // Disable JS custom default values
    if (disabledForm.customDefaultValue) {
      disabledForm.customDefaultValue = '';
    }
    // Disable JS custom calculated values
    if (disabledForm.calculateValue) {
      disabledForm.calculateValue = '';
    }
    // Disable JS validation
    if (disabledForm.validate) {
      disabledForm.validate.custom = '';
    }
  }
  return disabledForm;
}

function initPrintableBuiltIn(containerId) {
  const $container = $("#" + containerId);
  const formUrl = $container.data("url");
  const baseUrl = $container.data("formserver_url") + "/printable"

  getFormSchema(formUrl).then((formSchema) => {
    Formio.icons = "fontawesome";
    Formio.setBaseUrl(baseUrl);

    Formio.createForm(document.getElementById(containerId), formSchema, {
      noAlerts: true,
      language: $container.data("locale"),
      i18n: FormioI18n.languages(),
      readOnly: true,
      buttonSettings: {showCancel: false},
      //renderMode: 'html'
    }).then(function (form) {
      form.submission = {
        data: $container.data("submission"),
      };

      form.ready.then(() => {
        setTimeout(function () {
          console.log('initialized');
          window.status = 'ready';
        }, 3000)
      });
    });
  });
}


$(document).ready(function () {

  // Init form printable
  if ($("#formio.printable").length > 0) {
    initPrintable('formio');
  }

  // Init built-in printable
  if ($("#formio.built-in-printable").length > 0) {
    initPrintableBuiltIn('formio');
  }

});

