import Auth from "../../rest/auth/Auth";
import axios from "axios";

$(document).ready(function () {

  if($('#container-f24').length > 0) {
    const f24Element = $('#container-f24');
    const auth = new Auth()
    let token = null
    auth.getSessionAuthTokenPromise().then((data) => {
      token = data.token;
      const interval = setInterval(() => {
        axios.get(`${f24Element.data('url')}`, {
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
          .then(function (res) {
            if (res.data.status == '2000') {
              clearInterval(interval);
              window.location.reload()
            }
          })
      },10000) //Retry after 10 seconds
    }).catch((e) => {
      console.error(e);
    })
  }
})
