import {TextEditor} from "../utils/TextEditor";
import Form from '../Formio/Form';
import InfoPayment from "../rest/payment/InfoPayment";
import {ApplicationMap} from '../utils/ApplicationMap'
import {PrintButton} from "../utils/PrintButton";
import Swal from 'sweetalert2/src/sweetalert2.js'
import VueSweetalert2 from 'vue-sweetalert2';
// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';
import '../../../assets/styles/pages/user-show-application.scss'

import unicodeToChar from '../utils/unicodeToChar';
import Auth from "../rest/auth/Auth";
import axios from "axios";
const plugins = {
  install() {
    Vue.helpers = unicodeToChar;
    Vue.prototype.$helpers = unicodeToChar;
  }
}

Vue.config.productionTip = false
Vue.use(VueSweetalert2);
Vue.use(plugins);

const lang = document.documentElement.lang.toString();

$(document).ready(function () {
    //Disabled Init zoom function
    //Zoom($)

  //Used by tinymce
  let messageTextEditor = null;

  if ($('#answer-integration').length > 0) {
    $('#answer-integration').click(function (e) {
      e.preventDefault();
      $('#messaggi-tab').trigger('click');
      $('#messaggi-tab').on('shown.bs.tab', function (e){
        $('html, body').animate({ scrollTop: $('form').offset().top }, 800);
      });
    })
  }

  if ($('#message_applicant').length > 0) {
    $('#message_applicant').click(function (e) {
      //e.preventDefault();
      if (!messageTextEditor) {
        Swal.fire(
          `${Translator.trans('error_message_detail', {}, 'messages', lang)}`,
          `${Translator.trans('pratica.messaggio_operatore_vuoto', {}, 'messages', lang)}`,
          'error'
        );
        return false;
      }
      return confirm(Translator.trans('pratica.messaggio_operatore', {}, 'messages', lang));
    })
  }


  if($('#button-pre-submit').length > 0) {
    const element = $('#button-pre-submit');
    const auth = new Auth()
    let token = null
    auth.getSessionAuthTokenPromise().then((data) => {
      token = data.token;
      const interval = setInterval(() => {
        axios.get(`${element.data('url')}`, {
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
          .then(function (res) {
            if (res.data.status == '2000') {
              clearInterval(interval);
              $('#container-pdf').removeClass('d-none')
              $('#container-generate-pdf').hide()
              const linkPdf = $("#link-pdf").data('attachment') + res.data.compiled_modules[0].id
              $("#link-pdf").attr("href", linkPdf)
            }
          })
      },10000) //Retry after 10 seconds
    }).catch((e) => {
      console.error(e);
    })
  }


  //Init TextArea
  TextEditor.init(
    (editor,messageText)  => {
      messageTextEditor = messageText;
    });

  // Tooltips
  $('[data-toggle="tooltip"]').tooltip();

  // Init formIo
  if ($('#formio_summary').length > 0) {
    Form.init('formio_summary');
  }

  // Init Details Payment
  if( $('.payment-list').length > 0){
    InfoPayment.init();
  }

  // Map
  if ( $('#map').length > 0) {
    ApplicationMap.init()
  }

  if($('#download_module').length > 0) {
    PrintButton.init()
  }

});
