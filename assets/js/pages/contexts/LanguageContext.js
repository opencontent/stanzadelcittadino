import React, {useMemo} from 'react';
import {createContext, useContext, useState} from "react";

export const LanguageContext = createContext();

export function useLanguage(){
  return useContext(LanguageContext)
}

export function LanguageProvider(props) {

  const [userLanguage, setUserLanguage] = useState(document.documentElement.lang.toString() || 'it');


  const value = useMemo(() => ({
    userLanguage,
    setUserLanguage,
  }),[userLanguage])

  return (
    <LanguageContext.Provider value={value}>{props.children}</LanguageContext.Provider>
  )
}
