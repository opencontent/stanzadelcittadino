import React from 'react';
import {Button, Icon} from "design-react-kit"
import {useLanguage} from "../contexts/LanguageContext";
import {useNavigate} from "react-router-dom";


function PaymentNavSteps({goBack, enableBack}) {

    const navigate = useNavigate();

    const  {
        lang
    } = useLanguage()

    return (
        <div className="cmp-nav-steps">
            <nav className="steppers-nav" aria-label="Step">
                {enableBack ?
                    <Button className="btn btn-sm steppers-btn-prev p-0" onClick={() =>navigate(-1)}>
                    <Icon icon={'it-chevron-left'}></Icon>
                    <span className="text-button-sm t-primary">{Translator.trans('back_to_list', {}, 'messages', lang)}</span>
                </Button> :
                <a href={`${goBack}`} className="btn btn-sm steppers-btn-prev p-0">
                    <Icon icon={'it-chevron-left'}></Icon>
                    <span className="text-button-sm t-primary">{Translator.trans('back_to_list', {}, 'messages', lang)}</span>
                </a>
                }
            </nav>
        </div>
    )
}

export default PaymentNavSteps;
