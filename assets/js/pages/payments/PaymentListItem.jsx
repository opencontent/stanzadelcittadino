import React, {useId, useState} from 'react';
import moment from "moment";
import {getPaymentStatus, ifPaymentFailed, ifPaymentPending, ifPaymentToPay} from "../../utils/PaymentStatus";
import {Button} from "design-react-kit"
const tabElement = document.querySelector("[data-list-payment-path]")
import folderPay from '../../../images/folder-pay.svg';
import folderToPay from '../../../images/folder-to-pay.svg';
import {useLanguage} from "../contexts/LanguageContext";
import useCheckout from "../hooks/useCheckout";
import {STATUS_PAID, STATUS_TO_PAY} from "./Main";


function PaymentListItem({payment,status}) {

  const [collapse, setCollapse] = useState(false)
  const {execute} = useCheckout({payment})

  const accordion = useId();

  const  {
    userLanguage: lang
  } = useLanguage()


  const handleDropdown = () => {
    setCollapse(!collapse)
  };


  const handleCheckout = () =>{
    execute()
  }

  return (
      <div className="accordion-item">
        <div className="accordion-header" id={`heading-${accordion}`}>
          <button className={`accordion-button  ${collapse ? '' : 'collapsed'}`} type="button" onClick={handleDropdown}
                  data-bs-toggle="collapse" aria-expanded={collapse}
                  aria-controls={`collapse-${accordion}`} data-focus-mouse="false">
            <div className="button-wrapper d-block d-md-flex">
              {payment?.reason}
              <div className="icon-wrapper w-100 w-lg-25 mr-3 text-sm-left text-md-right">
                <img className="icon-folder" src={
                  ifPaymentFailed(payment?.status) ? folderToPay :
                    ifPaymentPending(payment?.status)
                    ? folderToPay : folderPay } alt={getPaymentStatus(payment?.status)}
                     role="img"/>
                <span className={
                  ifPaymentFailed(payment?.status) ? 'u-main-error' : ifPaymentPending(payment?.status) ? 'u-main-alert' : 'text-success' }>{' '}{getPaymentStatus(payment?.status)}</span>
              </div>
            </div>
          </button>
            <p className="accordion-date title-xsmall-regular mb-0 pb-0">{Translator.trans('payment.amount', {}, 'messages', lang)}: <span className="label"><strong>
              {new Intl.NumberFormat("it-IT", { style: "currency", currency: "EUR" }).format(
              payment?.payment?.amount,
            )}</strong></span></p>
            {ifPaymentToPay(payment?.status)
                ?  (
                    <div className={'mx-3 d-sm-flex d-inline-block'}>
                        {payment?.links.online_payment_begin.url && payment?.links.online_payment_begin.url !== '' ?
                            <Button type={"button"} color={"primary"} onClick={()=> handleCheckout()} className="btn btn-primary justify-content-center my-3 le-3 w-auto">
                                <span className="">{Translator.trans('payment.paga_online', {}, 'messages', lang)}</span>
                            </Button> : null }
                        {payment?.links.offline_payment.url !== null && payment?.links.offline_payment.url !== '' ?
                            <a href={payment?.links.offline_payment.url} className="btn justify-content-center my-3 ml-sm-3 ml-0 btn-outline-primary" target={'_blank'}>
                                <span className="">{Translator.trans('payment.paga_offline', {}, 'messages', lang)}</span>
                            </a> : null }
                    </div>
                ) : null}
          {status === STATUS_PAID ? (
            <p className="accordion-date title-xsmall-regular mb-2 pb-0">{Translator.trans('payment.paid_at', {}, 'messages', lang)}:  <b>{moment(payment?.payment.paid_at).isValid() ? moment(payment?.payment.paid_at).local(lang).format('L LTS'): '--'}</b></p>

          ):status === STATUS_TO_PAY ?
            <p className="accordion-date title-xsmall-regular mb-2 pb-0">{Translator.trans('payment.expired_at', {}, 'messages', lang)}:  <b>{moment(payment?.payment.expire_at).isValid() ? moment(payment?.payment.expire_at).local(lang).format('L LTS'): '--'}</b></p>

            :
            <p className="accordion-date title-xsmall-regular mb-2 pb-0">{Translator.trans('issued_on', {}, 'messages', lang)}:  <b>{moment(payment?.created_at).isValid() ? moment(payment?.created_at).local(lang).format('L LTS') : '--'}</b></p>
          }
        </div>

        <div id={`collapse-${accordion}`} className={`accordion-collapse p-0 collapse ${collapse ? 'show' : ''}`}
             role="region" aria-labelledby={`heading-${accordion}`}>
          <div className="accordion-body">
            <p className="accordion-date title-xsmall-regular mb-0 pl-0 pb-0">{Translator.trans('payment.iud', {}, 'messages', lang)}: <span className="label">{payment?.id}</span></p>
            {payment?.type ? <p className="accordion-date title-xsmall-regular mb-0 pl-0 pb-0">{Translator.trans('payment.type', {}, 'messages', lang)}: <span className="label">{payment?.type}</span></p> : null}
            {!payment?.service_id ?
            <a className="chip chip-simple" href="#">
              <span className="chip-label">{Translator.trans('no_digital_service', {}, 'messages', lang)}</span>
            </a>
              : null}
            <div className="d-flex flex-column flex-md-row mt-2 align-baseline">
              <Button size={'xs'} tag={'a'} outline color='primary' role='button' className="title-xsmall-semi-bold mr-3 mb-2 min-w-120 font-weight-bold" href={`${tabElement}/${payment.id}`}>
                <span className="">{Translator.trans('operatori.vai_al_dettaglio', {}, 'messages', lang)}</span>
              </Button>
              <Button size={'xs'} tag={'a'} outline color='primary' role='button' className="title-xsmall-semi-bold mr-3 mb-2 min-w-120 font-weight-bold" href={`/${window.location.pathname.split('/')[1]}/servizi/${payment?.service_id}`}>
                <span className="">{Translator.trans('go_service_card', {}, 'messages', lang)}</span>
              </Button>
              {payment?.status === 'COMPLETE' && payment?.links?.receipt.url !== null ? (
                <Button size={'xs'} tag={'a'} outline color='primary' role='button' className="title-xsmall-semi-bold mr-3 mb-2 min-w-120 font-weight-bold"
                        download target={'_blank'}
                        href={payment?.links?.receipt.url}>
                  <span className="">{Translator.trans('payment.download_receipt', {}, 'messages', lang)}</span>
                </Button>
              ) : null}
            </div>

          </div>
        </div>
      </div>
  );
}

export default PaymentListItem;
