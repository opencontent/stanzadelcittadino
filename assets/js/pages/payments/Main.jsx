import React from 'react';
import { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import PaymentList from "./PaymentList"
import {AuthProvider} from "../contexts/AuthContext";
import {LanguageProvider} from "../contexts/LanguageContext";
import PaymentPageDetails from "./PaymentPageDetails";


function Main() {
  return (
    <LanguageProvider>
      <AuthProvider>
        <Router>
          <Routes>
            <Route exact
                   path={`${window.location.pathname.split('/')[1]}/${window.location.pathname.split('/')[2]}/user/payments`}
                   element={<PaymentList/>}/>
            <Route
              path={`${window.location.pathname.split('/')[1]}/${window.location.pathname.split('/')[2]}/user/payments/:id`}
              element={<PaymentPageDetails/>}/>
          </Routes>
        </Router>
      </AuthProvider>
    </LanguageProvider>
  );
}


export default Main;

if (document.getElementById('payments')) {
  const rootElement = document.getElementById("payments");
  const root = createRoot(rootElement);
  root.render(
    <StrictMode>
      <Main />
    </StrictMode>
  );
}

export const STATUS_ALL = ''
export const STATUS_TO_PAY = 'PAYMENT_STARTED,PAYMENT_PENDING,EXPIRED'
export const STATUS_PAID = 'COMPLETE'
export const QUERY_STATUS_PAID = 'paid'
export const QUERY_STATUS_TO_PAY = 'to-pay'
export const QUERY_STATUS_ALL = 'all'
