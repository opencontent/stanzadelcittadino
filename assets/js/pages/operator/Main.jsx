import React from 'react';
import { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import PaymentsServiceList from "./PaymentsServiceList"
import {AuthProvider} from "../contexts/AuthContext";
import {LanguageProvider} from "../contexts/LanguageContext";
import OperatorPaymentPageDetails from "./OperatorPaymentPageDetails";


function Main() {
  return (
    <LanguageProvider>
      <AuthProvider>
        <Router>
          <Routes>
            <Route exact
                   path={`${window.location.pathname.split('/')[1]}/${window.location.pathname.split('/')[2]}/operatori/payments-service-list`}
                   element={<PaymentsServiceList/>}/>
            <Route
                path={`${window.location.pathname.split('/')[1]}/${window.location.pathname.split('/')[2]}/operatori/payments-service-list/:id`}
                element={<OperatorPaymentPageDetails/>}/>
          </Routes>
        </Router>
      </AuthProvider>
    </LanguageProvider>
  );
}


export default Main;

if (document.getElementById('payments-service-list')) {
  const rootElement = document.getElementById("payments-service-list");
  const root = createRoot(rootElement);
  root.render(
    <StrictMode>
      <Main />
    </StrictMode>
  );
}
