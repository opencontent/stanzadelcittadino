import axios from "axios";
import 'formiojs/dist/formio.form.min.css';
import {Formio} from "formiojs";
import Gateways from "./Gateways";
import Swal from "sweetalert2/src/sweetalert2.js";
import Auth from "../auth/Auth";
import Services from "../services/Services";


class ApplicationPaymentConfig {

  static paymentConfigsField = null;
  static gatewayUrl = '';
  static language;
  static tenantId;
  static paymentConfigIds = [];
  static _configs = [];
  static auth;

  static serviceID;

  static init() {

    const $paymentsContainer = $('#release-payments');
    this.paymentConfigsField = $('#outcome_paymentConfigs');

    this.auth = new Auth();

    this.tenantId = $paymentsContainer.data('tenant-id');
    if (this.paymentConfigsField.length > 0) {
      this.paymentConfigIds = JSON.parse(this.paymentConfigsField.val());
    }
    this.serviceID = $paymentsContainer.data('service-id')
    this.gatewayUrl = $paymentsContainer.data('gateway-url')
    this.language = document.documentElement.lang.toString();

    $(document).on('click', '.add-payment', (e) => {
      this.handleAddPayment(e)
    });

    $(document).on('click', '.edit-payment', (e) => {
      this.handleEditPayment(e)
    });

    $(document).on('click', '.delete-payment', (e) => {
      this.handleDeletePayment(e)
    });

    this.loadPaymentConfigs()
  }

  static handleAddPayment(e) {
    const target = $(e.target);
    const phase = target.data('phase');
    let data = {
      tenant_id: this.tenantId,
      active: true
    }

    Swal.fire({
      title: `Aggiungi pagamento`,
      width: '80%',
      html: '<div class="overflow-hidden"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>',
      showCloseButton: false,
      showConfirmButton: false,
      didOpen: () => {
        const html = '<div id="form"></div>';
        $(Swal.getPopup()).children('.swal2-html-container').html(html);
        Gateways.getTenantsSchema(this.gatewayUrl + '/configs/schema').then((result) => {
          Formio.createForm(document.getElementById('form'), result, {
            noAlerts: true,
            buttonSettings: {showCancel: false},
          })
            .then(function (form) {
              form.submission = {
                data: data
              };
              form.nosubmit = true;
              form.on('submit', function (submission) {

                submission.data.remote_collection = {
                  id: ApplicationPaymentConfig.serviceID,
                  type: 'service'
                }

                // Creo la configurazione
                ApplicationPaymentConfig.auth.execAuthenticatedCall((token) => {
                  const authConfig = {
                    headers: {
                      Authorization: `Bearer ${token}`
                    }
                  };
                  axios.post(ApplicationPaymentConfig.gatewayUrl + '/configs', submission.data, authConfig)
                    .then(function (response) {

                      // Aggiorno il servizio
                      ApplicationPaymentConfig.paymentConfigIds[response.data.id] = {
                        'id': response.data.id,
                        'phase': phase
                      };
                      ApplicationPaymentConfig.updatePaymentConfigField();
                      ApplicationPaymentConfig._configs[response.data.id] = submission.data;

                      // Aggiorno il template
                      ApplicationPaymentConfig.renderPaymentConfig(response.data, phase, 'application');

                      Swal.fire({
                        title: `Pagamento creato correttamente`,
                        icon: "success",
                        showConfirmButton: true
                      });
                    })
                    .finally(() => {
                      ApplicationPaymentConfig.updatePaymentConfigsCounter(phase)
                    })
                    .catch(function (error) {
                      console.log(error);
                    });
                });
              });
            })

        }).catch(err => {
          if (err.status === 404) {
            $('form').html(`<div>${Translator.trans('iscrizioni.no_payments_config', {}, 'messages', this.language)}</div>`)
          } else {
            $('form').html('<div>' + err.statusText + '</div>')
          }
        })
      },
    })

  }

  static handleEditPayment(e) {
    const target = $(e.target);
    const parent = $(target.parents('.payment-config')[0]);
    const id = parent.data('id');
    const source = parent.data('source');
    const data = this._configs[id];
    const phase = 'release';

    Swal.fire({
      title: `Modifica pagamento`,
      width: '80%',
      html: '<div class="overflow-hidden"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>',
      showCloseButton: false,
      showConfirmButton: false,
      didOpen: () => {
        const html = '<div id="form"></div>';
        $(Swal.getPopup()).children('.swal2-html-container').html(html);
        Gateways.getTenantsSchema(this.gatewayUrl + '/configs/schema').then((result) => {
          Formio.createForm(document.getElementById('form'), result, {
            noAlerts: true,
            buttonSettings: {showCancel: false},
          })
            .then(function (form) {
              form.submission = {
                data: data
              };
              form.nosubmit = true;
              form.on('submit', function (submission) {
                if (source === 'application') {
                  // Modifico la configurazione
                  ApplicationPaymentConfig.auth.execAuthenticatedCall((token) => {
                    const authConfig = {
                      headers: {
                        Authorization: `Bearer ${token}`
                      }
                    };
                    axios.put(ApplicationPaymentConfig.gatewayUrl + '/configs/' + id, submission.data, authConfig)
                      .then(function (response) {
                        Swal.close();
                      })
                      .catch(function (error) {
                        console.log(error);
                      });
                  });
                } else if (source === 'service') {
                  // Creo la configurazione
                  ApplicationPaymentConfig.auth.execAuthenticatedCall((token) => {
                    const authConfig = {
                      headers: {
                        Authorization: `Bearer ${token}`
                      }
                    };
                    axios.post(ApplicationPaymentConfig.gatewayUrl + '/configs', submission.data, authConfig)
                      .then(function (response) {
                        ApplicationPaymentConfig.paymentConfigIds[response.data.id] = {
                          'id': response.data.id,
                          'phase': phase
                        };
                        ApplicationPaymentConfig._configs[response.data.id] = submission.data;
                        ApplicationPaymentConfig.renderPaymentConfig(response.data, phase, 'application');
                        Swal.close();
                      })
                      .finally(() => {
                        console.log(ApplicationPaymentConfig._configs);

                        /*parent.remove();
                        ApplicationPaymentConfig._configs.splice(id, 1);
                        const keys = Object.keys(ApplicationPaymentConfig._configs);
                        const indexToRemove = 0;
                        if (indexToRemove >= 0 && indexToRemove < keys.length) {
                          const keyToRemove = keys[indexToRemove];
                          delete ApplicationPaymentConfig._configs[keyToRemove];
                        }*/

                        ApplicationPaymentConfig.updatePaymentConfigsCounter(phase)
                        delete (ApplicationPaymentConfig.paymentConfigIds[id]);
                        ApplicationPaymentConfig.updatePaymentConfigField();
                        ApplicationPaymentConfig._configs.splice(id, 1);
                        parent.remove()

                      })
                      .catch(function (error) {
                        console.log(error);
                      });
                  });
                }

              });
            })

        }).catch(err => {
          if (err.status === 404) {
            $('form').html(`<div>${Translator.trans('iscrizioni.no_payments_config', {}, 'messages', this.language)}</div>`)
          } else {
            $('form').html('<div>' + err.statusText + '</div>')
          }
        })
      },
    })
  }

  static handleDeletePayment(e) {
    const target = $(e.target);
    const parent = $(target.parents('.payment-config')[0]);
    const id = parent.data('id');
    const source = parent.data('source');

    if (source === 'application') {
      ApplicationPaymentConfig.auth.execAuthenticatedCall((token) => {
        const authConfig = {
          headers: {
            Authorization: `Bearer ${token}`
          }
        };
        axios.delete(ApplicationPaymentConfig.gatewayUrl + '/configs/' + id, authConfig)
          .then(function (response) {
            ApplicationPaymentConfig.updatePaymentConfigsCounter(ApplicationPaymentConfig.paymentConfigIds[id].phase)
            delete (ApplicationPaymentConfig.paymentConfigIds[id]);
            ApplicationPaymentConfig._configs.splice(id, 1);
            ApplicationPaymentConfig.updatePaymentConfigField();
            parent.remove()
            Swal.close();
          })
          .catch(function (error) {
            console.log(error);
          });
      })
    } else if (source === 'service') {
      ApplicationPaymentConfig.updatePaymentConfigsCounter(ApplicationPaymentConfig.paymentConfigIds[id].phase)
      delete (ApplicationPaymentConfig.paymentConfigIds[id]);
      ApplicationPaymentConfig._configs.splice(id, 1);
      ApplicationPaymentConfig.updatePaymentConfigField();
      parent.remove()
    }

  }


  static loadPaymentConfigs() {
    $.each(this.paymentConfigIds, (index, item) => {
      ApplicationPaymentConfig.auth.execAuthenticatedCall((token) => {
        const authConfig = {
          headers: {
            Authorization: `Bearer ${token}`
          }
        };
        axios.get(this.gatewayUrl + '/configs/' + item.id, authConfig)
          .then((response) => {
            this._configs[response.data.id] = response.data;
            this.renderPaymentConfig(response.data, item.phase, 'service');
          })
          .catch((error) => {
            Swal.fire('Error', 'Failed to load payment config', 'error');
          });
      })
    })
  }

  static renderPaymentConfig(config, phase, source) {
    this.removeNoPaymentConfigsAlert(phase);
    let paymentTitle = config.reason ?? config.items[0].reason;
    if (!paymentTitle) {
      paymentTitle = 'Pagamento'
    }


    let paymentAmount = 0;
    if (Array.isArray(config.items)) {
      paymentAmount = config.items.reduce((sum, item) => sum + item.amount, 0);
    } else {
      paymentAmount = config.amount;
    }
    const formattedAmount = new Intl.NumberFormat(ApplicationPaymentConfig.language, {
      style: 'currency',
      currency: 'EUR'
    }).format(paymentAmount);
    console.log(paymentAmount);

    $('#' + phase + '-payment-configs-table').append(
      `<tr class="payment-config" data-id="${config.id}" data-config="${config}" data-source="${source}">
        <td class="w-60 pl-2">
            <strong class="text-primary small">${paymentTitle}</strong>
        </td>
        <td class="text-right py-2">
            <span class="small text-primary">${formattedAmount}</span>
            <a href="#" class="text-secondary text-decoration-none edit-payment small ml-3"><i class="fa fa-pencil"></i> ${Translator.trans('modifica', {}, 'messages', this.language)}</a>
            <a href="#" class="text-secondary text-decoration-none delete-payment small ml-3"><i class="fa fa-trash-o"></i> ${Translator.trans('elimina', {}, 'messages', this.language)}</a>
        </td>
      </tr>`
    )
  }

  static removeNoPaymentConfigsAlert(phase) {
    $('#' + phase + '-payment-configs-table').find('tr.no-payment-configs').remove();
  }

  static updatePaymentConfigsCounter(phase) {
    let count = $('#' + phase + '-payment-configs-table').find('payment-config').length;
    $('#' + phase + '-payment-configs-counter').text(count)
  }

  static updatePaymentConfigField() {
    if (ApplicationPaymentConfig.paymentConfigsField) {
      const temp = {...ApplicationPaymentConfig.paymentConfigIds}
      ApplicationPaymentConfig.paymentConfigsField.val(JSON.stringify(temp));
    }
  }


}

export default ApplicationPaymentConfig;
