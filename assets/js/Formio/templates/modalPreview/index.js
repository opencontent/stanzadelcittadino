export default function modalPreview(ctx) {
  return `
<label id="l-${ctx.component.key}}" class="control-label ${ ctx.label.className }">
  ${ ctx.t(ctx.component.label, { _userInput: true }) }<span ref="modalLabelValue" class="visually-hidden"> ${ ctx.component.type === 'signature' ? ctx.self.getValueAsString(ctx.previewText) : ctx.previewText }</span>
</label><br>
<span class="visually-hidden" ref="modalPreviewLiveRegion" aria-live="assertive"></span>
<button
  lang="en"
  class="btn btn-secondary btn-md text-center open-modal-button form-control ${ctx.openModalBtnClasses || ''}"
  ref="openModal"
  aria-labelledby="l-${ctx.component.key}"
>
  ${ ctx.component.previewText ? ctx.component.previewText : ctx.previewText }
</button>
<div class="formio-errors invalid-feedback">
  ${ ctx.messages }
</div>`;
}
